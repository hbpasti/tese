## Stammler e a "superação" do materialismo histórico {#stammler}


O ensaio de Simmel "Para a metodologia das ciências sociais" é uma
resenha da obra do jurista Rudolf Stammler (1856--1938) *Economia e
direito na concepção materialista da história*
(*\foreignlanguage{german}{Wirtschaft und Recht nach der
materialistischen Geschichtsauffassung}*), publicada em primeira
edição em 1896 [@Stammler-WirtschaftUndRecht]. A resenha de Simmel
apareceu no mesmo ano de sua publicação. Quando da publicação da
segunda edição, Max Weber dedicaria ao livro uma cáustica resenha
crítica, que é compreensivelmente um de seus escritos menos lidos, com
o título de "R. Stammler e a 'superação' da concepção materialista de
história".[^weber-stammler-refs] A despeito de Stammler ter se tornado
mais conhecido por suas contribuições ao direito e de ter sua fama nas
ciências sociais ligada a essa resenha ridicularizante de Weber, sua
obra em questão teve considerável importância na
época.[^importancia-stammler]

Mas que dizia Stammler que tanto incomodava esses autores que se
tornariam tão importantes para a sociologia alemã então nascente?

Para não cair na malhação do cachorro morto que Stammler se tornou com
o tempo, particularmente em vista de que toda a sua notoriedade na
história do pensamento sociológico se limita às páginas mais difíceis
de Weber, pode ser interessante acompanhar a exposição que faz de seu
pensamento um admirador. Recorri para tanto a
@Sabine-StammlersPhilosophyOfLaw, que soa como um epígono da filosofia
do direito de Stammler --- pela qual Weber nutria particular
desprezo.[^weber-desprezo-direito-justo]

Stammler dedica sua polêmica obra *Economia e direito* ao amigo Paul
Natorp (1854--1924), um filósofo neokantiano que transitava entre as escolas de
Baden e de Marburg [Segundo @Ringer-Decline, p. 310], onde Stammler
lecionara entre 1882 e 1884; este dedica sua *Pedagogia social*
(*\foreignlanguage{german}{Sozialpädagogik}*), de 1895, ao amigo
Stammler. A causa da amizade e da dobradinha pode ser uma intensa
afinidade política:

> Stammler e seu amigo Paul Natorp, o distinto neokantiano e estudioso
> de Platão na assim chamada Escola de Filosofia de Marburg,
> apresentaram uma revisão, que resultava numa completa reversão, da
> filosofia marxiana que formava o credo oficial do socialismo
> partidário na Alemanha. Como Hermann Cohen, o fundador da Escola,
> Natorp sentia uma profunda simpatia pelos propósitos humanitários do
> socialismo e defendia uma filosofia social que teria alegremente
> chamado de socialismo, mas estava em total desacordo com as teorias
> do determinismo econômico e da luta de classes, assim como com o
> materialismo metafísico dos marxistas. Sua filosofia social, na
> verdade, se desenvolvia em torno do ideal de uma educação moral
> completamente socializada \[há uma referência aqui a algumas da
> obras de Natorp, mas particularmente à sua *Psicologia social*
> (*\foreignlanguage{german}{Sozialpädagogik}*), de 1895, que fora
> dedicada a Stammler\] e a ideia fundamental nela é platônica --- uma
> divisão tripartite das funções sociais. Isso, no entanto, se
> qualificava pela ideia kantiana de progresso ilimitado em direção a
> uma finalidade moral, que Platão dificilmente teria aceito.
> <!--325--> "A vida social, qualquer que seja o estágio que possa
> alcançar, nunca se torna estática; ela deve ser concebida como
> continuamente em processo. Por isso, sua ordem moral se torna um
> problema eterno, sua virtude se torna um ideal, isto é, apenas um
> ponto de referência num desenvolvimento infinito." \[A passagem é
> citada da *\foreignlanguage{german}{Sozialpädagogik}* na quinta
> edição de 1922, p. 179\] A força diretriz da sociedade humana é a
> socialização da vontade através da educação. De todas as teorias
> sociais, o socialismo é a menos capaz de prescindir de um ponto de
> vista ético. Se ele soubesse do que se tratava, ele teria visto que
> o materialismo é uma filosofia completamente incongruente para se
> construir sobre ela sua política social.
> [@Sabine-StammlersPhilosophyOfLaw, pp. 324-325]

Nessa parceira entre Natorp e Stammler --- que se dedica, é válido
enfatizar, a oferecer uma resposta à questão social --- teria recaído
sobre Stammler fazer a crítica do socialismo e particularmente da
concepção materialista da história, enquanto Natorp dedica-se, ao
mesmo tempo, a uma pedagogia voltada para uma socialização integral.
Essa análise das teorias socialistas e particularmente da filosofia
de Marx resulta na afamada obra de Stammler *Economia e direito*.

Stammler se atém à leitura de que a concepção materialista é uma
interpretação estritamente econômica da história, que atribui às
transformações tecnológicas da esfera produtiva a única causalidade
sobre todas as mudanças em direito.[^weber-materialismo]

> A conclusão da crítica \[de Stammler à concepção materialista da
> história\] é que a interpretação econômica da história ---
> ou, mais especificamente, a tese de que mudanças em direito e
> governo seguem causal ou logicamente de mudanças na tecnologia --- é
> vaga e mal desenvolvida. Está tão distante de ser um fato que o
> direito se ajusta automaticamente a mudanças no modo de produção de
> bens, que a luta de classes da a qual a teoria socialista depende
> deve-se precisamente à falha em se fazer tais ajustes. A tecnologia
> muda e o direito não, permanecendo, antes, um anacronismo legal que
> tira todo o sistema do ajuste. A teoria materialista é uma má figura
> de linguagem: se a sociedade se reajustasse a cada mudança na medida
> em que os desgastes e tensões são redistribuídos num sistema físico
> móvel, não haveriam atrasos nem conflitos.
> [@Sabine-StammlersPhilosophyOfLaw, p. 326]

A insatisfação de Stammler com a leitura economicista do direito vai
levá-lo, assim, a compreender que a leitura socialista da relação
entre direito e sociedade e, portanto, seu diagnóstico e intervenção
sobre a "questão social" era inadequado, propondo, substituir "o
materialismo social de Marx" por um "idealismo social".
[@Sabine-StammlersPhilosophyOfLaw, p. 327]

Invertendo a leitura materialista, Stammler concebe o direito como
fundador da sociedade, como seu antecessor lógico: qualquer esforço
cooperativo demanda algum tipo de regulação, e só a partir da
existência de tais normas é que se poderia falar em sociedade.

> Economia social, como Stammler emprega o termo, aplica-se a qualquer
> esforço cooperativo para a satisfação de necessidades e portanto a
> intercâmbios de bens e serviços de qualquer tipo, quer físicos ou
> espirituais. As tentativas de deixar de lado uma classe especial de
> necessidades econômicas como menores só leva a confusão. Agora,
> quando quer que esforço cooperativo ocorre, ele precisa ser
> regulado, isto é, não pode ser deixado meramente à inclinação. Deve
> haver algum tipo de regra ou padrão para governar a conduta humana
> com relação a outros seres humanos no processo de agir e viver
> juntos. E quando tal padrão regulador existe, há uma sociedade e a
> sociedade é criada pela existência do padrão. Na verdade, há muitos
> tipos de tais padrões sociais --- regras de moralidade, de costume,
> de direito e mesmo de etiqueta. Isso não quer dizer que há
> necessariamente um Estado, pois o Estado, onde ele chega a existir
> --- e Stammler o considera como uma instituição relativamente
> recente --- é ele mesmo uma associação no interior da sociedade e,
> portanto, criatura da lei. A raiz da questão é que as pessoas de
> fato satisfazem suas necessidades cooperativamente --- a conduta de
> uma é o meio para os fins de outra e por sua vez a conduta dessa
> outra é um meio para os fins da primeira --- e isso requer regras
> vinculadoras subordinando as vontades das várias partes cooperantes
> com os fins a serem atingidos pela cooperação. A existência de tais
> regras é o que faz uma sociedade.
>
> Daí segue que conflitos e pressão econômica ocorrem *no interior* do
> círculo social criado por padrões comuns de conduta. Aparte de tais
> padrões aceitos em geral como vinculadores, não há instituições,
> tais como casamento, propriedade ou crime, e portanto nenhuma
> instituição econômica. Pois instituições não são simplesmente
> eventos ou fatos, mas antes estados de vontade; elas consistem no
> todo no fato de que há certos modos socialmente aprovados de
> comportamento e que tais padrões são admitidos como adequadamente
> vinculadores. Conflito ou pressão econômicas, então, só podem
> existir na medida em que afetam a vontade e as regras segundo as
> quais ela é regulada; são discrepâncias entre as regras e os fins
> buscados. A tese de que arranjos econômicos produzem leis é pelo
> menos confusa e, num sentido, bastante falsa. A não ser que
> existissem leis, padrões vinculadores de comportamento, não haveria
> economia em absoluto. [@Sabine-StammlersPhilosophyOfLaw, p. 327]

O rigor metodológico, no entanto, não é o aspecto mais notável dessa
obra de Stammler; aqui tratar-se-ia de um momento fundador da teoria
do direito que Stammler desenvolveria mais tarde, particularmente de
um acerto de contas com a filosofia marxista rumo a um "idealismo
social".[^stammler:idealismo-social] A metodologia que o Stammler
tardio viria a desenvolver com mais desenvoltura, segundo Sabine, é a
de uma análise de conceitos: "Stammler declara lidar sempre com um
aparato mental e não com os objetos concebidos. Da mesma maneira, seus
resultados professam apresentar somente o uso de conceitos."
[@Sabine-StammlersPhilosophyOfLaw, p. 328]

Como análise puramente racionalista de conceitos jurídicos, o direito
de Stammler assim se afasta decididamente da tradição histórica de
direito; ele tornou-se posteriormente conhecido por uma frase:
"Direito natural com conteúdo variável", que Sabine considera não
fazer justiça a sua visão do direito. "Stammler nunca acreditou que a
jurisprudência poderia conquistar algum coisa ao apelar para um
sentimento ou direito naturais ou um senso de justiça porque não há
presunção de que a opinião popular seja mais validamente justa do que
o próprio direito positivo." [@Sabine-StammlersPhilosophyOfLaw, p. 329]
Sua interpretação do direito é devedora da tradição crítica, e
demonstra a intenção de pensar a partir de Kant e não da tradição
idealista pós-kantiana.[^stammler:direito-kantiano]

Toda essa discussão de @Sabine-StammlersPhilosophyOfLaw é feita com um
olho na obra mais tardia e de mais sucesso de Stammler, mas com a
intenção de demonstrar as linhas de continuidade entre esse primeiro
acerto de contas com o marxismo e a construção de um idealismo social
kantiano com a sua teoria do "direito justo" que seria explorada mais
consequentemente posteriormente.

A teoria da justiça de Stammler tem por ponto de partida a distinção
kantiana entre as esferas do ser e do dever, ou do valor. <!--em maneira
consoante com aquilo que @Ringer-Decline chama de "pensamento social
mandarim".--> Nesse sentido, distinguia uma visão da natureza "como um
sistema de causas e efeitos no interior do qual todo evento pode ser
explicado pelas suas relações espaciais, temporais ou causais com
outros eventos. No mundo assim concebido, não existe bem ou mal, nada
tem valor, e nada significa dizer que algo deveria ser de outra
maneira." Em contraponto, o espaço das realizações humanas exige uma
perspectiva que "olha para o mundo como o teatro da ação humana; ele
consiste de tarefas a serem feitas, de resultados a serem realizados
ou evitados. Os seus elementos são meios e fins, e essa é uma relação
radicalmente diferente da de causa e efeito uma vez que projeta-se
para além do fato e antevê um estado futuro como o fundamento ou razão
do ato presente." [@Sabine-StammlersPhilosophyOfLaw, p. 333]

Simmel faz precisamente a mesma leitura, sem daí derivar a necessidade
de regularidades, no contexto da apresentação de seu relativismo na
sua obra *Filosofia do dinheiro*, que veio a público em 1900.
[Particularmente em sua discussão de valor no cap. 1 de @PHG-orig-1e;
com uma segunda edição ampliada em @PHG-orig-2e; reimpressa em @PHG; e
da qual constam traduções em inglês, @PHG-en; espanhol, @PHG-es; e
francês @PHG-fr] Mas de fato digno de nota, aqui, é a maneira como se
aborda, a partir dessa distinção kantiana, como tantos outros, um tema
que parece ter se tornado a obsessão dos "mandarins" alemães naquele
final de século: os critérios e limites de divisão das disciplinas.

> Assumindo esta distinção, mais ou menos compartilhada por todos os
> kantianos, Stammler então procede a dar um longo passo para além de
> Kant, embora ele preserve cuidadosamente a analogia com Kant.
> Correspondendo à distinção entre *\foreignlanguage{german}{Sein}*
> \[ser\] e *\foreignlanguage{german}{Sollen}* \[dever\], ele supõe
> que devem existir dois tipos radicalmente distintos de ciência, uma
> ciência da natureza (*\foreignlanguage{german}{Naturwissenschaft}*)
> em termos da relação entre causa e efeito, e uma ciência de fins
> (*\foreignlanguage{german}{Zweckwissenschaft}*) em termos de
> relações de meios e fins. O fundamento para tal pressuposição está
> em que em ambos os campos nós de fato tomamos por certo que
> distinções válidas (isto é, não meramente pessoais ou subjetivas)
> podem ser estabelecidas. No reino dos fatos, assumimos a distinção
> entre verdadeiro e falso; na medida em que qualquer proposição
> alegue reportar àquilo que está de fato no mundo, ela deve ser
> correta ou incorreta. Mas da mesma maneira procedemos com a
> suposição de que há uma real diferença de valores. Na medida em que
> alguém afirma que um ato é bom e outro mau, que uma lei é injusta e
> deveria ser repelida enquanto outra é correta e deveria ser
> cumprida, professa-se um apelo a um <!--334--> padrão válido de
> certo e errado. Há, portanto, Stammler argumenta, dois tipos de
> padrão válido, o padrão da verdade e o padrão do direito, e em cada
> um deles deveria ser possível erigir uma ciência (ou classe de
> ciências). Pois um padrão válido pressupõe regularidade
> (*\foreignlanguage{german}{Gesetzmässigkeit}*) ou conformidade a
> leis gerais. [@Sabine-StammlersPhilosophyOfLaw, pp. 333-334]

Vale lembrar que a obra de Stammler vem a público no ponto alto da
controvérsia metodológica. Apenas dois anos antes da publicação da
obra de Stammler, Wilhelm Windelband, que capitaneava a chamada escola
neokantiana de Baden, num famoso discurso ao assumir a reitoria da
Universidade de Estrasburgo em 1894, fez uma crítica à divisão entre
ciências a partir de considerações ontológicas a respeito de seus
objetos, propondo que a verdadeira distinção encontrava-se na
diferença de métodos de atividades específicas que se encontram em
todas as ciências, nomeadamente os métodos nomotético e idiográfico
(já falamos disso na \autoref{ciencias-humanas-naturais}).

A distinção então sendo debatida, alimentada por esse tripé que
fundamenta a visão de mundo da comunidade acadêmica alemã como um todo
--- criticismo, idealismo e a tradição histórica --- era proeminente
porque servia para limitar as reivindicações do naturalismo e afastar
de vez o fantasma do positivismo. [Como vimos no cap.
\autoref{methodenstreit}. Cf. também @Ringer-Decline, caps. 3 e 6]
Nesse sentido, parecia voltar-se a proteger o universo humano da
abordagem nomotética que se voltava à busca de "regularidades". A
abordagem de Stammler destoa um pouco dessa tendência ao estabelecer
diferentes tipos de regularidades, em sua obsessão com a noção de lei.

> \[...\] Stammler faz uma diferença entre a ciência jurídica como
> "*ciência teleológica*"
> (*\foreignlanguage{german}{Zweckwissenschaft}*), cujo objetivo é uma
> aplicação funcional e a ciência (pura) do direito caracterizada por
> uma orientação à causalidade
> (*\foreignlanguage{german}{kausalorientiert}*).
> [@Coutu-Introduction-Weber-Stammler-fr, p. 8]

Na diferenciação stammleriana das disciplinas, assim, haveriam ciência
causais e ciências teleológicas, cada qual voltada a uma dessas
esferas --- ou fundadas num tipo distinto de sujeito transcendental,
com base na distinção entre ser e dever.

> O caráter das leis vai, é claro, ser diferente nas duas classes de
> ciências: nas ciências naturais, serão leis de causação; nas
> ciências de fins elas serão teleológicas. Assim como a ciência
> natural funciona à luz de uma explicação causal completa como seu
> objetivo, a ciência teleológica deve presumir um fim último ou um
> direito definitivo em direção ao qual fins menos definitivos
> contribuem.
>
> Agora, Stammler está completamente convencido de que Kant demonstrou
> que a regularidade ou conformidade a leis em ciências naturais
> depende de uma certa estrutura ou forma dos conceitos científicos,
> porque, a não ser que assumíssemos que todos os eventos empíricos
> possuiriam de fato essa forma, não deveríamos ser capazes de
> encontrar qualquer regularidade em absoluto entre tais objetos.
> Assim, por exemplo, não podemos, em geral, encontrar nenhuma
> uniformidade a não ser que as coisas se arranjem em grandes tipos de
> ordem tais como o tempo, o espaço ou a causalidade, e toda lei
> científica específica, como a lei da gravidade por exemplo, é
> meramente uma afirmação do que descobrimos pela experiência sobre o
> que o comportamento dos objetos é em termo desses tipos mais gerais
> e indispensáveis de ordens. Tais ordens Stammler chama, como Kant,
> de formas: elas são cientificamente indispensáveis porque sem elas
> não há nenhuma maneira de encontrar regularidade na natureza.
> \[...\]
>
> *Mutatis mutandis*, Stammler agora se propõe a aplicar exatamente o
> mesmo tipo de raciocínio à sua classe suposta de ciência télicas.
> Deve-se supor que todo desejo, necessidade ou inclinação ocorre no
> interior de um quadro ou ordem de meios e fins. Se não fosse esse o
> caso, não haveria diferença de valor ou correção entre diferentes
> inclinações, mas todas seriam do mesmo nível de sentimento
> subjetivo. Como vimos, Stammler assume que esse não é o caso: ele
> assume que a distinção entre mera inclinação e vontade justificado é
> tão inevitável quanto a distinção entre mera opinião e verdade. Dada
> tal distinção, tudo o que está envolvido em fazê-la deve ser dado
> também. Deve-se supor, portanto, que há formas ou ordens tão
> universais do mundo dos fins como tempo, espaço e causalidade o são
> para o mundo natural. [@Sabine-StammlersPhilosophyOfLaw, p. 334]

Vou deixar por aqui a apreciação que Sabine faz da filosofia do
direito posterior de Stammler porque o que nos interessa é
somente conhecer em parte a discussão que Stammler suscitava com
*Economia e direito*. Vale notar, antes de continuar, que Sabine
acrescenta que "na terminologia kantiana de Stammler, a justiça é
exclusivamente metodológica ou regulativa. Ela não tem conteúdo e não
estabelece nenhuma regra, mas é puramente uma 'ideia' a ser usada para
guiar o pensamento legal." [@Sabine-StammlersPhilosophyOfLaw, p. 331] Essa
interpretação será importante para pensar com Simmel como se poderia
escapar ao imbróglio da controvérsia metodológica com alguma segurança
epistemológica para as ciências sociais.

---

Em sua resenha, Simmel fala pouco do livro e de Stammler em si. Ele
prefere discutir as ideias, as suas, à luz das de Stammler que ele
apenas insinua: é uma resenha destinada a intervir num debate e a
dialogar com quem estava lendo e comentando a obra. Nesse sentido, é
absolutamente incomparável com a crítica que lhe destina Weber, que
por vezes esmiúça página por página o argumento de Stammler para
ridicularizar suas falhas lógicas e que é abusivamente mais volumosa
que a resenha de Simmel.

Sobre a obra, diz Simmel:

> \[...\] Rudolf Stammler provê, em sua obra "*Economia e sociedade
> segundo a concepção materialista da história*", uma aplicação da
> metodologia kantiana às ciências sociais. Em oposição às suas
> próprias tendências empíricas, ele quer demonstrar que uma
> experiência científica das coisas sociais nos é possível, em geral,
> se certos conceitos servirem como a fundação em que, do material de
> disposições e técnicas humanas, se ergue uma imagem da sociedade.
> Aos aspectos teóricos como aos práticos do conflito fundamental,
> Stammler aplica essa tentativa de solução crítica. [@Mthdk-orig p.
> 576]

Simmel vai tentar coisa parecida em sua resenha, onde ele propõe o
relativismo como uma saída da controvérsia metodológica, concebendo as
posições ali polarizadas como momentos de uma metodologia unificada.


<!-- NOTAS {{{ -->

[^weber-stammler-refs]: O ensaio foi originalmente publicado no
*Arquivo para a ciência e a política sociais*
(*\foreignlanguage{german}{Archiv für Sozialwissenschaft und
Sozialpolitik}*), em 1907 [reimpresso em @Weber-Stammler; um
pós-escrito foi publicado postumamente em @Weber-StammlerNachtrag;
consta uma tradução de baixa qualidade editorial em
@Weber-Stammler-pt; @Weber-StammlerNachtrag-pt; consultei também a
edição inglesa em @Weber-Stammler-en.] Julien Freund não o incluiu na
edição francesa dos escritos metodológicos de Weber sob a
justificativa de que "O longo estudo intitulado
*\foreignlanguage{german}{R. Stammlers 'Überwindung' der
materialistischen Geschichtsauffassung}*, \[A 'superação' da concepção
materialista de história de R. Stammler\] publicado em 1907 no *Archiv
für Sozialwissenschaft und Sozialpolitik* \[Arquivo para a ciência e a
política sociais\] é certamente aquele que oferece o menor interesse
para conhecimento da epistemologia weberiana. Em geral, a discussão
gira em torno de conceitos jurídicos, mesmo que Weber pretenda
examinar a teoria do conhecimento de Stammler, e muitas vezes o tom é
mais polêmico que verdadeiramente crítico. Assim, a leitura deste
artigo é particularmente dolorosa: ou Weber descasca uma a uma as
páginas do livro de Stammler consagradas à metodologia para destacar
os sofismas, as confusões, as verdades aparentes e as proposições
escolásticas, ou ele prende-se a um conceito, por exemplo o de
legalidade, para notar as frases <!--78--> em que o termo é empregado
e detectar as contradições"; ele acrescenta que Weber "simplesmente
recusa ao livro de Stammler o valor de um trabalho científico."
[@Freund-Introduction-GAzW, pp. 77-78] Muito mais tarde, quando da
publicação do ensaio em francês, os editores se oporiam à leitura de
Freund, destacando o papel do confronto com Stammler para a construção
das categorias de ação racional orientada a fins entre outras [Cf.
@Coutu-Introduction-Weber-Stammler-fr]

[^weber-materialismo]: Para o desespero de Weber, que sai em defesa da
concepção materialista da história como uma hipótese heurística:
"Nesta condição de hipótese tampouco muda algo se alguém, por exemplo,
declara que a teoria materialista de história não seria uma doutrina,
mas deveria apenas ser reconhecida como um 'princípio heurístico', e,
por causa disso, se apresenta como um método 'específico' para a
investigação do material histórico de 'pontos de vista econômicos'."
[@Weber-Stammler-pt, p. 223]

[^stammler-pedante]: Descreveu-se a obra de Stammler  como "numerosa,
densa e de uma leitura difícil." [@Coutu-WeberLecteurDeStammler,
p. 668] Creio que a grande dificuldade se deva ao insuportável
pedantismo; por exemplo: "Enorme, a maré de problemas indomados de
nossa existência social corre ao largo, sem chegar a uma solução na
barragem reguladora que só uma técnica cientificamente fundada pode
erigir; e as ondas incansavelmente oscilantes das pressões e lutas
sociais ameaçam com cego fatalismo, irreconhecido em sua regularidade
definitivamente determinada." [@Stammler-WirtschaftUndRecht, p. 3]
Esse tom profético e de retórica elevada segue interminável. Se
consegui entender a intenção: a grande questão de Stammler é a busca
de regularidades. Para ele, é preciso compreender a regularidade da
vida social para nela intervir a fim de evitar as convulsões sociais
que a "questão social" da época promoveriam. Minha leitura impaciente
é insuficiente e convida-me a recorrer a comentadores ---
preferencialmente não Weber que é tão, se não mais, insuportável que
Stammler ---, no caso, @Sabine-StammlersPhilosophyOfLaw

[^stammler:idealismo-social]: "O ideal de uma ciência crítica \[no
sentido de kantiana\] da metodologia legal cresceu continuamente em
Stammler conforme seu pensamento se amadurecia. Em sua obra importante
mais inicial, sua *\foreignlanguage{german}{Wirtschaft und Recht}*,
ele está largamente implícito. A maior parte de suas ideias
características já aparecem nessa obra e numa forma não
substancialmente diferente de sua forma mais tardia, mas o rigor de
método que ele emprega mais tarde ainda não está aí,
inquestionavelmente para grande vantagem do estilo. Nessa obra, ele
está principalmente preocupado em formular e amadurecer sua própria
teria do idealismo social através de uma crítica do materialismo
"social dos marxistas. [@Sabine-StammlersPhilosophyOfLaw p. 332]

[^stammler:direito-kantiano]: "Nunca foi a intenção de Stammler
reviver o direito natural em nada como sua forma histórica. Ele nunca
acreditou na existência de um direito ideal ou perfeito acima e além
do direito positivo, nem jamais defendeu que qualquer regra legal é
invariável ou livre de condições de tempo e espaço históricos. Direito
justo é parte do direito positivo. Todo direito positivo 'tenta' ser
justo, embora possa não ter sucesso, e por esse motivo como um todo
ele nunca é puramente positivo ou fatual. Ele tem, por assim dizer,
uma dimensão ideal que o projeta em direção a um fim não realizado,
mas esse esforço rumo ao ideal é uma fase do próprio direito
positivo." [@Sabine-StammlersPhilosophyOfLaw, p. 330]

[^importancia-stammler]: "Em sua época, entretanto, é certamente
*\foreignlanguage{german}{Wirtschaft und Recht nach der
materialistischen Geschichtsauffassung}* \[Economia e direito segundo
a concepção materialista de história\] que, no tocante às ciências
sociais, representa a obra mais importante de Stammler. Largamente
difundida à época, ela deu lugar a uma <!--10--> série de
interpretações divergentes. A pertinência do pensamento stammleriano
repousa sobre a síntese sistemática e original das questões e tarefas
formuladas de uma maneira equívoca para alguns e convincente para
outros. Stammler adapta a abordagem neokantiana (especialmente aquela
que elabora Paul Natorp, a quem ele dedica seu livro) e desenvolve uma
filosofia social. Ele destaca a relação estreita entre direito e
economia com a intenção de ultrapassar a perspectiva do materialismo
histórico. \[...\] Porém a teoria de Stammler é recusada pelos
representantes do empirismo e do positivismo como formalista. Ela é
considerada como uma ideologia burguesa pelos partidários do
materialismo histórico. Outros críticos acentuam os fundamentos do
direito natural ou a lógica intrínseca da economia. Para a sociologia
nascente, a definição da vida social e das tarefas do pesquisador em
ciências sociais ocupam um lugar particularmente importante."
[@Coutu-Introduction-Weber-Stammler-fr, pp. 9-10]

[^weber-desprezo-direito-justo]: "Pode-se deduzir que Weber esteve
particularmente encurralado, durante certo período de tempo, por seu
trabalho crítico sobre *\foreignlanguage{german}{Wirtschaft und
Recht}* \[Economia e direito\]. Ele ambicionava fazer, além disso, um
artigo subsequente --- que jamais foi concluído --- que foi prometido
ao editor para o final de abrol de 1907; numa carta a Hermann
Kantorowicz, Weber dá também a entender que ele sonhava reduzir a nada
a teoria do 'direito justo' de Stammler."
[@Coutu-Introduction-Weber-Stammler-fr, p. 5. A carta mencionada é de
Max Weber a Hermann Kantorowicz de 30 de out. de 1908, *Briefe*, p.
690]

<!-- }}} -->
