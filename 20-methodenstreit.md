# A controvérsia metodológica {#methodenstreit}


\epigraph{Amo os caminhos sem destino e os destinos sem
caminho}{Simmel, assinando \textcite{Jugend-Strandgut2}}

<!--
  **TODO**: Acrescentar esta bela passagem de Ringer alhures:
  
  > O vínculo entre causalidade e determinismo era certamente um fator
  > central na reação antipositivista que aparentemente cresceu em
  > intensidade entre os anos 1890 e o início dos anos 1930. Acadêmicos
  > alemães de fora das ciências naturais começaram se referir a uma
  > "crise da cultura" antes da virada do século; pelo período
  > entreguerras, muitos também acreitavam numa "crise da *Wissenschaft*
  > \[ciência\]" ou do saber. A palavra "crise" simultaneamente
  > assinalava os perigos do "positivismo" e a emergência de um novo
  > "idealismo" na filosofia e nas humanidades. Em torno dos anos 1920,
  > a "filosofia da vida" (*Lebensphilosophie*) e a busca por "síntese"
  > cognitiva e por discernimento intuitivo levou a flertes abertos com
  > o irracionalismo, pelo menos nalgyuns setores. Em suas origems
  > durante os anos 1880 e 1890, entretanto, a renovação das disciplinas
  > humanísticas era um esforço substancial e potencialmente criativo
  > \[...\] [@Ringer-MaxWebersMethodology, p. 26]
-->



<!-- ## As várias faces da controvérsia metodológica -->

Na academia alemã do final do século XIX, as ciências sociais
encontravam-se em rebuliço. O progresso intensivo que as ciências
naturais realizaram ao longo dos séculos XVIII e XIX, acompanhada da
crise filosófica do período que segue às revoluções de março de 1848,
somados à dinâmica da abertura de cátedras em humanidades [Ver
@Ringer-Decline, cap. 1; @Kohnke-NeoKantianismus-en, cap. 3,
especialmente § 3] punha em questão a metodologia das ciências sociais
que se encontravam na aurora de sua diferenciação.

O avanço do positivismo e a redefinição das tarefas, objetos e
objetivos da ciência que ele impunha, particularmente nas humanidades,
onde algumas disciplinas (em especial a economia, psicologia e a
sociologia nascente) buscavam em seus princípios bases para sua
autonomização, como vimos, teve grande resistência nos círculos
intelectuais alemães.

Nas duas últimas décadas do século XIX, essa resistência assumiu a
forma de uma série de disputas em diversas disciplinas em torno de
questões metodológicas --- a
*\foreignlanguage{german}{Methodenstreit}*, ou controvérsia
metodológica.

> No início dos anos 1880, surgiu uma série de disputas na academia
> alemã em torno do objetivos, objeto e método das ciências sociais.
> Embora a *\foreignlanguage{german}{Methodenstreit}* --- a
> controvérsia metodológica --- tenha começado como um debate entre
> historicistas na economia alemã e teóricos da utilidade marginal em
> Viena, na véspera da Primeira Guerra Mundial essas disputas
> abrangiam filosofia, historiografia e sociologia. \[...\]
>
> Muitas questões interligadas estavam em jogo. _Havia um debate sobre
> os objetivos das ciências sociais_. Aqui, a questão era formada como
> uma escolha entre uma teoria abstrata da sociedade, talvez
> fundamentada em leis gerais de desenvolvimento histórico, e uma
> exposição das características singulares de formações sociais e
> tradições culturais. _Havia um debate sobre métodos_. Existe algum
> sentido em que toda investigação científica legítima deve seguir a
> mesma lógica? <!--60--> Ou há métodos distintos das ciências
> sociais? Esses dois debates eram atados a uma _terceira controvérsia
> sobre o objeto das ciências sociais_. Seriam a história humana, a
> sociedade e a cultura indistinguíveis em princípio da natureza e
> abertos aos mesmos tipos de explicação e métodos usados nas ciências
> naturais? Ou o fato de que seres humanos atribuem sentido e valor a
> sua conduta requereria modos de interpretação para os quais não há
> modelos nas ciências naturais?
>
> Por fim, havia um _debate sobre a relação entre ciência social e
> política social_. Essa controvérsia era ancorada em visões opostas
> sobre ciência e política, razão teórica e prática e interesses em
> que a teoria e a prática eram baseados. Soluções para os problemas
> práticos da vida social podem ser derivados da ciência social? Pode
> a ciência social atingir o status de um juiz imparcial, qualificado
> para resolver conflitos entre valores políticos, econômicos e éticos
> porque ela está acima das lutas da história? \[...\]
> [@Oakes-ValueTheoryAndTheFoundationsOfTheCulturalSciences, pp.
> 59-60, grifos meus]

Desses quatro aspectos da controvérsia metodológica elencados por
Oakes, vou tratar o quarto, para o escopo deste trabalho, como um
subsidiário dos demais [^werturteilsstreit]; os outros, creio, podem
ser colididos em duas questões relacionadas:

- a definição do objeto das ciências humanas, põe em jogo a
  continuidade ou não entre, de uma perspectiva "substancialista" ou
  ontológica, a realidade natural ou material e a psíquica ou social,
  e de uma perspectiva "formal", a metodologia das investigações
  nomológicas e a das descrições históricas. O ponto que separa as
  perspectivas substantiva e formal aqui é a maneira como se concebe a
  relação entre fenômeno observado e objeto científico, ou seja: é a
  coisa quem demanda um método ou o método quem define a coisa?; e

- partindo da definição dos objetos e métodos, a questão da
  classificação das ciências humanas e naturais --- em que está em
  jogo a busca de legitimar a aplicação da metodologia das ciências
  naturais nas disciplinas humanísticas ou salvaguardar estas últimas
  dessa possibilidade.

<!-- Apresentação do capítulo -->

---

<!-- Narrativa -->

A economia política foi colocada numa encruzilhada para definir sua
identidade quando o economista austríaco Carl Menger (1840--1921)
atacou a metodologia histórica da "nova" escola histórica de economia
capitaneada por Gustav von Schmoller (1838--1917), em busca de uma
redefinição da classificação e tarefas da ciência econômica e propondo
uma reorientação em direção a tornar a economia uma ciência exata
baseada na formulação de leis abstratas de comportamento econômico
(com sua obra *Investigações sobre o método das ciências sociais, em
especial o da economia política*, de 1883) [@Menger-Untersuchungen].

Ao mesmo tempo, Wilhelm Dilthey (1833--1911), buscava refundar as
ciências humanas para preservá-las do avanço do positivismo,
estabelecendo uma separação entre as ciências humanas (ou espirituais,
*\foreignlanguage{german}{Geisteswissenschaften}*) e as ciências da
natureza fundada sobre uma diferenciação entre seus objetos: enquanto
as ciências naturais lidam com a natureza, o ser, buscando isolar
certos aspectos para tentar explicá-los, as ciências humanas tem por
objeto objetivações do espírito humano, realizações propositivas, que
seriam por isso passíveis de uma compreensão interpretativa (ao
contrário de uma explicação segundo leis). [@Dilthey-Einleitung-orig]

A resposta de Schmoller a Menger veio numa resenha conjunta dessas
duas obras [@Schmoller-Methodologie], de certa forma reivindicando a
formulação de Dilthey e recusando, contra os avanços da economia exata
austríaca, a possibilidade de construção de leis econômicas abstratas
ao reafirmar a orientação historicista da economia como ciência
histórica, de objetos concretos singulares e a importância da
descrição histórica dessas individualidades. A áspera tréplica de
Menger [@Menger-Irrthumer] não recebeu resposta de Schmoller, que
assim tentou silenciar a controvérsia.

Mas ela prosseguiu. O que era percebido como um pernicioso avanço do
positivismo promoveu outros debates entre os epistemólogos
neokantianos que não aceitaram a distinção metodológica com
fundamentos ontológicos de Dilthey. Num famoso discurso ao assumir a
reitoria da Universidade de Estrasburgo, que "soou para seus
contemporâneos como uma 'declaração de guerra contra o positivismo'"
[@Hughes, p. 47], Wilhelm Windelband (1848--1915) buscou diferenciar
de maneira puramente metodológica entre as abordagens "nomotética"
(que busca a construção de leis na explicação de fenômenos
recorrentes) e a "idiográfica" (que busca descrever fenômenos
singulares). Essa distinção foi posteriormente elaborada por seu aluno
Heinrich Rickert (1863--1936) que foi profundamente influente sobre o
a metodologia de Max Weber (1864--1920), que retomou o debate no
início do século XX. [em @Weber-Objektivitat; e em
@Weber-RoscherUndKnies]

A publicação de sua *História alemã* (1891--1909) colocou Karl
Lamprecht (1856--1915) no meio de uma disputa que debatia essas e
outras questões com relação ao método da história. Os primeiros
volumes de sua obra foram duramente criticados por Georg von Below
(1858--1927) em 1898 num ensaio sobre o *Novo método histórico*
[@VonBelow-DieNeueHistorischeMethode] que recebeu uma resposta de
Lamprecht no ano seguinte.
[@Lamprecht-DieHistorischeMethodDesHerrnVonBelow] Lamprecht ousava
inovações metodológicas que procuravam superar a tradição
historiográfica fundada por Ranke (ver \autoref{tradicao-historica}),
partindo de pressupostos tirados da psicologia social de Wilhelm Wundt
(1832--1920), o que o aproximava do positivismo, e sofreu uma ferrenha
oposição dos historiadores mais ortodoxos.

Aqui, discutiremos três dos vários aspectos da controvérsia
metodológica: a questão de demarcação das ciências humanas (com
Dilthey e Windelband), a controvérsia entre historiadores dos anos
1890 (com Lamprecht, e só muito brevemente), e a controvérsia entre
economistas políticos dos anos 1880 (com Menger e Schmoller). A
apresentação desobedece a ordem cronológica para terminar pela
controvérsia inaugural entre os economistas porque é nela que mais
aprofundei por acreditar que os termos nela definidos (abordagem
"exata" contra "empírico-realista" ou "dedutiva" contra "indutiva")
são os que mais alimentaram a oposição --- e a busca por sua superação
--- entre os cientistas sociais alemães na virada do século.


<!-- NOTAS {{{ -->

[^positivismusstreit]: "A origem imediata da controvérsia estava na
conferência realizada em Tübingen pela Associação Alemã de Sociologia
em 1961 sobre a lógica das ciências sociais onde Popper apresentou
suas vinte e sete teses sobre o assunto. Ele foi respondido por Adorno
e a discussão que seguiu àquela conferência foi resumida por
Dahrendorf. Numa forma diferente, a controvérsia foi continuada por
Habermas e Albert de 1963 em diante e muitos outros autores abordaram
várias questões e aspectos da controvérsia." "\[...\] Há alguma
dificuldade, também, em afirmar que esta controvérsia é meramente uma
disputa metodológica e pode assim ser vista como uma extensão de
controvérsias anteriores nas ciências sociais na Alemanha, notadamente
a *\foreignlanguage{german}{Methodenstreit}* e a
*\foreignlanguage{german}{Werturteilsstreit}*."
[@Frisby-Introduction-PositivistDispute, pp. ix, x]

[^sobrinhos]: Embora frequentemente esse tipo de investigação tenha
outros objetivos, quer optando por uma ou outra das posições clássicas
à luz de sua revisão --- frequentemente sob a forma de um "resgate" de
um autor ou uma escola injustamente esquecidos ou vítimas de um
processo de obliteração forçado [é nesse sentido, creio, que
@Pinto-CollectifIndividuel, cap. 3, fala dos "sobrinhos" de Simmel e
Tarde, ao tratar da maneira como se inserem no debate acadêmico
contemporâneo através do resgate de um ou outro e de um discurso do
papel de Durkheim em seu esquecimento] --- ou para oferecer uma
terceira via redentora, dialeticamente superando as posições
contraditórias numa posição mais elevada.  [Penso aqui na técnica que
@LeviStrauss-TristesTropiques-pt, p. 48, descreve ter sido amplamente
difundida na Sorbonne dos anos 1930: "Lá comecei a aprender que
qualquer problema, grave ou fútil, pode ser liquidado pela aplicação
de um método, sempre idêntico, que consiste em opor dois pontos de
vista tradicionais da questão; em introduzir o primeiro pelas
justificações do senso comum, em seguida destruí-lo por meio do
segundo; enfim, em rejeitá-los ambos, graças a um terceiro que revela
o caráter igualmente parcial dos dois outros, reduzidos por artifícios
de vocabulário aos aspectos complementares duma mesma realidade".]

[^controversia-contextualista]: Penso aqui na oposição de
@Harlan-ReturnLiterature à abordagem contextualista de
@Skinner-Meaning ou na de @Rorty-PragmatistsProgress à de
@Eco-OverinterpretatingTexts, mas ela diz respeito ao problema do
círculo hermenêutico --- o regresso infinito na interpretação de
textos à luz do contexto do qual o texto é parte, e cuja compreensão
ajuda a iluminar --- já de longa tradição.

[^schmoller-menger-atuais]: Estou pensado aqui sobretudo na
controvérsia entre Schmoller e Menger, que um sem número de
intérpretes atuais [como @Bostaph-Methodenstreit;
@Louzek-BattleOfMethods; @Dippoliti-SchmollersMethod-CommentToLouzek]
ou mesmo alguns contemporâneos [como
@Keynes-ScopeAndMethodOfPoliticalEconomy, cap. 1] procura demonstrar
não ser tão polarizada quanto os adversários pareciam compreendê-la.

[^passeron-extincao]: "Os problemas se extinguiram (*é a única maneira
como se veem os problemas teóricos se resolverem em ciências sociais*)
pelo *esquecimento* da animosidade que os fazia viver na imobilidade
de questões fechadas para a escuta de outras questões."
[@Passeron-Raisonnement-pt, p. 66, grifos meus] O argumento de
Passeron é o de que a natureza histórica do objeto das ciências
sociais faz com que todo conhecimento seja transitório [não muito
distante de @Weber-Objektivitat] e por isso a constituição de léxicos
comuns impossível (porque cada discurso teórico diz respeito a um
objeto singular específico, preso em seu contexto histórico). Isso
pode ser verdade para os conceitos das ciências sociais --- classe
social, capitalismo, indivíduo ---, mas, em se tratando de
controvérsias *metodológicas*, não estamos mais num espaço de discurso
"sociológico", sequer científico, mas filosófico, onde esses acertos
deveriam ser (suponho) possíveis. A meu ver, a causa da incoerência
perceptível entre os discursos metodológicos programáticos e as
práticas científicas efetivas repousa sobre a própria disjunção entre
os dois espaços discursivos em que ambos são feitos --- ciência e
filosofia da ciência.

[^werturteilsstreit]: Esse debate remonta à controvérsia inaugural em
vista de que muitos dos membros da "nova" escola histórica de economia
política eram "socialistas de cátedra" da
*\foreignlanguage{german}{Verein für Socialpolitik}* ("Associação para
a política social") e de que parte do argumento de Menger se dedica a
criticar essa crença no papel das ciências humanas em reformas
sociais. A chamada *\foreignlanguage{german}{Werturteilsstreit}*, a
controvérsia sobre julgamentos de valor nas ciências sociais teve
talvez seu ponto alto nas críticas de Max Weber à
*\foreignlanguage{german}{Verein}* e à Sociedade Alemã de Sociologia,
em seu apelo por uma ciência livre de julgamentos de valor (o que deve
ser devidamente nuançado diante de sua visão do papel dos valores do
pesquisador na seleção e construção de seus objetos nas ciências
sociais, e que portanto não implica num apelo à possibilidade de
uma neutralidade axiológica absoluta) imortalizadas em seu clássico
sobre a objetividade do conhecimento nas ciências sociais.
[@Weber-Objektivitat; @Weber-Objektivitat-pt;
@Weber-Objektivitat-en-GAzW] <!--Parte dos motivos de porque isso era
uma questão importante discuto mais à frente, na
\autoref{refracao}.-->

<!-- {{{ -->
