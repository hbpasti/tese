### Princípios regulativos e constitutivos {#principio-regulativos-constitutivos}

A intenção de Simmel no debate metodológico que o encontro com a obra
de Stammler promove é, parece-me, a de evitar a dicotomia entre as
duas alternativas metodológicas para as ciências sociais --- para
usarmos o vocabulário de @Windelband-GuNw, as abordagens nomotética (o
método que busca regularidades e as descreve sob a forma de leis) e
idiográfica (o método que busca compreender o objeto em sua
singularidade sob forma descritiva) ---, buscando uma "alternativa à
alternativa" que não interdite o recurso a uma ou outra.

Nesse sentido, Simmel procura estabelecer um caminho intermediário que
combine a possibilidade das duas abordagens. Mas não se trata de
somente afirmar a necessidade de um terceira via conciliadora
abstrata, mas de estabelecer um princípio metodológico "determinado"
para promovê-la:

> A exigência muito pouco significativa em sua universalidade de que
> ambos os métodos devem "complementar-se reciprocamente" é aqui
> substituída pelo princípio determinado: que todo teorema
> racionalista depende, em sua compreensão de uma dedução histórica e
> que essa gênese histórica, por sua vez, não pode ocorrer sem a
> priori racionalistas. O *\foreignlanguage{latin}{regressus in
> infinitum}* aqui é a completa expressão legítima para a imperfeição
> de nosso conhecimento quando tenta ir além de qualquer estado dado.
> Expresso kantianamente: ao invés de dois princípios constitutivos,
> e, enquanto tais, inconciliáveis, obtemos dois princípios
> regulativos que são um a subestrutura do outro. Não se trata,
> portanto, de uma mistura mecânica ou de um compromisso eclético dos
> métodos opostos, mas antes do emprego de ambos como estágios
> alternados de *uma* metodologia abrangente. [@Mthdk-orig, p. 585]

Ao contrário de "uma mistura mecânica" ou "um compromisso eclético"
entre os métodos adversários da
*\foreignlanguage{german}{Methodenstreit}*, assim, trata-se de
encontrar um ponto de unidade mais elevado em que ambos compareçam
como momentos ou "estágios alternados" num "regresso infinito" que
resulta numa "metodologia abrangente": a relativista.

O problema de tentar combinar essas duas abordagens é que elas
se reivindicam como mutuamente exclusivas: porque seus objetivos são
distintos --- conhecimento a priori e empírico ---, elas comportam-se
como princípios incompatíveis. Nesse sentido, uma abordagem
conciliadora que as tente combinar numa metodologia eclética é
impossível porque contraditória.

Mas isso, segundo Simmel, só ocorre porque ambas são tomadas como,
utilizando um vocabulário kantiano, "princípios *constitutivos*". Para
resolver esse problema e compatibilizar as duas abordagens como
momentos de uma metodologia única, relativista, Simmel sugere ser
preciso tratá-las, ao contrário, como "princípios *regulativos*".

O que isso significa?

> Em geral, Kant usava a palavra "constitutivo" em sua filosofia
> teórica para se referira a *conceitos* ou princípios que constituem,
> fundamentam e determinam a *experiência* e os *objetos* da
> experiência, isto é, que servem como as condições necessárias para a
> possibilidade da experiência e, ao mesmo tempo, como as condições
> necessárias para a possibilidade dos objetos de experiência. Entre
> tais conceitos, ele contava as *formas* da *intuição* *espaço* e
> *tempo*, as *categorias* e os *princípios do entendimento*.
>
> Aos \[princípios\] constitutivos ele opunha os regulativos,
> atribuindo esse qualificador sobretudo às *ideias* da *razão*. Como
> a palavra "regulativo" sugere, a referência é feita às regras que
> regulam ou guiam-nos em nossa investigação.
> [@HolzheyMudroch-KantKantianismDict 82]

Tomados como princípios constitutivos, as duas abordagens são
excludentes (como sugere a observação da controvérsia entre Menger e
Schmoller, como vimos em \autoref{methodenstreit-economistas}) porque
"constituem, fundamentam e determinam" a experiência e seus objetos,
ou seja: são, cada qual, "o mundo todo considerado de um ponto de
vista particular" [@PHG, p. 25], para evocar a separação entre as
ordens da natureza e do valor presente na epistemologia simmeliana
mais tardia (que vimos em \autoref{valor-conhecimento}).

> Em seu uso _regulativo_, a razão guia nosso trabalho no esforço por
> conhecimento, auxiliando-nos a corrigir erros e a alcançar
> concepções mais compreensivas. Em contraste, para Kant, o uso
> "_constitutivo_" de nossas faculdades ajuda de fato a constituir os
> objetos do conhecimento ao prover sua forma como objetos de
> experiência possível. _Princípios constitutivos, assim, tem uma
> posição fortemente objetiva, enquanto princípios regulativos
> governam nossas atividades teóricas_. Como Kant coloca, atividades
> precisam ter objetivos se não devem degenerar em mero tatear
> aleatório (cf. B vii, A 834/B 862); o objetivo da razão é prover
> unidade. Quando Kant fala da "unidade da razão" na primeira
> *Crítica*, ele quer dizer que a razão dá "unidade a priori através
> de conceitos às múltiplas cognições do entendimento" (A 302/B 359);
> cf. A 665/B 693, A 680/B 780). Essa unidade precisa ser a priori uma
> vez que não pode ser dada por nenhum conjunto de experiências.
> [@Williams-KantsAccountOfReason, ênfase minha]

Na medida, portanto, em que se consideram como princípios regulativos
as duas metodologias fundamentalmente opostas quando concebidas como
princípios constitutivos, a sua contradição deixa de ser necessária e
torna-se possível sua coexistência, manifesta na forma da alternação
entre um polo e outro, agora complementares. Retomando a formulação
da *Filosofia do dinheiro*,

> Assim, enquanto estes dois métodos, postos dogmaticamente e
> reivindicando cada um para si a verdade objetiva, entram num
> conflito irreconciliável e numa negação mútua, eles são tornados
> possíveis na forma da alternação de uma mutualidade orgânica:
> cada um é transformado num *princípio heurístico*, isto é, de cada
> um se exige que a cada ponto de sua aplicação específica busque sua
> fundamentação em instâncias mais elevadas no outro. [@PHG, p. 112,
> ênfase minha]

Na *Filosofia do dinheiro*, onde a formulação do relativismo que é
apenas insinuada em "Para a metodologia das ciências sociais" é mais
rigorosamente desenvolvida, Simmel discute a transição entre o papel
de princípios constitutivos a regulativos do método mais detidamente.
Ali, como aqui, em "Para a metodologia das ciências sociais", ele
oferece uma definição de relativismo como precisamente essa transição
ou tradução de princípios constitutivos em regulativos:

> \[...\] pode-se formular o relativismo no sentido de princípios de
> conhecimento assim: traduzindo princípios constitutivos, que
> expressam a essência das coisas de uma vez por todas, em
> regulativos, que são somente o ponto de vista para o conhecimento
> progressivo. Justamente as mais definitivas e elevadas abstrações,
> reduções ou sínteses do pensamento devem abandonar a reivindicação
> dogmática de completar o conhecimento. No lugar da afirmação: de tal
> e tal maneira comportam-se as coisas ---, deve-se tomar, antes, com
> respeito às visões mais exteriores e gerais, a seguinte: nosso
> conhecimento deve proceder *como se* as coisas se comportassem de
> tal e tal maneira. Com isso é dada a possibilidade de expressar
> muito adequadamente o tipo e o modo de nosso conhecimento em sua
> relação real com o mundo. [@PHG, p. 106]

Nesse novo papel de princípios regulativos, as afirmações tem um papel
tentativo, experimental (naquele ciclo que remete ao infinito e que só
pode prosseguir ao atribuir status de conhecimento a priori a
conhecimentos empíricos etc.), heurístico.

> Se agora as afirmações constitutivas, que querem estabelecer a
> essência das coisas, são convertidas em heurísticas, que só querem
> determinar nossos percursos de conhecimento mediante o
> estabelecimento de termos ideais, então isto permite aparentemente
> uma validade simultânea dos princípios opostos; agora, quando seu
> significado sé reside nos caminhos até eles, pode-se percorrê-los
> alternadamente e contradizer-se de fato tão pouco quanto
> se contradiz na mudança entre métodos indutivo e dedutivo. [@PHG, p.
> 107]

Essa "tradução" de "julgamentos constitutivos em princípios
regulativos ou heurísticos" representa para Simmel a maneira como o
relativismo poderia ser sustentável [@Millson-ReflexiveRelativismGS,
p. 197]: "O segredo para evitar o típico dilema do relativismo
repousa, segundo Simmel, em assumir uma posição autorreflexiva e
guiá-la ao seu extremo radical" [@Millson-ReflexiveRelativismGS, p.
204]

> Pois tanto faz se se expressa: há um absoluto, mas ele só pode ser
> concebido num processo infinito, ou: só há relações, mas elas só
> podem substituir o absoluto num processo infinito. O relativismo
> pode fazer a concessão radical de que seria possível ao espírito
> colocar-se para além de si mesmo. [@PHG, p. 118]

<!--
  > Princípios fundacionais a priori não *constituem* os objetos de
  > nosso conhecimento empírico. Ao contrário, eles formam as guias
  > metodológicas para nosso processo de verificação. Eles são
  > normativos no sentido em que eles nos dizem o que *deveríamos fazer*
  > dadas certas condições e propósitos. Sua "verdade"
  > reside em sua habilidade de desempenhar essa função.
  > [@Millson-ReflexiveRelativismGS, pp. 197-198]
-->






<!-- NOTAS {{{ -->

[^def:circulo-hermeneutico]: "O círculo hermenêutico é uma maneira de
explicar e expressar a compreensão e a interpretação de uma obra de
arte como um processo contínuo que se desdobra no tempo. A
interpretação gradualmente muda para incorporar uma nova informação
que é adquirida numa perspectiva ao mudar para seu modo complementar.
O processo pode ser vista como progressivo e infinito, mas a relação
entre as abordagens é uma de dependência e suporte metodológicos e
semânticos, não de implicação lógica estrita."
[@Millson-ReflexiveRelativismGS, p. 199] "O círculo hermenêutico é na
verdade um círculo continuamente preenchido que une o intérprete e seu
texto numa unidade dentro de uma totalidade processual. Compreender
sempre implica uma pré-compreensão que por sua vez é prefigurada pela
tradição determinada em que o intérprete vive e que dá forma a seus
preconceitos. [@Gadamer-ProblemHistoricalConsciousness, p. 108]
"Dilthey por exemplo escreve: 'Uma obra precisa ser compreendida a
partir de palavras individuais e sua combinação mas uma completa
compreensão de partes individuais pressupõe a compreensão do todo.
Esse círculo é repetido na relação de uma obra individual com a
mentalidade e desenvolvimento de seu autor, e ele recorre novamente na
relação de uma tal obra individual com seu gênero literário.'"
[Dilthey, *Selected Writings*, p. 259 apud @Davey-Gadamer, p. 256,
nota 24]

[^simmel-relatividade]: A respeito da teoria da relatividade, Simmel
teria dito "A nova doutrina da física, a teoria da relatividade e o
que a acompanha, me são indiferentes, mas me entusiasmam." [Segundo a
coleção de apotegmas de @Bloch-AussprucheSimmels, p. 250]

<!-- }}} -->
