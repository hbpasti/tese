## Uma dialética sem síntese? {#dialetica-sem-sintese}


Numa carta a Heinrich Rickert de julho de 1916, Simmel procura
esclarecer para o colega, com quem mantinha relações cordiais mas
distantes, e com quem tinha algumas diferenças filosóficas, sua
visão do relativismo.[^simmel-rickert-abril-1916] A passagem é longa,
mas, uma vez que responde às objeções ao relativismo mais costumeiras,
não somente esclarecendo a visão de Simmel mas também distanciando-a
de outros tipos de relativismo, vale a pena ser citada:

> De qualquer maneira, até aqui poderíamos eventualmente nos entender.
> Pelo contrário, na questão do relativismo e dos pressupostos
> fundamentais da verdade, isso não se pode esperar. Aqui há
> claramente uma fenda entre as necessidade de nosso pensamento, que
> vai até o fundamento e que se deixa expressar historicamente pelo
> fato de que o senhor se mantém mais próximo de Kant do que eu; meu
> desenvolvimento se distancia lentamente dele. Também tenho a
> suspeita de que o senhor toma-me por um cético oculto, o que é
> completamente errôneo. É claro que o que frequentemente se entende
> por relativismo não é senão: que todas as verdades são relativas,
> portanto talvez erros, que toda moral é relativa, isto é, que noutra
> parte tem outro conteúdo, e similares platitudes. O que eu entendo
> por R\[elativismo\] é uma imagem metafísica de mundo totalmente
> positiva e é tão pouco ceticismo quanto o R\[elativismo\] físico de
> *Einstein* ou *Laue*[^relatividade-fisica]. Ela parte mesmo direto
> do círculo da proposição: há verdades --- para *retirar* dela, do
> ponto de vista lógico, a dubiedade indisputável. \[...\] O senhor
> escreve: "Algumas verdades podem ser somente relativas, mas que
> *toda* verdade seja somente relativamente verdadeira é uma não-ideia
> \[\foreignlanguage{german}{Ungedanke}\]." Assim, obviamente não
> deixei claro o que compreendo como o relativismo da verdade. Ele
> significa para mim não que verdade e inverdade sejam relativas uma
> para a outra, mas: que verdade significa uma *relação* de conteúdos
> entre si, a qual nenhum deles possui, assim como nenhum corpo é
> pesado por si, mas somente o é em relação recíproca
> \[\foreignlanguage{german}{Wechselverhältnis}\] com um outro. Que
> verdades *singulares* sejam relativas no vosso sentido, isso não me
> interessa em absoluto, de fato só o seu *todo* me interessa, ou
> melhor: seu *conceito*. Sua expressão: "*somente* relativas" ---
> revela que entende mal o meu pensamento. Pois não quero fazer o
> conceito de verdade fluir de um "mais elevado", mas somente dar-lhe
> uma base mais duradoura do que é aquele círculo, uma base que não
> deve ser *contra* a lógica, mas que não pode ser ganha somente *da*
> lógica. Deste ponto, a mais profunda diferença entre nós há de se
> desenvolver.
>
> E agora só uma última coisa. O senhor escreve: "não há nada absoluto
> sem que haja o relativo, mas também não há nada relativo sem que
> haja o absoluto. Um exige o outro." Visto de maneira puramente
> intelectual, a primeira me é duvidosa. Posso muito bem imaginar um
> absoluto que seja, por assim dizer, sozinho consigo, sem que haja
> algo relativo a ele --- como Deus antes da criação. De fato, mesmo
> um "mundo" de pura absolutidade não me parece (sob a correspondente
> modificação do conceito de mundo) impensável: o mundo das "mônadas
> sem janelas" ou (seguindo o propósito) o herbartiano são
> aproximações. Mas aparte isso, *sua afirmação tenciona a
> relatividade mesma além de absoluto e relativo*, torna mesmo esses
> conceitos mais polarizados relativos um ao outro! Essa é
> precisamente a relatividade infinita em direção à qual quero ir
> metafisicamente! [Carta de Simmel a Heinrich Rickert de 15 de abr.
> de 1916, em @GSG23, pp. 636-641; reimpressão de @BDGS, pp. 117-119,
> onde a data é erroneamente atribuída como sendo de 1917]

Uma série de aspectos da concepção simmeliana de relativismo
comparecem aqui: Simmel situa brevemente sua relação com o neokantismo
com alguma distância; também se distancia do ceticismo, aproximando-se
de uma visão que, mais do que relativista, poder-se-ia considerar
relacionista (posto que concebe a verdade como uma relação, como uma
propriedade relacional que só existe na interação dos conteúdos)[Penso
aqui sobretudo na leitura de
@Vandenberghe-RelativismeRelationnismeStructuralisme; ao qual só tive
acesso na versão manuscrita disponibilizada pelo autor,
@Vandenberghe-RelativismeRelationnismeStructuralisme-ms; essa leitura
reaparece em seu @Vandenberghe-GS-pt.]; e demonstra como sua noção de
relativismo é mais radical do que a mais costumeira, procurando situar
numa relação absoluto e relativo numa "relatividade infinita".


A pergunta que eu queria colocar a essa noção de relativismo, então,
é, mobilizando uma interpretação bastante conhecida de seu pensamento:
pode-se pensar esse relativismo simmeliano como uma espécie de
"dialética sem síntese"?

---

É de Michael Landmann a interpretação do pensamento de Simmel como uma
"dialética sem conciliação" ou "dialética sem
síntese".[^landmann-refs]

> Dialética sem síntese \[...\] é uma posição que não pode ser
> sustentada, que, enquanto tal, se empurra para além de si mesma.
> \[...\] É um neohegelianismo que não se entende como
> hegelianismo.[^landmann-neohegelianismo] A consolidação das imagens
> objetivas que Marx elaborou para a esfera econômica volta a Hegel, e
> é então posta numa base mais ampla por Simmel. As antíteses, que
> Hegel sempre cobriu de maneira muito leve e muito rápida,
> autonomizam-se em Simmel, tornando-se dominantes, ameaçadoras,
> trágicas. [@Landmann-Konturen, p. 13]

Essa definição aparece na resposta a um questionamento, no registro da
discussão que seguiu à fala [@Landmann-Konturen, pp. 12-17,
"\foreignlanguage{german}{Aus der Diskussion}"]: a pergunta de
Hans-Joachim Lieber referia-se a uma espécie de cripto-hegelianismo
que atravessaria gerações de intelectuais alemães, passando por
Simmel, Mannheim, Lukács e Adorno (que são nominalmente mencionados) e
é talvez por isso que a resposta recai numa fórmula não vazia de
exageros "é um neohegelianismo que não se entende como hegelianismo".
Mas o debate remete, na fala de Landmann, à maneira como ele descreve
o que considera como a segunda fase do pensamento de Simmel, quando,
distanciando-se de seu evolucionismo e positivismo da juventude, ele
teria se aproximado dos neokantianos do sudoeste da
Alemanha[^landmann-fases]. No cerne dessa segunda fase de seu
pensamento (mas também permanecendo posteriormente, na sua fase
vitalista), estaria uma disposição a colocar as antíteses em relação
sem procurar superá-las numa síntese dialética.

> Da mesma visão, embora desenvolvido mais tarde, o pensamento de
> Simmel defende (antecipando as "formas simbólicas" de Cassirer) que
> os diversos "mundos" da realidade cotidiana prática (que não possui
> qualquer prioridade), da religião, da filosofia, da ciência e da
> arte (e talvez outros "mundos" futuros) remontam a diferentes
> princípios de organização que, em sua medida respectiva, formam a
> totalidade dos materiais do ser em um outra "forma". Cada um desses
> mundos obedece a uma lógica fechada soberana, não é rastreável a
> qualquer outro e fica num mesmo nível fundamental com os demais.
> Eles possuem sua própria verdade (e seu própria erro). Assim, por
> exemplo, o filósofo dispõe do órgão reativo à "totalidade" unitária
> das coisas; sua imagem obtida de uma grande "distância" não é
> confirmada pelas imagens de distâncias mais curtas, mas também não é
> por elas refutada: cada visão tem legitimidade. O individuo é sempre
> determinado por muitos de tais "mundos", mas precisamente por isso
> não é determinado completamente por nenhum deles. Ele entra assim em
> conflitos, que, por outro lado, dão à vida um pano de fundo.
>
> Assim como Simmel não se nega aqui oposições, mas as coordena como
> possibilidades típicas, ele também justapôs filosofias
> contraditórias, em "Kant e Goethe" mecanismo e organicismo, em
> "Schopenhauer e Nietzsche", desprezo e júbilo pela vida. Não
> deveríamos e não podemos decidir entre eles. Ao abarcar ambos, assim
> argumentam as conclusões de ambos os livros, o espírito adquire um
> maior alcance, provando-se-lhe renovada sua força criativa. A
> síntese, que no objetivo, onde Hegel e Troeltsch a tentaram, não
> sucede, consiste no movimento entre as antíteses, ocorre
> reflexivamente no sujeito compreensivo. Filosofar significa: deixar
> em pé as oposições, poder com elas circular, buscar um "terceiro
> império" que não é sua "superação"
> \[\foreignlanguage{german}{Aufhebung}\]. Além disso, "problemas" não
> existem somente para "soluções". A maior tarefa pode ser torná-los
> visíveis, experimentá-los. [@Landmann-Konturen, p. 4]

Deixo aqui de lado, relutantemente, essa construção simmeliana acerca
dos "mundos"[^simmel-mundos], que como que pulveriza aquela oposição
entre as ordens do ser e do valor para uma série de domínios culturais
vastos (dentre os quais Simmel concebe, sem qualquer prioridade sobre
os demais, a "realidade" cotidiana), para ater-me exclusivamente à
maneira como as oposições entre as suas marcas na formação do
individuo deixa entrever um aspecto mais geral da visão de mundo de
Simmel, um que é crucial para pensar também o seu debate metodológico:
a maneira como Simmel concebe as contradições.

As contradições, como vemos, não seriam negadas, nem buscaria Simmel a
sua superação dialética numa síntese mais elevada: antes, elas são
coordenadas, justapostas, postas, enfim, em relação, de uma
perspectiva que as concebe não como opostas, mas como complementares
ou pelo menos relacionais. "Deixar em pé as oposições, poder com elas
circular": eis o fundamento da dialética sem síntese de Simmel.
Dito de outro modo: a dialética de Simmel não tem síntese porque não
busca conciliações, mas reconhece o caráter essencialmente conflitual
delas, e a maneira como as oposições podem ser postas em
relação no entendimento: mais adiante, Landmann acrescenta que

> Conciliador, Simmel só o é da perspectiva da compreensão
> \[\foreignlanguage{german}{Verstehens}\]: não podemos suspender
> \[\foreignlanguage{german}{aufheben}\] as oposições numa unidade.
> Podemos fazer jus a cada uma dessas oposições no espírito, e esse é
> precisamente o mérito daquilo que não representa e não se identifica
> com uma dessas oposições por si mesmo. Ao contrário, no lado da
> realidade reina a irreconciliabilidade, que só permite a artistas,
> como em milagres --- numa lei individual --- reuni-las. Da
> perspectiva da filosofia da cultura e da história, é sempre um
> antagonismo entre duas forças, espontaneidade e solidez, que lutam
> entre si e movem o processo histórico em geral. [@Landmann-Konturen,
> p. 15]

A noção de conflito de Simmel rendeu bastante debate na segunda metade
do século XX, particularmente em vista da sociologia do conflito
elaborada por Lewis Coser nos anos 1950, partindo da leitura do
aspecto positivo, socializador, do conflito como "forma de
sociação".[^simmel-conflito-refs] Aqui, com Michael
Landmann[^julien-freund-compromisso], essa ênfase no conflito ganha um
papel ainda mais proeminente na medida em que fundamenta o "princípio
dualista" [Para falar com @Vandenberghe-GS-pt] de seu pensamento ---
ou pelo menos, com ele partilha seu fundamento.

---

Longe de emitir algum julgamento sobre a interpretação do pensamento
de Simmel segundo essa chave da dialética sem síntese, o que eu quero
aqui é tentar operá-la para pensar a relação entre absoluto e relativo
no relativismo de Simmel --- que, como vimos, é mais refinado do que o
que geralmente se atribui ao relativismo.

Não é nenhuma novidade que o argumento de Simmel seja assim
circulante, apresentando ora um, ora outro viés, frequentemente
levando argumentos que não são seus até seus limites para voltar e
apresentar seu oposto, de maneira a transparecer, muito
frequentemente, uma profunda ambivalência. Construído dessa maneira
dialógica, no entanto, seu argumento é dialético no sentido mais
clássico --- de colocar em diálogo posições opostas --- e não
necessariamente fichteana --- de colocar posições opostas numa relação
em busca de uma superação dialética, que suspende a oposição mantendo
aspectos de ambas as posições numa síntese mais abrangente.

Nesse sentido, e diante da "relatividade infinita" que acompanhamos
até aqui, em que medida se pode descrevê-la como uma "dialética sem
síntese"? Ou, dito de outra maneira: o emprego da preposição "*sem*
síntese" implica num dualismo de dialéticas possíveis --- as com e as
sem síntese --- e dicotômicas. Aplicando de volta a essa oposição o
princípio da relatividade infinita, como se pode conceber o
relativismo de Simmel como assentado numa ou noutra?

Não se pode não pensar a relação entre absoluto e relativo, posta numa
"relatividade infinita", senão como dialética, com ou sem síntese:
vimos como Simmel quer "tencionar" a própria relatividade "para além
de absoluto e relativo", de modo a tornar "mesmo esses conceitos mais
polarizados relativos um ao outro". Nesse sentido, trata-se de
concebê-los em sua relação recíproca, substituindo --- no que se
refere à metodologia --- seu papel de princípio constitutivo pelo de
regulativo.

Assim, se o relativismo aparece inicialmente como antítese do
absolutismo, onde se encontra a superação dessa contradição? Se a
dialética é "sem síntese", o próprio relativismo, embora enriquecido
ou ampliado ao ser assentado sobre uma "relatividade infinita", é quem
resta como o elemento capaz de superar a oposição, como a insistência
de Simmel em assim denominar sua metafísica positiva e sua teoria do
conhecimento; se a dialética é "com síntese", esse novo relativismo,
que altera os princípios constitutivos em regulativos e que concebe os
termos em sua função dependendo de uma perspectiva é alguma outra
coisa que o relativismo inicial.

<!-- Essa é, a meu ver, uma falsa questão (como disse, estou considerando a -->
<!-- "dialética sem síntese" como um princípio heurístico para pensar a -->
<!-- solução de Simmel ao impasse entre relativismo e absolutismo).  -->

O fundamento do relativismo metodológico de Simmel, como vimos, é
conceber as metodologias contraditórias como princípios regulativos, e
não mais como constitutivos. Isso implica em alterar seu status de
elemento capaz de ser mobilizado na construção do conhecimento para o
papel puramente regulativo, ou seja, de guiar o entendimento na
construção do conhecimento --- noutras palavras, para o status de
princípio "meramente" metodológico ---, que, como vimos, é concebido
por Simmel como sempre tentativo, incompleto, e dependente de uma
perspectiva ou escala.

Para efetuar essa conversão de princípio constitutivo em regulativo e
transformar a contradição em oposição é preciso colocar-se numa
posição exterior a cada uma dessas posições e percebê-las em sua
*relação*, a propriedade que as opõe. Isso é válido tanto para conceber
as duas posições metodológicas fundamentais --- empirismo e
"dogmática" --- como as duas disposições fundamentais com relação à
teoria da verdade --- absolutismo e relativismo ---, assim como as
várias visões de mundo --- idealismo e materialismo, por exemplo ---
ou, posteriormente, os próprios "mundos" --- religião e ciência, por
exemplo.

Esse ponto de vista externo à contradição, capaz de convertê-la em
relação e de, assim, transformar princípios constitutivos em
regulativos, creio, é a perspectiva a que Simmel aludia, seguindo o
emprego de Spinoza, como *\foreignlanguage{latin}{sub specie
aeternitatis}*: a exigência de que se considerem os objetos da
perspectiva da eternidade, tomados como universais; como descreve o
próprio Simmel, "Spinoza demanda do filósofo que considere as coisas
*\foreignlanguage{latin}{sub specie aeternitatis}*. Isso significa:
puramente de acordo com sua necessidade e significância internas,
aparte da contingência de seu aqui e agora."[^sub-specie-aeternitatis]

Dessa perspectiva que observa as coisas sob a forma da eternidade,
buscando as compreender em sua necessidade interna e enquanto
totalidades unitárias, as oposições aparecem não mais como
contradições numa dicotomia, mas como uma unidade, sua oposição
aparecendo como a lógica de um todo e só concebível enquanto tal em
sua relação.

Ambas as leituras --- de uma dialética com ou sem síntese ---, assim,
são igualmente válidas segundo o próprio princípio do relativismo de
Simmel: tanto faz se você prefere manter as oposições dançando ou se
quiser superá-las numa síntese mais ampla. O conflito não é
irreconciliável, pelo menos neste caso, porque a oposição é puramente
intelectual; se para você funciona como um princípio heurístico
superar a contradição numa síntese dialética, tudo bem.


---

Vimos como o relativismo de Simmel é diferente daquilo contra o que
boa parte do pensamento alemão se debateu durante todo o século XIX,
grosseiramente porque não desanda nem, na epistemologia, em ceticismo
e nem, na moral, em niilismo. Nesse sentido, é tentador, senão
necessário, qualificá-lo como algo outro (empreguei, alternadamente,
até aqui expressões como "metodologia relativista", "relatividade
infinita" etc., tentando sempre permanecer fiel à letra de Simmel),
mesmo que isso não implique em completar a síntese dialética que fica
como que pendente, apenas sugerida.

No entanto, uma leitura exagerada dessa disposição relativista, para a
qual na metodologia de Simmel "vale tudo", sem levar em consideração a
operação que situa o olhar metodológico numa distância segura das
contradições, concebendo-as em sua relação, resulta na leitura
de Simmel como o "esquilo filosófico" posterizado na metáfora de José
Ortega y Gasset, que descreve Simmel como "aquele espírito agudo,
espécie de esquilo filosófico", que "nunca problematizava os problemas
que escolhia, mas os aceitava como uma plataforma para executar seus
maravilhosos exercícios de análise."
[@Ortega-PidiendoUnGoetheDesdeDentro, p. 398]

Mas para entender Simmel, o filósofo (e sociólogo), ao invés de
Simmel, o esquilo, é preciso ir além da aparência de uma metodologia
meramente eclética e compreender que "em coisas espirituais, o
afrouxamento dos fundamentos não precisa pôr em risco a solidez da
construção" [@Mthdk-orig, p. 578] e que não se tratava, para ele, de
combinar mecanicamente metodologias opostas --- como sugere o epíteto
de "pluralismo metodológico" [A expressão é de @Lukacs-obituario-orig;
consta uma tradução brasileira, traduzida do francês em
@Lukacs-obituario] que, embora frequentemente mobilizado para
qualificar o seu pensamento, refere-se à disposição de compreender a
necessidade de mais de uma metodologia (como em Schmoller, que também
assim é frequentemente qualificado) --- mas de encontrar um ponto que
tornasse possível o "emprego de ambos como estágios alternados de
*uma* metodologia abrangente." [@Mthdk-orig, p. 585]

Para assim qualificá-lo (talvez nomeando a síntese das posições), além
da "dialética sem síntese" ou conciliação de Landmann, poder-se-ia
também falar naquilo que Leopoldo Waizbort denomina, a partir de uma
expressão do próprio Simmel, "panteísmo estético" [@Waizbort, passim];
naquilo que David Frisby descreve como "perspectivismo", a partir da
ênfase que Simmel dá às diferentes e igualmente válidas perspectivas
na *Filosofia do dinheiro* [@Frisby-SociologicalImpressionism,
passim]; naquilo que Natàlia Cantó-Milà se refere como "relacionismo
sociológico" ou mesmo "sociologia relacional"
[@CantoMila-ASociologicalTheoryOfValue, passim, mas especialmente pp.
23, 43, 161 ff.]; no que Frédéric Vandenberghe se refere como um
"espinosismo sem substância e mesmo como um misticismo sem Deus"
[@Vandenberghe-GS 16; @Vandenberghe-ImmanentTranscendence; que tem uma
primeira versão em
@Vandenberghe-RelativismeRelationnismeStructuralisme]; ou naquilo que
Jared Millson se refere como um "relativismo reflexivo"
[@Millson-ReflexiveRelativismGS, passim]. Em suma, trata-se da visão
de que

> Simmel não apenas salta de um tema ao outro, ele também
> continuamente muda sua perspectiva interpretativa. O real é
> inesgotável e só pode ser compreendido de muitas perspectivas
> diferentes, em que cada qual captura um aspecto da vida sem jamais
> lhe esgotar o significado. [@Vandenberghe-PhiloHistGermanSoc, p. 70]

Todas essas qualificações de seu relativismo, em especial aquelas que
o concebem da perspectiva de seus desdobramentos para sua obra
posterior --- com o olhar mais voltado para sua metafísica tardia de
Vandenberghe, o mais voltado para sua teoria da cultura de Waizbort,
ou o focado em sua sociologia de Cantó-Milà --- servem com igual
validade para descrevê-lo adequadamente.

Quero discutir alguns desses qualificadores, *perspectivismo* e
*relacionismo*.


### Perspectivismo {#perspectivismo}

Simmel nos convida a pensar seu relativismo como um "perspectivismo"
--- como a visão de que qualquer ponto de vista é igualmente válido e
que se pode ou se deve observar um objeto de várias tais perspectivas
--- quando afirma, no prefácio da *Filosofia do dinheiro* que

> As intenções e métodos aqui mencionados não podem reivindicar nenhum
> direito a princípio, se não puderem servir a uma multiplicidade
> substantiva de convicções filosóficas básicas. Os vínculos entre as
> singularidades e superficialidades da vida e seus movimentos mais
> profundos e essenciais e sua interpretação conforme um sentido total
> podem ser consumados tendo por base o idealismo ou o realismo, o
> intelecto ou a vontade, uma interpretação absolutista ou relativista
> do ser. O fato de que as investigações subsequentes fundamentam-se
> numa dessas imagens de mundo, a qual considero a expressão mais
> adequada dos conteúdos do saber e das direções dos sentimentos
> contemporâneos, e excluam resolutamente as opostas, pode
> assegurar-lhes no pior dos casos ao menos o papel de um mero exemplo
> didático que, mesmo que objetivamente incorreto, revele sua
> importância metodológica como forma de correções futuras. [@PHG, p.
> 13]

O relativismo de Simmel é radical, como vimos, ao ponto de tencionar a
própria oposição entre absoluto e relativo, concebendo a sua oposição
como uma relação; nesse sentido é que são igualmente válidos, como
pontos de vista equivalentes, tanto a interpretação absolutista quanto
a relativista. É que, parece, não é possível conceber um ponto fixo
em que se apoiar para decidir por um ou outro caminho, especialmente
em vista de que eles só se opõe na própria relação: dito de outro
modo, é somente de maneira relacional que se pode compreender a
oposição entre esses polos binários, e essa perspectiva --- que os vê
em relação --- é também aquela onde essas oposições não mais aparecem
como contraditórias, como dicotomias, mas como duas faces de uma mesma
moeda.

O perspectivismo, parece-me, diz mais respeito às visões de mundo
("*\foreignlanguage{german}{Weltanschauungen}*") que servem de
fundamento e ponto de partida para a imagem filosófica de mundo de
quem busca conhecê-lo: é mais a um relativismo axiológico que a um
metodológico que concerne ao perspectivismo. Que seja equivalente
conceber o mundo de maneira materialista ou idealista etc. é, no
fundo, um corolário do relativismo fundamental de Simmel, que, olhando
como que de fora, concebe essas visões de mundo opostas como
complementares. É isso que enseja a famosa proposta da *Filosofia do
dinheiro*, que estabelece que "a unidade destas investigações reside,
portanto, não numa reivindicação sobre um conteúdo singular do
conhecimento e sua comprovação gradualmente emergente, mas antes na
possibilidade a ser demonstrada de encontrar em cada particularidade
da vida a totalidade de seu sentido."[^totalidade-vida]


### Relacionismo {#relacionismo}


A "relatividade infinita" que Simmel almeja, como vimos, concebe a
verdade como uma *relação*, como uma propriedade relacional que só
existe na "interação recíproca" entre conteúdos. O relativismo
significa para Simmel "não que verdade e inverdade sejam relativas uma
para a outra, mas: que verdade significa uma *relação* de conteúdos
entre si, a qual nenhum deles possui, assim como nenhum corpo é pesado
por si, mas somente o é em relação recíproca com um outro." É por isso
que "o todo da verdade é talvez tão pouco verdadeiro quanto o todo da
matéria é pesado", como se alude na epígrafe; porque, como o peso, a
verdade é uma propriedade relacional, uma propriedade atribuída na
consideração recíproca de conteúdos distintos.

Nesse sentido, e em consonância com a busca de um termo conciliador
para a dialética até aqui sem síntese, o "relativismo reflexivo" de
Simmel, na trilha de um retorno infinito sobre si mesmo que tenciona a
própria oposição entre absoluto e relativo e assim por diante, é, antes, numa
definição anacrônica, um *relacionismo*.

Digo anacrônica porque "relacionismo" é uma bandeira metodológica que
surge com Mannheim, [Ver @Relationismus-HWPh] posteriormente à morte
de Simmel, como uma resposta anti-relativista ao relativismo
historicista. Depois de comprar o historicismo como a
*\foreignlanguage{german}{Weltanschauung}* essencialmente
contemporânea,[Em @Mannheim-Historicism] Mannheim pôs-se a cata de uma
posição que lhe permitisse avaliar e criticar as ideias estudadas da
perspectiva de uma sociologia do conhecimento --- a perspectiva
puramente relativista, a seu ver, ao estabelecer a equivalência entre
todas as ideias e ancorar todos os valores na sua "determinação
situacional" termina incapaz de se colocar em lugar de
julgá-las.[@Mannheim-IdeologyAndUtopia, cap. 2, §5]

O argumento de Mannheim é de que é preciso escapar a uma certa
epistemologia objetivista antiga e, reconhecendo que todo conhecimento
é relacional "e só pode ser formulado com referência à posição do
observador", estabelecer um ponto de vista social em que se possa
julgar a validade da verdade e dos valores desse conhecimento
"determinado situacionalmente"; essa perspectiva seria a sociologia do
conhecimento, e seu princípio fundamental, superando o limite do
relativismo e o ceticismo que seu apego a uma epistemologia
objetivista enseja, o relacionismo:

> Como uma concepção essencialmente dinâmica, o R\[elacionismo\] se
> distancia tanto de uma teoria estática do conhecimento que se
> constrói a partir da ahistoricidade da estrutura do sujeito e/ou do
> objeto, quanto de um niilismo nivelador de todo sistema de valores;
> "... o R\[elacionismo\], como o empregamos, defende que toda
> afirmação essencialmente só seria formulável de maneira relacional,
> e ele só se transforma em relativismo se vinculado com o mais antigo
> ideal estático de verdades eternas, dessubjetivadas,
> não-perspectivísticas e se é medido ... por este ideal que lhe é
> estranho." [@Relationismus-HWPh, p. 613; a citação é de
> @Mannheim-IdeologyAndUtopia, p. 270]

Mas o relativismo a que Mannheim alude é aquele que se prende a uma
epistemologia absolutista --- e a nega, negando-se também ele a si
mesmo, assim, uma posição capaz de, reconhecendo a "determinação
situacional" de todo conhecimento, julgar (ou jogar com) as posições
estudadas.

Na medida em que o relativismo de Simmel põe em jogo as oposições,
concebendo-as como princípios regulativos --- não se pode concebê-lo
como em tal posição? Embora eu tenda a responder de maneira negativa
a respeito da avaliação e julgamento das posições estudadas (pois
a meu ver o perspectivismo que coloca em jogo as visões de mundo não
estabelece um chão mais firme donde julgá-las, mas resulta numa
espécie de decisionismo em que as alternativas estão dadas como
escolhas pelas quais se deve decidir, sem jamais ter acesso a uma
posição teórica superior), mas de maneira positiva no tocante à
dimensão epistemológica. Pois se há verdade, ela só existe como uma
relação, e situar-se para além da contradição entre verdadeiro e
falso, absoluto e relativo, é que estabelece a possibilidade de
observá-la.

Assim, a meu ver, o relativismo de Simmel, devidamente qualificado,
deve ser considerado por um lado perspectivista, porque concebe
toda perspectiva normativa de ordenação de mundo (visão de mundo)
como essencialmente igualmente válida, recusando-se a fornecer um
ponto de apoio metafisicamente seguro para a escolha entre elas, e
relacionista porque se posiciona numa perspectiva capaz de olhar as
contradições *sub specie aeternitatis*, concebendo-as como oposições
em relação, num totalidade unitária, e assim capaz de mobilizá-las
de maneira alternada ou justaposta como princípios regulativos.


<!-- NOTAS {{{ -->

[^simmel-rickert-abril-1916]: A conversa começa com a leitura da
terceira edição, de 1915, da tese de habilitação de Rickert, *O objeto
do conhecimento: uma introdução à filosofia transcendental*
(*\foreignlanguage{german}{Der Gegenstand der Erkenntnis: Einführung
in die Transzendentalphilosophie}*, de 1892, completamente
retrabalhada nas segunda e terceira edições, respectivamente, de 1904
e 1915), e segue por todo o abril de 1916. Em suas cartas, Simmel
debate com Rickert sua concepção da separação entre as séries do ser e
do valor (que Simmel discute no primeiro capítulo da *Filosofia do
dinheiro* e pela qual passamos o olho na
\autoref{valor-conhecimento}), epistemologia e a sua noção de
relativismo. As cartas datam de 3, 8 e 15 de abril de 1916 [@GSG23,
pp. 619-620, 624-627, 636-641].

[^relatividade-fisica]: Sobre isso, é válido mencionar que, numa
seleção de "ditos" de Simmel por Ernst Bloch, consta o seguinte a
respeito da teoria da relatividade: "A nova doutrina da física, a
teoria da relatividade e o que a acompanha, são-me indiferentes, mas
entusiasmam-me." [@Bloch-AussprucheSimmels, pp. 250, 251].

[^landmann-neohegelianismo]: @Landmann-Konturen, p. 13. Sobre o
suposto hegelianismo de Simmel, o autor explica: "Na época de Simmel,
Hegel era conhecido, se tanto, somente a partir de livros didáticos.
Para Franz Brentano, Hegel era a mais profunda degeneração do
pensamento humano. O pensamento de Hegel aparece em Simmel e outros
sem que eles, talvez, soubessem o quanto eles estavam vinculados a ele
como seus netos e herdeiros." [@Landmann-Konturen, p. 13] Isso evoca
de certa maneira uma impressão registrada por Célestin Bouglé em seu
diário de viagem pela Alemanha: "Descobri outro dia, numa pequena
praça, escondido sob as tílias, humilde e melancólico, o busto de
Hegel. Ele observa a Universidade, que lhe dá as costas. As babás
indiferentes, com seus uniformes do Spreewald, se instalam a seus pés.
Os estudantes passam rapidamente sem vê-lo. Sentei-me nos degraus de
seu pedestal e ele pareceu olhar-me com uma tristeza benevolente,
sussurrando para mim que, se vim aqui em busca de grandes filósofos,
posso voltar para casa." [Entrada de 8 de março em @Bougle-Breton, pp.
99-100] Sobre a reação contra o pensamento sistemático no período do
"pós-março" após as revoluções de 1848 e sua associação a ideologias
partidárias, ver @Kohnke-NeoKantianismus-en, cap. 3.

[^landmann-fases]: @Landmann-Konturen divide a obra de Simmel em três
fases, a que diz respeito ao recorte que discutimos aqui sendo a
segunda delas --- a grosso modo, uma primeira fase positivista e
evolucionista, uma segunda neokantiana e uma terceira vitalista: "O
jovem Simmel parte do pragmatismo, do darwinismo social, do
evolucionismo spenceriano e de seu princípio da diferenciação. O
atomismo de Fechner e da 'heterogeneidade determinada'
\[diferenciada\] de Spencer levam-no, já na dissertação sobre a
'monadologia' ao problema do individual." "Numa segunda fase, ele
coloca-se na vizinhança do neokantismo do sudoeste. Ele toma os
conceitos de valor e de cultura como uma esfera além da causalidade
natural, torna-se um codescobridor da posição própria das obras
'ateóricas' e toma parte na 'critica da razão histórica'. Uma
terceira fase fica sob a forte influência de Bergson (desde 1908)
\[...\]. Mas ela é preparada por estudos sobre Goethe, Schopenhauer e
Nietzsche. Agora, Simmel torna-se 'filósofo da vida'. Sua ideia
fundamental é que a vida sempre se limita por formas por ela mesma
engendradas, mas que só se completam nessa limitação."
[@Landmann-Konturen, pp. 3, 3-4] Suas palestras durante a Guerra
comporiam algo como uma quarta fase, mas porque não se encaixam muito
no restante de seu pensamento, e Simmel ter-se-ia distanciado dessas
ideias a partir de 1917. [@Landmann-Konturen, p. 10]

[^simmel-mundos]: \label{simmel-mundos} Essa visão, que antecipa
também a noção da diferenciação entre as esferas de Weber, aparece bem
elaborada já no ensaio sobre a religião de 1906: esses "mundos", para
Simmel, seriam cada uma das "grandes formas de nossa existência" --- o
que compreende as grandes esferas de produção cultural ocidentais ---
como esferas simbólicas que organizam a totalidade da existência (ou
melhor: dos conteúdos da existência) sob um único princípio, de modo a
construir mundos (simbólicos) incompatíveis com os mundos concorrentes
precisamente porque demandam a legitimidade sobre a totalidade dos
conteúdos que subsomem. Noutras palavras: arte, ciência, religião,
cada uma dessas grandes esferas simbólicas, significam todos os
conteúdos existentes sob princípios próprios, criando com isso seus
próprios mundos com uma legalidade própria que não pode aceitar a
perspectiva das demais. "Quando o problema da coexistência da
existência física e da mental começou a inquietar os pensadores,
Spinoza resolveu essa incompatibilidade definindo que a extensão, por
um lado, e o pensamento, por outro, expressavam em linguagem própria a
totalidade da existência; ambos só seriam capazes de coexistir caso
deixassem de encarar um ao outro como relativos, mas cada um
reivindicasse para si a totalidade do mundo e, a seu modo, a
representasse inteira. Segundo a máxima mais geral, *cada uma* das
grandes formas de nossa existência deve provar sua capacidade de
exprimir em termos próprios a totalidade da vida. A organização de
nossa existência mediante o domínio absoluto de um princípio sobre
todos os outros seria, assim, elevada a um plano superior: nenhum
desses princípios, dentro da imagem do mundo que produziu
independentemente, deveria sentir-se ameaçado pelo outro, pois aceita
o direito dele de formar o mundo a seu modo. Essas interpretações do
mundo não colidiriam, assim como os sons não se chocam com as cores. A
base dessa ideia é a distinção entre as formas e os conteúdos da
existência." [@Religion-orig, pp. 7-8; reimpresso em @Religion; citado
conforme a tradução brasileira, da edição de 1912, @Religion-pt 22]
Posteriormente, a questão é retomada em duas obras tardias, *Problemas
fundamentais da filosofia* e *Visão da vida: quatro capítulos
metafísicos*. Quem discute bem essa concepção simmeliana dos "mundos",
sob o nome de "formas-mundo" é @Weingartner-ExperienceAndCulture, cap.
1, especialmente §8.

[^simmel-conflito-refs]: Ela toda baseada num conjunto de escritos
sociológicos de Simmel acerca do conflito e da concorrência, que
viriam a compor o quarto capítulo de sua "grande" Sociologia de 1908
[@Soz-orig; reimpressa em @Soz], como @SozKonk-orig; reimpresso em
@SozKonk; @EnStr-orig; reimpresso em @EnStr-orig; e @MeFd-orig;
reimpresso em @MeFd.

[^julien-freund-compromisso]: Assim como com Julien Freund, que
acrescenta ao debate: "Síntese é um processo intelectual. Em Simmel
há, então, compromisso e não há síntese. Isso é coerente com sua
teoria do conflito. Ele afirma mesmo na Sociologia: 'a maior invenção
humana é o compromisso'. Só pode haver compromisso entre antinomias.
Mas sínteses são intelectuais demais para sua filosofia" E mais
adiante: "É claro, a tragédia vem daí, do fato de compromissos são
necessários. Esse é o trágico, que só possamos fazer compromissos que
são sempre provisórios." [@Landmann-Konturen, pp. 15-16,
"\foreignlanguage{german}{Aus der Diskussion}"]

[^landmann-refs]: Referências apontam para a presença da formulação em
@Landmann-Einleitung-DiG, que fora publicado em primeira edição em
1968, mas a que não tive acesso; ela reaparece mobilizada numa
resposta de Michael Landmann registrada na discussão publicada em
anexo a sua apresentação em @Landmann-Konturen, pp. 12-17,
"\foreignlanguage{german}{Aus der Diskussion}", especialmente na p.
13. É a esta versão --- que é muito breve --- que tive acesso.

[^sub-specie-aeternitatis]: @BoecklinsLandschaften, pp. 96-97;
consultei também a edição italiana, @BoecklinsLandschaften-it, p. 92.
No próprio Spinoza: "Tudo o que a mente compreende sob a perspectiva
da eternidade não o compreende por conceber a existência atual e
presente do corpo, mas por conceber a essência do corpo sob a
perspectiva da eternidade". [@Spinoza-Ethica-pt, l. V, proposição 29]
Simmel também usa desse expediente numa série de escritos publicados
anonimamente na revista *\foreignlanguage{german}{Jugend}* entre 1897
e 1907, alguns dos quais intitulados "Instantâneos *sub specie
aeternitatis* ("\foreignlanguage{german}{Momentbilder} *sub specie
aeternitatis*") [que se encontram em @Beitraege-aus-derJugend-GSG17; e
dos quais há tradução em espanhol, @Momentbilder-es]

[^totalidade-vida]: @PHG, p. 12. Mais à frente, Simmel retoma o
argumento do ciclo de retorno infinito ao precisar a maneira como
pretende construir um "andar inferior ao materialismo histórico":
"\[...\] em toda interpretação de uma formação ideal mediante uma
econômica deve encerrar-se na reivindicação de conceber esta, por sua
parte, de profundidades mais ideais, enquanto para estas, novamente, é
preciso encontrar as bases econômicas gerais, e assim por diante
ilimitadamente. Em tal alternação e entrelaçamento de princípios
conceitualmente opostos de conhecimento, a unidade das coisas, que
parece impalpável para nosso conhecimento e ainda assim estabelece a
sua coerência, torna-se prática e vital para nós." [@PHG, p. 13]

<!-- }}} -->
