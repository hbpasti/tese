
\hr

No plano ideal, eu queria que este estudo combinasse uma análise
"internalista" com uma "externalista", à maneira sugerida por um
@Bourdieu-Oeuvres-pt ou um @Candido-CriticaSociologia, isto é: que
respeitando o movimento interno do pensamento de Simmel pudesse
localizá-lo no contexto intelectual, cultural, político e econômico de
sua produção. A tarefa é excessivamente hercúlea, como demonstra sua
realização, nunca plena, na obra de alguns poucos mestres do
pensamento social. [Penso sobretudo nos já mencionados clássicos
@Candido-DialeticaDaMalandragem; e @Bourdieu-Heidegger-pt; mas também,
de maneira mais ou menos parcial, em @Hughes; @Ringer-Decline;
@Lepenies-DreiKulturen-en; @Kohnke-NeoKantianismus-en, entre outros.]
Mas posso talvez apontar para algumas trilhas que teria seguido.

Para o contexto cultural das Alemanhas guilhermina e weimariana, o
capítulo introdutório de @Bourdieu-Heidegger-pt é magistral, mas o
livro vale a pena como um todo. Outros aspectos desse contexto
encontra-se em Lepenies [@Lepenies-DreiKulturen-en; e
@Lepenies-seduction], que é de leitura muito mais agradável [Mas se
for pela leitura agradável, recomendo também passar por
@Zauberberg-pt; e @Musil-DerMannohneEigenschaften-pt]. Sobre o
contexto mais específico da obra de Simmel, a segunda parte de
@Waizbort é parada obrigatória, assim como a introdução, os prefácios
e o posfácio de David Frisby às várias edições da tradução inglesa da
*Filosofia do Dinheiro*
\nocite{PHG-en-intro,PHG-en-Preface2,PHG-en-Preface3,PHG-en-afterword}[Ver
"Introduction", "Preface to the Second Edition", "Preface to the Third
Edition" e "Afterword" em @PHG-en] e o capítulo introdutório de
@CantoMila-ASociologicalTheoryOfValue.  Aspectos mais específicos
encontram-se em @MarchandLindenfeld-GermanyAtTheFinDeSiecle, em
@Umbach-GermanCitiesAndBourgeoisModernism e em @Norton-SecretGermany.
Passei por @LowySayre, mas o argumento do romantismo revolucionário é
um pouco forçado demais para mim.

Com o contexto intelectual pode-se familiarizar com o grande clássico
de Fritz Ringer [@Ringer-Decline; quem preferir uma análise comparada,
consulte, também seu, @Ringer-FieldsOfKnowledge.] Ainda dele, há belas
introduções em seus dois livros sobre Weber.
[@Ringer-MaxWebersMethodology; e @Ringer-Weber-bio] Ainda sobre Max
Weber, há uma belíssima introdução de @GerthMills-IntroductionFMW.  Os
capítulo 13 e 16, pelo menos, de @Parsons-Structure continuam
relevantes, assim como o hoje clássico @Hughes. Sobre os neokantianos,
ver @Beiser-ContextProblematicPostKantian-CCP e novamente
@Kohnke-NeoKantianismus-en. As memórias (mas aqui já se tratam de
fontes) de @Honigsheim-OnMW e @MarianneWeber-Weber-es, assim como
algumas das colecionadas por @BDGS e a biografia de Stefan George por
Norton já mencionada ajudam muito.

O contexto político e econômico eu não saberia muito como trabalhar.
Com o clássico de @Wehler-DasDeutscherKaiserreich-en tive pouco
contato.  Para uma leitura com o pé atrás, o capítulo 1 de @Asalto
permanece relevante, embora de uma "análise frequentemente não muito
sutil", como sugere @Herf-ReactionaryModernism-pt, do qual o segundo
capítulo pode ser uma leitura também interessante. Os já mencionados
clássico de Ringer e Köhnke ajudam também nesse sentido, e são muito
mais recomendados.

Em suma, não realizo essa análise combinada de texto e contexto, mas
faço um pouco de cada coisa ao tentar situar o pensamento de Simmel,
cuja ambivalência não ajuda, num momento específico da história do
pensamento social alemão: a *Methodenstreit*. Assim, embora comece
tentando reconstruir muito parcialmente o ambiente desse debate,
termino por discutir a metodologia relativista de Simmel mais preso à
sua letra. <!--De certa maneira, evocando a operação relacionista de
relativização das oposições dicotômicas da metodologia que Simmel faz
e que acompanharemos, poder-se-ia dizer que não completo a tarefa de
conceber a oposição entre análise "interna" e "externa" de sua obra
como princípios regulativos e não constitutivos (ver
\autoref{relativismo}), mas a própria operação permanece como
princípio regulativo, inspirando o horizonte metodológico que não
cumpro (por isso o lamento). -->

---

Simmel era muito mais filósofo que sociólogo, e exortava seus alunos a
rejeitar o historicismo e a mergulhar na filosofia ignorando a sua
história, pois a seu ver,

> \[...\] nesse caminho, passa-se tão ao largo da compreensão
> essencial das filosofias como quando se compreende somente de
> maneira histórica Fídias e Michelangelo, Dante e Goethe; então eles
> se assemelham a um vaso fechado que passa de mão em mão sem mostrar
> seu conteúdo. Dessa forma, compreende-se de fato o vir-a-ser da
> coisa, mas não a própria coisa.  Isso embaralha o ponto de vista
> quando se considera cada filósofo somente em sua posição numa série
> histórica, isto é, numa luz exclusiva, por meio de um Antes-dele e
> um Após-ele.  No que diz respeito ao desenvolvimento de
> conhecimentos objetivos, isso pode ser válido; mas se se entende
> toda reivindicação sobre as coisas como a forma ou a vestimenta por
> trás da qual, como o essencial, como o que é efetivamente manifesto,
> reside a alma do filósofo realizando em si a imagem e o sentimento
> da existência --- então todo grande filósofo como todo grande
> artista é um começo e um fim, sejam seus meios técnicos,
> historicamente determinados, tão primitivos, como os de Heráclito e
> de Giotto, ou tão refinados como os de Schelling e de Whistler.
> Mesmo a conexão com a cultura <!--Kul-286-tur--> geral não é, na
> filosofia como na arte, tão completamente importante como em outros
> produtos espirituais porque repousa mais do que estes sobre a
> personalidade; o elemento da tradição, em oposição ao criativo,
> neles é *relativamente* menor, aquilo que, na pessoa, só é *formado*
> exclusivamente por influências históricas e sociais passa a ser
> determinado em seu estilo e sua expressão e aparece aqui como
> decisivo. [@GeschtPh 285-286]

Talvez, apoiando-me nisso (com o que, no entanto, jamais poderia
concordar plenamente), possa justificar o resultado, e dizer como
Natàlia Cantó-Milà, que

> Para além do ainda interessante e emocionante exercício de revisitar
> obras clássicas da perspectiva de uma história da sociologia, é
> cativante pensar além do ponto em que seus autores deixaram suas
> obras e continuar a desenvolver pedaços e fragmentos de teoria
> sociológica e trabalho sociológico nos ombros de gigantes magníficos
> como Georg Simmel. [@CantoMila-Gratitude 10]

