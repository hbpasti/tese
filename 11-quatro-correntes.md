## Quatro correntes {#quatro-correntes}


Quando da publicação da segunda edição de seu *As ciências sociais na
Alemanha: os métodos atuais*, em 1902, Célestin Bouglé (1870--1940)
reivindica a atualidade da obra recorrendo à controvérsia metodológica
entre os cientistas sociais alemães:

> Depois de seis anos, a "*\foreignlanguage{german}{Methodenstreit}*",
> cujas tendências tentou-se aqui precisar, não cessou e não parece
> mais perto de terminar. \[...\]
>
> Nesse sentido, pode parecer que não é inútil hoje --- a despeito do
> inegável progresso das ciências sociais --- relembrar por quais
> razões e em que medida elas devem, se querem impôr os materiais que
> demandam à história, se libertar provisoriamente de toda preocupação
> prática, especificar metodicamente seu objeto próprio, e se servir,
> sem portanto se confundir pura e simplesmente com ela, da
> psicologia.  [@Bougle-LesSciencesSocialesEnAllemagne-2ed, pp. i-ii]

O livro que Bouglé havia escrito seis anos antes, em 1896, se propunha
a analisar a metodologia de quatro autores de áreas ligadas às
ciências sociais: com Moritz Lazarus (1824--1903), a psicologia social
ou psicologia dos povos
(*\foreignlanguage{german}{Völkerpsychologie}*)[^volkerpsychologie-ref];
com Georg Simmel (1858--1918), a "ciência da moral" (sociologia); com
Adolph Wagner (1835--1917), a economia política; e com Rudolf von
Jhering (1818--1892), a filosofia do direito. A característica desses
métodos alemães à qual ele quer dar destaque é sua ênfase na
psicologia, que Bouglé vê como o caminho mais adequado para a nascente
sociologia.[^bougle-breton]

A fim de compreender o estado do debate metodológico das ciências
sociais alemãs naquele contexto de fundação da sociologia, Bouglé
sugere uma reconstrução da "história geral do método das ciências
sociais na Alemanha no século XIX"
[@Bougle-LesSciencesSocialesEnAllemagne, p. 2], que, a seu ver, pode
ser dividida em quatro fases que ele considerará dominadas, em
distintos momentos, pelas disposições especulativa, historicista,
naturalista e psicológica --- com a devida ressalva de que "com essas
quatro palavras, especulação, historicismo, naturalismo e psicologia,
pretendemos somente designar quatro pontos ideais em torno dos quais
oscilam, com lacunas mais ou menos amplas, os pensadores alemães."
[@Bougle-LesSciencesSocialesEnAllemagne, pp. 7-8] Não se trata de uma
sequência distinta de fases ou momentos, mas de tendências ou
disposições que se opõe, se combinam e se
complementam.[^bougle-ressalva]

A primeira, diz Bouglé, "corresponde à era heroica da filosofia
alemã: ela é especulativa". Entre seus principais representantes
--- Kant, Fichte, Schelling e Hegel ---, o elemento comum que Bouglé
encontra é "o desprezo pelos 'fatos'": "Eles constroem a Sociedade,
o Direito, o Estado, sem se preocupar em observar as sociedades, os
direitos, os Estados. O século XVIII, confundindo teoria e prática
sociais, deixou-lhes o exemplo e como que o hábito do a priori".
Herdeiros do direito natural, concebem "a sociedade como a obra das
liberdades individuais". [@Bougle-LesSciencesSocialesEnAllemagne,
p. 2] Àqueles que quisessem contrapor ao argumento de Bouglé que
Hegel se ocupou da história, ele se antecipa: "Hegel, a fim de
refutar a Kant, não apela a uma história mais informada, mas a uma
lógica mais abrangente. Da antítese do ser com o não-ser e de sua
síntese, ele deduz o devir e suas fases, mas vangloria-se de não
observá-los. Diz-se que ele se mostra cético a respeito dos resultados
da pesquisa histórica. Ele não tem confiança senão nas ideias."
[@Bougle-LesSciencesSocialesEnAllemagne, p. 3]

Mas a observação de fatos que se recusam a se deixar encaixar nos
movimentos prescritos pela lógica leva a uma reação que tende a
exagerar o desprezo pelas ideias. "Ao desprezo dos fatos sucede e
se opõe a desconfiança das ideias. Os jogos de ideias nos fazem
conhecer o individualismo do autor, mas nos escondem a realidade
histórica. Quer-se ser 'objetivo'; Ranke fará o voto de se despir de
seu eu para deixar falar as coisas." Mas também deste lado peca-se
por excesso: "Eles deleitar-se-ão no concreto como se deleitavam no
abstrato. As diferenças locais e temporais atrairão sua atenção.
Eles tenderão a distinguir como os especulativos a assimilar."
[@Bougle-LesSciencesSocialesEnAllemagne, p. 4]

Nesse movimento de reação à primeira fase especulativa, forma-se,
portanto, uma tendência que favorece os fatos em detrimento das
ideias: empiricistas convictos, os historicistas, no entanto, acabam
por recusar toda possibilidade de conquista de leis científicas. Tudo
o que é possível fazer é descrever fatos históricos particulares,
nenhuma generalização é aceitável. Essa escola historicista, a
despeito de seu início na jurisprudência, solidifica-se de maneira
particularmente acentuada na economia política, conquistando hegemonia
na disciplina na Alemanha.[^bougle:economistas-juristas]

Vemos aqui portanto não somente um ataque às doutrinas do direito
natural, mas um ataque a toda a tradição "especulativa": ao
racionalismo abstrato da geração do século XVIII segue-se um
empiricismo historicista que desconfia de toda generalização e limita
o escopo das ciências sociais à descrição histórica.

> Mas essa reação do fato contra a ideia teve seus excessos. O
> espírito se sentiu como que oprimido pela multidão de fatos que os
> historiadores lhe trouxeram. Desejava-se restabelecer a unidade
> nessa multiplicidade, e, depois de tê-la distinguido, assimilá-la
> novamente.  Lutando contra as brechas das concepções do século
> XVIII, juristas e economistas parecem abalar a própria ideia de lei
> científica; opondo o cosmopolita ao nacional, o perpétuo ao
> passageiro, parece-se renunciar à constituição das ciências sociais
> e se contentar com a história.  Mas o conhecimento do particular não
> pode nutrir por muito tempo as inteligências. As pessoas se cansam
> da "micrologia" como se cansam da ontologia. Demandam-se as leis
> para se encontrar no infinito dos fatos.
> [@Bougle-LesSciencesSocialesEnAllemagne, pp. 4-5]

Mas agora, destruído o encanto das teorias racionalistas e das
coleções de fatos, onde encontrar inspiração metodológica senão nas
ciências naturais? Eram as humanidades como até então praticadas que
se encontravam em crise epistemológica uma vez deslegitimados seus
pontos de apoio: a tradição e a autoridade, com o racionalismo, a
especulação com o historicismo, e, agora, com o positivismo [Bouglé,
esse "durkheimiano ambíguo" --- cf. @Vogt-Bougle --- não acusa aqui o
positivismo, mas fala em naturalismo], o empiricismo
historicista.[^bougle:ciencias-sociais-naturais]

Nesse movimento positivista que vai buscar inspiração nos métodos das
ciências da natureza parecem ter sido tomadas como modelo inicial as
ciências biológicas: surge aqui a infame metáfora organicista, com
Schäffle ou Lilienfeld e a tentativa de aproximar a estrutura da vida
social a um organismo. Esse "naturalismo", no entanto, não se difunde
com tamanha ênfase na Alemanha como parece ter feito na
França.[^durkheim-organicismo]

> \[...\] O método biológico, que causou tanto furor em sociologia,
> parece <!--128--> ser, na Alemanha, cada vez mais desacreditado.
> Lê-se Spencer, mas para refutá-lo. A enorme obra de Schäffle, na
> qual se compara a família à célula, os meios de comunicação aos
> tecidos intercelulares e todas as partes da sociedade aos membros do
> corpo animal, se impõe menos aqui do que entre nós. Não se teme, na
> intimidade, tratá-lo como um livro miserável. De uma maneira geral,
> o movimento atual das ciências sociais os empurra mais à psicologia
> do que à biologia: o princípio das formas e dos movimentos das
> sociedades, da estática como da dinâmica sociais, está na alma
> humana. É à psicologia que devemos demandar a explicação dos
> <!--129--> fenômenos sociais. Ela deve, dizem, numa fórmula doutro
> modo frutífera em equívocos, ser para as ciências históricas aquilo
> que a matemática é para as ciências da matéria. [@Bougle-Breton,
> pp. 127-129, da entrada de 18
> de março; essa recusa do
> "naturalismo" em Bouglé o coloca como crítico do próprio Durkheim,
> como argumenta @Policar-CritiqueSociologieBiologiqueBougle; cf.
> também @Bougle-LesSciencesSocialesEnAllemagne, pp. 144 ff.: "O sr.
> Durkheim, para constituir uma sociologia verdadeiramente científica,
> quer dotá-la de três qualidades que tendem, todas, a distanciá-lo da
> psicologia. A sociologia deve ser objetiva, específica e
> mecanista."]

Entre os alemães, o traço que esse naturalismo teria deixado é a noção
de que "se pode, para explicar os fenômenos sociais, analisá-los e
procurar as leis de seus elementos. Mas ele \[o pensamento alemão\]
reconhece que devolver sistematicamente a sociologia à biologia, isso
pode ser tomar por mote: *\foreignlanguage{latin}{obscurum per
obscurius}*". [@Bougle-LesSciencesSocialesEnAllemagne, p. 6]

Assim, contra o "monismo que reduz ativamente à unidade as forças da
natureza e aquelas do espírito \[que\] pode prejudicar a precisão
científica", a reação veio na forma da "ideia da especificidade
dos métodos, correspondente à diversidade dos objetos de estudo".
[@Bougle-LesSciencesSocialesEnAllemagne, p. 6] Tratar-se-ia, agora, de
demonstrar como os diferentes objetos de estudo requerem diferentes
métodos.

> O renascimento do kantianismo permite-lhes tomar posição tanto
> contra a escola naturalista quanto contra a escola dialética.
> \[...\] Quer-se com isso associar fenômenos sociais não a fenômenos
> biológicos, que não fornecem mais que analogias superficiais, mas a
> fenômenos psicológicos, que podem fornecer relações de causalidade.
> Ao mesmo tempo em que distinguirá os fatos sociais dos fatos
> naturais, a psicologia nos permitirá assimilar os fatos sociais uns
> aos <!--7--> outros, e redescobrir, sob a diversidade das economias
> nacionais, dos direitos particulares, alguns traços gerais. Dá-se,
> assim, algum valor, pelo menos metodológico, a certas teorias da
> escola especulativa, que são transpostas em termos psicológicos para
> serem aplicadas à historia. Pretende-se evitar assim tanto os erros
> do nacionalismo quanto os erros complementares do historicismo. Não
> haverá mais desprezo pelos fatos nem desconfiança das ideias, mas
> tentar-se-á unir uns aos outros para constituir verdadeiras ciências
> sociais. [@Bougle-LesSciencesSocialesEnAllemagne, pp. 6-7]

Neste ponto, a questão central dos debates metodológicos entre os
filósofos da ciência passa a ser a demarcação entre as ciências
naturais e as humanas. De diferentes perspectivas, autores vinculados
à recuperação do pensamento de Kant no final do século XIX se dedicam
a estabelecer as bases da diferenciação entre as ciências das coisas
naturais e suas leis e as ciências das coisas históricas, sociais,
culturais, espirituais ou, simplesmente, humanas, e sua
individualidade. Dessas várias tentativas de demarcação --- a
diferença entre as abordagens que saem em busca de leis, nomotéticas
(com Windelband) ou exatas (com Menger) e as que descrevem fenômenos
singulares, idiográficas (com Windelband) ou empírico-realistas (com
Menger) --- a mais influente foi provavelmente a de Dilthey, que
diferenciava entre as ciências naturais, cujo objeto é a natureza que
demanda uma abordagem que considera os processos observados como casos
de leis abstratas, e as ciências do espírito, cujo objeto é sempre
produto da atividade humana, que demandam uma abordagem que leve em
conta o seu contexto concreto (discutiremos isso mais detidamente na
\autoref{ciencias-humanas-naturais}).

<!--
  ---
  
  Na condição de representantes dos últimos avanços em metodologia das
  ciências sociais segundo Bouglé, assim, Lazarus, Simmel, Wagner e
  Jhering, "ao vincular as ciências sociais à psicologia, pretendem
  evitar de uma só vez os excessos do naturalismo, do historicismo e da
  especulação." [@Bougle-LesSciencesSocialesEnAllemagne, p. 7; é digno
  notar, antes de deixarmos a exposição de Bouglé, que a conclusão
  semelhante chegou também, anos depois, @Hughes, como quando afirma (p.
  66): "\[...\] as novas doutrinas eram manifestamente subjetivas.
  Processos psicológicos haviam substituído a realidade externa como o
  tópico mais urgente de investigação. Não era mais o que realmente
  existia o que parecia o mais importante: era o que as pessoas pensavam
  que existia.  E o que elas sentiam no nível inconsciente tornou-se
  ainda mais interesse do que elas racionalizavam conscientemente."]
-->

---

O relato de Bouglé nos ajuda aqui a situar um pouco o contexto
intelectual da controvérsia metodológica que agitou as ciências
sociais alemãs do final do século XIX. Gosto de sua análise do
contexto desses debates e das linhas históricas que levam a ele não
somente porque é um relato mais distante que os relatos dos alemães da
época, nem devido a seu relacionamento próximo com Simmel, mas porque
ele põe em evidência algumas das tendências que serão recuperadas
nesse contexto de crise intelectual que é a controvérsia metodológica.

Mas aquele contexto era certamente mais complexo do que uma sucessão
de reações filosóficas poderia explicar. Longe de se traçar numa
comunidade homogênea de pensadores em diálogo, a controvérsia
metodológica atravessa uma série de espaços mais ou menos
heterogêneos, pondo em disputa duas linhagens de análise que se
poderia denominar, com @Ringer-MaxWebersMethodology (seguindo a
distinção proposta por Dilthey), como as abordagens "interpretativa" e
a "explicativa" ou, nas expressões dos economistas contendentes, as
orientações "exata" e a "realista-empírica" com @Menger-Untersuchungen
ou ainda os métodos dedutivo e indutivo com @Schmoller-Methodologie.

Muitas questões estavam envoltas nessas disputas. O progresso
intensivo que as ciências naturais realizaram ao longo dos séculos
XVIII e XIX, acompanhada da crise filosófica do período que segue às
revoluções de março de 1848, somados à dinâmica da abertura de
cátedras em humanidades [Esse percurso é objeto tanto dos estudos de
@Hughes; quanto de @Ringer-Decline; quem melhor chama atenção para a
dinâmica das aberturas de cátedras, a meu ver, é
@Kohnke-NeoKantianismus-en] punha em questão a epistemologia das
ciências sociais que se encontravam na aurora de sua diferenciação.
Com o surgimento de novos objetos e abordagens, novos campos se
desenhavam: antropologia e etnografia, psicologias de diversas
abordagens, novos horizontes historiográficos, e, claro, a sociologia.
Com o avanço do positivismo nas ciências naturais, e a crise da
filosofia e, por tabela, das humanidades, os pensadores alemães viam
as tradições filosóficas que marcavam sua herança intelectual
ameaçadas. As novas disciplinas que emergiam encontravam-se envoltas
num conflito de classificação e demarcação que dizia respeito a sua
legitimação no campo científico; o debate epistemológico que o avanço
do positivismo e outras disposições legíferas (organicismo e
materialismo histórico) provocava as fragilizava ainda mais.


<!-- NOTAS {{{ -->

[^durkheim-organicismo]: O jovem Durkheim, num escrito muito parecido
com este de Bouglé (também um relato das teorias alemãs, feito por um
estudante francês em intercâmbio --- e ao qual Bouglé remete) recorre
a Schäffle para fundamentar uma ciência da moral contra os economistas
--- o organicismo de Schäffle parece-lhe superar uma leitura
mecanicista da sociedade em Wagner e Schmoller ---, mas o abandona ao
recorrer à psicologia social de Wundt
[@Durkheim-SciencePositiveDeLaMoraleEnAllemagne, pp. 45 ff.; na
tradução brasileira @Durkheim-SciencePositiveDeLaMoraleEnAllemagne-pt,
pp. 43 ff.].

[^volkerpsychologie-ref]: Vale dizer que é preciso diferenciar esta
primeira geração de fundadores da
*\foreignlanguage{german}{Völkerpsychologie}* --- Lazarus e Steinthal,
que antecipariam a antropologia cultural
[@Kalmar-VolkerpsychologieConceptCulture] ---, com a de Wilhelm Wundt,
que se aproxima muito mais da psicofísica de Fechner (ver, à frente,
\autoref{circulo-leipzig}).

[^bougle:economistas-juristas]: "Os economistas, como os juristas,
refutarão as teorias do século XVIII. Knies, desenvolvendo
sistematicamente as ideias lançadas por List, acusará tais teorias de
aspirar a um valor universal, de serem 'perpetualistas' e
'cosmopolitas'; Roscher, discípulo do 'polihistorismo' de Tübingen,
reforçará as teses da *\foreignlanguage{german}{nationalökonomie}*
\[economia política\] com uma massa de fatos emprestados de todas as
histórias. E a medida que seus discípulos, compondo aquilo que Wagner
chama de 'jovem escola histórica' dirigida por Schmoller, contribuem
documentos muito numerosos e muito precisos, vê-se se afastar e se
apagar a ideia de uma ciência universal e abstrata dos fenômenos
econômicos." [@Bougle-LesSciencesSocialesEnAllemagne, p. 4]

[^bougle:ciencias-sociais-naturais]: "Para descobrir essas leis, as
ciências sociais devem se dirigir de imediato às ciências mais
florescentes de nosso século, e tentar emprestar seus métodos. \[...\]
Assim, as ciências sociais depois de ter sofrido as influências
alternadas das matemáticas, da astronomia, da própria química, devem
se modelar sobre a pressão das ciências naturais."
[@Bougle-LesSciencesSocialesEnAllemagne, p. 5] Bouglé aqui remete a
*Introdução às ciências do espírito* de Dilthey.  Vale dizer que
Bouglé demonstra ter profundo desprezo pela "psicofísica", a tendência
quase proto-behaviorista de buscar explicações comportamentais de
estímulos físicos [como se pode observar a entrada de 8 de março em
@Bougle-Breton, pp. 99-100].

[^bougle-ressalva]: "Não reivindicamos que, na realidade, períodos
claramente distintos, que se seguem sem se confundir, correspondem a
essas quatro tendências. Advertimos de início que nenhum deles, ao
aparecer, fez desaparecer os outros. \[...\] Além disso, no espírito
daqueles mesmos que tomamos como representantes de uma dessas
tendências, pode-se encontrar traços de outras."
[@Bougle-LesSciencesSocialesEnAllemagne, pp. 7-8]

[^bougle-breton]: "É à psicologia que devemos demandar a explicação
dos fenômenos sociais. Ela deve, dizem, numa fórmula doutro modo
frutífera em equívocos, ser para as ciências históricas aquilo que a
matemática é para as ciências da matéria." A passagem é de um diário
da viagem pela Alemanha que resultou em *As ciências sociais na
Alemanha*, publicado como livro por Bouglé sob pseudônimo,
@Bougle-Breton, p. 129, da entrada de 18 de março em pp. 127-129.

<!-- }}} -->
