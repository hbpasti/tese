# O relativismo de Georg Simmel {#relativismo}

\epigraph{Com o ceticismo, que agora parece a muitos como o liberador
e guia da vida, ocorre o mesmo que com balas mágicas. Nove vezes
podemos encontrar com ele o que queremos, na décima, aponta o diabo
para onde \emph{ele} quer, frequentemente o coração do mais
amado.}{Simmel, assinando \textcite{Jugend-ZehnEinzelheiten}}

<!--
Os autores que se tornaram clássicos da sociologia --- desde os mais
assiduamente estudados Marx, Durkheim e Weber aos menos revisitados
Simmel e Tönnies --- não parecem se deixar levar por essas disputas
dicotômicas, caminhando por trilhas intermediárias que evitam a
polarização das posições mais radicais.

MARX

O comprometimento de Marx com a história evita que seus conceitos
extrapolem em robinsonadas abstratas na mesma medida em que sua
dialética permite-lhe extrapolar o limite da descrição histórica e
construir conceitos gerais.

DURKHEIM: é mais difícil porque ele não se envolve com a polarização
da methodenstreit

A reificação metodológica dos fatos sociais em Durkheim permite-lhe
desvencilhar-se dos pressupostos morais de sua situação e procurar
pelas determinações coercitivas dos fenômenos, o que ele faz partindo
de uma teoria abstrata --- do postulado do caráter coercitivo dos
fenômenos sociais --- e que ele mobiliza na descrição de fenômenos
concretos; ao mesmo tempo, indo além do positivismo rasteiro que o
antecede, ele sai à cata da gênese de fenômenos sociais para
compreender os princípios fundamentais de suas instâncias
"contemporâneas".

WEBER

A maneira como Weber recusa o positivismo sem negar a orientação
nomotética das ciências sociais, buscando resgatar a dignidade da
construção de conceitos numa disciplina necessariamente calcada em
contextos históricos tornou-se, num certo sentido, paradigmática. Com
a noção de tipo ideal, um conceito abstrato, construído mediante a
seleção dos aspectos mais relevantes do fato observado, e a atribuição
de centralidade à busca de compreender o sentido da ação social, ele
rejeita ao mesmo tempo tanto a orientação puramente "exata" quanto à
descrição exaustiva, incapaz de conceituar a realidade descrita, para
as ciências sociais.

Simmel não é exceção, embora suas observações sejam menos explícitas
que as de Weber.

Incluir breve apresentação do percurso e do capítulo
-->
