<!-- ## Metodologia relativista {#metodologia-relativista} -->


Aqui, acompanho o argumento que Simmel apresentou num ensaio de 1896,
"Para a metodologia das ciências sociais" [@Mthdk-orig; reimpresso em
@Mthdk, e do qual segue uma tradução em \autoref{methodik}] a respeito
dessa possibilidade de superação do impasse da controvérsia
metodológica, enquanto continuo de olho em outros escritos da década
de 1890 [^refs-escritos-1890]. Embora a
*\foreignlanguage{german}{Methodenstreit}* em si só seja
mencionada nesse ensaio e num trecho da *Filosofia do
dinheiro*[^methodenstreit-phg], pode-se considerar que boa parte de
seus escritos da época toquem no tema, embora de perspectiva bastante
distinta daquela dos contendentes --- Simmel aborda a questão de uma
perspectiva kantiana, se opõe resolutamente ao realismo histórico
rankeano e ao historicismo e trata mais de epistemologia que de
metodologia. Esse ensaio/resenha é interessante, de meu ponto de
vista, porque a sua brevidade e efemeridade obrigam Simmel a se
posicionar com mais clareza do que noutras obras (onde a questão
aparece tematizada de maneira mais marginal), e porque aqui Simmel se
concentra mais no debate metodológico.

<!-- NOTAS {{{ -->

[^refs-escritos-1890]: Particularmente em seu *Problemas da filosofia
da história* [@PG-1892-orig; reimpresso em @PG-1892; com uma segunda
edição em @PG-1905-orig; e uma terceira de @PG-1907-orig; reimpressa
em @PG-1907; da qual consultei as traduções em espanhol, @PG-1907-es;
e francês, @PG-1907-fr], em especial seu segundo capítulo, e sua
*Filosofia do dinheiro* [@PHG-orig-1e; com uma segunda edição em
@PHG-orig-2e; reimpressa em @PHG; da qual consultei a tradução
inglesa, @PHG-en; espanhola, @PHG-es; e francesa, @PHG-fr], em
especial o primeiro capítulo.

[^methodenstreit-phg]: Simmel repete parte do argumento dessa resenha
na terceira seção do primeiro capítulo da *Filosofia do dinheiro*,
como ao afirmar: "A solidariedade substantiva
\[\foreignlanguage{german}{inhaltliche Zusammengehörigkeit}\] de
conceitos e de elementos subjacentes das imagens de mundo
frequentemente se apresenta como um tal ritmo temporal e mútuo de
alternar-se \[em que 'a relatividade, isto é, a reciprocidade em que
as normas do conhecimento atribuem-se significado, aparecem separadas
como definitivas nas formas da sucessão, da alternação'\]. Pode-se
assim conceber a relação, no interior da ciência econômica, <!--112-->
entre os métodos histórico e o que se dirige a leis gerais. É certo
que todo processo científico só se deriva compreensivelmente de uma
constelação histórico-psicológica específica. Mas tal derivação ocorre
sempre sob o pressuposto de relações definidas, regulares; se não
puséssemos como fundamento, acima do caso individual, relações gerais,
impulsos consistentes, séries causais regulares, então não poderia
haver qualquer divisão histórica, ou melhor, o todo se desintegraria
num caos de ocorrências atomizadas. Agora, pode-se admitir, porém, que
aquelas regularidades universais, que possibilitam a vinculação entre
o estado ou evento existente e suas condições, também por sua vez
dependem de leis mais elevadas, de tal modo que elas mesmas só possam
valer como combinações históricas; outros eventos e forças
temporalmente passados trouxeram as coisas em e para nós em formas,
que agora aparecem como válidas em geral e suprahistoricamente, que
formam os elementos casuais do tempo posterior em suas manifestações
específicas. Assim, enquanto estes dois métodos, postos dogmaticamente
e reivindicando cada um para si a verdade objetiva, entram num
conflito irreconciliável e numa negação mútua, eles são tornados
possíveis na forma da alternação de uma mutualidade
\[\foreignlanguage{german}{Ineinander}\] orgânica: cada um é
transformado num princípio heurístico, isto é, de cada um se exige que
a cada ponto de sua aplicação específica busque sua fundamentação em
instâncias mais elevadas no outro. Não é diferente com a antítese mais
universal no interior de nosso pensamento: aquela entre a priori e
experiência. Que toda experiência, à exceção de seus elementos
sensíveis, deve se apresentar em certas formas que habitam a alma e
através das quais todo dado é formado em conhecimento --- disso
sabemos desde Kant. Esse a priori como que trazido por nós deve
portanto valer absolutamente para todo conhecimento possível e é
privado de toda mudança e toda corrigibilidade da experiência,
enquanto sensível e casual. Mas a certeza de que devem haver normas
desse tipo não corresponde na mesma medida na de quais sejam elas.
Muito do que numa época foi tido por a priori <!---113--> foi mais
tarde sabido como formações empíricas e históricas. Se portanto, por
um lado, existe a tarefa de buscar as normas duradouras de todo
fenômeno existente em si, para além de seu conteúdo sensível dado
mediante o qual elas são formadas --- ao lado disso, existe a máxima:
buscar a recondução genética à experiência de cada a priori singular
(mas, portanto, não do a priori em geral!)." [@PHG, pp. 111-113]

<!-- }}} -->
