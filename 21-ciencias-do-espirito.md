## Ciências humanas e naturais {#ciencias-humanas-naturais}


### O positivismo subterrâneo {#positivismo-subterraneo}

Como vimos, o positivismo enfrentou na Alemanha um ambiente hostil
que, apoiado numa tradição idealista oriunda do romantismo, se
rebelava contra a reintrodução de princípios "mecanicistas" na
explicação da ação humana.

Ao longo do tempo, no entanto, também ali o positivismo começou a ter
alguma difusão. Ainda que poucos fossem os seus abertos defensores, o
fato é que seus pressupostos e visão de mundo aos poucos passaram a
ser aceitos.

> Em torno dos anos 1880, a situação intelectual na Alemanha era mais
> ou menos a seguinte: o positivismo, como vimos, havia feito poucos
> convertidos declarados. Mas, num sentido mais difuso, a mentalidade
> positivista havia avançado enormemente. <!--189--> O grande período
> da filosofia alemã estava terminado: para os cientistas naturais é
> que estava se acumulando o prestígio dos símbolos culturais que
> anteriormente havia sido desfrutado pelos filósofos.
> Semelhantemente, a escrita da história aparentemente havia visto
> seus melhores dias: Ranke havia sobrevivido a sua própria época por
> tempo suficiente e seus pupilos e os pupilos de seus pupilos haviam
> mantido somente uma parte de sua herança; o aspecto espiritual de
> seus ensinamentos haviam sido largamente esquecidos, e não mais que
> sua metodologia, com sua ênfase em cuidado meticuloso e
> *\foreignlanguage{german}{Sitzfleisch}* \[uma expressão que se
> refere à habilidade de conquistar algo mediante um paciente
> exercício de obstinada sedentariedade, algo como "sentar a bunda na
> cadeira"\], é que ainda era tido com honra. Na verdade, num sentido
> pedestre e não filosófico, um grande número de rankeanos dos últimos
> dias comportavam-se bastante como positivistas. [@Hughes,
> pp. 188-189]

De certa maneira, poder-se-ia considerar toda a
*\foreignlanguage{german}{Methodenstreit}*, em suas várias
encarnações, como uma resposta ao positivismo. Se a indisposição
prévia do pensamento alemão com ele já se manifestara anteriormente,
ainda que não o tratando conforme sua definição própria, a sua
resiliência se mostrara mais forte do que a da reação difusa das
tradições intelectuais "mandarins" e ele encontrara, mesmo onde não se
esperava, seus espaços de inserção.[^invasao-subterranea]

Diante de um avanço da agenda positivista nas ciências sociais, uma
resposta se fazia urgente. Não se tratava mais de defender a noção
alemã de ciência (sobre a definição da noção, ver
\autoref{wissenschaft-bildung}) como um todo dos ataques do
materialismo estrangeiro, com sua mania de especialização e
fragmentação disciplinar; diante de sérios desenvolvimentos nas
disciplinas singulares das ciências da natureza no século XIX, onde o
positivismo passou a reinar solto mesmo no campo acadêmico alemão, as
humanidades tornaram-se o último espaço de resistência ao seu
progresso desenfreado --- e mesmo elas, particularmente as disciplinas
mais jovens, já se viam embebidas do espírito metodológico
positivista.

Essa ascensão do positivismo tornou central a questão da distinção
entre as ciências da natureza e da cultura. A essa questão de
demarcação dedicaram-se os epistemólogos neokantianos a partir dos
anos 1880.

Seguindo o argumento de @Hughes, corremos aqui o risco de uma
simplificação excessiva, por isso vale a advertência: há certamente
uma boa dose de exagero na interpretação de que a guinada
irracionalista do pensamento alemão do final do século XIX possa ser
atribuída à controvérsia metodológica, assim como na amplificação dos
discursos de demarcação das ciências humanas e naturais suscitados
pela controvérsia --- a diferenciação remonta, pelo menos, às origens
da tradição histórica, quando o movimento não era uma reação ao
positivismo, mas ao espírito jusnaturalista do Iluminismo.

> A famosa distinção entre os métodos aplicáveis às ciências culturais
> e aqueles das ciência naturais não surge repentinamente com Dilthey
> e Windelband, mas remonta a uma linha de pensamento que teve suas
> origens na revolta da escola histórica alemã contra a tradição do
> direito natural. Talvez muito mais importante do que o
> desencantamento com a religião e a metafísica entre autores
> positivistas, no curso do século XIX foi a insistência de autores
> alemães historicamente orientados em abordar ideias e valores não em
> termos de normas absolutas de verdade ou bem, mas como expressões de
> uma era, cultura ou povo específico.
> [@Iggers-GermanConceptioOfHistory, pp. 126-127]


### Coisas naturais e humanas {#dilthey}

A "premissa idealista de que a realidade definitiva do universo
repousa no 'espírito' ou 'ideia' ao invés de nos dados da percepção
sensível" era o que separava a tradição do pensamento alemão do restante
do pensamento europeu, em que "o primado da percepção sensível e a
validade dos procedimentos empíricos eram assumidos como certos como a
supremacia da 'ideia' era aceita na Alemanha." [@Hughes, pp. 183-184]
Essa premissa é que era mobilizada na tentativa de frear o avanço do
positivismo e seus "pares lógicos" (utilitarismo, democracia, livre
mercado), passando a ser empregada na diferenciação entre as
atividades das ciências da natureza e da cultura.

A separação entre os universos da natureza e da atividade humana
promovida por tal visão ecoava uma distinção kantiana entre os
diferentes modos de apreensão do ser e do dever-ser. Dessa separação,
radicalizada com a tradição idealista, estabeleceu-se um princípio de
separação das ciências que lidavam com a natureza e aquelas que
lidavam com o resultado da atividade humana.

> O princípio básico do pensamento social idealista pode ser
> rapidamente resumido. Entre os mundos fenomênico e espiritual, entre
> o mundo da ciência natural e o mundo da atividade humana, se
> presumia existir uma clivagem radical. Assim, os alemães passaram a
> estabelecer uma distinção aguda entre
> *\foreignlanguage{german}{Naturwissenschaft}*, ou
> ciência natural, e as
> *\foreignlanguage{german}{Geisteswissenschaften}*, as "ciências
> culturais" ou "ciências do espírito" --- incluindo tanto o que
> chamaríamos de humanidades quanto o que chamaríamos de história ou
> ciência social. As ciências culturais não poderiam, então, pegar a
> deixa da ciência natural, como os positivistas defendiam. Na
> verdade, elas não poderiam procurar por "leis" absolutamente.
> [@Hughes, p. 186]

Em sua *Introdução às ciências do espírito*, de 1883, Wilhelm Dilthey
(1833--1911) propõe tal separação entre as ciências da natureza e o
que ele denomina as "ciências do espírito",
*\foreignlanguage{german}{Geisteswissenchaften}*,
baseada numa distinção ontológica entre os seus objetos.

Às ciências da natureza cabe um método explicativo orientado à
construção de leis porque seus objetos são naturais, inacessíveis
senão pelas impressões sensíveis e sua interpretação conforme os
esquemas da percepção e do entendimento. Às do espírito, cabe um
método interpretativo porque seus objetos são resultado da atividade
humana, objetivações do espírito, e assim passíveis de uma
interpretação imediata, mediante o exercício de uma espécie de
intuição empática. O sujeito que conhece, em cada caso, vê sua
condição humana determinando sua relação com os objetos do
conhecimento: os objetos naturais, dados, não se permitem conhecer da
mesma maneira que aqueles que representam realizações propositivas do
espírito humano, que precisamente porque são objetificações da vontade
e intenção humanas, podem ser compreendidos de maneira mais completa.
[@Bransen-VerstehenErklaren-IESBS]

É, portanto, a partir de uma diferença dada, essencial, entre os
objetos dos diferentes grupos de ciências que Dilthey parte em sua
distinção:

> Conhecemos os objectos naturais a partir do exterior mediante os
> nossos sentidos. Por muito que os analisemos ou os dividamos, não
> chegamos às suas últimas componentes. Sobrepensamos tais elementos,
> graças a um perfazimento da experiência. Os nossos sentidos,
> considerados na sua pura actividade fisiológica, também nunca nos
> podem proporcionar a unidade dos objectos. Esta é-nos dada,
> igualmente, só em virtude de uma síntese das percepções sensíveis
> que <!--46--> dimana de dentro. \[...\] Seja, pois, qual for o modo
> de se conceber a origem das representações dos objectos e das suas
> relações causais, o certo é que nas impressões sensíveis, na sua
> coexistência e sucessão, nada se contém da conexão implicada pelos
> objectos e pelas suas relações causais.
> [@Dilthey-IdeenPsychologie-pt, pp. 45-46]

Os fenômenos que se tornam objeto de uma ciência
natural são fenômenos externos à consciência que os capta e busca
compreendê-los, que é a única responsável por sua objetivação,
separando-os, classificando-os e atribuindo-lhes lugar entre os demais
fenômenos em sua "coexistência e sucessão". Porque não podemos
compreendê-los em sua totalidade, tais objetos exigem uma ação
consciente que estabelece as causalidades necessárias para sua
explicação, estabelecendo "hipóteses". "Como nos sentidos somente é
dada a coexistência e a sucessão, sem o nexo causal daquilo que se
apresenta simultânea ou sucessivamente, o vínculo causal surge na
nossa apreensão da natureza só graças a uma acção que a completa."
[@Dilthey-IdeenPsychologie-pt, p. 11]

Os fenômenos que são objeto das ciências do espírito, por sua vez, são
obras humanas, realizações que incorporam propósito e vontade. Dessa
maneira, são imediatamente acessíveis, de maneira empática, pelo
sujeito que conhece, na medida em que este poderia conceber como suas
as intenções nelas incorporadas, sendo assim capaz de atribuir-lhes o
sentido adequado.

Essa distinção implica em atividades distintas na concepção de cada
tipo de objeto: os objetos naturais exigem uma explicação governada
por leis, os espirituais uma compreensão empática.


### Compreender e explicar {#verstehen}

> \[...\] para o pensamento idealista alemão como um todo, o problema
> de como seria possível chegar a uma compreensão do comportamento
> humano (i. é, espiritual) permanecia peculiarmente incômoda. Uma
> determinação de tipo positivista das "causas" de um ação claramente
> não bastaria. Um procedimento mais flexível, livre de manchas
> mecanicistas ou naturalistas, era urgentemente requerido. O
> resultado foi uma elaboração do método da compreensão interna ou
> *\foreignlanguage{german}{Verstehen}*. [@Hughes, p. 187]

Dilthey diferencia assim dois métodos distintos, dos quais um é
predominante nas ciências naturais e o outro a direção que pretende
influenciar as ciências humanas a abraçar: são os métodos da
compreensão interpretativa (*\foreignlanguage{german}{verstehen}*) e o
da explicação nomológica (*\foreignlanguage{german}{erklären}*). Este
último, o método predominante nas ciências naturais (e a bandeira
metodológica do positivismo), busca estabelecer as leis que governam
os fenômenos que procura conhecer, enquanto o primeiro, projetado por
Dilthey como a metodologia mais adequada para as ciências do espírito,
"tenta dar sentido empático aos fenômenos procurando pela perspectiva
da qual os fenômenos parecem ser significativos e adequados."
[@Bransen-VerstehenErklaren-IESBS, p. 16165]

Essa compreensão, enquanto busca de atribuição empática de sentido aos
fenômenos (sempre, por definição, resultados da ação de uma vontade
inteligente), não por acaso, soa bastante parecida com aquela
faculdade especial intuitiva que Ranke exigia dos historiadores: como
Ranke, Dilthey é profundamente influenciado pelos românticos, e é da
hermenêutica --- prática da interpretação de textos que surge como
interpretação bíblica --- meio secularizada de Schleiermacher
(1768--1834) que Dilthey tira inspiração para a metodologia
compreensiva.

> Interpretar é obter a compreensão do outro graças à "revivência"
> (*\foreignlanguage{german}{Nacherlebnis}*) da experiência alheia,
> isto é, através de uma "transposição" empática ou da captagem do
> sentido das expressões corporificadas nas obras. Com que
> pressupostos se dá esse salto empático? Não por introspecção, como
> se fosse possível adentrar-se e imergir na subjetividade de outrem.
> A autocompreensão em face das realizações culturais assenta na
> revelação das semelhanças e diferenças, das variações e
> particularidades, que assomam não só na reflexão, mas também na
> interação social, e a partir das quais se aprende a empatia e se
> desenvolve a imaginação.
> [@Morao-Apresentacao-Dilthey-IdeenPsychologie, p. 5]

A proposta de usar a metodologia interpretativa da exegese bíblica na
compreensão da ação humana e seus resultados --- objetivações do
espírito humano --- advém da maneira como Dilthey concebe a relação
das ciências específicas com seus objetos e do lugar que essa
concepção atribui às ciências humanas: em Dilthey trata-se, como vimos
acima, de uma separação radical entre dois tipos de objetos das
ciências empíricas, fenômenos naturais e espirituais. Segundo esse
pressuposto, este último grupo de objetos poderia ser conhecimento de
maneira imediata.

> Seja, pois, qual for o modo de se conceber a origem das
> representações dos objetos e das suas relações causais, o certo é
> que nas impressões sensíveis, na sua coexistência e sucessão, nada
> se contém da conexão implicada pelos objetos e pelas suas relações
> causais. A vida anímica é-nos dada de modo muito diferente. _Em
> contraste com a percepção externa, a percepção interna assenta num
> apercebimento íntimo, numa vivência: é-nos imediatamente dada_. Na
> sensação ou no sentimento de prazer que a acompanha dá-se-nos algo
> simples e indivisível. Seja qual for o modo como se tiver originado
> a sensação de uma cor violeta, olhada como fenômeno interno é algo
> indivisível. Se levarmos a cabo um ato mental, veremos que nele
> conflui, na unidade indivisível de uma função, uma pluralidade
> discriminável de factos internos, pelo que encontramos na
> experiência interna algo novo que não tem analogia alguma na
> natureza. [@Dilthey-IdeenPsychologie-pt, p. 46, grifo
> meu]

Aqui, Dilthey discorre a respeito da psicologia, mas a mesma lógica se
aplica a todas as ciências humanas: trata-se sempre de um objeto
compreensível empaticamente porque, enquanto realização espiritual
dotada de sentido, imediatamente concebível na vivência interior do
sujeito que conhece. Sendo assim, não se trata nas ciências humanas de
buscar leis: essas ciências encontram-se, ao invés de numa maior
dificuldade, numa relação muito mais direta com seus objetos, que não
carecem de serem construídos, do que as ciências
naturais.[^dilthey-metodo]

Assim, é a apreensão dos processos internos mediante a sua vivência
quem garante um acesso imediato à "conexão", àquela unicidade que
encerra em si uma totalidade, dos fenômenos psíquicos; e a analogia
que atribui às ações humanas e seus resultados o caráter de espírito
objetivado com a estrutura do "espírito", aqui no claro sentido de
"alma", é quem garante a todos os objetos das ciências humanas um
semelhante acesso imediato à consciência mediante a compreensão
interpretativa.

> \[...\] Para as *\foreignlanguage{german}{Geisteswissenschaften}*
> terem sucesso, Dilthey argumentava, elas não deveriam procurar
> explicações em termos de leis abrangentes, mas deveriam
> concentrar-se na tentativa de dar sentido empático a fenômenos
> mentais por meio de *\foreignlanguage{german}{verstehen}*. Afinal, a
> única maneira de adquirir conhecimento cientificamente respeitável
> de um fenômenos é ganhar discernimento compreensivo sobre o que é de
> importância crucial para a essência do fenômeno; no caso de
> fenômenos mentais, isso significa compreender seu significado.
> [@Bransen-VerstehenErklaren-IESBS, pp. 16165-16166]

A diferença entre *\foreignlanguage{german}{verstehen}* e
*\foreignlanguage{german}{erklären}*, compreender e explicar, proposta
por Dilthey, corresponde, num certo sentido, aquela de que fala, ao
discutir a metodologia de Max Weber, @Parsons-Structure: "Em alemão,
*\foreignlanguage{german}{verstehen}* \[compreensão interpretativa\]
veio a ser aplicada à situação e que uma referência motivacional
subjetiva ou simbólica está envolvida, enquanto
*\foreignlanguage{german}{begreifen}* \[conceituação\] é empregada
para a compreensão 'externa' de uniformidades onde nenhuma de tais
evidências adicionais estão disponíveis." [@Parsons-Structure, p. 584,
nota 2] Weber não opõe as metodologias ou disposições gerais da
controvérsia metodológica, mas enfatiza o papel absolutamente crucial
--- e o delírio absurdo de procurar esvaziar as ciências sociais dele
--- do conhecimento "conceitual" ou nomológico. [@Weber-Objektivitat]


### História e ciência natural {#windelband}

Como vimos, talvez como resposta ao avanço do positivismo nas ciências
humanas (particularmente na psicologia), Dilthey propôs uma "distinção
essencial ôntica entre natureza e história" [@Kruse-Paradigmenwechsel,
p. 151]. Sua distinção é substancialista porque deduz o método mais
adequado para cada classe de objetos a partir do que caracterizariam
estes últimos em sua essência, ou seja, seria a natureza de cada tipo
de objeto quem determina a relação do conhecimento com esse objeto e
demanda um tipo específico de método. Essa definição dá explicação à
separação entre ciências naturais e culturais e funciona para
preservar as últimas do avanço do naturalismo positivista ao
atribuir-lhes um acesso especial e imediato a seus objetos, a vivência
ou *\foreignlanguage{german}{erleben}*, e um método específico, o da
compreensão interpretativa ou *\foreignlanguage{german}{verstehen}*.

A essa concepção ontológica da distinção entre os métodos explicativo
e interpretativo, o filósofo neokantiano Wilhelm Windelband
(1848--1915) procurou responder com uma separação puramente
metodológica entre dois métodos: aqueles que buscam o estabelecimento
de leis e aqueles que buscam a descrição de fenômenos singulares.

Em seu discurso de posse como reitor da Universidade de Estrasburgo em
1894 [@Windelband-GuNw-orig; reimpresso em @Windelband-GuNw; consultei
a tradução inglesa, @Windelband-GuNw-en], Windelband procura superar a
dicotomia "substantiva" entre natureza e espírito que fundamenta a
distinção entre ciências naturais e do espírito, posto que, a seu ver,
"essa oposição dos objetos não coincide com uma tal dos modos de
cognição" [@Windelband-GuNw-orig, p. 9<!--; reimpresso em
@Windelband-GuNw, p. 142; @Windelband-GuNw-en, p. 173-->].

Windelband parte, a título de exemplo, do lugar da psicologia em tal
classificação substancialista das ciências: "segundo seu objeto, ela é
apenas ciência do espírito e, em certo sentido, deve ser caracterizada
como o fundamento de todas as demais; mas segundo seu método, seu
comportamento metodológico é de início a fim aquele das ciências
naturais." [@Windelband-GuNw-orig, pp. 9-10<!--; @Windelband-GuNw, p.
143; @Windelband-GuNw-en, p. 174-->] Não existe, assim, qualquer
"fundamentação sistemática" para a coincidência entre objeto e método.

Por tal fundamentação sistemática, Windelband compreende uma
fundamentação puramente metodológica e não substantiva: assim,
trata-se de classificar os diferentes *modos* de investigação e suas
propriedades específicas e não os objetos próprios, que podem ser
compartilhados, das diferentes áreas do saber.

A psicologia experimental, que se desenvolvia intensamente na Alemanha
[A disciplina das humanidades mais próxima do positivismo, na Alemanha
a psicologia experimental viu intenso progresso no chamado "círculo de
Leipzig" (ver \autoref{circulo-leipzig}), desenvolvendo-se em torno da
investigação da influência dos sentidos e do meio nos fenômenos
psicolóigicos (o que desembocou no que se costumava chamar
"psicofisica"). Sobre a psicologia da época, ver
@Dilthey-IdeenPsychologie-orig; do qual consta uma tradução portuguesa
em @Dilthey-IdeenPsychologie-pt;
@Windelband-PositivismusHistorismusPsychologismus; e para uma
interpretação mais atual,
@Smith-PoliticsAndTheSciencesOfCultureInGermany, cap. 11], compartilha
com as ciências naturais um conjunto de procedimentos: ambas buscam
"estabelecer, coletar e processar fatos sob o ponto de vista e com o
fim de compreender daí a regularidade geral a que esses fatos estão
submetidos." [@Windelband-GuNw-orig, p. 10<!--; @Windelband-GuNw, p.
143; @Windelband-GuNw-en, p. 174-->] A maioria das demais ciências do
espírito tem um objetivo distinto: elas "se dirigem decididamente a
trazer uma descrição completa e exaustiva de um evento singular, mais
ou menos extensivo, de uma única realidade temporalmente limitada."
[@Windelband-GuNw-orig, pp. 10-11<!--; @Windelband-GuNw, p. 144;
@Windelband-GuNw-en, p. 174-->]

Essa equivalência metodológica dos procedimentos das disciplinas
tornariam insignificantes as diferenças substantivas --- como os
métodos especializados das disciplinas singulares, o conteúdo de suas
leis etc. --- aproximando-as da perspectiva do "caráter formal de
seus objetivos cognitivos" [@Windelband-GuNw-orig, p. 10]; assim, o
estudo dessas propriedades formais --- os métodos --- funcionaria mais
adequadamente para fundamentar logicamente a classificação das
ciências.

> Temos, aqui, diante de nós agora uma classificação puramente
> metodológica, a ser fundada em conceitos lógicos seguros. O
> princípio classificatório é o caráter formal de seus objetivos
> cognitivos. Uma busca leis gerais, a outra fatos históricos
> particulares: expresso na linguagem da lógica formal, o objetivo da
> primeira é o julgamento geral, apodítico, o da última, a proposição
> singular, assertórica. [@Windelband-GuNw-orig, p. 11<!--;
> @Windelband-GuNw, p. 144; @Windelband-GuNw-en, p. 175-->]

Ao contrário de duas classes de objetos, naturais e espirituais,
o que existem são duas classes de modos de investigação que resultam
em diferentes tipos de conhecimento --- leis gerais, por um lado,
descrições exaustivas singulares, por outro --- e que abordam, assim,
de perspectivas distintas quaisquer fenômenos que tomam por objeto.

> As ciências empíricas buscam, no conhecimento do real ou o geral na
> forma de lei natural ou o singular na formação historicamente
> determinada; elas consideram em parte a forma sempre invariável, em
> parte o conteúdo único, imanentemente determinado, do evento real.
> As primeiras são ciências nomológicas
> \[\foreignlanguage{german}{Gesetzeswissenschaften}\], as últimas são
> ciências de eventos
> \[\foreignlanguage{german}{Ereigniswissenschaften}\]; aquelas
> ensinam o que sempre é, estas o que foi uma vez. O pensamento
> científico é --- se se permite formar novas expressões técnicas ---
> num caso *nomotético*, noutro *idiográfico*. Se quisermos atermo-nos
> às expressões usuais, podemos ainda falar nesse sentido da oposição
> entre disciplinas das ciências naturais e históricas, contanto que
> nos lembremos de contar, nesse sentido metodológico, a psicologia
> definitivamente como uma ciência natural. [@Windelband-GuNw-orig,
> p. 12]

Essa "oposição metodológica somente classifica o manejo, não o
conteúdo do próprio conhecimento" adverte Windelband a respeito da
consequência mais profunda dessa mudança de critério classificatório
das ciências: "Permanece possível e se demonstra de fato que os mesmos
fenômenos \[Gegenstände\] podem ser feitos objetos de uma investigação
nomotética e além disso de uma idiográfica." [@Windelband-GuNw-orig,
p. 12] Novamente: trata-se, ao invés de uma distinção com base na
diferença ontológica, retirada da natureza dos próprios fenômenos
tomados como objeto das ciências empíricas, de uma diferenciação a
partir dos objetivos e métodos de distintas abordagens ou
perspectivas.

> Sem mencionar Dilthey, Windelband enfatiza que a dicotomia
> nomotético/idiográfico não deveria ser confundida com a distinção
> entre as ciências naturais e as
> "*\foreignlanguage{german}{Geisteswissenschaften}*" ---
> literalmente, "ciências do espírito" ou "ciências humanas". Ele
> insiste que sua dicotomia é puramente baseada em considerações
> formais e lógicas. Ela marca uma diferença "metodológica" ao invés
> de uma "substantiva" e distingue não duas esferas de coisas, mas
> dois tipos de conhecimento. A dicotomia não é fundamentada
> ontologicamente em diferenças entre dois tipos de entidades, mas
> definida axiologicamente por diferenças entre dois tipos de
> interesse. Segue-se que a natureza e a história não são dois modos
> de ser, mas os objetos lógicos de dois modos de investigação.
> [@Oakes-ValueTheoryAndTheFoundationsOfTheCulturalSciences, p. 66,
> nota 4]

Os mesmos fenômenos podem ser observados ora por um
olhar nomotético, em busca de leis, ora por um idiográfico, que o
descreve em sua unicidade. Como exemplo dessa plasticidade dos
fenômenos, Windelband cita a "ciência da natureza orgânica": enquanto
taxonomia das diferentes espécies de seres vivos, ela tem caráter
nomotético; enquanto descrição da história evolutiva da totalidade dos
seres vivos, ela tem caráter idiográfico. O mesmo poderia ser dito de
um sem número de fenômenos das ciências históricas e a sociologia é
talvez a mais nomotética delas. Mesmo que seus fenômenos permaneçam
ancorados em seus contextos históricos específicos, a própria formação
de conceitos --- particularmente tipos ideais --- é animada por uma
disposição nomotética.

---

O princípio puramente metodológico de classificação das ciências de
Windelband foi posteriormente trabalhado por Heinrich Rickert
(1863--1936), que mantinha uma relação cordial com
Simmel[^simmel-rickert] e que teve grande influência sobre o
pensamento metodológico de Max Weber.

> As ideias de Windelband foram posteriormente refinadas e elaborados
> por Heinrich Rickert, um membro mais jovem da escola de Baden ou do
> sudoeste alemão de neokantianos. Windelband começou atribuindo à
> filosofia a tarefa de esclarecer logicamente os conceitos das
> disciplinas especializadas. Como um exemplo, ele criticou a divisão
> convencional de estudos epíricos em ciências naturais e disciplinas
> humanísticas. Ele observou que essa divisão era baseada na diferença
> "material" ou substantiva entre "natureza" e "mente" como objetos de
> estudo; mas ele instanciou a psicologia para mostrar que a separação
> era às vezes difícil de ser mantida. Em seu lugar, ele propôs uma
> distinção "formal" ou metodológica. [@Ringer-MaxWebersMethodology,
> p. 32]

A obra de Heinrich Rickert, *Os limites da formação de conceitos nas
ciências naturais* (*\foreignlanguage{german}{Die Grenzen der
naturwissenschaftlichen Begriffbildung}*)[^rickert-grenzen-refs] tem
uma dupla estratégia: por um lado, é uma crítica da tentativa de
transportar o naturalismo positivista das ciências naturais para as
ciências da cultura, afirmando a impossibilidade da fundação de uma
ciência histórica em bases positivistas; por outro, é uma crítica do
relativismo historicista, afirmando a necessidade de premissas
independentes da história.
[@Oakes-ValueTheoryAndTheFoundationsOfTheCulturalSciences, p. 61]

> Por fim, ele propõe-se a realizar tudo isso mediante argumentos que
> são puramente "formais" or, como diríamos hoje, epistemológicos. Em
> suas intenções, a filosofia da história de Rickert é antimetafísica.
> Ele repudia as construções do processo total, alcance e significado
> da história produzidos pela tradição do idealismo alemão e
> reconceitua a filosofia da história como uma teoria do conhecimento
> histórico. Porque ele entendia o domínio da história como cultura, a
> teoria do conhecimento histórico de Rickert era uma metodologia das
> ciências culturais.
> [@Oakes-ValueTheoryAndTheFoundationsOfTheCulturalSciences, p. 61]

Em *Limites*, Rickert elabora a diferenciação entre ciência nomotética
e idiográfica estabelecida por Wilhelm Windelband em seu discurso
inaugural como reitor em Estrasburgo em 1894 [@Windelband-GuNw-orig].
Rickert parte de duas premissas avançadas por Windelband: a teoria da
individualidade dos valores e a distinção entre conhecimentos
nomotéticos e idiográficos.
[@Oakes-ValueTheoryAndTheFoundationsOfTheCulturalSciences, p. 63]
Rickert segue Windelband também em sua intenção programática de
repelir os avanços dos adversários da filosofia --- cujas correntes
principais seriam o positivismo e o relativismo historicista (este
último porque só via como legítimo o estudo da história da filosofia)
--- e em restaurar a dignidade perdida da disciplina (e preservar seu
lugar nos currículos acadêmicos).

Rickert elabora a visão da classificação das ciências históricas e
naturais de Windelband --- que este só formulara em seus termos mais
genéricos no referido discurso ao assumir a reitoria --- numa teoria
completa, baseada fundamentalmente na questão de como os valores dos
pesquisadores atuam na construção dos objetos já na seleção dos
fenômenos e na formação dos conceitos.

No entanto, o que nos interessa aqui, mais do que conhecer
profundamente a metodologia de Rickert, é compreender esse momento da
controvérsia metodológica: a discussão sobre a classificação das
ciências humanas e naturais, e dos objetos das ciências históricas.

\hr

Entretanto, antes de prosseguirmos, é preciso falar algo (mesmo que
rapidamente) sobre o papel de Simmel neste primeiro debate da
*\foreignlanguage{german}{Methodenstreit}*.

Ao discutir o método de Max Weber, @Ringer-MaxWebersMethodology
atribui a Simmel uma influência sobre Weber muito maior do que a de
Windelband, afirmando que Simmel antecipou, na primeira edição de seu
*Problemas da filosofia da história* de 1892 [@PG-1892-orig;
reimpresso em @PG-1892; a obra foi completamente revisada para a
segunda edição, @PG-1905-orig; com pequenas alterações dessa para a
terceira, @PG-1907-orig; reimpressa em @PG-1907, que se estabeleceu
como a edição "final" como base para as edições posteriores e suas
traduções; consultei as traduções em espanhol, @PG-1907-es; e francês,
@PG-1907-fr; Simmel pretendia reescrevê-la novamente em meados dos
anos 1910, mas dessa reescrita só concluiu três palestras: @PhZ-orig;
@WhV-orig; e @HFor-orig; esses três ensaios encontram-se compilados em
tradução brasileira em @Simmel-EnsaiosTeoriaHistoria] a distinção
entre abordagens nomotéticas e idiográficas, mas concebendo-as sem os
problemas da abordagem de Windelband.

> Dois anos antes do discurso de Windelband, Simmel havia distinguido
> entre as ciências nomológicas
> (*\foreignlanguage{german}{Gesetzeswissenschaften}*) das disciplinas
> ocupadas com a realidade concreta
> (*\foreignlanguage{german}{Wirklichkeitswissenschaften}*). Além
> disso, ele havia reconhecido o caráter "ideal" de generalizações
> estritamente universais, os usos possíveis de regularidades
> estatísticas, e a impossibilidade de explicar eventos particulares
> *como totalidades*.
>
> Assim, Simmel havia se protegido contra a mais séria falha na
> posição de Windelband, que era precisamente sua tendência a tratar
> particulares idiográficos como totalidades. O que pode significar
> dizer, enfim, que o historiador deve descrever "completa e
> exaustivamente" um evento, para "reproduzir e compreender" uma
> situação humana em sua "individualidade", "atualidade única", ou
> "imediaticidade"? Como pode-se dizer que um dado fato é
> significativo como um elemento numa "totalidade"? --- A "conclusão"
> de um silogismo em que leis e condições figuram como premissas maior
> e menor certamente não pode ser um "evento singular real", mas
> somente um evento conforme definido e descrito de uma certa forma.
> Pois não se pode jamais descrever *exaustivamente* ou explicar um
> evento ou um estado do mundo. [@Ringer-MaxWebersMethodology, p. 34]

O trecho a que Ringer se refere foi completamente alterado nas edições
posteriores, e a distinção entre "ciências nomológicas" e "ciências da
realidade" não aparece senão sugerida (Simmel fala, no entanto,
exaustivamente do lugar da história como ciência da realidade).

> O dualismo entre ciência narrativa e nomológica \[erzählender und
> Gesetzeswissenschaft\], que deu ocasião a tantos conflitos de
> competência, é aqui certamente tocado. Olhado de maneira lógica e
> conceitual, existe entre ambas a maior diferença que pode haver no
> domínio do saber em geral. As leis das coisas não tem absolutamente
> nada a ver com a sua realidade; elas permanecem em sua validade não
> importa se o caso que elas descrevem ocorra uma ou milhões de vezes;
> à excepcionalidade incondicional com a qual elas determinam: quando
> A ocorre, deve ocorrer B --- elas pagam com a completa incapacidade
> de determinar se A ocorre. Do conhecimento mais perfeito de toda lei
> da natureza não se pode obter o menor conhecimento de qualquer
> conduta real da mesma, se não houver em mãos além dela um fato do
> qual outros podem ser deduzidos. A lei tem caráter ideal, nenhuma
> ponte vai dela à realidade tangível que deve, ao contrário, ser
> posta inteiramente fora dela por um ato específico. Na medida,
> portanto, em que a ciência histórica tem que descrever o que
> aconteceu realmente, enquanto ela é efetivamente ciência da
> realidade \[Wirklichkeitswissenschaft\], ela existe no mais agudo
> contraste concebível com toda ciência nomológica
> \[Gesetzeswissenschaft\]. É precisamente o caso determinado
> individual segundo tempo e espaço que forma seu conteúdo, que à
> última é absolutamente indiferente. [@PG-1892-orig, pp. 41-43;
> reimpresso em @PG-1892, pp. 346-349]

É importante notar, entretanto, que Simmel não está tratando
especificamente da diferenciação segundo objetos ou abordagens, como
Windelband, mas, dialogando com Ranke e Dilthey, discutindo o lugar
antes da filosofia da história que da própria história. Seu propósito
é, contra o realismo histórico de Ranke, demonstrar que a história,
como as ciências naturais, opera de maneira apriorística, isto é, que
também o seu objeto é constituído segundo estruturas cognitivas e não
dado. Para Simmel,

> \[...\] "história" significa a formação do evento imediato,
> apenas vivenciado de acordo com os a priori do espírito
> cientificamente formado, da mesma maneira que "natureza" significa a
> formação do material sensível mediante as categorias do
> entendimento. [@Simmel-Selbstdarstellung]



<!-- NOTAS {{{ -->

[^invasao-subterranea]: Creio que parte dessa invasão subterrânea do
positivismo deva-se precisamente à recusa de atacá-lo em seus próprios
termos, reconhecendo-lhe suas reivindicações e princípios e
abordando-os de frente, naquela reação difusa que, como vimos,
respondia ao positivismo como um conjunto de disposições vagamente
definidos e não como uma doutrina com princípios claros estabelecidos.

[^windelband-weber]: "Alguns anos antes, Windelband havia proferido seu
discurso de posse de reitor em Estrasburgo. O discurso fora intitulado
'História e ciência natural', o que talvez não foi uma escolha feliz.
Essa era uma época em que a metodologia da ciência natural ainda era
pensada como um cura-tudo por várias pessoas. Por isso, o discurso
atraiu atenção e, além disso, pode ser considerado como um ponto de
partida para muito do que Jellinek, Troeltsch e Max Weber tinham a
dizer sobre metodologia." [@Honigsheim-ErinnerungenMW-en-OnMW, p. 15]

[^exagero-guinada-irracionalista]: Seguindo o argumento de @Hughes,
corremos aqui o risco de uma simplificação excessiva, por isso vale a
advertência: há certamente uma boa dose de exagero na interpretação de
que a guinada irracionalista do pensamento alemão do final do século XIX
possa ser atribuída à controvérsia metodológica, assim como na
amplificação dos discursos de demarcação das ciências humanas e
naturais suscitados pela controvérsia --- a diferenciação remonta,
pelo menos, às origens da tradição histórica, quando o movimento não
era uma reação ao positivismo, mas ao espírito jusnaturalista do
Iluminismo, como afirma @Iggers-GermanConceptioOfHistory, pp. 126-127:
"A famosa distinção entre os métodos aplicáveis às ciências culturais e
aqueles das ciência naturais não surge repentinamente com Dilthey e
Windelband, mas remonta a uma linha de pensamento que teve suas
origens na revolta da escola histórica alemã contra a tradição do
direito natural. Talvez muito mais importante do que o desencantamento
com a religião e a metafísica entre autores positivistas, no curso do
século XIX foi a insistência de autores alemães historicamente
orientados em abordar ideias e valores não em termos de normas
absolutas de verdade ou bem, mas como expressões de uma era, cultura
ou povo específico."

[^simmel-rickert]: Simmel e Rickert mantinham intenso contato e
frequentemente trocavam seus trabalhos, mas sem grande intimidade.
Segundo Hans, o filho de Simmel, a relação era motivada pela amizade
entre as esposas, "Mas eles eram tão extremamente opostos na maneira e
conteúdos de seus pensamentos que se encaravam com um certo frio
respeito." [@HansSimmel, p. 259]

[^rickert-grenzen-refs]: Publicada em duas partes em 1896 e 1901, com
uma primeira edição completa de 1902 que foi posteriormente ampliada
numa segunda, de 1913, numa quarta de 1921 e numa quinta de 1929
[@Oakes-ValueTheoryAndTheFoundationsOfTheCulturalSciences, p. 61,
nota 1]

[^dilthey-metodo]: Nesse ensaio sobre a psicologia [Refiro-me a
@Dilthey-IdeenPsychologie-orig; da qual consultei a tradução
portuguesa, @Dilthey-IdeenPsychologie-pt], assim Dilthey descreve o
método da compreensão empática: "Só de modo fragmentário
experimentamos em nós esta conexão; ora neste ponto, ora naquele, cai
sobre ela a luz do apercebimento: pois a força psíquica, de acordo com
uma importante peculiaridade sua, só pode elevar à consciência um
número limitado de membros da conexão interna. Mas temos, de um modo
constante, consciência de tais uniões. Na variabilidade imensa dos
conteúdos de consciência retornam as mesmas combinações e assim
sobressai, pouco a pouco, com claridade, a sua forma. Igualmente, a
consciência de como estas sínteses ingressam em combinações mais
amplas e constituem, por fim, uma só textura torna-se cada vez mais
distinta, clara e segura. Se um membro evoca regularmente um segundo
ou uma classe de membros outra classe, se, em seguida, noutros casos
repetidos, este segundo membro evoca um terceiro, ou uma segunda
classe de membros uma terceira, e isto continua num quarto ou quinto
membro, terá, por fim, de se constituir, e com certeza universalmente
válida, uma consciência da conexão de todos os membros, uma
consciência da textura de classes inteiras de membros. Noutros casos,
costumamos também destacar do caos de processos, graças à concentração
discriminadora atenção, um só processo, procurando mantê-lo em
percepção ou recordação constante para a sua apreensão mais minuciosa.
No rápido, demasiado rápido, fluir dos processos internos, destacamos,
isolamos um deles, e elevamo-lo a uma atenção mais intensa. Nesta
atividade isoladora reside a condição para a marcha ulterior da
abstração. Só mediante uma abstração destacamos numa conexão concreta
uma função, um modo de união. E só por meio de uma generalização
estabelecemos a forma sempre recorrente de uma função ou a constância
de certas gradações de conteúdos sensíveis, a escala das intensidades
sensoriais ou afetivas, que a todos nos são conhecidas. Em todos estes
atos lógicos estão incluídos o diferenciar, o equiparar, a
determinação dos graus de diferença. Brotam assim necessariamente das
operações lógicas a divisão e a designação, na qual reside o germe da
definição. Gostaria eu de dizer que as operações lógicas elementares,
tal como cintilam nas impressões e nas vivências, se podem apreender
melhor na experiência interna. Diferenciar, equiparar, determinar
graus de diferença, unir, separar, abstrair, ligar várias conexões
numa, obter a partir de diversos factos uma uniformidade: tais
operações estão contidas em toda a percepção interna ou destacam-se da
sua composição. Emerge assim, como primeira peculiaridade da captação
dos estados internos, que condiciona a investigação psicológica, a
*intelectualidade da percepção interna*. Tal como a percepção
exterior, a interna ocorre mediante a cooperação dos processos lógicos
elementares. E na percepção interna adverte-se, com especial
claridade, como os processos lógicos elementares são inseparáveis da
captação das suas componentes.\/ Existe assim uma segunda
característica da apreensão de estados psíquicos. Esta apreensão
provém da *vivência* e a ela se mantém vinculada. Na vivência cooperam
conjuntamente os processos de *todo o ânimo*. Nela é-nos dada a
conexão, enquanto os sentidos oferecem apenas uma multiplicidade de
particularidades." [@Dilthey-IdeenPsychologie-orig; da qual consultei
a tradução portuguesa, @Dilthey-IdeenPsychologie-pt, pp. 47-48]

<!-- }}} -->
