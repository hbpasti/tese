#!/usr/bin/env python
from pandocfilters import toJSONFilter, RawInline

"""
Pandoc filter that causes bold to be rendered using
the custom macro '\strong{...}' rather than '\textbf{...}'
in latex.  Other output formats are unaffected.
Based on https://github.com/jgm/pandocfilters/blob/master/examples/myemph.py
"""


def latex(s):
    return RawInline('latex', s)


def strong(k, v, f, meta):
    if k == 'Strong' and f == 'latex':
        return [latex('\\strong{')] + v + [latex('}')]

if __name__ == "__main__":
    toJSONFilter(strong)
