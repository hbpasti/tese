#!/usr/bin/env python

"""
Pandoc filter to add blankspace instead of a \rule
"""

from pandocfilters import toJSONFilter, RawBlock

def latex(s):
    return RawBlock('latex', s)

def hr(key, value, format, meta):
  if key == 'HorizontalRule' and format == 'latex':
    return [latex('\plainfancybreak{\\baselineskip}{1}{\\rule[.5em]{4em}{.2pt}}')]

if __name__ == "__main__":
  toJSONFilter(hr)
