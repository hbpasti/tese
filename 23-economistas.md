## Os problemas lógicos da economia política histórica {#methodenstreit-economistas}


Na economia germanófona do final do século XIX, a escola histórica era
hegemônica, e sua repulsa à economia clássica a ortodoxia. Nesse
contexto, a "revolução" marginalista da década de 1870 --- quando três
autores (Menger na Áustria, Jevons na Inglaterra, e Walras na
França) descobriram, separadamente, a noção de utilidade marginal,
partindo de uma orientação nomotética, isto é, em busca de leis, comum
--- não alterou o estado das coisas.

Em 1883, o economista austríaco Carl Menger (1840--1921), que já havia
publicado sua teoria da utilidade marginal em 1871, em seus
*Princípios de teoria econômica* (*\foreignlanguage{german}{Grundsätze
der Volkswirtschaftslehre}*), publicou um tratado metodológico que
promovia uma ciência econômica de orientação nomotética, em antítese à
economia histórica praticada na Alemanha, e em tom intensamente
polêmico com esta última. As *Investigações sobre o método das
ciências sociais, em especial o da economia política*
[*\foreignlanguage{german}{Untersuchungen über die Methode der
Socialwissenschaften und der Politischen Ökonomie insbesondere}*,
@Menger-Untersuchungen; do qual consta uma tradução em inglês em
@Menger-Untersuchungen-en] serviu, assim, de pontapé inicial na
disputa metodológica mais famosa daquele final de século. A obra, que
frequentemente perde o tom ao tratar da orientação historicista da
ortodoxia econômica alemã, recebeu uma resenha crítica de Gustav von
Schmoller (1838--1917), [@Schmoller-Methodologie] que paga a investida
na mesma moeda, e foi seguida de uma reação ainda mais vigorosa de
Menger, um libelo intitulado *Os erros do historicismo na economia
política alemã* [*\foreignlanguage{german}{Die Irrthümer des
Historismus in der deutschen Nationalökonomie}*, @Menger-Irrthumer]
que Schmoller se recusou a comentar. O fim abrupto da
interlocução,[^schmoller-fim-abrupto] no entanto, não pôs fim à
controvérsia, que sobreviveu à virada do século e permaneceu como uma
cisma incontornável até o presente.

> Como Schmoller se negou por razões de estilo a reagir ao panfleto de
> seu adversário, a controvérsia metodológica na economia política
> alemã terminou, mas não foi resolvida. Seu resultado é uma cisma na
> economia política alemã. Vinte anos depois, Max Weber reconhece "Em
> inesperada e aparentemente inconciliável brutalidade, o método
> teórico-'abstrato' confronta ainda hoje a pesquisa
> empírico-histórica (em nossa disciplina)" [@Kruse-Paradigmenwechsel,
> p. 153]

O objeto da disputa, como no restante das controvérsias metodológicas
da época, não é difícil de supor: qual a natureza das explicações nas
ciências econômicas, quais seus objetos, qual a sua orientação
fundamental, qual o método mais adequado para atingir os seus
objetivos?

---

A obra de Menger é um discurso sobre o método que se dedica a refundar
a economia política e suas divisões, e é talvez isto, antes do que o
conflito em torno de métodos, que provocou a reação de Schmoller.

No apêndice IV de sua obra ("A terminologia e a classificação das
ciências econômicas"), Menger estabelece uma tábua da economia e suas
subdisciplinas ou áreas de concentração.
É válido notar que nesse anexo, Menger procura
estabelecer a classificação das ciências econômicas
("*\foreignlanguage{german}{Wirtschaftswissenschaft}*");
anteriormente, ele estabelece que a economia política teórica envolve
dois desses grandes tipos, o teórico e o prático: "Sob a *economia
política* \[\foreignlanguage{german}{politischen Oekonomie}\]
compreenderemos, no entanto, aquela totalidade de ciências
teórico-práticas da economia (a economia política teórica
\[\foreignlanguage{german}{theoretische Nationalökonomie}\], a
política econômica e a ciências das finanças) que no presente
geralmente é combinada sob a designação acima."
[@Menger-Untersuchungen, p. 10] *\foreignlanguage{german}{Nationalökonomie}*,
literalmente "economia nacional" é a maneira como os alemães costumam
traduzir "economia política", e tem por objeto a
*\foreignlanguage{german}{Volkswirtschaft}*, a "economia do povo", ou
"economia nacional" (aqui, economia tem o sentido do fenômeno, ali da
ciência). Na definição da ciência econômica, Menger fala tanto em
*\foreignlanguage{german}{politischen Ökonomie}* quanto
*\foreignlanguage{german}{Nationalökonomie}*, porém atribuindo a este
último um escopo mais reduzido porque uma ciência econômica que
concebe a economia somente sob a perspectiva coletiva.

> A totalidade das ciências que dizem respeito à economia humana, as
> *ciências econômicas*, no sentido mais amplo da palavra, se decompõe
> em três grandes grupos correspondentes às três tarefas principais
> que o espírito humano pode se colocar na investigação de fenômenos
> econômicos:
>
> I\. no histórico,
>
> II\. no teórico,
>
> III\. no prático.
>
> I\. As ciências econômicas *históricas* tem a essência individual e
> as relações individuais dos fenômenos econômicos para investigar e
> apresentar, e se decompõe, conforme procuram resolver suas tarefas
> sob o ponto de vista da condicionalidade
> \[\foreignlanguage{german}{Zuständlichkeit}\] ou do desenvolvimento,
> na *estatística* e na *história* da economia humana. \[...\] As
> ciências econômicas históricas são, para o bem de sua tarefa
> científica universal, necessariamente *apresentações da economia
> humana sob a perspectiva da observação coletiva, isto é, da economia
> política* \[\foreignlanguage{german}{Volkswirtschaft}\], no sentido
> recentemente mencionado  da palavra \[no sentido de "economia
> nacional"\].
>
> II\. As ciência *teóricas* da economia humana tem a essência geral e
> as relações gerais (as leis) dos fenômenos econômicos para
> investigar e apresentar; elas formam em sua totalidade da *teoria da
> economia política*, enquanto singularmente correspondem às distintas
> orientações de investigação teórica no campo da economia política.
> Destas últimas, aprendemos a distinguir as orientações exata e
> empírica da investigação teórica e, dentro desta última, ainda as
> *histórico-filosófica*, *estatístico-teórica*,
> "*anatômico-fisiológica*" etc. \[...\]
>
> III\. As *ciências econômicas práticas*, finalmente, devem nos
> ensinar os princípios segundo os quais as intenções econômicas das
> pessoas (conforme as condições) podem ser atingidas mais
> efetivamente. Estas são:
>
> 1\. A *política econômica*, a ciência dos princípios para a promoção
> mais efetiva (adequada às condições) da "economia política" do lado
> do poder público.
>
> 2\. A *doutrina prática das economias singulares*, a ciência dos
> princípios segundo os quais os fins econômicos das economias
> singulares (conforme as condições) pode ser atingidas da maneira
> mais completa.
>
> Estas últimas se decompõe em:
>
> a\) na *ciência das finanças*, a ciência dos princípios para a
> instituição mais adequada, correspondente às condições, da maior
> economia singular da nação, do orçamento do governo e de outros
> sujeitos econômicos providos de poder financeiro; e
>
> b\) na *doutrina da economia privada* prática, a ciência dos
> princípios segundo os quais (vivendo sob as nossas condições sociais
> presentes!) pessoas privadas (conforme as condições) podem
> estabelecer sua economia da maneira mais adequada.
> [@Menger-Untersuchungen, pp. 252-256; @Menger-Untersuchungen-en, pp.
> 208-211]

Longe, portanto, de desprezar a orientação histórica como um todo,
Menger parece reservar-lhe um espaço --- embora evidentemente
acessório, quase subalterno --- em sua classificação da economia. À
economia histórica caberia --- o que nenhum economista historicista
negaria --- o papel de descrever fenômenos econômicos em sua
"condicionalidade", portanto, enquanto eventos singulares. Um dos
desdobramentos dessa classificação é que os fenômenos que a economia
histórica toma como objeto são tomados "sob a perspectiva da
observação coletiva", o que significa que o singular aí é sempre uma
economia nacional, não o comportamento econômico do indivíduo isolado
(ou seja: a economia histórica não se ocupa de "robinsonadas" --- mas
a economia teórica sim).

Ao mesmo tempo, a economia teórica estuda a essência e as leis dos
fenômenos econômicos. Nesse sentido, sua orientação é sempre "exata",
nunca empírica, e seu resultado não guarda qualquer relação com a
realidade: trata-se de buscar o que há de essencial, por via puramente
dedutiva, nos fenômenos observados, e construir leis, compreendidas
como relações essenciais, a despeito da observação ou não de
regularidades. Os fenômenos econômicos que esta divisão da economia
toma por objeto são sempre isolados, não lhe interessam as
condicionalidades e interações entre os elementos. Assim, ela não
estuda a economia nacional, mas a economia individual
("robinsonadas")[^def:robinsonadas] de um indivíduo abstrato, que age
de maneira economicamente racional sempre --- o chamado *homo
oeconomicus*, o "homem econômico".

Um dos maiores vilões da história da metodologia das ciências sociais,
o *homo oeconomicus* poderia ser assim definido: trata-se de um
"modelo de comportamento econômico" que concebe a ação econômica como
uma escolha racional entre alternativas presentes segundo certas
preferências:

> conforme essas preferências, o indivíduo avalia as várias
> alternativas à sua disposição; ele pesa os prós e contras, os custos
> e benefícios das alternativas umas contra as outras e finalmente
> escolha aquela(s) alternativa(s) que chega(m) mais perto de suas
> preferências ou que promete(m) trazer o maior benefício líquido.
> Portanto, nesse modelo o comportamento humano <!--13--> é
> interpretado como escolha racional pelo indivíduo de alternativas
> disponíveis ou --- para falar na linguagem da economia --- como
> "maximização de utilidade sob confrangimentos com incerteza".
> [@Kirchgassner-HomoOeconomicus, pp. 12-13]

Ou ainda:

> Numa palavra: o *homo economicus* calcula; ele sempre pesa coisas.
> Ele contempla os objetivos mais diversos, objetivos que devem
> frequentemente ser numerosos e os quais ele deve perceber como
> noções distintas. Nada previne que os objetos de seu desejo estejam
> entre os mais humanos, os mais "nobres" que temos --- um gosto por
> amor sublime, ideal heroico, devoção altruísta etc. Pois o homem
> econômico, como um bom benthamita, não conhece nenhuma hierarquia
> axiológica a priori: ele pode executar seu cálculo em qualquer tipo
> de valor. \[...\] A única condição em sua escolha é que ele deve
> *escolher* em plena consciência da relação fins/meios assim como das
> perdas e ganhos da perspectiva de *outros* valores. O que define o
> comportamento do homem econômico não é então a qualidade de seus
> objetivos, mas antes a lucidez de suas ações.
> [@Merquior-DeathToHomoEconomicus, p. 356]

Posto em seu contexto histórico, esse é o modelo formado pelos
economistas clássicos ingleses na explicação da ação econômica (e
moral), particularmente com Adam Smith (1723--1790) e, posteriormente,
com John Stuart Mill (1806--1873), e tão resolutamente rejeitado pelos
mandarins alemães (ver \autoref{smithianismus}).

> Em primeiro lugar, o homem econômico é o herói de Adam Smith e o
> sujeito do cálculo hedonista de Bentham --- em resumo, uma figura
> legitimada dois séculos atrás pela *Riqueza das Nações* e *Um
> Fragmento sobre o governo*, ambos datados de 1776. É claro, ele
> seguia o ideal aristocrático de honra, a *virtu* renascentista, o
> *Homo religiosus* puritano e, finalmente, o hedonismo frívolo
> reprimido na cultura barroca, então liberado no período rococó (o
> "homem de prazer" não é necessariamente um utilitarista uma vez que
> Bentham prontamente admitia que o princípio do prazer pode tomar
> outras formas que não o hedonismo). Pelo fim do Iluminismo, o
> paradigma moral do *homo oeconomicus* encontrava três rivais: o
> cidadão fanaticamente civil, na maneira de Saint-Just; o homem
> virtuoso que seguia o imperativo categórico kantiano; e a bela alma
> de Schiller, formada pelo "autocultivo" --- a
> *\foreignlanguage{german}{Bildung}* --- do classicismo alemão.
> [@Merquior-DeathToHomoEconomicus, pp. 354-355]

Não é preciso dizer, no final do século XIX e início do XX, seus
maiores rivais vem das ciências sociais, particularmente da sociologia
e antropologia. 

A questão da alternativa entre uma perspectiva individualista e uma
coletivista na concepção do singular na economia é um dos aspectos do
conflito que mais renderia frutos em meados do século XX, reavivado no
debate sobre individualismo e "holismo" metodológicos. Neste contexto,
entretanto, a questão para Menger é legitimar a abordagem do
individualismo abstrato que ele propõe, e que já é vigorosamente
rejeitada pelos economistas alemães (ver excurso
*\foreignlanguage{german}{Smithianismus}*,
\autopageref{smithianismus}).


O núcleo dessa controvérsia gira em torno da possibilidade de
estabelecimento de uma ciência exata da economia. A escola histórica
não recusa a possibilidade de teorização econômica, mas procrastina a
sua realização para um momento posterior, quando se dispuser de uma
descrição completa de um número considerável de fenômenos
concretos.[^schmoller-esgotamento] A escola austríaca, ao contrário,
não julga necessário esperar porque concebe a economia como uma
ciência abstrata em busca de leis; seu descolamento dos fenômenos
concretos é propositadamente aceito com tranquilidade porque a
economia é por eles concebida como uma ciência exata, não
descritiva.[^menger-confusao]

Nesse sentido, o ponto de controvérsia é sobre a orientação geral da
ciência econômica como um todo. Os esforços teórico e descritivo
caminham em terrenos e em direções completamente distintos: mesmo que
a escola histórica alcance o dúbio estágio de coleta de dados
descritivos em que se torne possível a criação de teorias, estas
referir-se-iam a fenômenos concretos, singulares, situados em seu
contexto e concebidos individualmente; em oposição, a orientação exata
não se interessa por esse tipo de abordagem dos fenômenos concretos,
mas quer considerá-los como instâncias de leis gerais. Assim, mesmo
que a escola histórica chegue ao ponto de construção de teoria ---
coisa que o próprio Schmoller já arriscava --- as teorias
historicamente informadas referem-se a objetos concebidos de maneira
distinta (e inconciliável) que aqueles trabalhados na orientação
exata.

Tratam-se de duas direções que, conquanto refiram-se à mesma categoria
de fenômenos, concebem-nos de maneira oposta e operam em universos
completamente estranhos. O encontro dessas duas orientações é
impensável e poder-se-ia tranquilamente conceber um campo científico
em que ambas operassem sem conflito (precisamente porque nunca se
encontram). O único ponto de toque das duas orientações é operarem
sob o rótulo [Para tomar de empréstimo a expressão de
@Soz, pp. 14-15: "\[...\] colocar todos esses *domínios* científicos
anteriores juntos  não produz nada de novo. Significa apenas que todas
as ciências históricas, psicológicas, normativas são postas num grande
pote e neste colada o rótulo: Sociologia. Com isso apenas se obtém
um novo *nome*, enquanto tudo aquilo que ele designa ou já está fixado
em seu conteúdo e relações, ou continua a ser produzido nos domínios
de investigação anteriores."] "economia política"; mas é precisamente
aí que reside o ponto conflituoso. É porque ambas as orientações
querem reivindicar a definição adequada para tal rótulo e
estabelecer assim suas tarefas, objetivos e orientação geral é que a
colisão improvável dessas duas direções é necessariamente
antagônica: o que se encontra em jogo é a própria classificação da
disciplina, e cada lado promove uma definição que exclui o seu oposto
de participação nela.<!-- [^discurso-metodologico-coabitacao] -->

Minha disposição de implicar a coexistência de ambas as orientações
sem um conflito incontornável é quase utópica, mas se alimenta da
vizinhança algo pacífica das orientações etnográfica e sociológica na
própria sociologia. Mas é preciso levar em conta que nos momentos em
que as tarefas e objetivos das disciplinas são conscientemente
elaborados e descritos nos discursos metodológicos, transita-se para
um espaço discursivo normativo, não mais o campo das práticas de
pesquisa cotidianas das disciplinas, mas no da "dogmática" (para usar
a expressão desses alemães do final do XIX) e assim circunscrevem-se
esferas de atuação em termos que não são nunca os mais adequados para
uma coabitação pacífica sob um mesmo rótulo de orientações diversas.

---

É válido notar, com @Louzek-BattleOfMethods, que já na primeira
recepção da controvérsia metodológica entre economistas a disputa era
concebida como uma oposição entre os métodos dedutivo e indutivo, e
que a tradução da controvérsia nesses termos é devedora da maneira
como Gustav von Schmoller interpretou e reagiu à crítica de Carl
Menger --- mas que os próprios austríacos incorporaram, em certa
medida, a interpretação da controvérsia dada por
Schmoller.[^methodenstreit-recepcao-inglesa]

A recepção atual da controvérsia tende, não infrequentemente, a
enfatizar os pontos de aproximação dos discursos programáticos ou, por
assim dizer, "metametodológicos"[^metametodo] dos adversários,
lamentando o desfecho triste e amargo de uma grande incompreensão.
[Estou pensando sobretudo em @Bostaph-Methodenstreit; e
@Louzek-BattleOfMethods, que se colocam ao lado dos austríacos] Tudo
se passa como se Schmoller e Menger tivessem grandes pontos de
concordância, mas se tivessem deixado levar pelas discordâncias, que
seriam em pontos menores. Mas mesmo na época percebia-se que tudo não
passava de um grande exagero:[^schmoller-exagero-reacao] John Neville
Keynes (pai do mais famoso John Maynard Keynes), ainda no início dos
1890, já denunciava o fato de que as posições metodológicas
mobilizadas no conflito não eram aquelas aplicadas por seus defensores
em suas obras não metodológicas.

> Os pontos centrais envolvidos em controvérsias sobre método
> econômico podem ser indicados em linhas gerais ao contrastar
> brevemente duas escolas amplamente distintas, uma das quais descreve
> a economia política como positiva, abstrata e dedutiva, enquanto a
> outra a descreve como ética, <!--10--> realista e indutiva. Deve-se
> compreender distintamente que esse contraste agudo não é encontrado
> nos próprios escritos econômicos dos melhores economistas de uma ou
> outra escola. Nos métodos que eles empregam --- quando estão de fato
> discutindo os mesmos problemas --- há, em larga medida, concordância
> substancial. Eles diferem, no entanto, na importância relativa que
> atribuem a diferentes aspectos de seu trabalho; e, em suas
> afirmações formais sobre método essas diferenças são exageradas.
> [@Keynes-ScopeAndMethodOfPoliticalEconomy, pp. 9-10]

Apesar desses lamentos, o fato é que a controvérsia metodológica ficou
marcada como uma disputa inconciliável entre duas abordagens
opostas polarizadas, uma dedutiva e uma indutiva. Dito de outro modo:
ainda que a oposição não tivesse fundamento metodologicamente ou mesmo
na prática de pesquisa de seus proponentes, a sua recepção foi
constitutiva na formação do campo da economia política da época,
estabelecendo claras linhas de pertencimento em grupos opostos --- e
foi a partir dessa constituição de posições, mesmo que virtuais,
opostas, é que boa parte do debate sobre a controvérsia metodológica
se deu.



<!-- NOTAS {{{ -->

[^metametodo]: Na definição de
@Zhao-MetatheoryMetamethodMetaDataAnalysis, "Metamétodo em sociologia
é o estudo de métodos de pesquisa sociológica existentes.  De acordo
com Furfey ([1953] 1965), metamétodo consiste de três tipos de
estudos: (1) o exame dos pressupostos metodológicos necessários para a
realização de pesquisa sociológica; (2) a avaliação dos métodos de
pesquisa sociológica existentes em termos de seus pontos fracos e
fortes; e (3) a codificação de novas regras procedurais para a
pesquisa sociológica." [@Zhao-MetatheoryMetamethodMetaDataAnalysis, p.
378] A minha impressão é que "metodologia", pelo menos no sentido
empregado por esses pensadores alemães no final do século XIX, já dá
conta desse sentido, mas Zhao se antecipa ao argumentar com Parsons
que essa diferença diz respeito ao sentido americano do termo
metodologia em oposição ao sentido alemão.  "Um termo que foi usado
mais frequentemente na literatura e supostamente carregando a mesma
conotação de metamétodo é 'metodologia'. \[...\] Parsons, por exemplo,
fez a seguinte comparação entre as maneiras em que 'metodologia' é
empregada nos Estados Unidos e na Alemanha: 'na ciência social
americana, a palavra 'metodologia' refere-se em geral a técnicas de
pesquisa, enquanto no emprego alemão ela refere-se antes ao que às
vezes chamamos de filosofia da ciência', isto é, ao estudo das
fundações das técnicas de pesquisa. Metodologia é, portanto,
frequentemente reduzida a métodos e técnicas no contexto americano."
[@Zhao-MetatheoryMetamethodMetaDataAnalysis, pp. 383-384] Para uma
definição do início do século XX, ver o verbete
"\foreignlanguage{german}{Methodik}" no @Meyers-6ed, v. 13, p. 710:
"Instrução para a solução metódica, isto é, de forma lógica e
expediente, de uma tarefa científica. \[...\] A lógica pode ser vista
como uma metodologia científica universal." Ou, ainda mais explícito,
o verbete "\foreignlanguage{german}{Methodenlehre (Methodologie)}" no
@Eisler-2ed, v. 1, p. 669: "Doutrina do método
\[\foreignlanguage{german}{Methodenlehre}\] (Metodologia
\[\foreignlanguage{german}{Methodologie}\]) é aquela parte da lógica
que estuda a metodologia \[\foreignlanguage{german}{Methodik}\] geral
de pesquisa (definição, prova etc) e os métodos especiais das
ciências particulares com relação ao seu valor lógico e a sua correção
e adequação lógicas: a metodologia \[ou doutrina do método,
*\foreignlanguage{german}{Methodenlehre}*\] é a análise e a crítica do
proceder científico."

[^discurso-metodologico-coabitacao]: Minha disposição de implicar a
coexistência de ambas as orientações sem um conflito incontornável é
quase utópica, mas se alimenta da vizinhança algo pacífica das
orientações etnográfica e sociológica na própria sociologia. Mas é
preciso levar em conta que nos momentos em que as tarefas e objetivos
das disciplinas são conscientemente elaborados e descritos nos
discursos metodológicos, transita-se para um espaço discursivo
normativo, não mais o campo das práticas de pesquisa cotidianas das
disciplinas, mas no da "dogmática" (para usar a expressão desses
alemães do final do XIX) e assim circunscrevem-se esferas de atuação
em termos que não são nunca os mais adequados para uma coabitação
pacífica sob um mesmo rótulo de orientações diversas.

[^schmoller-fim-abrupto]: Na seção de resenhas do segundo número do
*Anuário de Schmoller* daquele 1884, consta uma entrada para os
*Erros* de Menger que, no entanto, não é resenhado, acompanhada da
seguinte explicação: "A redação do anuário não está em posição de
resenhar este livro, uma vez que o devolveu ao autor imediatamente com
as notas seguintes. 'Estimado Senhor! Recebi seu escrito ‘Os erros do
historicismo na economia política alemã’. Ele trás impresso ‘do autor’
de modo que também devo agradecer pelo envio pessoalmente. Já há algum
tempo fui informado por diversas partes de que este contém
essencialmente um ataque contra mim, e o primeiro olhar à primeira
página confirmou isso. Por mais que eu gostaria de reconhecer sua boa
vontade em ocupar-me e esclarecer-me, tanto mais creio que devo
permanecer fiel a meus princípios sobre transações literárias desse
tipo. Devo, assim, revelá-los e também recomendar a sua imitação ao
senhor; eles poupam-lhe bastante tempo e incômodo. Jogo todos os
ataques pessoais semelhantes, especialmente quando não espero do
respectivo autor nenhum novo estímulo para mim, sem ler, no forno ou na
lixeira. Não caio nunca na tentação de enfastiar o público com a
continuação de rixas literárias na maneira combativa de alguns
professores alemães. Não serei tão rude com o senhor ao ponto de
destruir um livrinho tão adequado de sua mão; por isso, o envio de
volta com o agradecimento mandatório e o pedido de que faça melhor uso
dele. Por futuros ataques, aliás, permanecerei sempre grato ao senhor.
Pois ‘grande inimigo, grande honra’. Aceite a afirmação de meus
cumprimentos cordiais. G. Schmoller.'" [@Schmoller-MengerIrrthumer]

[^tabua-menger]: É válido notar que nesse anexo, Menger procura
estabelecer a classificação das ciências econômicas
("*\foreignlanguage{german}{Wirtschaftswissenschaft}*");
anteriormente, ele estabelece que a economia política teórica envolve
dois desses grandes tipos, o teórico e o prático: "Sob a *economia
política* \[\foreignlanguage{german}{politischen Oekonomie}\]
compreenderemos, no entanto, aquela totalidade de ciências
teórico-práticas da economia (a economia política teórica
\[\foreignlanguage{german}{theoretische Nationalökonomie}\], a
política econômica e a ciências das finanças) que no presente
geralmente é combinada sob a designação acima."
[@Menger-Untersuchungen, p. 10] Vale um breve comentário sobre a
terminologia: *\foreignlanguage{german}{Nationalökonomie}*,
literalmente "economia nacional" é a maneira como os alemães costumam
traduzir "economia política", e tem por objeto a
*\foreignlanguage{german}{Volkswirtschaft}*, a "economia do povo", ou
"economia nacional" (aqui, economia tem o sentido do fenômeno, ali da
ciência). Na definição da ciência econômica, Menger fala tanto em
*\foreignlanguage{german}{politischen Ökonomie}* quanto
*\foreignlanguage{german}{Nationalökonomie}*, porém atribuindo a este
último um escopo mais reduzido porque uma ciência econômica que
concebe a economia somente sob a perspectiva coletiva.

[^schmoller-esgotamento]: Schmoller não rejeita --- à maneira da
escola histórica de direito --- a formação de teoria, mas considera
esgotadas as possibilidades de sua feitura em modelos abstratos,
visando, assim, a construção de teoria historicamente informada, rica
em conhecimento descritivo etc.: "Depois que a velha economia
política abstrata criou grandes coisas, secou a fonte de sua
vitalidade porque ela volatilizou os seus resultados em esquemas
excessivamente abstratos que careciam de qualquer realidade. \[...\]
E, no futuro, virá uma nova era para a economia política, mas somente
através da exploração da totalidade de materiais descritivos
históricos e estatísticos que já estão sendo criados, não através de
uma destilação adicional das proposições abstratas já centenas de
vezes destiladas do velho dogmatismo." [@Schmoller-Methodologie,
p. 978]

[^menger-confusao]: "A *teoria* da economia nacional não deve em
nenhum caso ser confundida com as ciências *históricas* ou *práticas*
da economia nacional. Só para quem está completamente no escuro no
tocante à natureza e as tarefas formais da economia política teórica é
que é possível fazer isso, porque os conhecimento (teóricos) gerais
que ela abrange, suposta ou verdadeiramente, exibem menor rigor que
nas ciências naturais, ou, embora por uma razão mais remota, porque o
fato do desenvolvimento dos fenômenos econômicos, como veremos, não
deixa de ter influência sobre a maneira em que a economia política
consegue resolver sua tarefa teórica --- uma *ciência histórica*. Só
quem não consegue manter separadas as essências das ciências teóricas
e práticas reconhece nela --- em parte pelo fato de que ela, como
outras teorias, forme a fundação das ciências práticas --- uma
*ciência prática*." [@Menger-Untersuchungen, pp. 26-27;
@Menger-Untersuchungen-en, p. 51]

[^schmoller-exagero-reacao]: "Schmoller poderia ter reagido calmamente
face à sua posição excepcional nas ciências econômicas alemãs,
especialmente porque Menger não havia negado o direito à existência da
escola histórica, mas, em sua resenha de Menger, ele adotou a postura
de um desafio \[er nimmt ... den Fedehandschuh auf\]. Seu
contra-ataque concentrou-se em quatro pontos centrais: em primeiro
lugar, pertenceria a 'uma ingenuidade escapista e diletante ver de
partida nas necessidades humanas ou no impulso aquisitivo ou no
egoísmo os últimos elementos simples no sentido científico da palavra.
Se o impulso aquisitivo ou o egoísmo fosse um último elemento num
sentido cientificamente útil, então ele deveria ser claramente
delimitado, numa psicologia científica, de outras forças anímicas
paralelas. Mas não há menção disso ...' (Schmoller 1883: 243) O
segundo ponto crítico central de Schmoller a Menger afirma que este
parte 'exclusivamente da consideração singular da economia individual'
('troca, valor, dinheiro ...') e ignora 'os órgãos e instituições
econômicos que constituem o esqueleto do corpo econômico' (Schmoller
1883: 247). Em terceiro, Menger negligencia a integração sociológica da
economia em outros contextos sociais, especialmente a relação entre a
economia e o Estado (Schmoller 1883: 244). E em quarto, Schmoller
repreende em seu adversário austríaco, de maneira realmente histórica,
que ele, junto com a economia política clássica 'compartilha o grande
erro metodológico de se fiar na essência de seu tempo como a essência
geral da economia política' (Schmoller 1883: 247)."
[@Kruse-Paradigmenwechsel, pp. 152-153]

[^methodenstreit-recepcao-inglesa]: "Na literatura de língua inglesa,
a *\foreignlanguage{german}{Methodenstreit}* é normalmente descrita
como uma disputa entre um método abstrato-dedutivo representado por
Menger por um lado e o método empírico-indutivo representado por
Schmoller por outro. \[...\] A primeira pessoa a trazer esse conceito
à literatura ocidental foi Böhm-Bawerk (1890: 244-271). John Neville
Keynes (1891), o pai do famoso John Maynard Keynes, também ocupou um
papel importante na disseminação da interpretação anglo-saxã padrão.
As sementes da terminologia futura já haviam estado presentes com um
dos participantes na disputa, Schmoller. Ele compreendeu sua disputa
com Menger essencialmente como uma luta entre indução e dedução
(Schmoller 1883)." [@Louzek-BattleOfMethods, p. 440]

[^def:robinsonadas]: "Robinsonada" era o termo pejorativo destinado
aos estudos que se dedicavam a explicar comportamentos sociais a
partir do comportamento hipotético de um indivíduo abstrato em
isolamento, como Robinson Crusoé, do romance de Defoe de 1719.

<!-- }} -->
