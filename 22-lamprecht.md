## O caso Lamprecht {#lamprecht}

O "escândalo Lamprecht", como Simmel se refere [A única referência que
encontrei em suas cartas foi numa carta a Heinrich Rickert de 15 de
agosto de 1898, na qual Simmel explica seu então distanciamento da
polêmica: "Sobre o escândalo *Lamprecht*, não o tenho acompanhado;
sobre questões histórico-epistemológicas estou suficientemente
esclarecido subjetivamente e deixo estar assim, pois meus interesses
produtivos estão agora em problemas muito diferentes. Somente sobre o
materialismo histórico eu pediria novamente a palavra." @GSG22,
p. 305] à polêmica envolvendo a volumosa obra do historiador Karl
Lamprecht, a *História alemã* em doze volumes, publicada entre 1891 e
1909 e duramente criticada pelo historiador Georg von Below no final
da década de 1890, foi uma dessas disputas, entre historiadores. Ela
teve ecos profundos na historiografia alemã da época e funciona como
uma espécie de reação generalizada do campo intelectual alemão a
influências francesas e inglesas que são rotuladas numa única
categoria acusatória: positivismo.

Vimos como a tradição histórica alemã tornou hegemônica uma
historiografia que se focava no estudo de grandes personalidades
históricas e do Estado e que recusava por princípio a possibilidade de
explicação da ação histórica mediante a observação de regularidades.
Naturalmente, uma tal posição não permaneceria inconteste.

> A ciência histórica alemã construiu a fortaleza do historicismo. Ela
> rejeita o pensamento conceitual e a generalização teórica, insiste
> no monopólio do método histórico, "que requer que o historiador
> descreva épocas passadas exclusivamente com a terminologia com que
> se defronta nas próprias fontes" (Mommsen 1973: 302), e restringe
> sua pesquisa ao Estado e à política. Contra isso, um estranho à
> guilda, o influenciado pelo positivismo Karl Lamprecht, representa a
> tese de que as estruturas econômicas e espirituais, assim como as
> forças coletivas, são mais efetivas no processo histórico que os
> atos de grandes estadistas e comandantes. Ele reivindica uma
> fundação sociopsicológica para a história, a inclusão de dimensão
> históricas não-estatais no processo de pesquisa, investigações de
> relações causais entre os vários níveis sociais e o emprego de
> conceitos tipológicos na estruturação da matéria histórica. Esse
> avanço foi rejeitado pelo establishment acadêmico, representado
> sobretudo por Georg v. Below, Heinrich von Sybel, Dietrich Schäfer,
> Friedrich Meinecke, Max Lenz, Felix Rachfahl e Hermann Oncken, "sob
> a bandeira do livre arbítrio e da personalidade na história"
> (Oestreich 1969: 352). A controvérsia metodológica na ciência
> histórica terminou com uma vitória absoluta dos advogados
> tradicionalistas de uma história política personalista; só depois da
> Segunda Guerra Mundial é que a história social pôde firmar-se
> laboriosamente nas faculdades de história alemãs.
> [@Kruse-Paradigmenwechsel, p. 151]

Apesar de se tratar fundamentalmente de uma disputa polarizada entre
duas visões acerca dos métodos, tarefas e limites das ciências
sociais, a *Methodenstreit* instanciou-se em diferentes esferas em
torno de objetos de disputa específicos, que traziam para o debate
outras questões, mais ou menos relevantes para a controvérsia mais
geral.

A despeito de sua má fama geral nos círculos eruditos alemães da
época, o positivismo teve alguma inserção entre o chamado "círculo de
Leipzig", ao qual Lamprecht é associado. A semelhança entre o
positivismo de Comte, Spencer ou Mill e o do círculo de Leipzig é
duvidosa, em especial porque o grupo não abandona a tradição
idealista; no entanto, em comum com o positivismo clássico, eles
acreditam na unidade fundamental entre os mundos físicos e psíquico e
na possibilidade de uma descrição de tal totalidade sob a forma de
leis, além da intenção de mobilização dessas leis na solução de
problemas sociais. [Cf.
@Smith-PoliticsAndTheSciencesOfCultureInGermany, cap. 11. Ver também,
abaixo, \autoref{circulo-leipzig}]

Lamprecht se alimentou dessa inclinação positivista de Leipzig e da
psicologia de Wundt para buscar uma quebra de paradigma na metodologia
historiográfica ortodoxa de sua época, com sucesso questionável. Sua
impetuosidade metodológica refletia também em seu descuido documental
e sua obra acabou sendo criticada pelos dois aspectos. [Ver a respeito
@Chickering-Lamprecht, passim] Um opositor particularmente ferrenho
foi Georg von Below. @Chickering-Lamprecht menciona uma rixa algo
pessoal como uma das causas da disputa entre os dois historiadores,
mas a disputa era metodológica e dizia respeito, antes, à insistência
ortodoxa de atribuição de total centralidade ao papel do Estado e
certa prioridade ao estudo de grandes "personalidades históricas", e
no tratamento de ambos (Estado e personalidades) como
individualidades, como prescrito por Ranke, um alvo preferido de
Lamprecht.

> O aparato teórico de Lamprecht era completamente infalsificável e
> manifestamente injustificado em relação às descobertas empíricas que
> ele combinava sem esforço. De qualquer maneria, seu programa atingiu
> a maioria de seus colegas como subversivo, não somente em sua
> metodologia mas também em suas implicações sociais e políticas. Ele
> era desmerecidamente criticado por sua erudição desleixada e era
> suspeito de "materialismo econômico" mesmo pelo normalmente moderado
> Friedrich Meinecke. O historiador conservador nacionalista Georg von
> Below foi bem mais longe. Usando um conjunto de armas do arsenal
> anti-"positivista", ele associou a abordagem de Lamprecht ao
> racionalismo iluminista, empiricismo estreito, cosmopolitismo e o
> positivismo histórico inglês. Ele notou que campeões da democracia e
> das massas davam as boas vindas à história cultural, enquanto
> historiadores mais representativos continuavam a enfatizar o Estado,
> a nação e os conceitos holísticos desenvolvidos pelo românticos
> alemães. De modo geral, Lamprecht encontrou poucos defensores no
> interior da profissão histórica alemã.
> [@Ringer-MaxWebersMethodology, p. 24]

Inspirado pelo ideal de uma ciência única da totalidade da existência
nutrido no Círculo de Leipzig --- o de uma ciência que "seria
histórica na estrutura, incorporando, entre outras coisas, as
concepções da economia histórica, mas o conceito central em torno do
qual ela seria construída era *cultura*"
[@Smith-PoliticsAndTheSciencesOfCultureInGermany, pp 208-209] ---
Lamprecht procurou fundar uma história cultural que, "repudiando a
ênfase dominante sobre o Estado e o papel dos grandes indivíduos na
tradição histórica alemã", "dava atenção a tudo, desde condições
econômicas a relações interpessoais e cultura popular, ao mesmo tempo
baseando-se fortemente na história da arte e da arquitetura."
[@Ringer-MaxWebersMethodology, p. 22]

"Sua cruzada em nome de suas grandes teorias de
*\foreignlanguage{german}{Kulturgeschichte}*
\[história cultural\]", escreve Chickering, "que se propunha a
capturar toda faceta da história humana no interior de uma
regularidade legal que ele mesmo havia definido, também fez dele o
mais controverso historiador de sua época." [@Chickering-Lamprecht,
p. xii]

A influência da orientação nomotética de Leipzig levava Lamprecht a
descrever sua história cultural como "psicologia aplicada" ---
especificamente, a psicologia social de Wilhelm Wundt (1832--1920)
aplicada, o que o levava a procurar "regularidades nos 'mecanismos
psíquicos' de épocas culturais e das 'transições' entre eles."
[@Ringer-MaxWebersMethodology, pp. 22, 23]

> Lamprecht traçava os antecedentes de sua abordagem à ideia de uma
> 'alma do povo' de Herder, aos estudos de cultura popular de Wilhelm
> Heinrich Riehl e à análise documental da arte de Jakob Burckhardt.
> Ele aparentemente acreditava que esses precursores da história
> cultural não poderiam ter dado atenção adequada à "psiquê social"
> antes que o indivíduo moderno tivesse começado a emergir. Ao
> retraçar a segunda fase da época subjetivista aos estímulos
> promovidos pela urbanização e rápida mudança tecnológica, ele evocou
> as pressões psicológicas da modernidade. Isso permitiu-lhe seguir
> com notável facilidade à "busca por um novo dominante", o "anseio da
> época" por uma nova moralidade, uma nova
> *\foreignlanguage{german}{Weltanschauung}* ou
> religião, o deslocamento do naturalismo artístico por um novo
> "idealismo" e o novo primado das disciplinas humanísticas.
> [@Ringer-MaxWebersMethodology, p. 23]


### O Círculo de Leipzig {#circulo-leipzig}

O positivismo teve alguma inserção nas humanidades alemãs no que
@Smith-PoliticsAndTheSciencesOfCultureInGermany chama de "círculo de
Leipzig":

> O Círculo de Leipzig tinha seu centro em torno de um arranjo frouxo
> de relações pessoais e intelectuais entre importantes membros do
> corpo docente de Leipzig entre cerca de 1890 e a Primeira Guerra,
> aumentado por conexões extra-acadêmicas com redes de editores,
> empresários e oficiais que por décadas foi uma característica
> significativa da vida intelectual na Saxônia e na Turíngia.
> [@Smith-PoliticsAndTheSciencesOfCultureInGermany, p. 215]

Dentre os membros do círculo de Leipzig encontravam-se figuras como o
psicólogo Wilhelm Wundt (1832--1920), o historiador Karl Lamprecht
(1856--1915), o economista Karl Bücher (1847--1930), o geógrafo
Friedrich Ratzel (1844--1904) e o químico Wilhelm Ostwald (1853--1932)
[@Smith-PoliticsAndTheSciencesOfCultureInGermany, p. 204]; "o núcleo
central de membros do Círculo de Leipzig era suplementado por outros
professores da Universidade de Leipzig de menor distinção ou
comparecimento menos regular nas trocas de ideias e, aparentemente,
também por pessoas importantes dos mundos editorial, empresarial e
governamental." [@Smith-PoliticsAndTheSciencesOfCultureInGermany,
p. 208]

A crença fundamental do grupo era um amálgama das tradições idealista
--- e histórica, até onde o pensamento de Roscher está envolvido ---
com o positivismo: assim, seu horizonte normativo metodológico era a
construção de uma ciência humana universal, baseada em leis, que
pudesse ser mobilizada para a resolução das questões sociais
contemporâneas (assim, muito próximos da concepção de Auguste Comte),
baseada num princípio metafísico da unidade dos mundos físico e
psíquico (assim seguindo a tradição idealista).

> A agenda do Círculo de Leipzig pode ser descrita em termos muito
> amplos, embora muito tenha de ser inferido das afirmações individuais
> de membros e das direções teóricas paralelas que eles tomaram depois
> de se associarem uns com os outros ao invés de de algum programa
> formal. Todos eles acreditavam que era possível criar uma ciência
> humana geral nomotética aplicável à compreensão da sociedade moderna
> e de problemas sociais correntes. Eles concordavam que tal ciência
> era, nalgum sentido, imanente na abordagem humanista ao conhecimento
> central à educação tradicional na Alemanha e que a completa
> afirmação de sua existência restabeleceria as conexões entre as
> disciplinas que haviam recentemente divergido. A ciência universal
> também proveria um meio de estabelecimento de coerência entre as
> várias comunidades de discurso na política intelectual e de
> recomendar políticas públicas decisivas para o *Reich*. A ciência
> seria histórica em sua estrutura, incorporando, <!--209--> entre
> outras coisas, ideias da economia histórica, mas o conceito central
> em torno do qual ela seria construída era *cultura*. A ciência
> humana universal seria, na realidade, a culminação do
> desenvolvimento da ciência cultural nomotética através da virada do
> século. Ela seria construída dentro da tradição geral do liberalismo
> intelectual e de maneira completamente liberal: através das trocas
> entre pessoas sérias, educadas, conduzido abertamente diante de um
> público educado. [@Smith-PoliticsAndTheSciencesOfCultureInGermany,
> pp. 208-209]

A tradição intelectual de que esse grupo se alimentava vinha de dois
antigos professores de Leipzig: o economista Wilhelm Roscher
(1817--1894) e o psicólogo fundador da psicofísica Gustav Fechner
(1801--1887). De Roscher vinha a intenção de evitar a especialização
da economia, de conceber a análise empírica histórica em quadros
comparativos e de buscar construir teoria econômica no quadro de um
amplo estudo cultural. "Boa parte do legado intelectual de Roscher
para a Universidade de Leipzig depois de sua morte em 1894 \[...\]
repousa nos pressupostos sobre a conectividade de todo conhecimento
social que estavam implícitos em sua obra."
[@Smith-PoliticsAndTheSciencesOfCultureInGermany, p. 206] De Fechner
vinha o "postulado da existência de uma unidade subjacente das
existências física e psíquica" e o objetivo de "estabelecer,
preferencialmente na forma de leis matemáticas, a natureza dessa
unidade".

> Fechner assim combinava elementos do vitalismo que havia sido
> popular no pensamento científico romântico no início do século
> \[XIX\] com um comprometimento com uma abordagem rigorosamente
> nomotética à ciência que atrairia até o mais extremo positivista.
> Isso fazia Fechner especialmente popular entre intelectuais que
> queriam ser capazes de se considerar cientistas do tipo agora
> dominante nas ciências físicas exatas sem ter de aderir ao
> materialismo filosófico radical com o qual o tipo havia sido
> comumente identificado.
> [@Smith-PoliticsAndTheSciencesOfCultureInGermany, p. 207]

Dentre esses intelectuais, a figura "indubitavelmente dominante"
[@Ringer-MaxWebersMethodology, p 20] era Wilhelm Wundt, que se tornara
o herdeiro do prestígio da psicofísica de Fechner. É a psicologia
social de Wundt, mais do que a de Moritz Lazarus (1823--1904) e
Heymann Steinthal (1823--1899), a responsável pela visão da psicologia
como uma fundação para as ciências sociais --- a crença de poder
encontrar na psicologia "senão uma 'mecânica', pelo menos uma
'química' da vida social" [@Weber-Objektivitat, p. 173]. A passagem
por Leipzig, e particularmente por Wundt, a julgar pela série de
artigos sobre as "ciências positivas da moral na Alemanha"
[@Durkheim-SciencePositiveDeLaMoraleEnAllemagne; de que consta uma
tradução brasileira em
@Durkheim-SciencePositiveDeLaMoraleEnAllemagne-pt], marcou
profundamente a formação do jovem Émile Durkheim (1858--1917), que
encontra em Wundt uma abordagem superior à do organicismo de um
Schäffle ou do mecanicismo de Schmoller.

Simmel teve seu encontro com o Círculo de Leipzig por ocasião de seu
agora clássico ensaio sobre as grandes cidades, "As grandes cidades e
a vida do espírito" [@orig-Grst; reimpresso em @Grst; com uma tradução
brasileira em @Grst-pt]: o que se tornaria um dos mais conhecidos
ensaios de Simmel fora originalmente apresentado num ciclo de
palestras organizado pela Fundação Gehe, como preparativo para a feira
municipal de Dresden de 1903, e contava com palestras de membros do
círculo de Leipzig, Karl Bücher, Friedridch Ratzel, Dietrich Schäfer
entre outros, além da de Simmel. Cada palestrante discutiria um
aspecto relacionado às metrópoles (geografia, economia, demografia,
segurança pública, por exemplo), e Simmel ficou incumbido de falar
sobre as suas consequências espirituais. A palestra de Simmel ficou
dissonante do esperado pelo círculo, em vista de que o editor do
volume dedicado a publicar as palestrar incluiu uma resposta sua,
abordando o mesmo tema, intitulada "O significado espiritual das
grandes cidades" [@Petermann-DieGrosstadt]. Petermann explica o motivo
no prefácio do volume:

> No plano que situa as razões das palestras individuais da Fundação
> Gehe no inverno de 1902-1903, as três primeiras deveriam lidar com
> as origens, com a arena e com o pessoal da vida metropolitana e as
> três seguintes dedicar-se com a discussão do significado econômico,
> espiritual e político das grandes cidades.
>
> No entanto, uma vez que as explicações espirituosas do Sr. Prof. Dr.
> Simmel sobre as grandes cidades e a vida do espírito tomaram por
> objeto mais a influência das grandes cidades sobre a vida espiritual
> do metropolitano individual do que sobre as forças coletivas das
> grandes cidades e seus efeitos coletivos, criou-se uma lacuna na
> implementação do programa original, para cuja melhor realização
> possível e com a aprovação do comitê científico, o autor do ensaio
> "O sentido espiritual das grandes cidades" foi autorizado pelo
> conselho. [@DieGrossstadt, "\foreignlanguage{german}{Vorbemerkung
> des Herausgegebers}", s.n.]

É comum confundir-se esse ciclo de palestras com a própria feira
quando se fala da palestra de Simmel, mas a feira ocorreu em maio,
enquanto as palestras ocorreram em março de 1903. [^feira-cidades]

<!-- NOTAS {{{ -->

[^petermann]: Em @Petermann-DieGrosstadt. Petermann explica o motivo
no prefácio do volume: "No plano que situa as razões das palestras
individuais da Fundação Gehe no inverno de 1902-1903, as três
primeiras deveriam lidar com as origens, com a arena e com o pessoal
da vida metropolitana e as três seguintes dedicar-se com a discussão
do significado econômico, espiritual e político das grandes
cidades.\/\/ No entanto, uma vez que as explicações espirituosas do
Sr. Prof. Dr. Simmel sobre as grandes cidades e a vida do espírito
tomaram por objeto mais a influência das grandes cidades sobre a vida
espiritual do metropolitano individual do que sobre as forças
coletivas das grandes cidades e seus efeitos coletivos, criou-se uma
lacuna na implementação do programa original, para cuja melhor
realização possível e com a aprovação do comitê científico, o autor do
ensaio 'O sentido espiritual das grandes cidades' foi autorizado pelo
conselho." [@DieGrossstadt, "\foreignlanguage{german}{Vorbemerkung des
Herausgegebers}", s.n.] É comum confundir-se esse ciclo de palestras
com a própria feira quando se fala da palestra de Simmel, mas a feira
ocorreu em maio, enquanto as palestras ocorreram em março de 1903. [Os
anais da feira --- debates de urbanismo e adminstração pública ---
encontram-se em @Wuttke-DieDeutscheStadteausstellung, onde não há
menção às palestras da Fundação Gehe; encontra-se uma descrição da
feira em @Woodhead-Dresden1903-Book]. Sobre o desencontro entre Simmel
e o projeto do ciclo, ver
@Smith-PoliticsAndTheSciencesOfCultureInGermany, cap. 11,
especialmente pp. 209-216 e @EB-GSG7, pp. 364-365.

[^feira-cidades]: Os anais da feira --- debates de urbanismo e
adminstração pública --- encontram-se em
@Wuttke-DieDeutscheStadteausstellung, onde não há menção às palestras
da Fundação Gehe. Encontra-se uma descrição da feira em
@Woodhead-Dresden1903-Book. Sobre o desencontro entre Simmel e o
projeto do ciclo, ver @Smith-PoliticsAndTheSciencesOfCultureInGermany,
cap. 11, especialmente pp. 209-216 e @EB-GSG7, pp. 364-365.

[^ranke]: Leopold von Ranke, 1795--1886, historiador alemão. Ranke
defendia uma história que narra os fatos históricos "como de fato
aconteceram" (*\foreignlanguage{german}{wie es eingentlich gewesen}*).
Pode-se argumentar que Ranke é um dos principais expoentes da tradição
historicista alemã [com @Loader-GermanHistoricismAndItsCrisis, por
exemplo, que repetidamente menciona a expressão de Ranke de que "todas
as épocas são imediatas aos olhos de Deus" como uma profissão de
relativismo]

<!-- }}} -->
