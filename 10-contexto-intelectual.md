# O contexto intelectual da controvérsia metodológica {#contexto}

\epigraph{Não há nada tão errático como a assim chamada realidade;
esta é certamente a única que poderia se conciliar com
ela.}{Simmel, assinando \textcite{Jugend-Strandgut}}



Antes de falarmos de como surgiram e se desenrolaram os vários debates
metodológicos da *\foreignlanguage{german}{Methodenstreit}*, quero
tentar situar, ainda que com brevidade, o contexto intelectual e as
correntes de pensamento que criaram as condições para que, naquele
final de século, as ciências sociais se vissem no ponto culminante de
uma crise de identidade e de legitimidade que estabeleceria, ainda que
de maneira inconclusa, o tom de sua prática futura.

Para tanto, assumo como correta a descrição da "tradição mandarim" de
@Ringer-Decline, para o qual "os elementos formais mais importantes na
herança erudita mandarim eram a crítica kantiana, as teoria do
idealismo e a tradição histórica alemã."[^tradicao-mandarim]
Nesse sentido, enfatizo o papel dessas orientações na constituição da
constelação das ideias eruditas em que se estabelece a controvérsia
metodológica, em especial das duas últimas (a tradição idealista e a
histórica). A partir dessa interpretação, em que a tradição ou herança
compartilhada dos intelectuais alemães do final do século XIX, de
maneira independente de seu posicionamento no espectro ideológico
(que, se acompanharmos Ringer, não era muito diverso), se
caracterizava pela hegemonia dessas tradições, destaca-se a
centralidade da crítica ao positivismo como terreno fecundo em que a
controvérsia metodológica se desenvolveu. "Em história e em economia,
em sociologia e em direito", escreve Hughes, "o pensamento social
alemão se baseava em poucos princípios relativamente simples que,
através de todas as diferenças de método e de campo, permaneceram
surpreendentemente uniformes." [@Hughes, p. 186]

Essa linha de interpretação, entretanto, não é a única, nem a mais
frutífera, da história do pensamento alemão naquele final de século.
Nesse sentido, é preciso tratar como heurística a minha escolha de
autores na reconstrução desse espaço de ideias eruditas. É assim,
portanto, que sigo @Bougle-LesSciencesSocialesEnAllemagne numa
reconstrução do pensamento social e da filosofia da ciência alemães do
final do século XVIII até a segunda metade do XIX --- na
\autoref{quatro-correntes} ---; assim também quando acompanho @Hughes
na reconstrução de uma disposição antipositivista quase hegemônica no
pensamento alemão desse período, em particular entre a geração de
intelectuais dos anos 1890 --- na \autoref{antipositivismo}. Da mesma
maneira, as incursões com @Parsons-Structure --- na
\autoref{tradicao-idealista} --- e outros devem ser compreendidas.

<!-- NOTAS {{{ -->

[^tradicao-mandarim]: \label{def:mandarinato} @Ringer-Decline, p. 91.
A "tradição mandarim" é o tipo ideal construído por Ringer para falar
dos intelectuais alemães do entorno da virada do século. Numa
definição lacônica, "Eu definiria 'os mandarins' simplesmente como uma
elite cultural e social que deve seu status essencialmente a
qualificações educacionais ao invés de a direitos hereditários ou
riqueza." [@Ringer-Decline, p. 5] Naquele contexto de transição (nesse
contexto específico, bastante acelerada) de uma economia
essencialmente agrária para uma industrializada, quando o capital
econômico ainda não tem ainda legitimidade suficiente para ocupar os
espaços deixados pelo declínio da legitimação de uma aristocracia
hereditária, essa classe média educada encontra espaços de
legitimação. 

<!-- }}} -->
