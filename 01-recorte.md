
A formulação do relativismo como solução à disputa entre
racionalismo e empiricismo na epistemologia das ciências sociais
encontra-se mais claramente desenhada na resenha do livro de Rudolph
Stammler, *Economia e direito segundo a concepção materialista da
história*, de 1896, exposta num pequeno artigo publicado no anuário
de Schmoller sob o título de "Para a metodologia das ciências
sociais" [@Mthdk-orig; reimpresso em @Mthdk], do qual segue uma
tradução no Anexo \ref{methodik} (p. \pageref{methodik}).

Argumentarei que ali Simmel desenhara uma metodologia relativista que
se destinava a responder, superando o impasse como um caminho
intermediário mais elevado, à oposição entre racionalismo e
empiricismo nas ciências sociais. Essa metodologia, creio, não se pode
reduzir ao seu chamado "pluralismo metodológico" que diz mais
respeito ao perspectivismo --- a noção de que todo ponto de vista
(onde pode-se ler *\foreignlanguage{german}{Weltanschauung}*, visão de
mundo) é igualmente válido e legítimo como ponto de partida para a
observação de qualquer coisa ---, que é um corolário do relativismo
metodológico que busca demonstrar a insuficiência das abordagens
dogmática ou racionalista e empírica ou realista sozinhas: para
Simmel, trata-se de um movimento de vaivém ininterrupto entre ambas as
abordagens, em que o conhecimento empírico só é possível a partir de
critérios apriorísticos que são, também por sua vez, conhecimento
empírico que funciona como a priori.

Esse ensaio, portanto, serve de ponto de partida para o estudo que
também recorre, onde necessário, a outros escritos de Simmel: a
formulação mais notória e clara do relativismo de Simmel encontra-se
em sua *Filosofia do dinheiro*.[^phg-economia] Na discussão da
teoria do valor (no primeiro capítulo), particularmente, Simmel aponta
para a maneira como seu relativismo pode servir de caminho
intermediário para a oposição rígida entre as abordagens racionalista
e empiricista. Para qualificar o relativismo evocado em "Para a
metodologia das ciências sociais", recorro a parte das formulações
presentes em sua teoria do valor. Também recorro a algumas das
formulações de sua obra *Problemas da filosofia da história*.
[Publicado em primeira edição em @PG-1892-orig; com uma segunda edição
completamente alterada em @PG-1905-orig; e uma terceira em
@PG-1907-orig] Alguns de seus escritos menores, artigos e ensaios do
período, assim como parte de sua correspondência, são discutidos com
brevidade onde necessário.

<!-- NOTAS {{{ -->

[^phg-economia]: @PHG; publicada originalmente em
@PHG-orig-1e; com uma segunda edição ampliada de sete anos depois
@PHG-orig-2e. É preciso mencionar que Simmel recusava à obra qualquer
sentido econômico: como reivindica no prefácio, "Nenhuma
linha destas investigações deve ser entendida economicamente. Isso
quer dizer que os fenômenos da valoração e da compra, do intercâmbio
e dos meios de troca, das formas de produção e de propriedades, que
a economia concebe de *um* ponto de vista, aqui serão concebidos de um
outro." [@PHG, p. 11] 

[]

<!-- }}} -->
