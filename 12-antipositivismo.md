## O antipositivismo alemão no final do século XIX {#antipositivismo}


### Conhecimento puro e cultivo pessoal {#wissenschaft-bildung}

No universo erudito alemão do século XIX, a filosofia era tão
científica quanto as demais disciplinas acadêmicas, devido a uma
concepção específica de ciência.  A ciência alemã era concebida num
sentido bastante mais amplo que o da *science* inglesa ou francesa; e
assim, como a filosofia, sua tarefa era a construção de
*\foreignlanguage{german}{Weltanschauungen}*, visões de mundo.

Essa noção de "*\foreignlanguage{german}{Weltanschauung}*" se definia como
"uma unidade segundo um princípio definido, uma ideia básica, um olhar
metódico que leva ao ordenamento de uma variedade de conhecimentos
numa visão de todo logicamente fundamentada, uma fundação educacional
na qual cada parte ocupa seu lugar racionalmente determinado", "a
unificação de conhecimentos científicos numa doutrina de educação
completa e ordenada segundo fundamentos determinados, também um livro
com tal conteúdo", mas mais especificamente para a filosofia: "A
unificação de conhecimentos filosóficos (ou teológicos) em geral numa
visão de mundo e da vida unitária em relação a uma ideia fundamental
portadora de todos os pensamentos específicos", "num sentido mais
restrito, mais como um princípio formal, a unidade lógica de uma
multiplicidade de conhecimentos racionais sempre parciais."
[@Weltanschauung-DWB-dtv, pp. 1435, 1436, 1437]

A noção alemã de ciência, *\foreignlanguage{german}{Wissenschaft}*,
tinha então uma acepção mais abrangente do que hoje, referindo-se a um
esforço de constituir os saberes específicos das disciplinas
especializadas numa visão de mundo integral, rechaçando (ou pelo menos
atrasando) assim as tendências ocidentais de especialização
científica.[Cf. @Methodologie-HWPh, p. 1382: "Kant praticamente
canonizou esse conceito de ciência quando estabeleceu a conhecida
definição: 'Toda doutrina, se deve ser um sistema, isto é, um todo
de conhecimento ordenado segundo princípios, chama-se ciência'
\[...\]" A citação é do prefácio dos *Princípios metafísicos da
ciência da natureza* de Kant, de 1786 (p. 467); cf. ainda no mesmo
prefácio (p. 470): "Afirmo, porém, que em qualquer doutrina da
natureza particular só pode ser encontrada ciência propriamente na
medida em que nela é encontrada matemática", apud
@Watkins-KantsPhilosophyScience]

> A palavra alemã *\foreignlanguage{german}{Wissenschaft}* não é o
> equivalente à inglesa \[ou portuguesa\] "ciência", pois esta implica
> certos comprometimentos metodológicos. No emprego alemão, qualquer
> corpo organizado de informações é referido como
> *\foreignlanguage{german}{eine Wissenschaft}*, com o artigo
> indefinido. Ao mesmo tempo, todo conhecimento formal, e a atividade
> coletiva de estudiosos em obtê-lo, interpretá-lo e organizá-lo, pode
> ser apresentada <!--103--> como
> *\foreignlanguage{german}{Wissenschaft}* ou, mais comumente,
> *\foreignlanguage{german}{die Wissenschaft}*, com o artigo definido.
> Assim, *\foreignlanguage{german}{die Wissenschaft}* pode ser
> traduzida como "erudição" ou "saber", raramente como "ciência", e
> *\foreignlanguage{german}{eine Wissenschaft}* significa simplesmente
> "disciplina". Em alemão, a história é *\foreignlanguage{german}{eine
> Wissenschaft}* por definição e perguntar se a sociologia é
> *\foreignlanguage{german}{eine Wissenschaft}* é indagar sobre o seu
> status como uma disciplina distinta e claramente circunscrita, não
> sobre sobre seus métodos mais ou menos "científicos".
> [@Ringer-Decline, pp. 102-103]

Um dos pontos de conflito centrais para a reação do campo intelectual
alemão ao positivismo encontra-se na interpretação do fim da busca por
conhecimento: o positivismo, pelo menos o de Comte, atribui ao
conhecimento científico o papel de meio para o estabelecimento de
reformas "esclarecidas", enquanto na ideologia "mandarim" dos alemães
do século XIX, o conhecimento aparece como fim em si mesmo, ou como
meio para o enriquecimento da personalidade do indivíduo.

A noção de cultura pessoal, herdada sobretudo do romantismo alemão
como reação ao Iluminismo, que valorizava o desenvolvimento da
personalidade integral de um indivíduo de maneira orgânica, mantinha
viva uma tradição (neo)humanista de educação que repudiava a onda de
educação técnica mais especializada que começava a dar seus primeiros
passos na Alemanha. [Cf. @Ringer-MaxWebersMethodology, cap. 1;
@Kohnke-NeoKantianismus-en, cap. 3; @Lepenies-DreiKulturen-en, cap. 8]

Em torno de 1800 ocorrera uma "revolução educacional" nos estados
alemães, um conjunto de reformas profundas que resultaram na
universidade moderna e que tiveram lugar ali muito antes tanto da
ocorrência de reformas educacionais semelhantes na França e na
Inglaterra quanto da chegada da Revolução Industrial à própria
Alemanha. Naquele ambiente de "pré-capitalismo", somente a valorização
normativa da educação podia competir com a nobreza de sangue como
fonte de estima pessoal e social. [@Ringer-Decline; ver também, para
um quadro comparativo dos casos alemão e francês,
@Ringer-FieldsOfKnowledge]

Os elementos centrais dessas profundas reformas universitárias rumo à
autonomização e institucionalização do campo acadêmico foram o
imperativo da pesquisa e o estabelecimento de credenciais e de exames
oficiais credenciadores, aplicados a futuros professores secundaristas
e, por fim, a introdução de exames semelhantes para outras profissões.
Essa radical renovação das universidades na Prússia e nos demais
estados alemães dava um papel centras às faculdades de filosofia
perante as faculdades de formação mais técnica ou profissional.
Inspirada pelo idealismo e o entusiasmo neo-humanista com a Grécia
clássica, tal renovação sustentou-se sobre o ideal de
*\foreignlanguage{german}{Bildung}* (cultura ou formação), que
implicava na valorização da interação interpretativa do aprendiz com
textos clássicos como enriquecimento do todo da personalidade.

> *\foreignlanguage{german}{Bildung}* é um antigo substantivo alemão
> derivado do verbo *\foreignlanguage{german}{bilden}*: formar, dar
> forma, criar. Assim, o significado primário de
> *\foreignlanguage{german}{Bildung}* é "formação". Ele possui,
> contudo, uma forte associação com substantivo
> *\foreignlanguage{german}{Bild}*, que significa imagem ou quadro. Os
> místicos alemães do final da Idade Média e do Renascimento (Meister
> Eckhart, Seuse, Jakob Bohme) e, em seu rastro, os escritores
> pietistas dos séculos XVII e XVIII (Arndt, Oetinger) já haviam
> combinado esses dois sentidos associados e jogado-os uns contra os
> outros: *\foreignlanguage{german}{Bildung}* denotava então que o
> processo espiritual de formação e reforma através do qual o
> indivíduo humano transformava, por meio de sua prórpia atividade,
> sua alma na imagem de Deus. Esse significado religioso do termo foi
> então secularizado pelos representates alemães do Iluminismo. Com
> eles, na segunda metade do século XVIII,
> *\foreignlanguage{german}{Bildung}* adquiriu o sentido do processo
> pedagógico de auto-cultivo compreendido como o desenvolvimento
> interior das disposições e capacidades inatas, a formação de uma
> particularidade natural numa personalidade moral madura. \[...\] Só
> a partir da segunda metade do século XIX o sentido comumente aceito
> do termo *\foreignlanguage{german}{Bildung}* gradualmente
> restringiu-se ao processo educacional e seu resultado propriamente.
> [@markus:culture, pp. 14-15]

A alta educação tornara-se, em quase todas as sociedades europeias
modernas, uma fonte da autoimagem das classes médias quase tão
importante quanto riqueza e poder econômico mas, "na Alemanha, o ethos
da *\foreignlanguage{german}{Bildung}* tornou-se um pathos quase
metafísico. Na linguagem dos idealistas alemães, o mundo existe de tal
forma que, ao conhecê-lo, o espírito humano pode realizar seu
potencial." [@Ringer-Weber-bio, p. 9] Nesse sentido, pode ser
considerada como a ideologia por excelência da chamada
*\foreignlanguage{german}{Bildungsbürgentum}*, a classe média educada
alemã.

Essa classe média educada caracterizava-se como uma elite cuja posição
era devedora de suas qualificações educacionais e que, portanto,
opunha-se às classes cuja posição era determinada pelo nascimento.
Compunha-se de altos oficiais e professores, membros do clero e de
profissões liberais educadas --- "os professores universitários eram
seu porta-voz natural." [@Ringer-Weber-bio, p. 9]

> O surgimento de seminários de pesquisa e a subsequente expansão das
> faculdades filosóficas veio acompanhado da emergência de disciplinas
> interpretativas, filológicas e históricas. Foram essas disciplinas,
> e não as ciências naturais, que inicialmente definiram as normas do
> saber rigoroso. A palavra *\foreignlanguage{german}{Wissenschaft}*
> abrangia amplamente todas as disciplinas sistemáticas, inclusive as
> interpretativas. Havia uma crença corrente de que o envolvimento
> produtivo em pesquisa normalmente teria, e certamente deveria
> fazê-lo, o efeito de *\foreignlanguage{german}{Bildung}*. O
> acadêmico original estava destinado a emergir de sua atividade
> enriquecido em espírito e *em sua pessoa*. Do século XIX em diante,
> essa expectativa também se expressava na proposição de que o saber
> ou a ciência (*\foreignlanguage{german}{Wissenschaft}*) deveria
> engendrar uma "visão de mundo"
> (*\foreignlanguage{german}{Weltanschauung}*), uma visão compreensiva
> e parcialmente valorativa do mundo. A busca da verdade deveria levar
> a algo como um discernimento integral e uma certeza moral, ou um
> conhecimento pessoal, ou sabedoria. Em todo caso, o desejo de
> derivar *\foreignlanguage{german}{Bildung}* e uma "visão de mundo"
> do aprendizado ou da ciência era quase universal nas universidades
> alemãs na época de Weber \[e Simmel\]. [@Ringer-Weber-bio, p. 9]

Dessa forma, erigia-se uma hierarquia simbólica entre as várias
ciências, que partia em sentido descendente da teoria abstrata,
passando pelas análises experimentais e acabando nos estudos
"técnicos" ou "aplicados" no nível mais desvalorizado. A
*\foreignlanguage{german}{Wissenschaft}* "pura" era essencialmente não
prática. [@Ringer-Weber-bio, p. 10]

O conceito de *\foreignlanguage{german}{Bildung}* resultava da
convergência de muitas correntes de pensamento cujo princípio
organizador repousava sobre um conjunto de crenças parcialmente
conscientes sobre a educação: os conceitos de
*\foreignlanguage{german}{Wissenschaft}* e
*\foreignlanguage{german}{Bildung}* eram afetados não apenas pelas
doutrinas do idealismo e o neo-humanismo alemães, mas também pelos
debates e pelas práticas pedagógicas da época que converteram o
impulso neo-humanista de recuperação de um saber clássico de modelo
grego num paradigma de saber sistemático. [@Ringer-Weber-bio, p. 11]
Na França e na Inglaterra, a educação, como o individualismo econômico
e a racionalidade política, era uma questão intelectual importante.
Mas, na Alemanha, tornara-se a preocupação central de um estrato
intelectual que mantinha o individualismo econômico como um tema
menor.

Mas é importante notar que no final do século XIX e início do XX, essa
noção de *\foreignlanguage{german}{Bildung}* passa por uma "mudança
melhor descrita como um deslocamento de uma ênfase progressista ou
'utópica' para uma defensiva ou 'ideológica'." [@Ringer-Weber-bio,
p. 11] Se no início do século XIX a ideia de autoaperfeiçoamento
através da *\foreignlanguage{german}{Bildung}* representava uma
posição de desafio ao status quo aristocrático --- com a defesa de uma
educação universal, a classe média educada emergente podia se
considerar uma elite aberta e meritocrática, oposta a uma elite
herdeira de títulos ---, já no final desse século (especialmente a
partir das revoluções de 1848 e 1849) o aspecto de privilégio dessa
educação do *\foreignlanguage{german}{Bildungsbürgentum}* encontra-se
suficientemente cristalizado. A institucionalização da educação
secundária e universitária e dos sistemas de credenciamento
profissional torna as qualificações educacionais em fontes rotineiras
de status e era interesse da classe média educada (que agora já tinha
a burguesia industrial como forte concorrente) impedir o influxo de
grupos sociais novos para dentro da universidade, a fim de reduzir ou
limitar a competição por postos acadêmicos. [@Ringer-Weber-bio, p. 11]

A despeito dos deslocamentos políticos do
"mandarinato"[^mandarinato-ref] alemão de grupo progressista a
reacionário, e das intensas disputas internas entre "ortodoxos" e
"modernistas" [Para falar com @Ringer-Decline] e de seus reflexos no
deslocamento semântico de suas bandeiras, em especial de sua defesa do
"Estado cultural" e da educação como fonte de aperfeiçoamento pessoal,
a defesa da *\foreignlanguage{german}{Bildung}*, ou da educação como
cultivo individual, permanece como um forte elemento identitário. Dito
de outro modo: mesmo quando se desloca a ênfase política da defesa de
acesso universal para a restrição do acesso à educação para a elite
educada, mesmo quando a classe média educada deixa de ser um grupo
emergente com uma identidade meritocrática e uma visão de mundo
"plebeia" e se torna um grupo reacionário em defesa de sua cultura
como um privilégio com uma visão de mundo aristocrática, a bandeira da
cultura, particularmente nesse sentido de cultivo da personalidade, de
enriquecimento e autoaperfeiçoamento pessoal, permanece como o
fundamento de sua identidade enquanto classe média *educada*.

É nesse universo semântico --- em que o cultivo pessoal é o objetivo
da educação, em que a filosofia e as demais ciências se voltam para a
construção de visões unitárias de mundo e em que as disciplinas se
hierarquizam segundo critérios aristocráticos onde os conhecimentos
técnicos ou aplicados são desvalorizados como plebeus ou pedestres ---
que se deu a contorcida introdução do positivismo no pensamento
alemão.


### A oposição ao positivismo {#oposicao-positivismo}

<!--
  Se você desconfia da leitura de Bouglé, que encontrava-se talvez
  demasiadamente envolvido nas disputas e movimento da metodologia na
  Alemanha --- a sua cruzada era em busca de uma psicologia que servisse
  de fundamento para a jovem sociologia, mas que não fosse positivista
  ou, em seu vocabulário, "psicofísica", isto é: uma psicologia não
  materialista, que não reduzisse os impulsos psicológicos a estímulos
  físicos, e que pudesse oferecer às ciências sociais auxílio na
  explicação dos impulsos e motivações inconscientes dos atores sociais
  [^bougle-wundt] ---, talvez seja melhor acompanhar o conflito em torno
  de uma dessas grandes tendências descritas por Bouglé, a da oposição
  ao positivismo.
-->

Em seu clássico do pensamento social,
*\foreignlanguage{english}{Consciousness and Society}*, @Hughes se
dedica a compreender as mudanças de ênfase e significado no pensamento
social do final do século XIX. Para tanto, ele se dedica a estudar
como um grupo de intelectuais europeus --- que passam por Durkheim,
Weber, Pareto, Croce e Freud, entre outros --- renovou o pensamento
social ao rejeitar as explicações anteriores, a partir de um novo
interesse no papel do inconsciente na ação social. O foco nessa
geração dos intelectuais dos anos 1890 deve-se à suposta presença ali
de uma revolução intelectual focada na constituição de uma nova
perspectiva que teria servido de fundação para o pensamento social do
século XX (ou, pelo menos, de sua primeira metade, que é quando a obra
foi escrita). [@Hughes, p. 33]

Os elementos principais dessa virada promovida pela geração dos anos
1890 seriam: em primeiro lugar, um interesse no papel do inconsciente
na ação social, guiado pelo reconhecimento dos limites da ação
racional;[^teoria-do-conhecimento-sociologico] em segundo, a questão
do sentido do tempo e da duração, um interesse que o pensamento social
compartilha com as ciências naturais da época (onde ele resultaria na
teoria da relatividade no início do século XX, por exemplo); em
terceiro, a questão da natureza do conhecimento do mundo social, e em
especial a distinção entre conhecimento histórico e natural; e por fim
e algo como um corolário desse conjunto de problemas, a noção de que
as bases da ação política encontrar-se-iam por trás do discurso
político, num sentido muito próximo da elaboração da noção de
ideologia levada a cabo alguns anos antes por Karl Mannheim. [Em
@Mannheim-IdeologyAndUtopia, que, partindo de uma generalização da
noção marxiana de ideologia, procura estabelecer as bases de uma
sociologia do conhecimento que reconhece a "determinação situacional"
de todo discurso como seu ancoramento na vida prática imediata de seus
defensores, visando relacionar toda visão de mundo ao contexto
concreto em que surge e circula.]

O alvo principal dessa geração de "inovadores" estudada por Hughes é o
positivismo, mas num sentido bem característico que é muito pouco
específico: a noção de positivismo seria aqui mobilizada na forma de
uma categoria acusatória [^conceitos-antiteticos-assimetricos], ou
seja: longe de reconhecê-lo como uma doutrina com seus princípios e
pressupostos claramente estabelecidos, ressignificado de maneira
puramente negativa como o oposto do que se defende --- no caso, o
tripé formado pelo criticismo e as tradições idealista e histórica
alemãs [Com @Ringer-Decline] ---, atribuindo-lhe princípios que não
são os seus a partir dessa oposição.

> O ataque principal contra a herança intelectual do passado era, na
> verdade, num front mais estreito. Ele se dirigia principalmente
> contra o que os escritores dos anos 1890 escolheram chamar de
> "positivismo". Com isso, eles não se referiam simplesmente às
> doutrinas excêntricas associadas ao nome de Auguste <!--37--> Comte,
> que havia cunhado o termo originalmente. Nem se referiam a filosofia
> social de Herbert Spencer, que era a vestimenta em que o pensamento
> positivista tornara-se mais aparente em seu próprio tempo. Eles
> usavam a expressão num sentido vago para caracterizar toda a
> tendência a discutir o comportamento humano em termos de analogias
> emprestadas da ciência natural. Ao reagir contra ela, os inovadores
> dos anos 1890 sentiam estar rejeitando o princípio intelectual mais
> penetrante de sua época. [@Hughes, pp. 36-37]

Os positivismos inglês e francês não foram bem recebidos na Alemanha,
tendo sido introduzidos num contexto em que parte hegemônica do
pensamento filosófico se dedicava a combater as influências tidas como
subversivas do materialismo e do panteísmo. A acusação de positivista
era associada a opositores, que assim se viam acusados de promover
tendências subversivas materialistas, mesmo nas ciências naturais. O
positivismo foi recebido na Alemanha como uma ameaça entre outras para
à permanência da filosofia:

> A história de como o positivismo foi recebido na Alemanha de maneira
> alguma é determinada pelos interesse meramente arbitrários de
> filósofos ou cientistas individuais: era a postura fatual prevalente
> no interior das ciências individuais, com a qual a filosofia também
> tinha em larga medida de disputar, que garantiu que só a partir do
> início dos anos 1860 surgisse algum interesse mais ativo em Mill ou
> Comte, embora ambos tivessem então sido acessíveis para os alemães
> por algum tempo em tradução ou resenha. Que a filosofia alemã dos
> anos 1850 exibiu tão pouco interesse no positivismo, isso se deve à
> condição de aflição sem precedentes em que ela então se encontrava,
> pois na época se acreditava que ela estava engajada numa luta pela
> sobrevivência: contra Feuerbach e o antropologismo a que ele dera
> origem, que ameaçava substituir a filosofia, mediante uma
> naturalização universal de todo fenômeno espiritual, com pesquisas
> confinadas à ciência natural (Prantl, Harms); contra a psicologia
> empírica, natural (Drobisch, Waitz) e o materialismo que era uma
> manifestação ainda mais grosseira dela e que, além disso, exibia
> consequências políticas e
> *\foreignlanguage{german}{weltanschaulich}* \[de visão de mundo\]
> que ameaçavam até mesmo a sociedade humana enquanto tal; e,
> finalmente, contra a importação de lógicas positivistas ou indutivas
> estrangeiras, como aquelas de Herschel, Comte, Mill e Opzoomer, que
> eram igualmente incompatíveis com qualquer concepção tradicional das
> tarefas e temas da filosofia. [@Kohnke-NeoKantianismus-en,
> pp. 88-89]

O positivismo original sempre tivera uma forte direção normativa: era
a fim de promover reformas sociais racionais que se buscava, aos olhos
de Comte, as leis da vida social --- como as ciências da vida são
aplicáveis na cura de patologias biológicas, também as ciências
sociais destinavam-se a remediar as patologias sociais. Na releitura
do antipositivismo alemão do século XIX, assim, atacava-se a tendência
a emprestar abordagens e explicações das ciências naturais para a
compreensão do comportamento humano, assim como a disposição de
considerar o conhecimento assim elaborado como uma fonte legítima de
reformas sociais.

O positivismo, nesse contexto, veio se combinar a outras categorias
acusatórias que vinham sendo mobilizadas de maneiras semelhantes desde
as revoluções de 1848: materialismo e panteísmo, por
exemplo.[^simmel-positivismo] A fim de permanecer sendo empregada
dessa maneira, é interessante que se mantenha a categoria ou definição
tanto mais frouxa quanto possível; daí a maneira como essas categorias
são concebidas como "tendências" e não como uma doutrina específica
com princípios mais ou menos estabelecidos. [Há ainda hoje aqueles que
se dedicam a defender o positivismo de Comte e Spencer, inclusive do
que lhes parece uma vulgarização no positivismo lógico de Viena, como
@Turner-Positivism-HoST, que, ao tentar explicar a má fama
contemporânea do positivismo, salta de Spencer ao positivismo lógico
de Viena e atribui à sua recepção na sociologia americana mais do que
lhe caberia nesse movimento, ignorando toda a oposição ao positivismo
do pensamento alemão do século XIX]

> \[...\] os críticos do positivismo do final do século XIX não
> escreveram sobre ele em termos muito precisos. \[...\] eles pensavam
> nele mais como uma tendência intelectual difusa do que como um
> conjunto específico de princípios. Consequentemente, eles usavam a
> palavra "positivismo" de maneira quase intercambiável com um
> número de outras doutrinas filosóficas que <!--38--> consideravam
> com igual desagrado --- "materialismo", "mecanicismo" e
> "naturalismo". [@Hughes, pp. 37-38]

Assim, "positivismo" apontava para um conjunto de doutrinas
frouxamente definidas e vinculadas como que por cadeias de
"causalidade fraca" ou afinidades eletivas, associadas sobretudo ao
materialismo (normalmente o materialismo estrangeiro, particularmente
francês ou inglês) e ao naturalismo e, no fundo, se voltava à crítica
da associação da explicação dos fenômenos sociais ou históricos aos
métodos das ciências naturais.

Tal indisposição era devedora de uma certa recusa do que se percebia,
nessa busca por regularidades comportamentais, como um reducionismo
explicativo que, na medida em que abstraía a complexidade da atividade
humana de um número reduzido de regularidades observáveis, esvaziava a
ação humana de sentido e negava ao humano a liberdade que lhe
prescrevia a noção romântica de indivíduo (como veremos mais à frente,
na \autoref{tradicao-idealista}).


### Excurso: "\foreignlanguage{german}{Smithianismus}" {#smithianismus .unnumbered}

Para um exemplo mais concreto e de certa maneira fundamental para a
controvérsia metodológica que seguiria, tome-se a recepção negativa da
obra de Adam Smith (1723--1790) na Alemanha. No segundo Império, já
havia uma ciência econômica mais ou menos estabelecida no cameralismo,
a escola de política econômica estatal. É importante destacar que a
burocracia estatal alemã era poderosa, compondo, junto dos
professores, uma classe média educada politicamente vigorosa que se
manteve em posição de destaque durante a maior parte do século XIX;
[Sobre isso, cf. @Ringer-Decline, cap. 1] sua reação ao liberalismo
era naturalmente hostil:

> Considerando que os cameralistas, que ensinavam
> *\foreignlanguage{german}{Cameralwissenschaft}*---nomeada em alusão
> às *\foreignlanguage{german}{Kammer}* \[câmaras\] em que o poder
> político administrativo dos Estados deliberava questões políticas e
> econômicas---, mantinham uma perspectiva nacionalista e
> protecionista, não é excessivamente audacioso supor que a
> disseminação das visões liberais de Smith era geralmente vista por
> acadêmicos e políticos tradicionais como uma ameaça ao status quo.
> [@Montes-Smith, p. 21]

O que nos interessa desse movimento é a maneira como a obra de Smith
era desqualifica por sua associação ao materialismo francês por esses
economistas mais conservadores, em especial na sua vertente
historicista: os
fundadores da chamada Escola história de economia política, desde
Friedrich List (1789--1846), passando por Bruno Hildebrand
(1812--1878) e Karl Knies (1821--1898), viam uma incompatibilidade
entre a concepção de empatia da *Teoria dos Sentimentos Morais* e a
noção de egoísmo da *Riqueza das Nações*. Essa contradição passou a
ser chamada, de maneira bastante controversa, de "Problema Adam Smith".

O "problema Adam Smith" residiria em dois níveis distintos: o primeiro
referente à incompatibilidade entre "o foco intensamente moral das
*Teorias dos Sentimentos Morais* e a indiferença moral da *Riqueza das
Nações*"; o segundo referente à questão de "se uma sociedade virtuosa,
não apenas viável, pode constituir-se a partir das premissas
sócio-psicológicas em que Smith constrói ambos os livros".
[@Heilbroner-SocializationOfTheIndividualSmith, p. 427] Não demorou
muito para essa contradição ser atribuída a uma suposta conversão ao
materialismo promovida pelo contato de Smith com autores franceses no
intervalo entre as obras. [^associacao-smith-materialismo-frances]

O próximo passo, e o que mais nos interessa no tocante à controvérsia
metodológica, seria repudiar, a partir da rejeição das tendências
positivistas, a própria construção de teoria geral, associando-a ao
mecanicismo naturalista do positivismo. Como afirma Talcott Parsons,

> \[...\] teoria analítica *geral* foi associada a essas visões
> positivistas censuráveis --- daí a tendência a repudiá-la para os
> fins das ciências não-naturais. Talvez a mais clara expressão disso
> foi a hostilidade alemã quase universal, através do século XIX, à
> economia clássica, *\foreignlanguage{german}{Smithianismus}*, como
> era frequentemente chamada. [@Parsons-Structure, p. 476]

Os verbetes sobre "Sistema fisiocrático"
(*\foreignlanguage{german}{Physiokratisches System}*) e "Economia"
(*\foreignlanguage{german}{Volkswirtschaft}*) no @Meyers-6ed falam de
*\foreignlanguage{german}{Smithianismus}* como sinônimo da "economia
política burguesa" ou da "escola liberal"
(*\foreignlanguage{german}{Freihandelsschule}*), ou como exagero na
busca do princípio do laissez faire. A expressão, que era de fato
usada de maneira acusatória pela escola histórica em sua crítica da
economia política clássica inglesa, era restrita ao discurso político
contra o liberalismo econômico, não em sentido metodológico. Nesse
sentido estava mais para um sinônimo da expressão que Parsons também
menciona, *\foreignlanguage{german}{Manchestertum}*, que se poderia
traduzir por "manchesterismo", mas com maior carga pejorativa, e que
se referia ao liberalismo de Manchester como um
todo.[^hostilidade-manchesterismo]

Tomei esse repúdio da escola histórica alemã à obra de Adam Smith como
exemplo porque aqui já se antecipam, de certo modo, as questões
centrais que dão sentido à controvérsia metodológica --- e não no
sentido de que esse movimento de recusa da economia clássica provoca
uma reação que busca restabelecê-la. O repúdio se dirige a um conjunto
de disposições, algo fugidias e associadas por laços de afinidades
que, postos num discurso coerente, exigem alguma manobra para serem
postas lado a lado. Por um lado, recusa-se a antropologia filosófica
abstrata da economia clássica, que concebe o comportamento humano a
partir da ação econômica consciente e racional de um ator individual
cuja orientação psicológica é egoísta; por outro, uma indisposição
política (que se alimenta de uma série de fatores, nem sempre
correlatos, indo desde um certo ufanismo até a defesa da posição
privilegiada da classe média educada no segundo império) com os
liberalismos econômico e político, acompanhada de uma inclinação a
exaltar uma economia intensamente regulada pelo Estado numa visão
frequentemente paternalista. Dentre as questões metodológicas --- a
antropologia filosófica artificial do comportamento econômico
individual abstrato --- e as questões éticas e políticas --- da melhor
maneira de se administrar a economia nacional ---, a indisposição ao
pensamento econômico clássico desagua numa recusa da própria feitura
de teoria econômica.



<!-- NOTAS {{{ -->

[^bougle-wundt]: Enquanto Émile Durkheim fora profundamente
influenciado pela psicologia de Wilhelm Wundt durante sua estadia em
Leipzig --- pode-se dizer que foi ali que Durkheim abraçou o
positivismo ---, Bouglé prefere distanciar-se de Wundt e reivindicar
uma tradição anterior da psicologia dos povos, a
*\foreignlanguage{german}{Völkerpsychologie}*,
ao trabalhá-la a partir da obra de Lazarus [cf. o ensaio de
@Durkheim-SciencePositiveDeLaMoraleEnAllemagne; publicado em português
como @Durkheim-SciencePositiveDeLaMoraleEnAllemagne-pt; a
@Bougle-LesSciencesSocialesEnAllemagne]. Wundt era peça chave do
chamado círculo de Leipzig (ver nota \ref{circulo-leipzig} na
p. \pageref{circulo-leipzig}), espaço menos hostil ao positivismo na
academia alemã da época. Sua obra procura expandir o escopo da
psicologia para além da psicologia individual, associando-a à
psicologia social ou psicologia dos povos e à antropologia da época
para uma psicologia mais global, abordando ética, linguagem etc. É
esse aspecto que chama tanto a atenção de Durkheim. Ver, a respeito,
@Espagne-CerclePositivisteLeizpigWundt.

[^teoria-do-conhecimento-sociologico]: Há um interessante ponto de
encontro entre essa formulação e aquele princípio metodológico que
Bourdieu, Chamboredon e Passeron encontram na confluência do
pensamento de Marx, Durkheim e Weber e que chamam de "teoria do
conhecimento sociológico": na difícil formulação dos autores,
"Semelhante convergência explica-se facilmente: o que poderíamos
designar por princípio da não consciência, concebido como condição
*sine qua non* da constituição da ciência sociológica, é simplesmente
a reformulação na lógica dessa ciência do princípio do determinismo
metodológico que nenhuma ciência poderia negar sem se negar como tal."
Ainda, com respeito ao paradigma da liberdade da ação individual (que,
veremos, era parte fundamental do pensamento social dos intelectuais
"mandarins" alemães): "\[...\] os sociólogos que pretendem conciliar o
projeto científico com a afirmação dos direitos da pessoa, direito à
ação livre e direito à consciência clara da ação, ou, simplesmente,
evitam submeter sua prática aos princípios fundamentais da teoria do
conhecimento sociológico, voltam a encontrar, inevitavelmente, a
filosofia ingênua da ação e da relação do sujeito à sua ação aplicada
na sociologia espontânea por sujeitos preocupados em defender a
verdade vivida de sua experiência da ação social."
[@BourdieuChamboredonPasseron-Metier-pt, pp. 26, 27]

[^conceitos-antiteticos-assimetricos]: \label{gegenbegriffe} Algo como
aquilo que Koselleck chama de "conceitos antitéticos assimétricos":
tratam-se de conceitos estabelecidos unilateralmente, que descrevem o
outro somente a partir de sua negatividade (como o outro "deste" ou de
"nós"), de maneira tal que o outro não se reconheça nessa descrição.
"\[...\] a história conhece numerosos conceitos antitéticos
\[*\foreignlanguage{german}{Gegenbegriffe}*\] que servem para negar o
reconhecimento mútuo. Do conceito de si mesmo, surge uma definição do
estranho, a qual parece linguisticamente ao definido como estranho
como uma privação, fatualmente como um roubo. Assim opera um conceito
antitético assimétrico. Seu oposto é um contrário desigual."
[@Koselleck-asymmetrischerGegenbegriffe, p. 213;
@Koselleck-asymmetrischerGegenbegriffe-en, p. 156]

[^sociologia-biologia]: "Como Saint-Simon, Comte via a sociologia como
uma extensão da biologia em seu estudo de organismos. A sociologia
deveria ser o estudo da organização social, com ênfase em totalidades
sociais. \[...\] Assim, enquanto o objetivo fundamental da sociologia
era produzir leis como aquelas da astrofísica de seu tempo, o objeto
era uma extensão da biologia." [@Turner-Positivism-HoST, p. 34]

[^simmel-positivismo]: Cf. @Kohnke-NeoKantianismus-en, cap. 3. A
disposição hegemônica dos intelectuais alemães contra o positivismo
era tamanha que ele funcionava como acusação capaz de invocar
desconfiança automática sobre o pensamento de um adversário. Em mais
de uma ocasião Simmel esteve envolvido com uma desqualificação,
particularmente de sua sociologia, que envolvia o descrédito
automático invocado pela insinuação de positivista; a título de
exemplo, considere-se a maneira como os colegas Dilthey, Schmoller,
Wagner, entre outros, da Universidade de Berlim, solicitando a
efetivação de Simmel como Professor Extraordinário, descreveram a
sociologia: "Sua área de estudo, como nenhuma outra, é certamente um
antro de pseudociência (*\foreignlanguage{german}{ein Tummelplatz der
Halbwissenschaft}*). Mas precisamente porque o Dr. Simmel extraiu um
nexo de investigações úteis do indeterminado conceito coletivo de
sociologia e trabalhou-o com exatidão científica, ele distinguiu-se
dos demais sociólogos." [Citada em @Landmann-BausteineZurBiographie,
p. 24; apud @FrisbyGS, p. 15] Dilthey, que é conhecido por sua aversão
à sociologia positivista, reveria sua posição diante da definição
simmeliana; num pós-escrito a sua obra *Introdução às ciências do
espírito*, redigido entre 1904 e 1906 e destinado a uma segunda
edição, ele diz: "Minha polêmica contra a sociologia diz respeito ao
estágio de seu desenvolvimento que era representado por Comte,
Spencer, Schäffle e Lilienfeld. O conceito de sociologia encontrado em
suas obras era aquele de uma ciência da vida humana social e comunal
que também estuda o direito, a moral e a religião. Assim, não era uma
teoria sobre as formas que a vida psíquica assume dentro do quadro das
relações sociais entre indivíduos. Uma concepção de sociologia tal
como a avançada por Simmel estuda as formas sociais enquanto aquilo
que permanece o mesmo dentre variações. \[...\] Devo, é claro,
reconhecer a demarcação de tal domínio científico; sua delimitação
repousa sobre o princípio de que podemos estudar, nós mesmos relações
que permanecem constantes como formas da vida social dente variações
dessa vida e seu conteúdo. \[...\] Meu ataque contra a sociologia,
assim, não pode ser dirigido a uma disciplina deste tipo, mas é melhor
direcionado a uma ciência que procura compreender *numa* ciência tudo
o que ocorre *de facto* dentro da sociedade humana."
[@Dilthey-Einleitung-en, pp. 487-498] Vale dizer porém que Dilthey
representaria um antipositivismo mais sofisticado que o então difuso
entre os "mandarins" alemães porque procura dirigir-se ao positivismo
em seus próprios termos, isto é, sem recorrer à metafísica [pelo menos
segundo @Hughes, pp. 90-91]

[^mandarinato-ref]: Sobre a definição ver nota \ref{def:mandarinato},
na p. \pageref{def:mandarinato}, assim como a introdução de
@Ringer-Decline. Ver também: "Assim, para se impor nas lutas que a
opõem a outras frações dominantes, nobres de espada e também burgueses
da industria e dos negócios, a nova classe, cujo poder e autoridade
repousam sobre o novo capital, o capital cultural, deve alçar seus
interesses particulares a um grau de universalização superior, e
inventar uma versão que podemos chamar de 'progressista' (em contraste
com as variantes aristocráticas que os funcionários alemães e os
funcionários japoneses inventaram um pouco mais tarde) da ideologia
do serviço publico e da meritocracia \[...\]"
[@Bourdieu-RaisonsPratiques-pt, p. 41]

[^hostilidade-manchesterismo]: "\[...\] Smith tornou-se conhecido como
o fundador da materialista 'Escola de Manchester' que pregava o
evangelho do interesse individual e da livre concorrência, em clara
oposição à tradição cameralista mais antiga que assumia que a
sociedade e seus membros precisavam de orientação." [@Montes-Smith,
p. 24] A respeito da hostilidade da economia alemã ao liberalismo de
Manchester, cf. @Montes-Smith, cap. 2.

[^associacao-smith-materialismo-frances]: Cf. @Montes-Smith, cap. 2; é
interessante notar como essa crítica que se dirigia ao que era visto
como mecanicismo etc., tanto na França quanto na Alemanha,
frequentemente se materializava na forma de atribuição desse aspecto
ao caráter nacional do outro: aquela reação tipicamente alemã aos
efeitos deletérios da *\foreignlanguage{german}{Zivilisation}*
francesa ou inglesa sobre a *\foreignlanguage{german}{Kultur}* alemã
[explorada por @Elias-Zivilisation-pt], também ocorria do lado
francês, particularmente na crítica que Durkheim recebera de alguns de
seus conterrâneos [essa questão é bem trabalhada em
@Lepenies-DreiKulturen-en, parte I]

<!-- }}} -->
