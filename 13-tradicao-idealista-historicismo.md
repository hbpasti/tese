## A tradição idealista alemã {#tradicao-idealista}


Ao tratar da reação antipositivista na Alemanha, particularmente no
que diz respeito à fundação da sociologia, @Parsons-Structure
considera fundamental compreender o movimento da tradição idealista. É
de algumas das proposições dessa tradição que muitos dos fundamentos
da reação ao positivismo poderiam ser encontrados.

> \[...\] a filiação comum do braço idealista do dilema kantiano
> resultou numa oposição comum às tendências positivistas de
> pensamento a qualquer coisa da natureza de uma "redução" dos fatos
> da vida e destino humanos aos termos do mundo físico ou do
> biológico. [@Parsons-Structure, p. 475]

Dito de outro modo, a hegemonia do idealismo como visão filosófica de
mundo, mesmo que somente como um fundamento distante, faz com que boa
parte dos intelectuais alemães evite aceitar a analogia das ciências
históricas ou sociais com as naturais, particularmente no tocante à
possibilidade do estabelecimento de leis do comportamento social
semelhantes às mobilizadas nas ciências sociais para a explicação de
fenômenos físicos, químicos ou biológicos.

Os principais fundamentos dessa repulsa à redução do humano ao tipo de
generalidade e regularidade construídos nas explicações das ciências
naturais encontram-se em dois elementos da tradição idealista alemã:
por um lado, um tipo específico de individualismo, com a concepção de
indivíduo e de humano que exige, e por outro, a noção peculiarmente
idealista alemã de *\foreignlanguage{german}{Geist}*, ou espírito.


### Um individualismo da unicidade {#individualismo-qualitativo}

Num ensaio de divulgação da *Filosofia do dinheiro*, Simmel
estabeleceu a diferença entre o que ele considerava duas formas
fundamentais do individualismo: o individualismo igualitário do século
XVIII, herdeiro da Revolução Francesa, e o individualismo
individualizante do século XIX, herdeiro do romantismo. [^fois-bib]
Mais tarde ele os nomearia, respectivamente (e para
prejuízo[^prejuizo] da forma mais antiga), de individualismos
"quantitativo" e
"qualitativo"[^individualismo-quantitativo-qualitativo-refs], a fim de
enfatizar, entendo, a sua oposição.

A contradição, que não fica evidentemente somente com o Romantismo,
mas já no liberalismo clássico inglês, entre os ideais revolucionários
da igualdade e liberdade é o ponto de partida dessa oposição: a
igualdade limita a liberdade na medida em que nivela os indivíduos
segundo o que têm em comum, abstraindo as suas diferenças naturais; a
liberdade, por sua vez, tende a suprimir a igualdade na medida em que
dá livre expressão às diferenças naturais. [@FoIs, p. 49] Simmel
acrescenta:

> Foi talvez um instinto para essa situação que fez somar à liberdade
> e à igualdade, como uma terceira demanda, a fraternidade.  Pois
> somente mediante o altruísmo manifesto, mediante a renúncia ética à
> afirmação das vantagens naturais, é que a igualdade seria restaurada
> depois que a liberdade a tivesse suprimido. [@FoIs, p. 49]

Essa contradição conceitual, contudo, desempenhou um importante papel
histórico de dissolução dos laços de sociabilidade medievais,
franqueando o indivíduo de uma série de opressões. Mas com isso, a sua
liberdade de realizar sua individualidade como um todo se viu limitada
pelo ideal da igualdade, e os indivíduos viram-se assim reduzidos à sua
humanidade abstrata.  O indivíduo liberado por esse ideal iluminista
da igualdade não tem sua dignidade e valor associados a si mesmo, mas
àquilo que compartilha com todos os demais, isto é: à natureza humana,
à fagulha de humanidade que reside em todas as pessoas. Todo o valor
do indivíduo, assim, é atribuído a um indivíduo abstrato, não às
pessoas concretas, em sua própria individualidade.

É com ênfase no ideal da liberdade e da concepção do valor das
diferenças individuais que um outro tipo de individualismo teria
surgido, então, no século XIX: o individualismo qualitativo ou da
unicidade, que concebe os indivíduos como valorosos em si mesmos, em
suas particularidades concretas.

> Depois que a dissolução fundamental do indivíduo dos grilhões
> enferrujados da guilda, da posição hereditária, da igreja, foi
> concluída, ela segue agora pelo fato de que os indivíduos assim
> tornados independentes querem também se diferenciar uns dos outros;
> ela não se basta ali, onde se é um singular mais livre em geral, mas
> onde se é este indivíduo determinado e inconfundível. A formação
> ideal do século XVIII demandava indivíduos isolados e essencialmente
> iguais, que eram mantidos juntos mediante uma lei racional universal
> e através da harmonia natural de interesses. A característica do
> século XIX conta somente com indivíduos verdadeiramente
> diferenciados segundo a divisão do trabalho, mantidos juntos
> mediante organizações que repousam de fato sobre a divisão de
> trabalho e o acoplamento dos diferenciados. Para a realidade da
> economia moderna, ambos os princípios desenvolveram-se de maneira
> inextricável. [@FoIs, p. 52]

Foi a partir da "enorme decepção" com a realização da liberdade após a
Revolução Francesa que as duas correntes do ideal revolucionário se
separaram em duas tendências ideológicas opostas, uma herdeira do
ideal iluminista, igualitária, outra herdeira do ideal oitocentista,
individualizante. Simmel atribui a formação do individualismo
qualitativo a uma linhagem que, partindo de Schleiermacher, se
realizou em Goethe tendo, por fim, sua "expressão filosófica absoluta"
[@FoIs, p. 54] em Nietzsche. [Simmel faz uma rápida comparação entre
as éticas de Nietzsche e Kant que vem ao encontro dessa formulação em
@Simmel-NietzscheUndKant-orig]

> Essa união de liberdade e igualdade ou, dito em outras palavras, da
> individualidade e igualdade, agora, no século XIX, se dividiu em
> duas correntes completamente divergentes. Em conceitos gerais que
> requerem muitas reservas, chamemo-las de tendência à igualdade sem
> individualidade e à individualidade sem igualdade. A primeira,
> incorporada essencialmente no socialismo, está aqui fora de nosso
> interesse; a outra criou o tipo de individualismo que, diante da
> sobrevivência da anterior, pode ser considerada como a
> especificamente moderna e se desenvolveu a partir de Goethe através
> de Schleiermacher e os românticos até o nietzscheanismo. [@FoIs, p. 52]

Quando pensamos na tradição idealista, particularmente nas formas que
ela adota no final do século XIX, é preciso ter em mente que sua visão
de mundo é engendrada por esta concepção "qualitativa" do
individualismo.


### Um espírito unificador {#geist}

Embora o noção idealista de "espírito" ou
*\foreignlanguage{german}{Geist}* fosse também aplicada à ordem
natural, ela era especialmente apta para conceber o universo de
realizações humanas. "Na tradição idealista, mesmo o universo natural
é em algum sentido a criação do \foreignlanguage{german}{geist}. Mas
num modo muito mais direto e significativo, é o mundo da história e da
cultura em que o \foreignlanguage{german}{geist} se expressa ou se
realiza." [@Ringer-Decline, p. 311]

Alimentado pela crítica kantiana que, a fim de salvaguardar a ciência
da metafísica, promovia o divórcio entre os fenômenos acessíveis pela
percepção e o mundo das "coisas-em-si" inacessíveis, o idealismo
procurava, assim, com essa noção de *\foreignlanguage{german}{Geist}*,
estabelecer as condições para o reencontro entre as aparências e as
coisas, postulando-o como uma espécie de consciência transcendental
onde observação e coisa observada se reúnem: é como se a distinção
crítica entre fenômeno e coisa-em-si encontrasse nesse "espírito" um
ponto de afluxo, onde os dados da experiência sensível e a essência
das coisas coincidiam numa ideia mais elevada.[^definicao-geist]

Essa tradição idealista e particularmente essa noção de "espírito"
repousam mais do que parcialmente sobre aquele segundo tipo de
individualismo de que Simmel falava, o qualitativo. É naquela
atribuição de dignidade e valor ao indivíduo singular pela chave de
sua unicidade que residiam os vínculos de afinidade dessa visão para
com as disciplinas humanísticas.

> \[... No idealismo,\] o humano como um ser ativo, propositivo, um
> ator, não deveria ser tratado pelas ciências do mundo fenomênico nem
> mesmo por seus métodos analíticos, generalizantes. Nessa esfera
> \[ideal\], o humano não estaria sujeito à lei no sentido físico, mas
> seria livre. Uma apreensão intelectual de sua vida e ação poderia
> ser obtida somente pelos métodos especulativos da filosofia,
> especialmente por um processo de intuição de totalidades
> (*\foreignlanguage{german}{Gestalten}*) que seria ilegítimo ser
> quebrado por uma análise "atomística". [@Parsons-Structure, p. 475]

Dito de outro modo: uma vez que o novo individualismo enfatiza a
liberdade sobre a igualdade, atribui ao humano uma liberdade de ação
essencial, que substitui a igualdade abstrata do individualismo
anterior; nesse movimento que eleva a dignidade do indivíduo,
instauram-se disposições que servem de barreira à concepção de leis de
comportamento humano à maneira proposta pelo positivismo, inspirado
pelas ciências naturais.

A influência do idealismo sobre a concepção das tarefas, objetivos e
métodos das humanidades --- em especial nesse momento em que o
positivismo promove como que uma redefinição dos campos disciplinares
--- entre os acadêmicos alemães do final do século XIX não pode ser
suficientemente enfatizada: a própria classificação das disciplinas,
no contexto de difusão da expressão
*\foreignlanguage{german}{Geisteswissenschaften}* ("ciências do
espírito") para se referir às humanidades, que passa por uma série de
reformulações no período e que é objeto de extenso debate entre
epistemólogos (particularmente os neokantianos) "parece implicar numa
abordagem idealista às disciplinas humanísticas que ela representa."
[@Ringer-Decline, p. 96]

As noções desse indivíduo digno, único, e do espírito conciliador
tocam-se em muitos pontos, numa interação de efeito recíproco, para
falar com Simmel, em que uma alimenta a outra. Assim como se pode
admitir, a despeito da ocorrência cronológica, que a noção de
*\foreignlanguage{german}{Geist}* é que dá sentido e propósito a esses
indivíduos ricos e valorosos em suas diferenças, também a própria
concepção de tal dignidade dos indivíduos demanda e alimenta essa
noção de um espírito conciliador.

> Contra o mecanicismo, individualismo, atomismo, ela \[a tradição
> idealista alemã\] pôs o organicismo, a subordinação da unidade,
> incluindo o indivíduo humano, ao todo. Contra a continuidade
> essencial em seu campo de estudo, que olhava para casos particulares
> como instâncias de um princípio ou lei geral, ela enfatizou a
> individualidade irredutivelmente qualitativa dos fenômenos que
> estudava e havia emitido um relativismo histórico abrangente.
>
> \[...\] O pensamento positivista sempre dirigiu seus esforços à
> descoberta de relações causais nos fenômenos; o pensamento
> idealista, à descoberta de relações de sentido, de
> *\foreignlanguage{german}{Sinnzusammenhang}* \[contextos de
> significado\]. Com essa diferença caminhou a do método --- por um
> lado, explicação causal teórica, por outro, interpretação de
> sentido, *\foreignlanguage{german}{Sinndeutung}*, que viu nos fatos
> concretos de seu campo símbolos, cujos sentidos <!--486--> devem ser
> interpretados. A ordem e sistema dos fenômenos sociais foi uma ordem
> significativa, jamais uma causal. [@Parsons-Structure, p. 485-486]

As consequências dessa indisposição prévia com o positivismo e desse
encontro conflituoso tornariam central a questão da distinção entre as
ciências naturais e as ciências humanas (ou do espírito, culturais,
sociais, históricas etc.). A essa questão de demarcação dedicaram-se
os epistemólogos e filósofos da ciência neokantianos ao longo dos anos
1880 e 1890 (e a acompanharemos mais à frente, na
\autoref{ciencias-humanas-naturais}).




<!-- NOTAS {{{ -->

[^fois-bib]: \label{fois-refs}@FoIs-orig; reimpresso em @FoIs. Esse
ensaio recebeu pelo menos duas novas formulações: num ensaio sobre o
individualismo de Kant [@KaIs-orig; reimpresso em @KaIs] que é uma
versão da décima sexta aula de seu livro de aulas sobre Kant
[@Simmel-Kant-orig; reimpresso em @Simmel-Kant]. A discussão é
retomada, embora rapidamente, no cap. 10 da chamada "grande"
*Sociologia* [@Soz-orig; reimpressa em @Soz]. O texto é também
retomado em numa palestra de 1910 [@IFrht-orig; do qual consta uma
tradução brasileira em @IFrht-pt; publicada postumamente, esta versão
teve uma lacuna preenchida com trechos da versão mais tardia, presente
no último capítulo da "pequena" sociologia, *Questões fundamentais da
sociologia*, @orig-GdSoz; da qual consta uma tradução brasileira em
@GdSoz-pt; na *\foreignlanguage{german}{Gesamtausgabe}*, corrigiu-se o
equívoco, atribuindo ao manuscrito póstumo uma data precisa: ele era o
manuscrito de uma palestra apresentada em março de 1910 em Munique, e
tratava-se de uma discussão do ensaio mais antigo, de 1901 --- essa
nova versão do manuscrito encontra-se em @NLImZ]. Por fim, há uma
versão mais ufanista da distinção, escrita durante a Primeira Guerra,
que atribui ao individualismo qualitativo um caráter "germânico" [em
@Is-orig; reimpresso em @Is]. Sobre a história desse ensaio, ver
@EB-GSG20, pp. 544-545 e @EB-GSG7, p. 363.

[^prejuizo]: Minha insistência no "prejuízo" do individualismo
"quantitativo" é devida ao fato de que os assim individualistas, seus
defensores, não se nomeariam dessa maneira --- é um conceito antitético
assimétrico, concebido, assim, somente em sua negatividade enquanto
outro do individualismo qualitativo, a que Simmel dá a entender se
subscrever [seria essa a sua forma "especificamente moderna" --- cf.
@FoIs-orig, p. 400; @FoIs, p. 52], enquanto a outra, a antiga, é deixada para
se desdobrar entre os socialistas  --- uma visão que é posteriormente
nuançada, ao lado de uma aparente maior afinidade (ou menor repulsa?)
de Simmel ao socialismo [cf. @GdSoz-pt, cap. 4, esp. pp. 94-95].

[^individualismo-quantitativo-qualitativo-refs]: "\[...\] pode-se
descrevê-los como o individualismo da particularidade e o da
unicidade, ou como o quantitativo e o qualitativo \[...\]",
[@Simmel-Kant-orig, p. 180; reimpresso em @Simmel-Kant, p. 226] ou,
posteriormente: "O romantismo foi talvez o mais vasto canal através do
qual este individualismo --- poder-se-ia denominá-lo o qualitativo
frente ao numérico do século XVIII, ou o da unicidade frente o da
particularidade --- fluiu na consciência do século XIX." [@NLImZ,
p. 256; @IFrht-pt, p. 116]

[^definicao-geist]: "A palavra alemã *\foreignlanguage{german}{Geist}*
\[...\] significava não somente 'mente' mas também 'espírito' ou
'alma'. Na obra formal de alguns idealistas,
*\foreignlanguage{german}{Geist}* representava o pensamento coletivo
da humanidade e às vezes mesmo uma consciência transcendental que
garantia a concorrência de aparência e realidade." [@Ringer-Decline,
p. 95]

<!-- }}} -->
