## Para a metodologia das ciências sociais {#mthdk}


Em sua resenha da obra de Stammler, o ponto de partida é o mesmo de
muitos de seus escritos metodológicos: por que se ocupar da "fundação,
do sentido e da validade" do conhecimento? O posicionamento de Simmel
nesse tocante evoca o do pragmatismo americano na medida em que a
resposta tende a se voltar para o papel prático do conhecimento. A
reflexão metodológica é, nesse sentido, provocada por algum movimento
que põe em cheque a legitimidade do conhecimento e a crença em sua
validade. Assim, o ceticismo provoca a crítica kantiana, e também
assim, as ciências sociais parecem exigir uma crítica semelhante.
Nesse sentido, Simmel parece concordar com Stammler: uma crítica do
conhecimento das ciências sociais é necessária para que ela possa ser
mobilizada para atender às "necessidades urgentes do tempo", a chamada
"questão social". [Segundo @Waizbort, particularmente na parte II,
Simmel se distancia de suas inclinações socialistas a partir de meados
dos anos 1890 --- este escrito é de 1896]

> Assim, o conhecimento das ciências sociais parece hoje requerer uma
> crítica fundamental de si mesmo, já que sua interminável
> controvérsia sobre os seus conteúdos só revela inequivocamente uma
> coisa: sua inadequação às necessidades urgentes do tempo, que
> teriam, de fato, de apelar a esse conhecimento antes de tudo.
> [@Mthdk-orig, p. 575]

Ao falar da controvérsia sobre os "conteúdos" que serve de impedimento
para que as ciências sociais desempenhem o seu papel na solução dos
problemas sociais, creio, Simmel se refere antes de à controvérsia
metodológica em si, à disputa sobre a classificação das ciências
sociais e naturais (que discutimos na
\autoref{ciencias-humanas-naturais}); o ensaio, no entanto, se dedica
à controvérsia sobre métodos (da qual aquela questão é uma parte
significativa, vale dizer), nos termos em que ela acabou sendo posta
pela disputa entre Menger e Schmoller.

> As grandes oposições partidárias de toda vida epistêmica
> encontram-se também aqui em primeiro plano. De um lado, um empirismo
> que se apraz com a constatação de fatos, com a descrição histórica
> da vida social; de outro lado, uma sistemática construtiva, que
> parte de conceitos gerais e, de seu desenvolvimento, espera pela
> verdade, que só pode ser verificada a partir dos fatos de maneira
> retrospectiva e incompleta. [@Mthdk-orig, p. 576]

O problema que Simmel usa Stammler para abordar, portanto, não passa
da controvérsia metodológica, em especial na maneira como a disputa
entre economistas a posterizou: método dedutivo contra indutivo,
abordagem nomotética contra idiográfica, orientação exata contra
realista --- ou, como aqui, com Simmel: empirismo contra sistemática.

O recurso a Kant, aqui, reaparece no sentido de demonstrar quanto o
conhecimento empírico é dependente do teórico: Kant demonstrou que o
conhecimento da natureza não é imediato, mas que a própria experiência
"não é uma aceitação passiva das impressões fatuais, mas um tratamento
delas mediante categorias que nos são apriorísticas." [@Mthdk-orig, p.
576] Simmel relembra a maneira como procurou aplicar à história essa
mesma abordagem quatro anos antes, em seu *Problemas da filosofia da
história* [@PG-1892-orig; @PG-1905-orig; @PG-1907-orig]:
"demonstrando o quanto a pesquisa histórica depende também de
pressupostos apriorísticos, enquanto ela acredita proceder de modo
puramente empírico, e quanto o supraempírico se coloca em sua
atividade aparentemente exata." [@Mthdk-orig, p. 576]

> \[...\] "história" significa a formação do evento imediato, apenas
> vivenciado de acordo com os a priori do espírito cientificamente
> formado, da mesma maneira que "natureza" significa a formação do
> material sensível mediante as categorias do entendimento.
> [@Simmel-Selbstdarstellung]

Seguindo o argumento de Stammler nesse tocante --- Simmel reconhece
que a problematização em ambos é semelhante, embora em direções
distintas ---, Simmel parece concordar com a precedência lógica da noção
de sociedade e de legalidade de Stammler. Este defende, contra o que
concebe como a concepção materialista da história, que antes de se
formar como uma superestrutura que reflete relações econômicas, a
ordem jurídica é que serve de fundamento para a sociedade na medida em
que não se poderia falar em economia social sem algum tipo de ordem
legal que instituísse a possibilidade de socialização da economia.

> A lei causal não pode ser deduzida da experiência --- porque forma,
> antes, esta última da matéria crua das impressões sensíveis --- na
> mesma medida em que o direito pode surgir da economia: pois ou
> economia significa a mera técnica e, nesse sentido, não seria
> assunto social, ou ela significa a economia social e isso resulta,
> em geral, primeiramente como manifestação de um material técnico em
> *determinadas formas legais*. Por isso, direito e economia não
> ficam, assim definidos, na relação de uma interação: as regras
> legais constituem, antes, o aspecto formal do objeto unitário das
> investigações em ciências sociais, a vida social, cuja mera matéria
> é a produção técnica. [@Mthdk-orig, p. 577]

O ponto de concordância reside na prioridade lógica da "teoria" sobre
a experiência, relação que é semelhante àquela entre direito e
economia explorada por Stammler: assim como a "lei causal" tem
precedência lógica sobre a economia --- a economia concebida como
economia social (ao contrário do que como mera técnica) só pode
existir onde uma ordem legal estabeleça as condições de sociabilidade
para sua existência --- também a experiência surge formada pela "lei
causal" da "matéria crua das impressões sensíveis".

> A tarefa, portanto, não será mais: explicar, a partir das mudanças
> da matéria econômica, as mudanças do direito, como "superestrutura"
> --- mas, mais completa e profundamente, explicar essas mudanças a
> partir dos fenômenos específicos que evocam as transformações
> técnico-econômicas mediante a sua introdução numa ordem legal já
> dada e que, mantendo-se constante todo fator material, seriam
> completamente diferentes se a constituição legal em que se
> encontrassem fosse outra. [@Mthdk-orig, p. 578]

Aqui Simmel já parece apontar para o argumento de que não se trata de
uma alternativa entre uma via empírica indutiva ou uma via nomológica
dedutiva, como a *\foreignlanguage{german}{Methodenstreit}* entre
economistas polarizava o debate: a experiência não existe sem
princípios a priori que atuem na seleção dos dados relevantes (os
quais, por sua vez, frequentemente tratam-se de conhecimentos obtidos
de experiências anteriores que apenas atuam como a prioris para o
conhecimento em questão).

A crítica de Simmel a Stammler concentra-se na segunda seção de sua
resenha e diz respeito mais à maneira como Stammler conceitua
"sociedade" (e "regulação", que serve de base para sua noção de
sociedade) do que à metodologia (tocando, porém, nesse ponto, na
medida em que Stammler é inconsistente [Como Weber enfadonhamente
demonstra no começo da terceira parte de seu ensaio, @Weber-Stammler,
ao listar uma série de ocorrências incoerentes de "regularidade"
(*Gesetzmäßigkeit*) em Stammler] na aplicação do princípio anterior à
sua noção de sociedade, particularmente na insistência do papel de uma
ordem reguladora na gênese da sociedade --- com o que Simmel
discorda). Esse trecho é, talvez, o mais importante do ensaio para o
pensamento de Simmel, particularmente o seu pensamento sociológico:
aqui reaparece sua definição de sociedade --- "sociedade está ali onde
vários indivíduos entram em interação"[^conceito-sociedade-mthdk-psoz]
--- de seu ensaio fundador de 1894, *O problema da sociologia*
[@orig-PSoz; reimpresso em @PSoz; consta uma tradução brasileira em
@PSoz-pt], e algumas de suas consequências são exploradas; mas no
tocante à metodologia, ou pelo menos no que se refere à controvérsia
metodológica, a primeira e última seções são mais relevantes e é a
eles que vou me ater.

O mais importante de todo este escrito porém é que aqui Simmel de fato
dirige-se à controvérsia entre economistas, explicitamente defendendo
seu relativismo como uma saída para o impasse entre abordagens
exclusivamente empíricas ou sistemáticas:

> Se se toma o olhar relativista, que aqui defendo, de maneira só
> suficientemente elevada e abrangente, ele deve conter em si mesmo os
> complementos que procuram ter suas formas mais baixas no oposto às
> teorias racionalistas e absolutistas. Assim, por exemplo, a oposição
> das escolas histórica e dogmática na economia política seria
> resolvida como segue. De cada "lei" econômica, pode-se assumir que
> sua validade pode ser derivada das condições históricas específicas
> da situação econômica, seu conhecimento derivado daquele da situação
> econômica da época. Mas esse processo histórico só é compreensível
> sob o pressuposto e com a utilização de certas proposições e
> conceitos *objetivamente* válidos que formam o a priori daquela
> dedução histórica. Estes, por outro lado, repousam num
> desenvolvimento histórico anterior que repousa mais aquém; e este,
> por sua vez, carece, em seu estado (tanto em e para si, como no
> conhecimento), de certas <!--585--> normas mais simples,
> objetivamente válidas etc. até o indeterminado. [@Mthdk-orig, pp.
> 584-585]

Simmel extrapola esse contexto do debate metodológico entre
economistas para tratar da oposição entre as abordagens empírica ou
dogmática, idiográfica ou nomotética, realista ou exata etc. em
geral, da perspectiva da teoria kantiana do conhecimento e, assim,
como discurso metodológico geral ou uma teoria da ciência --- não
apenas as ciências históricas.[^pg-vs-phg] Mas, com o olho na
metodologia das ciências sociais e alterando um pouco a linguagem de
Simmel, pode-se, então dizer: os conceitos das ciências sociais,
porque elaborados para dar conta de uma realidade histórica única e,
por isso, irremovível de seu contexto específico, referem-se, sempre,
a essas condições históricas específicas[^influencia-passeron], que só
são conceituadas, por sua vez, dentro de um quadro teórico que
"funciona como a priori" na sua constituição como objeto do
conhecimento, o qual, também por sua vez, é constituído anteriormente,
visando dar conta e a partir da referência a outro contexto histórico
--- e assim por diante, ao infinito. "O
*\foreignlanguage{latin}{regressus in infinitum}* aqui é a completa
expressão legítima para a imperfeição de nosso conhecimento quando
tenta ir além de qualquer estado dado", diz Simmel pouco à frente.

Esse movimento infinito de, por assim dizer, "'vaivém' entre
contextualização histórica e raciocínio experimental" [Para falar com
@Passeron-Raisonnement-pt, cap. 3, esp. pp. 87ss] tem dois aspectos
importantes: por um lado, trata-se de compreender cada conhecimento
específico como passível de funcionar como a priori de configurações
ulteriores, assim como resultado de semelhantes formações anteriores;
por outro, esse processo pode ser concebido como um processo infinito.


### A prioris relativos {#aprioris}

Em seu *Problemas da filosofia da história*, Simmel falava de
proposições empíricas atuando como formas a priori do conhecimento de
"províncias inteiras do conhecimento", da mesma maneira aqui descrita,
e em seu papel na formação do conhecimento histórico. Esse é, diante
da interdição kantiana da história à categoria de ciência
propriamente, um requisito para legitimar a cientificidade do
conhecimento das ciências históricas.

> Ele \[Kant\] demonstrou que todo conhecimento, que segundo a opinião
> ingênua se projetava das coisas em nós, os receptores passivos,
> resulta de uma função do entendimento que, através de suas formas
> trazidas a priori, molda a totalidade do conteúdo do saber. Porém,
> essa extensão formal pode facilmente tornar-se uma restrição
> objetiva, se se esquece que as funções espirituais que Kant
> descreveu como os a priori do conhecimento devem valer
> exclusivamente para o conhecimento existente das *ciências
> naturais*. \[...\] Mais essencial porém é ver que o a priori
> kantiano que "torna possível a experiência em geral" é somente o
> nível mais extremo de uma série cujos níveis mais profundos alcançam
> fundo nos domínios particulares da experiência. Proposições que como
> se vistas de cima são empíricas, isto é, que representam um emprego
> das formas mais gerais de pensamento em materiais especiais, podem
> funcionar para províncias inteiras do conhecimento como a prioris.
> Elas atuam como formas de ligação
> \[\foreignlanguage{german}{Verbindungsformen}\] servindo àquela
> capacidade peculiar do espírito que, por meio do modo de ordenar,
> afinar e enfatizar, pode modelar todo conteúdo dado nas formas
> definitivas mais variadas. Essas ligações que, expressas na forma de
> proposições, aparecem como pressupostos apriorísticos, permanecem
> inconscientes na medida em que a consciência, em geral, se dirige
> mais ao dado, ao relativamente externo, do que a sua própria função
> interna. [@PG-1907, pp. 237-238; @PG-1907-es, pp. 17-18]

Um interessante paralelo pode ser traçado com a maneira como o próprio
Kant conceberia as leis do movimento, enquanto "constitutivas do
quadro espaçotemporal da teoria newtoniana", "contando como a priori"
[@Watkins-KantsPhilosophyScience]: na medida em que determinado
conhecimento empírico é constitutivo da maneira como os fenômenos são
abordados, ou seja, ao constituir um quadro teórico que recorta,
ordena e classifica os dados em objetos do conhecimento, ele atua como
uma forma a priori, de maneira semelhante como as formas a priori de
toda a experiência descritas por Kant em sua *Crítica da razão pura*.

Não é uma leitura estritamente ortodoxa de Kant,[^aprioris-boudon] mas
uma no espírito do neokantismo que pretende superar alguns de seus
limites. Para Kant, um julgamento que é a priori é logicamente
"independente de toda experiência e mesmo de todas as impressões dos
sentidos. A independência em questão é lógica. Dois julgamentos são
logicamente independentes se nenhum deles implica o outro ou o seu
contraditório". [@Korner-Kant, p. 19] A distinção entre julgamentos a
priori e a posteriori, em Kant, diz respeito à dependência lógica de
um julgamento da experiência, ou seja, se o julgamento permanece
válido mesmo a despeito da possibilidade de uma
experiência.[^kant:a-priori-a-posteriori]

Simmel parece utilizar-se dessa distinção de maneira mais analógica
que direta: ele concebe a distinção entre conhecimento empírico e a
priori como uma diferença na maneira como concebemos aquele
conhecimento ou, dito de outro modo, mediante a sua função. Simmel
como que relaxa a definição dos julgamentos sintéticos a priori de
Kant para observar a função que eles exercem no processo de
conhecimento: eles servem como fundamento, como crenças básicas na
atribuição de valor de verdade para outras proposições e, por essa
basicalidade não podem ser assumidos senão sob a forma de conhecimento
a priori. Assim, certas proposições passam a atuar como a prioris na
medida em que seu caráter empírico passa a ser ignorado, esquecido ou
recalcado --- tudo se passa como se aqueles conhecimentos fossem
puramente racionais, e não mais empíricos, sua "indexicalidade" como
que reificada.

Nesse processo de reificação ou coisificação de conhecimentos
empíricos que lhes atribui legitimidade de conhecimentos a priori,
fazendo com que sirvam de critério para a seleção e construção de
objetos, esses saberes adotam, diante das "aspirações singulares" que
os defrontam tanto como objetos de conhecimento como de atuação
prática, o caráter de algo objetivo.

> Toda época, é sabido, possui um tesouro especialmente testado de
> convicções e tendências --- no prático como no teórico --- que se
> torna o critério de todas as representações e aspirações singulares
> que ocorrem, e, assim, as representa como subjetivas diante do
> objetivo. Esse complexo de critérios, porém, como surgiu
> historicamente, está sujeito ainda à reorganização, por um lado
> mediante o processo ainda muito pouco esclarecido do
> autodesenvolvimento orgânico dos conteúdos sociopsicológicos, por
> outro, mediante o fato de que esse complexo contém elementos que são
> diversamente enfatizados e estes crescem de posições dominantes e,
> por isso, podem alterar o caráter do todo. Assim, ergue-se, acima do
> até aqui mais elevado e objetivo, através de correções imanentes ou
> externas, uma nova camada de máximas definitivas, que agora
> torna-se, por sua vez, critério das que a antecedem. [@Mthdk-orig,
> p. 583]

Simmel brinca com a maneira como a subjetividade e a objetividade do
conhecimento se transformam uma na outra na medida em que eles
funcionam como conhecimento empírico ou a priori no processo de
conhecimento do real: ou, dito de outro modo, sua subjetividade ou
objetividade é *relativa*, dependendo da *função* que exercem no
processo cognitivo. "A separação entre sujeito e objeto não é tão
radical como faz crer a divisão absolutamente legitimada sobre essas
categorias tanto no mundo prático como também no científico." [@PHG,
pp. 29-30]

O conhecimento é essencialmente subjetivo apenas na medida em que não
é uma qualidade associada aos objetos, mas somente a realidade
constituída conforme critérios de apreensão do sujeito que os conhece.
Mas essa subjetividade não deve ser confundida com a subjetividade
individual do sujeito, pois ela tem caráter de objetividade para o
indivíduo.

> Não é necessária a menção de que esta subjetividade não tem nada a
> ver com aquela de que a totalidade do mundo é "minha representação".
> Pois a subjetividade que é predicada do valor coloca-o em oposição
> aos objetos dados, prontos, de maneira completamente indiferente a
> em que maneira estes tenham surgido. Dito de outro modo: o sujeito
> que contém todo objeto é diferente daquele que os defronta; a
> subjetividade, que o valor compartilha com todos os outros objetos,
> não entra em questão aí. Sua subjetividade também não pode ter o
> sentido de arbitrariedade: toda aquela independência do real não
> significa que a vontade poderia distribuí-lo com liberdade
> irrestrita e caprichosa aqui e ali. Pelo contrário, a consciência
> descobre-o como um fato que ela pode modificar diretamente tão pouco
> quanto a realidade. [@PHG, pp. 28-29]


#### Teoria do valor e teoria do conhecimento {#valor-conhecimento}

Recorro aqui, para clarear a teoria do conhecimento de Simmel que me
parece estar por trás deste escrito metodológico, a uma parte de sua
obra posterior *Filosofia do dinheiro*[^phg-refs], particularmente à
teoria do valor elaborada em seu capítulo inicial. Por quê? Em
primeiro lugar, deve-se compreender este escrito, como outros do
período entre 1889 e 1900  --- foi em 1889 que Simmel apresentou a
palestra "Para a psicologia do dinheiro" [@orig-PsyG; reimpresso em
@PsyG; com tradução braisleira em @PsyG-pt_br] que foi o pontapé
inicial para o livro que viria à luz onze anos depois [Isso é ponto
pacífico na literatura e é bem documentado na correspondência do
autor. Ver, a esse respeito, @CantoMila-PsyGzurPHG; e @EB-PHG, onde se
lê: "A primeira referência conhecida a um tratamento monográfico é
encontrada numa carta a Célestin Bouglé de 22 de junho de 1895: 'No
momento, trabalho numa ‘Psicologia do dinheiro’ que espero será
concluída no próximo ano'" (p. 726); a carta encontra-se em @GSG22,
pp. 149-153] --- como parte do que Waizbort chama o "complexo de
textos da filosofia do dinheiro" [@Waizbort, passim, especialmente pp.
42-43, 146]: um conjunto de escritos que gravitam em torno da temática
da obra --- mais evidentemente, modernidade, cultura e reificação, mas
também e talvez sobretudo, relativismo. Embora esta resenha não figure
na reconstrução do complexo de textos descrito por Waizbort, creio que
aqui se tem um importante ponto de encontro entre os complexos de textos
da filosofia da história e da do dinheiro, que são escritas na mesma
década de 1890. Outro ponto, mais importante, é que a teoria do valor
elaborada por Simmel, porque não é uma teoria do valor econômico, mas
uma teoria do valor em geral --- do qual o valor econômico, como o
moral, o estético ou o epistêmico, é, para Simmel, uma instância ---,
é calcada numa teoria do conhecimento. É em analogia à epistemologia
kantiana que Simmel concebe a teoria do valor e boa parte de sua
exposição diz mais respeito à epistemologia que à axiologia
propriamente.

No centro do relativismo de Simmel está a separação entre duas ordens
distintas de compreensão da totalidade do mundo, que mesmo quando
classificam as mesmas coisas, não coincidem jamais, porque dizem
respeito a duas esferas distintas de classificação: as ordens ou
séries do ser e do valor.

A ordem do ser é a ordem da unidade geral da natureza, onde o
pressuposto é a igualdade fundamental assentada numa essência comum;
na ordem dos valores, por outro lado, concebemos as coisas em sua
individualidade, classificando-as mediante as diferenças que
estabelecem as distâncias entre elas, e ordenando-as numa hierarquia
que não guarda qualquer relação com sua ordenação natural.

> A ordenação das coisas, em que elas se apresentam como realidade
> natural, repousa sobre o pressuposto de que toda a multiplicidade de
> suas qualidades advém de uma unidade da existência: a uniformidade
> das leis naturais, a soma persistente de matéria e energia, a
> convertibilidade entre fenômenos os mais variados conciliam as
> distâncias da primeira vista numa relação universal, numa
> equivalência de tudo. \[...\] Entretanto, sem consideração por sua
> ordenação naquela série, arranjamos suas imagens internas numa
> outra, em que a igualdade é completamente rompida, na qual a mais
> alta elevação de um ponto fica ao lado da mais firme depressão de
> outro e cuja essência mais profunda não é a unidade, mas a
> diferença: a ordenação segundo *valores*. [@PHG, p. 23]

"Que objetos, ideias, eventos sejam valorosos, isso não é nunca lido
de sua existência e conteúdo meramente naturais; e seu ordenamento
levado a cabo conforme os valores se distancia do natural ao extremo"
[@PHG, p. 23] --- com o que não se deve imaginar que haja uma relação
de oposição ou complementariedade quaisquer entre as duas ordens: "Com
isso não se pretende uma oposição em princípio e uma completa
exclusividade mútua entre as séries", escreve Simmel, "pois isso
indicaria pelo menos uma relação de uma com a outra \[...\]. Antes, a
relação entre ambas é absoluta contingência." [@PHG, pp. 23-24]

Cada série pode assim ser mencionada sem qualquer relação com a outra:
pode-se falar do ser sem lhe atribuir qualquer valor, como um valor
permanece significativo mesmo na inexistência de seu objeto na
realidade natural. Cada uma dessas ordens é, assim, "*o mundo todo
considerado de um ponto de vista particular*" [@PHG, p. 25, ênfase
minha]. Ser e valor "não se tocam em nenhuma parte porque questionam os
conceitos  das coisas segundo o completamente diferente" [@PHG, p.
27], ou seja, são duas *perspectivas* distintas, duas línguas, duas
"compilações, a cognitiva e a valorativa", da alma:

> A realidade e o valor são como que duas línguas distintas nas quais
> o conteúdo logicamente interrelacionado do mundo, válido em unidade
> ideal, o que se chamou de o seu "quê" \[Was\], faz-se inteligível
> para a alma unitária. [@PHG, p. 28]

Comparada com a ordenação natural do mundo, a valorativa adquire um
caráter subjetivo que não deve ser considerado senão no sentido de
independente da realidade. O valor, como o conhecimento, é um atributo
da apreensão do mundo, mas de alguma maneira compartilhado por uma
cultura e uma época. Essa subjetividade, que do ponto de vista do
indivíduo é objetiva, poderia ser melhor descrita, talvez como
interssubjetividade.

---

Falamos da maneira como Simmel concebe conhecimentos empíricos e
apriorísticos segundo sua função, relativizando seu caráter segundo o
papel que desempenham na constituição de objetos. Agora é preciso
falar rapidamente da maneira como Simmel concebe esse processo em que
conhecimentos empíricos funcionam como a priori para a constituição de
novos conhecimentos empíricos que se estabelecem posteriormente como
critérios num ciclo infinito.


### Regresso infinito e fundacionalismo {#regresso-infinito}

Simmel não evita o regresso infinito que a busca de uma fundação para
o conhecimento exige de uma posição relativista que não tem absolutos
em que se apoiar. A busca por uma verdade incondicionada
(fundacionalismo) atua como um "ideal" [Estou seguindo, neste tocante,
@Millson-ReflexiveRelativismGS, que diz respeito à teoria do
conhecimento presente na *Filosofia do dinheiro*, mas que julgo
apropriadas para o escrito sobre Stammler], e esse processo é descrito
por Simmel como interminável. Tomando por ponto de partida uma teoria
da justificação fundacionalista (que considera os conhecimentos
estruturados como num edifício, com uma base de crenças básicas dando
justificação para os conhecimentos superestruturais) [Cf.
@Epistemology-SEP], Simmel vai eliminar as duas alternativas que uma
tal teoria estabelece: tanto a reivindicação que, visando evitar o
regresso infinito, postula a existência de uma verdade incondicionada
que sirva de fundação para o conhecimento, quanto da afirmação cética
de que, na ausência de uma fundação absoluta definitiva, nenhum
conhecimento é possível. "Simmel forja uma nova abordagem ao problema
que vê o regresso infinito como uma condição positiva para o
conhecimento": Simmel "apresenta uma resposta ao dilema do
critério[^dilema-criterio] que evita o ceticismo ao projetar um estado
ideal do conhecimento indicado pelas nossas investigações
particulares." [@Millson-ReflexiveRelativismGS, pp. 189, 190]

> Dizer que certos itens do conhecimento só *funcionam*
> fundacionalmente em relação com as afirmações particulares sob
> escrutínio e que tais itens poderiam muito bem resultar eles
> mesmos condicionados rende as mesmas consequências que a asseveração
> de que afirmações particulares são verificadas contra o pano de
> fundo de <!--192--> itens de conhecimento cuja própria verificação
> não foi ainda levada a cabo. \[...\] A oposição entre um regresso
> infinito e um círculo massivo de raciocínio é tornada evidente pelo
> fato de que ambos são os ideias insinuados nas e pelas nossas
> atividades epistêmicas concretas. Assim, a resposta primária de
> Simmel ao dilema do critério é sugerir que as respostas fundacionais
> e circulares são igualmente plausíveis e inteiramente compatíveis
> desde que sejam vistas pelo que realmente são: a estrutura
> idealmente possível do conhecimento que é projetada pelas, embora
> nelas não verificada, nossas investigações concretas, mundanas e
> finitas. As últimas são sempre cumpridas em referência ou resposta a
> uma afirmação particular problemática e contra o pano de fundo de
> cognições cuja verdade é tomada de maneira provisória.
> [@Millson-ReflexiveRelativismGS, pp. 191-192]

Ao projetar a possibilidade de justificação calcada num conhecimento
fundacional a um estado ideal, Simmel concebe o conhecimento como
sempre aproximado. É como se a justificação de uma afirmação se
apoiasse numa outra afirmação básica, que, no entanto, não é
incondicionada, mas que funciona assim na medida em que é tratada como
tal, quando, portanto, uma afirmação condicionada, um conhecimento que
é empírico (enquanto oposto a um conhecimento a priori), é reificado
por um processo de "esquecimento" de sua condicionalidade. O
conhecimento se torna possível porque a circularidade das
justificações é garantida pela reificação, pelo esquecimento do
caráter empírico de uma afirmação e sua conversão em conhecimento que,
se não o é, ao menos funciona como *a priori*.[^waizbort-reificacao]

> \[...\] mantemos frequentemente uma posição determinada não pelo que
> ela é, nomeadamente, uma visão não arbitrária das coisas, mas as
> interpretamos como uma propriedade das coisas. Essa tendência à
> objetificação e à reificação é reforçada pelo fato de que os pontos
> de vista dos quais afirmações "verdadeiras" podem ser feitas são
> frequentemente de um caráter supraindividual. Noutras palavras, eles
> são institucionalizados e tem o caráter "objetivo" de toda
> instituição. Assim, a química, a física e a biologia olham para os
> mesmos objetos a partir de distintos princípios ou perspectivas.
> Mas, dado que os princípios que implicitamente definem a
> "perspectiva" em que se colocam possui uma validade supraindividual,
> esses cientistas são facilmente convencidos da verdade objetiva de
> suas afirmações, ou mais precisamente, de que suas afirmações
> descrevem o mundo como ele é. [@Boudon-ErkenntnistheoriePHG, p. 417]


<!--

### Circularidade epistêmica

  Assim, de certa maneira, Simmel combina uma teoria da justificação
  fundacionalista com seu oposto, o coerentismo, na medida em que
  postula que as crenças básicas que funcionam como tal fundação só são
  concebíveis num plano ideal do conhecimento, que só é, por sua vez,
  concebível devido à maneira como o conhecimento se estrutura,
  apoiando-se uns nos outros, sem que seja possível chegar a um
  conhecimento básico definitivo. Dito de outro modo, se o
  fundacionalismo concebe as crenças justificadas como um edifício em
  que a base fornece justificação para a superestrutura e o coerentismo
  o concebe como uma rede em que um conhecimento se apoia na
  justificação das convicções que o avizinham [@Epistemology-SEP]. Mas o
  coerentismo concebe a estrutura das afirmações não como uma estrutura
  hierárquica, com fundação e superestrutura como no fundacionalismo,
  mas como uma rede.

  Simmel, por sua vez, adotando a perspectiva fundacionalista somente
  num plano ideal, postula a estrutura de justificação como um círculo
  porque o ponto inicial, no processo de fundamentação da legitimidade
  de cada afirmação subsequente, volta a si mesmo.
  @Millson-ReflexiveRelativismGS chama essa abordagem da epistemologia
  relativista de Simmel "circularidade epistêmica":

  > \[...\] essa visão é o que eu chamaria de *circularidade
  > epistêmica* porque ela refere a uma estrutura de princípios de
  > verificação que forma um círculo, ao invés de um aglomerado
  > \[cluster\], uma constelação, ou algum outro modelo espacial para a
  > configuração de suporte epistêmico. [@Millson-ReflexiveRelativismGS,
  > p. 190]

  Essa maneira circular de raciocínio não incomoda Simmel como na
  formulação original do dilema do critério porque o ponto de retorno
  não representa o mesmo estado do ponto de partida: o requisito é que o
  encadeamento dos pressupostos seja suficientemente longo para que o
  retorno não apareça assim na consciência:

  > Se se considera o enorme número de pressupostos superimpostos e que
  > se perdem no infinito dos quais depende todo conhecimento
  > determinado por seu conteúdo, então não parece excluído que
  > demonstremos a proposição A mediante a proposição B, mas a
  > proposição B através da verdade de C, D, E etc. até que por fim só
  > seja provável mediante a verdade de A. A corrente de argumentação C,
  > D, E etc. só precisa ser suposta suficientemente longa para que seu
  > retorno a seu ponto de partida seja afastado da consciência \[...\]
  > a conexão que assumimos no interior de nosso conhecimento do mundo:
  > que podemos alcançar, de qualquer ponto do mesmo, qualquer outro
  > através da prova --- parece plausível de se fazer. Se não quisermos
  > parar de uma vez por todas dogmaticamente numa verdade que segundo a
  > sua essência não requer nenhuma prova, então estamos perto de tomar
  > essa reciprocidade do provar-se pela forma básica do conhecimento. O
  > conhecimento é assim um processo flutuante, cujos elementos
  > determinam suas posições mutuamente \[...\] [@PHG, pp. 99-100;
  > citado em @Millson-ReflexiveRelativismGS, p. 189]

-->


<!-- NOTAS {{{ -->

[^simmel-marginalista]: Com isso quero dizer que Simmel passa longe de
uma teoria do valor-trabalho como a de Marx, abraçando a teoria
"subjetivista" de valor dos teóricos da utilidade marginal, como Carl
Menger. "As ideias de Simmel são intimamente relacionadas com o que
hoje é chamada de economia 'austríaca' e não por acaso: não somente
Simmel parece ter emprestado muito da obra de Carl Menger, mas
austríacos posteriores (para não mencionar alemães), por sua vez,
emprestaram dele." "Para começar, ele rejeita a versão da teoria do
valor-trabalho de Karl Marx em favor de uma não sempre claramente
formulada abordagem da utilidade marginal que obviamente deriva de
Carl Menger (1871) (embora tal dívida não seja explicitamente
reconhecida)" [@LaidlerRowe-PHGReviewForEconomists, pp. 97, 98;
reimpresso em @LaidlerRowe-PHGReviewForEconomists-GSAS] "A lei da
utilidade final repousa \[..\] numa substituição peculiar de bens,
devido a considerações prudentes seguras. Aqueles bens que mais
facilmente podem ser dispensados ficam prontos a preencher as lacunas
que podem a qualquer momentos serem feitas num ponto mais importante."
"\[...\] o preço ou o "valor objetivo" dos bens é um tipo de
resultante de diferentes estimativas subjetivas dos bens que os
compradores e vendedores fazem de acordo com a lei da utilidade final"
[@BohmBawerk-TheAustrianEconomists, pp. 365-366, 367]

[^conceito-sociedade-mthdk-psoz]: @Mthdk-orig, p. 580. Cf.:
"Sociedade, em seu sentido mais abrangente, é encontrada ali onde
vários indivíduos entram em interação
\[*\foreignlanguage{german}{Wechselwirkung}*\]. Desde numa associação
efêmera para um passeio casual como na íntima unidade de uma família
ou de uma guilda medieval, deve-se constatar socializações
\[*\foreignlanguage{german}{Vergesellschaftung}*\] dos mais diversos
tipos e graus. As causas e fins particulares, sem os quais certamente
jamais haveria socialização, constroem em certa medida o corpo, a
*matéria* dos processos sociais; que o efeito dessas causas, a
conquista desses fins já faça surgir, entre seus portadores, uma
interação, uma socialização, essa é a *forma* na qual os conteúdos se
revestem e é da separação entre estes através da abstração científica
que se baseia toda a existência de uma ciência *social* especial."
[@PSoz, p. 54]

[^pg-vs-phg]: Nesse tocante, é talvez essa a principal diferença entre
as considerações de *Problemas da filosofia da história* --- que mais
do que um estudo sobre a metodologia da história, debate a relação
entre esta e a filosofia da história --- e a epistemologia presente
na *Filosofia do dinheiro*

[^influencia-passeron]: Posso ter-me deixado influenciar em demasia
pelo discurso metodológico de @Passeron-Raisonnement-pt, para quem
"\[...\] o sentido das abstrações ou das tipologias históricas jamais
pode ser desindexado de 'contextos' que são, por bem ou por mal,
levados em conta pela designação (*deixis*), ou seja, enumerativamente
referidos em sua singularidade global, como configurações que não
podem ser esgotadas por análise e construção de propriedade puras."
[@Passeron-Raisonnement-pt, p. 69] Mas em minha defesa, devo dizer que
a concepção do "raciocínio sociológico" como "'vaivém' entre
contextualização histórica e raciocínio experimental"
[@Passeron-Raisonnement-pt, cap. 3, esp. pp. 87ss] é precisamente o
que Simmel aqui propõe.
<!-- , apesar da "instigação metafísica" -->
<!-- "furiosamente filosófica" [Passeron-Raisonnement-pt, p. 90] de um -->
<!-- autor que teima em buscar a totalidade da sociedade em cada ínfima -->
<!-- parte. -->

[^dilema-criterio]: "Embora nunca mencione sua dívida com os céticos
da antiguidade, o que Simmel desenha aqui é essencialmente o dilema do
critério desenvolvido por Sextus Empiricus para gerar ceticismo e
encorajar a suspensão do julgamento. O argumento é relativamente
direto: demonstrar a verdade de qualquer afirmação requer um critério
de verdade com o qual julgá-la. Um critério de verdade determina a
verdade de uma afirmação quando esta última pode ser inferida do
primeiro, deixando o caráter preciso da inferência (i. é, lógica,
epistêmica, material etc) não especificado. Assim que se introduz tal
critério, entretanto, ele também se torna uma afirmação que requer um
outro critério de verdade para determinar sua verdade. O resultado,
segundo Sextus, é ou uma corrente inferencial infinitamente
regressiva ou uma admissão de raciocínio circular, isto é, que nalgum
ponto nosso desenvolvimento de critérios nos traz de volta à afirmação
original. \[...\] Simmel toma ambos os resultados como partes cruciais
em sua descrição do relativismo." [@Millson-ReflexiveRelativismGS, p.
184]

[^phg-refs]: A obra-prima de Simmel, a
*\foreignlanguage{german}{Philosophie des Geldes}* ("Filosofia do
dinheiro") foi publicada pela primeira vez no final de 1900
[@PHG-orig-1e], com uma segunda edição ampliada em 1907 [@PHG-orig-2e;
reimpressa em @PHG]. Constam traduções em espanhol [@PHG-es], francês
[@PHG-fr] e inglês [@PHG-en; da qual também consultei a versão
eletrônica, @PHG-en-epub]. Simmel trabalhava na obra desde 1889,
quando ela seria uma "psicologia do dinheiro", sua "pedra fundamental"
sendo a palestra de maio de 1889 "Para a psicologia do dinheiro"
[@orig-PsyG; reimpresso em @PsyG; cf. @EB-GSG2, p. 426; consultei
também as traduções brasileira, @PsyG-pt; portuguesa, @PsyG-pt; e
inglesa @PsyG-en].

[^aprioris-boudon]: "\[...\] os a prioris de Simmel não são universais
e atemporais, mas variáveis no tempo e no espaço; eles podem mesmo
ser variáveis entre um sujeito e outro. Além disso, eles não se
reduzem a um conjunto limitado de variáveis: sua diversidade e sua
complexidade tornam impossível mesmo a sua enumeração e sua
descrição. Ainda, eles são como que estratificados e entrelaçados."
[@Boudon-ErkenntnistheoriePHG, pp. 414-415]

[^kant:a-priori-a-posteriori]: "Se um julgamento deve ser *a priori*
ele deve ser logicamente independente de todos os julgamentos que
descrevem experiências ou mesmo impressões de sentido. \[...\] É claro
que esses julgamentos tem um tipo de dependência na experiência. Nós
formamo-los como o resultado de certas experiências e ao reagir a
elas. No entanto, essa dependência não é o que se entende por
dependência lógica." "Julgamentos que não são *a priori* são *a
posteriori*, isto é, eles dependem logicamente de outros julgamentos
que descrevem experiência ou impressões de sentido. Não somente
julgamentos que descrevem uma experiência ou impressão de sentido
particulares são *a posteriori*. Mesmo julgamentos gerais podem ser
logicamente dependentes de tais descrições e portanto *a posteriori*."
[@Korner-Kant, pp. 19-20]

[^waizbort-reificacao]: Waizbort lembra, recuperando a formulação de
Adorno e Horkheimer de que "toda reificação é um esquecimento", que
"na base de toda reificação está um esquecimento porque foi graças a
ele que um meio tornou-se autônomo, um fim em si mesmo, foi
'naturalizado'." [@Waizbort, p. 162]

<!-- }}} -->
