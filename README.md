tese
====

Este repositório servirá para o texto de minha tese de Doutorado a ser apresentada para o [Programa de Pós-Graduação em Sociologia](//www.ifch.unicamp.br/pos/sociologia) do [Instituto de Filosofia e Ciências Humanas](//www.ifch.unicamp.br) da [Universidade Estadual de Campinas](//www.unicamp.br).

Colofão
-------

O texto é escrito em [markdown](//daringfireball.net/projects/markdown/ "Sintaxe original markdown"), com as extensões do [pandoc](//pandoc.org/README "Pandoc: a universal document converter") (que é copyright de John MacFarlane e é publicado sob uma licença [GNU General Public License 2+](http://www.gnu.org/copyleft/gpl.html)). Os arquivos em markdown são preprocessados pelo mesmo `pandoc` em formato [LaTeX](//latex-project.org) (criado por Leslie Lamport, copyright do LaTeX3 Team e licenciado sob a licença [The LaTeX Project Public License](https://latex-project.org/lppl.txt)). O arquivo é compilado com o [pdfTeX](//www.pdftex.org) ([copyright de Han The Thanh](https://www.tug.org/applications/pdftex/README) e publicado sob a licença [GNU GNU General Public License](http://www.gnu.org/copyleft/gpl.html)) e as referências bibliográficas são processadas com o pacote [biblatex](//ctan.org/pkg/biblatex) (copyright de Philipp Lehman, Philip Kime, Audrey Boruvka e Joseph Wright e distribuído sob a licença [The LaTeX Project Public License 1.3](https://latex-project.org/lppl.txt "The LaTeX Project Public License")) através do processador [biber](//ctan.org/pkg/biber) (copyright de François Charette and Philip Kime e publicado sob a licença de software livre [Artistic License 2.0](http://www.opensource.org/licenses/artistic-license-2.0.php)). Todo esse processo é facilitado pela ferramenta de automação [arara](//ctan.org/pkg/arara "arara ― the cool TeX automation tool") (copyright de Paulo Cereda e publicada sob [a nova licença BSD](//www.opensource.org/licenses/bsd-license.php)).

A tipografia tem por base a fonte [XCharter](//texdoc.net/pkg/XCharter), distribuída sob uma licença [Original Bitstream Free Font license](https://www.ctan.org/tex-archive/fonts/xcharter/README), uma fonte de serifa retangular (ou egípcia) adaptada por Michael Sharpe da [Bitstream Charter](//ctan.org/pkg/charter), desenhada por Matthew Carter e [marca registrada e copyright da Bitstream, Inc.](http://linorg.usp.br/CTAN/fonts/charter/readme.charter). A fonte de largura fixa utilizada em URLs é a [Inconsolata](//texdoc.net/pkg/inconsolata), copyright de Michael Sharpe, publicada sob a licença [The LaTeX Project Public License](https://latex-project.org/lppl.txt). 

O controle de versão é feito com o [git](//git-scm.org) (escrito por Linus Torvalds e outros e publicado sob a [GNU GPL 2.0](http://opensource.org/licenses/GPL-2.0)), e hospedado no serviço de repositórios [BitBucket](//bitbucket.org).

O editor de texto não podia ser outro que não o [Vim](//www.vim.org/about.php "Vim") ― criado por Bram Moolenaar e publicado sob a [licença Vim](//vimhelp.appspot.com/uganda.txt.html#uganda.txt "Licença vim") (uma licença [compatível](//www.gnu.org/licenses/license-list.en.html#Vim "sobre a compatibilidade da licença vim com a GNU GPL") com a [GNU GPL](//en.wikipedia.org/wiki/GNU_General_Public_License "GNU Public License") e que inclui cláusulas de [careware](//pt.wikipedia.org/wiki/Careware "Careware")), em ambiente GNU/Linux.

Processamento 
-------------

O preprocessamento dos arquivos markdown (sua conversão para arquivos `.tex`) é feito pelo `pandoc` com o modelo [`template.latex`](template.latex), que é derivado daquele [disponibilizado pelo grupo abnTeX2](github.com/abntex2/abntex-2-contrib/pandoc/). As opções utilizadas são:

    --template template.latex \
    --biblatex \
    --chapters \
    --number-sections \
    --smart \
    --standalone 

Utilizo dois filtros, um que converte as linhas horizontais em espaço vertical (`unrulefy.py`) e outro que converte marcação de negrito para um formato mais semântico, `\strong` (`strongify.py`). Os filtros residem na pasta `scripts`.

A compilação do documento em formato LaTeX para formato pdf é feita pelo pdftex e o processamento bibliográfico pelo biber, mas isso tudo quem faz é a [`arara`](//ctan.org/pkg/arara). 

Os parâmetros passados para `arara` ― configurado no topo do arquivo `.tex` no formato `% arara: <rule>` ― são configurados no arquivo YAML de metadados da seguinte maneira:

    arara:
      - pdflatex
      - biber
      - pdflatex
      - 'pdflatex: { shell: yes }'

Tudo isso fica amarrado num script bash de processamento, [scripts/build](scripts/build) que pode ser chamado sem nenhuma chave para processar o diretório "sujo", com a hash de um commit específico com a chave `-c <commit>`, ou para o último commit com a chave `-C`.

Licença
-------

Pede-se o favor de não citar o conteúdo deste trabalho enquanto encontrar-se em andamento. Depois ele será licenciado sob a [Licença Creative Commons Atribuição-NãoComercial-SemDerivações 4.0 Internacional](http://creativecommons.org/licenses/by-nc-nd/4.0/deed.pt_BR).

Código, onde não estabelecido em contrário, é distribuído sob a [Licença MIT](https://opensource.org/licenses/MIT). O [modelo do pandoc](template.latex) é licenciado pela [LPPL](https://latex-project.org/lppl.txt).
