## A tradição histórica alemã {#tradicao-historica}


Outro aspecto da herança intelectual dos acadêmicos alemães do século
XIX, ainda segundo @Ringer-Decline, foi a sua tradição histórica.

É do encontro de uma disposição empiricista com a tradição idealista
dominante no pensamento alemão do século XIX que se desenvolveu essa
tradição histórica: trata-se de uma espécie de "empiricismo idealista"
[Para falar com @Parsons-Structure, p. 477] que se fundamenta com
especial intensidade no individualismo qualitativo típico do
pensamento alemão do século XIX (ver acima
\autoref{individualismo-qualitativo}), ou seja: que valoriza a
unicidade das individualidades que compõe seu objeto, rejeitando toda
tentativa de generalização.

> Um corolário da liberdade humana era a individualidade única de todo
> evento humano, na medida em que eles sejam "espirituais".
>
> Daí o "empirismo idealista" não ter se tornado uma reificação
> determinista de sistemas de teoria analítica, mas ter envolvido um
> repúdio de tais teorias em favor da unicidade e individualidade
> concretas de tudo o que é humano. É nesse sentido que o
> "historicismo" foi a tendência predominante do pensamento social
> alemão numa base idealista. Uma vez que o nível geral analítico da
> compreensão científica é excluído a priori, as coisas humanas só
> podem ser entendidas em termos da individualidade concreta do caso
> histórico específico. É um corolário disso que todas as coisas
> importantes não podem ser conhecidas a partir de um número limitado
> de casos, mas cada uma deve ser conhecida por e para si mesma. A
> história é o caminho indispensável para a completude do
> conhecimento.
>
> \[...\] O interesse no detalhe concreto dos processos históricos por
> eles mesmos \[...\] é um esforço persistente do pensamento social
> alemão do século XIX, recebendo talvez sua mais notável formulação
> metodológica no famoso dito de Ranke de que a tarefa do historiador
> é apresentar o passado *\foreignlanguage{german}{wie es eigentlich
> gewesen ist}* \[como realmente aconteceu\], isto é, em todo seu
> detalhe concreto. \[...\] Metodologicamente, no entanto,
> dificilmente pode-se dizer que ele tenha criado uma escola de teoria
> em assuntos sociais --- ao contrário, ele emitiu uma negação da
> teoria em geral. [@Parsons-Structure, p. 477]

Assim, partindo da tradição idealista e embebida do individualismo
"qualitativo", com sua recusa da teoria geral e sua exaltação da
descrição exaustiva dos fenômenos observados em sua singularidade,
formou-se uma tradição histórica que viria a resultar no chamado
"historicismo alemão" e que desempenhou papel hegemônico nas ciências
históricas e sociais alemãs. Essa tradição, que tem em Ranke seu
herói fundador, influenciou todas as humanidades ao longo do século
XIX, e desempenha importante papel nos debates acerca da demarcação
das disciplinas no final do século.

### A historiografia alemã e o historicismo {#ranke}

Leopold von Ranke (1795--1886) teve um papel fundamental no processo
de institucionalização da história na nova universidade alemã do
início do século XIX, tornando-se assim um fundador de campo e
determinando as definições das tarefas, objetos e métodos da
historiografia alemã ao longo do século XIX.

O posicionamento de Ranke parece, à primeira vista, algo discrepante,
assim como a oposição bem posterior que lhe faz Lamprecht já na
*Methodenstreit*: se num primeiro olhar a exortação a descrever o
passado "como ele realmente aconteceu" parece evocar uma atitude
positivista, ela traduz, no fundo, um relativismo histórico que o
situa no coração da reação antipositivista --- particularmente, no
polo que recusa à "razão" sozinha a capacidade de engendrar
conhecimento histórico.[^desconcertante-ranke]

A intenção de Ranke não é o desenvolvimento de uma
metodologia objetiva, em busca de leis de desenvolvimento histórico,
mas promover um olhar que, de maneira intuitiva e empática, mergulha
no passado, colocando-se na época em estudo, sem permitir que as
questões contemporâneas "contaminem" o conhecimento histórico. "Eu
diria que cada época é imediata para Deus" [Apud
@Chickering-Lamprecht, p. 28] é que deveria ser o seu mote mais
conhecido. [Como sugere @Loader-GermanHistoricismAndItsCrisis]

> Quando Ranke fez sua famosa observação sobre somente descobrir "como
> de fato aconteceu" \[a famosa expressão *wie es eigentlich gewesen
> ist*\], ele certamente não queria estimular uma completa suspensão
> de julgamentos interpretativos até que toda evidência estivesse
> coletada e a história completa pudesse ser contada de uma vez por
> todas. Nenhum historiador alemão jamais tomou tal posição. Ranke
> estava simplesmente tentando evitar um tratamento presentista e sem
> imaginação do passado. [@Ringer-Decline, p. 98]

Essa visão, portanto, não tinha nada de positivista, mas pelo
contrário, era bastante ortodoxa em sua herança do idealismo alemão:
seu princípio metodológico central, longe de uma metodologia objetiva
de obtenção e classificação de dados, é o de uma intuição subjetiva
que busca captar os objetos segundo sua lógica própria, o passado
conforme os seus próprios valores.

> Ranke estava mais próximo que Hegel do mundo espiritual do
> romantismo. Por toda sua ênfase em métodos de pesquisa meticulosos,
> as categorias do pensamento de Ranke se assemelhavam <!--186-->
> àquelas dos românticos. Como eles, ele lidava com entidades
> espirituais que eram "intuídas" e "contempladas" de maneira quase
> mística ao invés de conceitos firmes, testados empiricamente ou
> analisados logicamente. E nesses procedimentos, o método de Ranke
> era típico do idealismo alemão. [@Hughes, pp. 185-186]

Tal abordagem, que se voltava para um "esforço estético" mais do que a
um intelectual [@Chickering-Lamprecht, pp. 29-30], poder-se-ia
caracterizar, não sem alguma ressalva, como fundamentada em dois
princípios: o de "empatia" e o da "individualidade". [Com
@Ringer-Decline, pp. 98-99, que entretanto adverte que o afirma
"Correndo o risco de ser algo explícito e esquemático demais"]

O princípio de empatia diz respeito ao esforço de situar-se na
perspectiva dos "indivíduos históricos" da época estudada; noutras
palavras, exige do pesquisador que abstraia seus valores presentes e
que olhe para o passado segundo os valores que lhe seriam próprios.
Com isso, os historiadores "enfatizam intenções conscientes e
sentimentos, ao invés de regularidades estatísticas ou leis atemporais
de comportamento." [@Ringer-Decline, p. 98; ver também
@Chickering-Lamprecht, p. 28: "A fórmula mais famosa de Ranke, sua
determinação de mostrar '*wie es eigentlich gewesen*' \[como de fato
aconteceu\], era portanto bem menos simples do que parecia.
Conhecimento histórico, compreender como as coisas 'realmente
aconteceram', requeria, em primeiro lugar, que o historiador
abandonasse toda especulação, julgamento moral, e todo outro tipo de
preconceito; acesso ao passado viria, ao invés, numa maneira
semelhante à indução, mediante a imersão nas fontes imediatas à época
sob escrutínio."]

O princípio da individualidade caminha as trilhas do que Simmel
caracterizou como o "individualismo qualitativo" do século XIX, isto
é: partindo da ênfase na unicidade de cada indivíduo, acentua-se o
estudo de grandes personalidades históricas e o tratamento de todo
tipo de objeto como uma individualidade --- "Uma ideia, uma época, uma
nação: todas essas podem ser retratadas como 'individualidades', se é
a sua unicidade e sua 'concretude' indivisa que deve ser enfatizada."
[@Ringer-Decline, p. 99] Com isso, "Ao ser focado no passado, um
estudioso nunca abstrai do contexto histórico ao qual busca
compreender como que 'de dentro'. Ele trata a cultura e o 'espírito'
como um todo de uma dada época como um complexo de valores e ideias
único e contido em si mesmo." [@Ringer-Decline, p. 99]

Ao buscar observar o objeto do estudo histórico a partir desses
princípios de empatia e individualidade, Ranke recusava à razão
dedutiva a capacidade de engendrar conhecimentos históricos,
opondo-se, assim, a qualquer tipo de filosofia racionalista da
história. Com essa atitude, sua visão da história opunha-se
radicalmente à filosofia de seu colega Hegel, para quem ela era o
vir-a-ser do espírito em seu autodesenvolvimento lógico, o que era
passível de compreensão racional (para Hegel, "o real é racional"):

> A razão dedutiva não poderia, Ranke argumentava \[contra Hegel\],
> prover conhecimento da história. O observador do passado humano que
> o forçasse nos constrangimentos conceituais de qualquer sistema,
> filosófico ou dogmático, não poderia conhecer a verdade revelada na
> história daquilo que uma vez ele chamou a "raça destas criaturas tão
> múltiplas e variadas (*\foreignlanguage{german}{vielgestaltige}*), à
> qual nós mesmos pertencemos."
>
> A premissa de Ranke era assim insistir na diversidade e absoluta
> historicidade do passado --- o fato de que toda época histórica,
> como cada unidade da organização humana, era enraizada em seu
> próprio desenvolvimento particular e poderia ser apreendida somente
> nesses termos. \[...\] "Eu diria que cada época é imediata para
> Deus", como ele descreveu essa proposição para o rei da Bavaria, "e
> que seu valor consiste não no que segue dela, mas em sua própria
> existência, em seu próprio ser mesmo." [@Chickering-Lamprecht,
> p. 28]

Ranke exigia do historiador que abandonasse todo julgamento a partir
dos seus valores contemporâneos da observação do fenômeno histórico.
Para tanto, entretanto --- e em consonância com o "individualismo
qualitativo" ---, não bastava uma faculdade comum, disponível a todos,
como a razão, mas um tipo específico de intuição ou predisposição
empática que se caracterizava mais como um talento que como uma
habilidade passível de ser ensinada.

> Ranke argumentava que interpretar fontes históricas, apreender a
> verdade contida nesses documentos, requeria o emprego de uma
> faculdade crítica especial chamada
> *\foreignlanguage{german}{Verstehen}* ou
> *\foreignlanguage{german}{Einfühlung}* \[Entendimento,
> Sensibilidade\] em alemão. Ela não deveria ser confundida com a
> razão; ela implicava, ao contrário, uma compreensão mais global,
> intuitiva do passado que quase corresponde à palavra *divinação*. O
> exercício dessa faculdade, ele defendia, permitia a historiadores
> vivenciar empaticamente o mundo mental dos objetos de seu estudo.
> Ele também lhes permitia transcender os limites desse mundo e
> atingir a compreensão de grandes ideias que operavam através da
> história, vinculavam o passado ao presente, e ofereciam
> continuidade, padrão e significado à história.
> [@Chickering-Lamprecht, p. 29]

Esse tipo de intuição histórica provocaria muita controvérsia no
contexto das disputas metodológicas. Mas o que nos interessa aqui, de
fato, é como com Ranke funda-se, ou ao menos dá-se expressão mais ou
menos organizada a, uma tradição histórica que, associada à tradição
idealista, "gerou uma ênfase incomumente insistente sobre grandes
indivíduos 'históricos'; uma tendência a tratar culturas, Estados e
épocas como 'totalidades' personalizadas; e a convicção de que cada
uma dessas totalidades incorporava seu próprio espírito único."
[@Ringer-Decline, p. 102]

> A premissa que Ernst Troeltsch descreveu como "a historização de
> toda a nossa existência", a ideia de que cada aspecto dos assuntos
> humanos poderiam ser adequadamente compreendidos somente à luz de
> seu próprio desenvolvimento histórico único, representava a base de
> uma reorientação intelectual geral na Alemanha no início do século
> XIX. Essa premissa era refletida não somente na fundação da história
> como uma disciplina independente, mas nos termos em que os
> praticantes de uma variedade de outros campos humanísticos
> redefiniram seus objetivos, objeto e métodos.
> [@Chickering-Lamprecht, p. 46]

Assim, essa tradição histórica fundada pelo realismo rankeano
tornou-se hegemônica não somente entre historiadores mas, amalgamada à
tradição idealista donde nascera, e fiel à herança romântica que a
alimentara, desenvolveu em outras áreas do conhecimento tradições
específicas, que mobilizavam esse método histórico em suas respectivas
disciplinas. Ela provê às disciplinas humanísticas uma filosofia e
metodologia que foram amplamente aceitas e difundidas --- o que se
poderia chamar, a despeito da polifonia e ambivalência do termo (ver
abaixo \autoref{historismus}), de *historicismo*.

> O núcleo da perspectiva historicista reside no pressuposto de que
> <!--5--> há uma diferença fundamental entre os fenômenos da natureza
> e aqueles da história, a qual requer, nas ciências sociais e
> culturais, uma abordagem fundamentalmente diferente daquelas das
> ciências naturais. A natureza, defende-se, é o cenário de fenômenos
> eternamente recorrentes, eles mesmos destituídos de propósito
> consciente; a história abrange atos humanos únicos e induplicáveis,
> plenos de volição e intenção. [@Iggers-GermanConceptioOfHistory,
> p. 5]

Tal método mobilizava, assim, os princípios idealistas,
particularmente a noção de espírito e a busca por um princípio
unificador da realidade fenomênica com a essência das coisas-em-si, na
construção do conhecimento histórico, atribuindo a cada uma das
"totalidades" observadas um espírito específico, valoroso em sua
especificidade e unicidade, e detentor de lógicas próprias de
desenvolvimento.

> O resultado dessa tendência \[idealista\] era organizar as
> atividades humanas em relação à compreensão de "padrões coletivos"
> ou "de totalidade". A atenção histórica se focava não em eventos ou
> atos individuais, mas no *\foreignlanguage{german}{Geist}* que
> constituía sua unidade.
>
> Sob essas condições, a tendência "histórica" de pensamento foi,
> entretanto, preservada intacta. O conceito unificador sob o qual se
> subsumiam dados empíricos discretos não era uma "lei" geral ou um
> elemento analítico, como na tradição positivista, mas antes um
> *\foreignlanguage{german}{Geist}* particular, único, uma totalidade
> cultural específica claramente distinta de e incomparável com
> quaisquer outras. [@Parsons-Structure, p. 478]

Para @Iggers-GermanConceptioOfHistory, aquilo que de fato
caracterizaria a tradição histórica alemã não era a "análise crítica
de documentos" associada à herança de Ranke, uma vez que o método
crítico havia sido desenvolvido por uma geração anterior de filólogos,
historiadores e estudiosos da bíblia: "O que distinguia os escritos
dos historiadores nas principais tradições da historiografia alemã",
ele afirma, "eram, antes, suas convicções teóricas básicas com relação
à natureza da história e ao caráter do poder político".
[@Iggers-GermanConceptioOfHistory, p. 4] Essas convicções básicas
tinham por fundamento o "papel central que atribuíam ao Estado"
[@Iggers-GermanConceptioOfHistory, p. 4] --- e uma exaltação normativa
de seus efeitos.

A filosofia historicista da tradição histórica alemã pode ser
compreendida como centrada em torno de três noções: uma filosofia do
Estado que o considera como um fim em si mesmo; uma filosofia
relativista, ou uma "antinormatividade", que rejeita o olhar normativo
(compreendido como atribuição de valores do presente no olhar sobre o
passado); e uma epistemologia que rejeita o "pensamento conceitual", a
abstração das contingências dos fenômenos históricos em busca de um
conceito "em forma de lei". [@Iggers-GermanConceptioOfHistory,
pp. 7 ff.]

A filosofia do Estado da tradição historicista envolve uma concepção
de Estado que combina aspectos das visões aristocrática e burocrática
(combinadas numa visão que valoriza a classe média educada, o chamado
*\foreignlanguage{german}{Bildungbürgentum}*).

> No lugar do conceito utilitário do Estado como um instrumento dos
> interesses e bem-estar de sua população, a historiografia alemã
> enfaticamente situa o conceito idealista do Estado como um
> "indivíduo", um fim em si mesmo, governado por seus próprios
> princípios vitais. [@Iggers-GermanConceptioOfHistory, p. 8]

Essa filosofia política vê em cada Estado uma individualidade única,
com um espírito e uma lógica interna de desenvolvimento próprios, e
tem como corolário ideológico a ideia de que as instituições são
únicas e próprias a cada contexto ou nação, trazendo consigo a
exigência de uma recusa dos discursos universalistas sobre os papéis
do direito e do Estado. [Ver @Iggers-GermanConceptioOfHistory,
pp. 8-9]

Tal rejeição da possibilidade de se estudar as instituições em
abstrato, de maneira universal e fora de seu contexto nacional
histórico, devidamente universalizada traz um outro aspecto central do
ideário da tradição historicista alemã: aquilo que
@Iggers-GermanConceptioOfHistory chama de "antinormatividade", o
desprezo pela concepção dos fenômenos em termos normativos, isto é: a
defesa de um relativismo moral e cultural na compreensão dos fenômenos
históricos. O que com isso se defende é a intenção de abandonar todo
julgamento no estudo do passado, visando compreendê-lo à luz
de seus próprios valores --- uma intenção, no entanto, nunca
plenamente realizada. [Cf. @Iggers-GermanConceptioOfHistory, p. 17:
"Contudo, o historicismo alemão, enquanto uma teoria da história,
possui muitas das características de uma ideologia. Longe de buscar
compreender cada situação histórico por dentro, os historiadores
alemães na tradição nacional geralmente cometiam o pecado do qual
acusavam os historiadores ocidentais: impôr conceitos ou normas à
realidade histórica. É talvez inescapável ao historiador abordar a
história de um ponto de vista que reflita a impressão de sua
personalidade e do quadro social e cultural dentro do qual escreve. O
que distinguia o historicismo alemão, entretanto, era a rigidez desse
ponto de vista, a recusa de seus historiadores em ver suas concepções
e normas políticas e sociais temporais em perspectiva histórica."]

A ruptura com as teorias universalistas do século XVIII vai, porém,
além, rompendo com a "crença da lei natural numa subestrutura racional
da existência humana". [@Iggers-GermanConceptioOfHistory, p. 10] O
irracionalismo resultante assumia a forma de uma oposição ao
pensamento conceitual como reducionista, e à denúncia do esvaziamento
da concretude vital dos fenômenos históricos, resultado da ação humana
propositiva, no olhar abstrato do pensamento conceitualizante.

> A unicidade das individualidades na história restringia a
> aplicabilidade de métodos racionais ao estudo de fenômenos sociais e
> culturais. A espontaneidade e o dinamismo da vida recusavam-se a
> serem reduzidos a denominadores comuns. De Humboldt e Schleiermacher
> em diante, historiadores e cientistas culturais alemães tenderam a
> enfatizar o valor muito limitado de conceitos e generalizações na
> história de nas ciências culturais
> (*\foreignlanguage{german}{Geisteswissenschaften}*).
> Conceitualização, eles afirmam, esvazia a realidade da história de
> sua qualidade vital. A história, a arena de ações humanas
> voluntárias, requer compreensão. Mas essa compreensão
> (*\foreignlanguage{german}{Verstehen}*) só é possível se nos
> lançamos para dentro do caráter individual de nosso objeto
> histórico. Esse processo não é realizado pelo raciocínio abstrato,
> mas pela direta confrontação com o objeto que desejamos compreender
> e pela contemplação (*\foreignlanguage{german}{Anschauung}*) de sua
> individualidade, livre das limitações do pensamento conceitual. Toda
> compreensão histórica, Humboldt, Ranke e Dilthey concordam, requer
> um elemento de intuição (*\foreignlanguage{german}{Ahnung}*).
> [@Iggers-GermanConceptioOfHistory, p. 10]

É esse talvez o elemento que ganha proeminência no combate ao
positivismo e na controvérsia metodológica do final do século XIX: a
defesa de uma abordagem compreensiva em oposição à explicativa para os
objetos históricos ou culturais, e a recusa da abordagem conceitual
orientada à conceitos gerais ou universais de tais objetos (que são,
além disso, concebidos como individualidades e estudados com intenção
realista).


### As escolas históricas de direito e economia política {#escolas-historicas}

Ranke tornou-se o mais conhecido historiador alemão do século XIX, e
sua abordagem da história --- contra a abordagem racionalista que
busca um sentido ou direção na história e que a vê como a realização
de um "espírito" ---, a fundação da tradição histórica alemã.

Mas a tradição historicista não se alimenta somente da metodologia
histórica apregoada por Ranke, mas também, e talvez sobretudo, do
chamado "método histórico" de seu contemporâneo Friedrich Carl von
Savigny (1779--1861). As escolas históricas de direito, e
posteriormente de economia política, que se formaram em torno do
método de Savigny, foram importantes portadoras dessa tradição no
pensamento alemão.


#### A escola histórica de direito alemã {#escola-historica-direito}

A escola histórica de direito surgiu num contexto de controvérsia
jurídica --- que, diferente das controvérsias metodológicas, é mais
explicitamente normativa porque diz respeito ao próprio direito --- em
torno da compilação de um código civil alemão. "Os objetivos dessa
escola \[histórica do direito\]", escreve @Krystufek-SavignyThibaut,
"foram definidos no tratado *\foreignlanguage{german}{Vom Beruf
unserer Zeit für Gesetzgebung und Rechtswissenschaft}* \[Da vocação de
nossa época para a legislação e a ciência jurídica\], escrito em 1814
(Heidelberg) por Carl von Savigny que, em colaboração com o germanista
Eichhorn, fundou também a revista
*\foreignlanguage{german}{Zeitschrift für geschichtliche
Rechtswissenschaft}* \[Revista de ciência jurídica histórica\], em que
o próprio título exprime um programa bem pensado."
[@Krystufek-SavignyThibaut, p. 62]

Com esse tratado, Savigny respondia ao livro de Anton Thibaut
(1772-1840), *Sobre a necessidade de um direito civil universal para a
Alemanha* (*\foreignlanguage{german}{Über die Notwendigkeit eines
allgemeinen bürgerlichen Rechts für Deutschland}*), publicado em 1814,
a favor do estabelecimento de um código civil pangermânico inspirado
no código civil napoleônico que, assim, demandava uma completa revisão
do direito tradicional segundo critérios racionalistas e fundamentada
no direito natural.

> Savigny era identificado, à época, como o principal representante da
> Escola Histórica do Direito e responsável pela formulação completa e
> sistemática de seu programa, o que ele realizou em 1814, numa obra
> escrita em polêmica contra seu colega da Universidade de Heidelberg,
> Anton Justus Thibaut. Este havia publicado *Da Necessidade de um
> Direito Civil Geral para a Alemanha*, em que argumentava pela
> unificação da legislação dos diversos Estados alemães em um Código
> único, inspirado no grande *Code Civil* de Napoleão.
>
> \[...\]
>
> Savigny responde a Thibaut em *Da Vocação da Nossa Época para a
> Legislação e a Jurisprudência*. Nesse pequeno livro, ele combate a
> razão "esclarecida" e sua arrogante pretensão legisladora, que faz
> tábua rasa da tradição e julga-se capaz de edificar artificialmente
> uma nova realidade. O direito, assim como todo produto espiritual (a
> moral, a arte, a linguagem), não nasce da criação racional do
> legislador, mas da "vivência" de um povo, da conformação de uma
> "existência espiritual" que se desenvolve espontaneamente ao longo
> das gerações. [@Enderle-MarxManifestoEscolaHistoricaDireito,
> p. 112]

A Alemanha, então uma federação de estados independentes, não possuía
um código civil unificado, e mesmo o direito em cada Estado era ainda
uma amálgama do direito romano e do direito canônico eclesiástico
medieval com jurisprudência do direito costumeiro germânico da
ordem feudal. Para Thibaut,

> Em vários estados existe "um pot-pourri variegado" dos antigos
> direitos alemães; é verdade que eles exprimem com clareza, aqui e
> lá, o espírito germânico, mas eles estão em geral caídos em desuso e
> não correspondem mais às necessidades atuais, marcados como são pela
> brutalidade e a visão curta de outrora. É porque o direito canônico
> e romano "recebido" deve completá-los em oitenta por cento. Mas o
> direito canônico não possui alcance suficiente fora da própria
> instituição eclesiástica. Não resta mais que o direito romano sob a
> forma da compilação justiniana, quer dizer, o produto de uma nação
> bem diferente do povo alemão e, além disso, datado da época mais
> decadente dessa nação, assim um direito em todos os aspectos marcado
> por essa decadência. [@Krystufek-SavignyThibaut, p. 65]

Daí a necessidade de criação de um código civil unificado, para o qual
Thibaut reivindicava a abordagem racionalista das escolas do direito
natural do século XVIII. Savigny reagiu à proposta de Thibaut com
ferrenha oposição. Crítico do racionalismo jusnaturalista, Savigny
considera o direito como uma expressão da vontade de um povo ou nação
que emerge de maneira orgânica de sua vida cotidiana. Dessa
perspectiva, para Savigny, o direito não deveria ser alterado
arbitrariamente exceto em pequenas intervenções. Assim, não se tratava
de fundamentar as leis num direito natural que se estabelecia segundo
um exame puramente racional, mas o direito advinha da cristalização de
costumes que representavam a vontade de um povo objetificada numa
formação orgânica. [Ver @Krystufek-SavignyThibaut; @Savigny-ERE]

> Savigny declara que o fundamento de todo direito é a opinião geral
> de um povo, o que ele nomearia mais tarde (pela primeira vez no
> primeiro volume de seu *\foreignlanguage{german}{System des heutigen
> römischen Rechts}* \[Sistema do direito romano contemporâneo\] ---
> Berlin 1840--1849 --- *\foreignlanguage{german}{Obligationenrecht}*
> \[Código de obrigações\] --- 1851--1853) o "espírito do povo". O
> legislador, diz Savigny, não pode criar o direito arbitrariamente. O
> direito se forma por um processo orgânico determinado pelo fato de
> que a opinião geral do povo o considera como necessário. O direito
> se desenvolve continuamente e não conhece um só momento de repouso
> absoluto.[^savigny-codigo-civil]

<!-- #### O "método histórico" e as escolas históricas -->

O "método histórico" do direito de Savigny se dedica a "perseguir todo
material dado até a sua raiz, e, assim, descobrir um princípio
orgânico, donde aquilo que ainda possui vida deve ser isolado daquilo
que já está morto e que pertence somente à história." [Savigny, *Vom
Beruf unserer Zeit für Gesetzgebung und Rechtswissenschaft* (1814)
apud @HistorischeSchule-HWPh, p. 1138] Assim, Savigny se aproxima da
doutrina do espírito do povo de Herder e da tradição romântica de
valorização do passado. O principal pressuposto desse método é que o
direito positivo, existente, não guarda nenhuma relação com um direito
natural racional, que aparece como completamente artificial: o direito
positivo é sempre a manifestação do espírito de um povo, e assim
constituído de maneira contingente, pelas demandas históricas das
relações concretas entre as pessoas.

> Esse conjunto de "forças internas" (costumes, opiniões, crenças,
> jurisprudência) configura o "espírito do povo"
> (*\foreignlanguage{german}{Volksgeist}*), compreendido como a
> *substância*, a *essência* que preside o desenvolvimento "orgânico"
> do direito na história. Não se trata, porém, de pensar a evolução
> das estruturas jurídico-institucionais no devir das sociedades
> humanas. O que está em jogo é, bem diferente, a afirmação
> conservadora de uma *matriz de identidade* que subjaz inalterada a
> toda mudança histórica: o *espírito do povo* adapta-se a novas
> realidades, incorpora novos conteúdos, mas o faz sem modificar sua
> "essência", sua "especificidade", aquilo que o constitui como o
> espírito de um povo particular. No caso do direito e dos institutos
> germânicos, a ênfase recai na identificação do *direito romano* como
> a base, a matriz a partir da qual foram acrescentadas, na Idade
> Média, modificações "orgânicas", "vitais". Com isso, Savigny não só
> confere ao direito romano o título de *fonte originária*, em relação
> à qual não caberia nenhuma inovação substancial, como também, ao
> outorgar aos *contributos medievais* o caráter de modificações
> "orgânicas", acaba por privar o direito romano de seus atributos
> republicanos ou democráticos para reduzi-lo a mero protótipo da
> feudalidade. [@Enderle-MarxManifestoEscolaHistoricaDireito, p. 113]

Longe de com esse método histórico pretender compreender a emergência
do direito em sua "determinação situacional" [Para falar com
@Mannheim-IdeologyAndUtopia], isto é, a maneira em que as várias
relações sociais, econômicas, morais e culturais interagem e modificam
o direito existente, essa concepção só é histórica na medida em que
não é racionalista ou dogmática, na medida em que se dedica a
compreender o direito positivo somente como um processo contingente e
não a fundamentá-lo segundo critérios universais atemporais. A
historicidade desse método histórico seria, assim, restrita ao estudo
das relações entre os fatos jurídicos:

> A concepção histórica do direito de Savigny é uma concepção da
> história do direito estudada de uma maneira inteiramente isolada,
> sem qualquer relação com um outra realidade histórica. Não é mais
> que uma coleção dos fatos jurídicos que revela no máximo certos
> encadeamentos recíprocos, mas nenhuma relação com as outras relações
> econômicas e sociais nem com as ideias políticas de seu tempo.
> [@Krystufek-SavignyThibaut, p. 66]


#### As escolas históricas de economia política {#escolas-historicas-economia-politica}

A nova orientação histórica da tradição histórica de Ranke associada
ao método histórico da reorientação do direito promovida por Savigny e
a escola histórica de direito afetou todo o campo das humanidades, se
instituindo como parte do que @Ringer-Decline estabelece como o
tripé da tradição mandarim (crítica kantiana, tradição idealista e
tradição histórica). Mas, "talvez com a exceção da própria história,
nenhuma disciplina foi mais direta e duradouramente afetada pela nova
orientação em direção à história do que a economia", escreve
@Chickering-Lamprecht, que acrescenta:

> Embora a profissionalização dessa disciplina tenha ocorrido um pouco
> mais tarde, o estabelecimento da "escola histórica de economia
> política" se assemelhou à emancipação da história, na medida em que
> os pioneiros da disciplina buscaram sistematicamente insulá-la de
> tradições mais antigas e das reivindicações dos rivais. \[...\]
>
> A redefinição do campo \[da economia política\] então assumiu duas
> frentes. Contra os cameralistas, os economistas históricos
> argumentavam, no espírito dos reformistas humanistas, que seu campo
> compreendia uma *\foreignlanguage{german}{Wissenschaft}*, um corpo
> sistemático <!--47--> de conhecimento apropriado para estudo para
> além de qualquer aplicação prática direta que pudesse render. Contra
> as teorias clássicas de Adam Smith, David Ricardo e seus discípulos
> alemães, os economistas históricos argumentavam, como Ranke havia
> argumentado contra Hegel, que teorias abstratas, o esforço em
> analisar a esfera de atividade econômica em isolamento, faziam
> violência à diversidade histórica do comportamento econômico, assim
> como à complexa interação entre a economia e outras esferas de
> atividade humana. A historicidade do comportamento econômico e sua
> interdependência com a política e a cultura, eles insistiam, tornava
> a análise histórica cuidadosa, baseada em fontes, de práticas,
> instituições e de sistemas econômicos o único método apropriado a
> sua disciplina. [@Chickering-Lamprecht, pp. 46-47]

Assim, contra o cameralismo --- que, fiel à sua origem junto das
câmaras de administração pública dos estados, mantinha-se largamente
preso a descrições estatísticas da economia nacional e às finanças e
políticas públicas nacionais ---, visava-se estabelecer a ciência
econômica como um "um corpo sistemático de conhecimento apropriado
para estudo para além de qualquer aplicação prática direta", um saber
cuja busca era válida enquanto esforço de erudição em si mesmo, a
despeito das exigências públicas. A recente institucionalização da
história como disciplina fornecia, assim, um modelo para a
institucionalização da economia política como disciplina acadêmica
mais independente do vínculo com o Estado.

Por outro lado, a economia política clássica britânica e, em parte
também a francesa, associada ao avanço do positivismo pelo aspecto
metodológico (como vimos na \autoref{smithianismus}) e à doutrina do
liberalismo econômico pelo aspecto normativo (uma dimensão que, a
despeito da oposição ao cameralismo, permanece sempre presente nas
escolas históricas), não somente não constituíam um modelo de
*\foreignlanguage{german}{Wissenschaft}* econômica aceitável segundo
os critérios científicos vigentes como também eram vistas como
reducionistas e portadoras de uma orientação metodológica a ser
combatida.

Assim, o método histórico da escola histórica de direito viria a
inspirar os economistas do que veio a ser chamado a "velha" escola
histórica de economia política, Wilhelm Roscher (1817--1894), Bruno
Hildebrand (1812--1878) e Karl Knies (1821--1898). Com o método
histórico, esses economistas encontraram um instrumento para reagir
contra as leis abstratas universalmente válidas da escola clássica de
economia política, principalmente inglesa.

> Frequentemente é dito que a escola histórica \[de economia
> política\] alemã foi fundada com a publicação em 1843 dos
> *\foreignlanguage{german}{Grundriss}* \[*\foreignlanguage{german}{zu
> Vorlesungen über die Staatswirtschaft nach geschichtlicher
> Methode}*, "Compêndio para aulas sobre a economia segundo o método
> histórico"\] de Wilhelm Roscher. Amplamente lida, essa obra foi
> considerada o manifesto da escola histórica. Ela foi reimpressa não
> menos do que 26 vezes. Roscher (1854, p. 42) via a economia política
> como "a ciência das leis <!--58--> de desenvolvimento da economia e
> da vida econômica." Consequentemente, a economia política tinha um
> objeto de estudo real.
>
> Contudo, um método claro e amplamente aceito não foi estabelecido.
> No nível analítico, o que unia a escola era mais uma reação contra
> os pressupostos individualistas e os métodos dedutivos da economia
> política clássica britânica, e uma preocupação em tornar os
> economistas sensíveis às diferentes culturas sociais e períodos
> históricos. [@Hodgson-HowEconomicsForgotHistory, pp. 57-58]

O método histórico de Roscher é mais devedor da tradição inaugurada
com Ranke do que à escola fundada por Savigny. Essa ambivalência
resulta numa certa tensão, que não passou despercebida pelos
contemporâneos (como Menger, Tönnies e Weber), entre as abordagens das
escolas históricas de direito e economia política. Havia pontos de
discordância entre Roscher, Hildebrand e Knies: Hildebrand negava
qualquer possibilidade de estabelecimento de regularidades e leis
gerais na economia histórica, enquanto Roscher buscava uma
regularidade empírica.[^ambivalencia-roscher]

A despeito da carência de uma metodologia claramente formulada e dos
empréstimos ecléticos das metodologias históricas em voga pela
primeira geração da escola histórica de economia, o fato é que "de
Roscher e List a Sombart e Weber, todos os principais membros da
escola histórica alemã engalfinharam-se com o problema da
especificidade histórica. Explorações desse problema apareceram de
maneira proeminente nos anos 1840 e persistiram por mais de cem anos."
[@Hodgson-HowEconomicsForgotHistory, p. 59] Nessa primeira geração da
economia histórica alemã, o desprezo pela abstração vazia e irrealista
da teoria econômica clássica era suplantado pela crença na
possibilidade de construção de explicação econômicas realistas, numa
"difundida fé empiricista na possibilidade da descrição pura, como se
os fatos pudessem falar por eles
mesmos". [@Hodgson-HowEconomicsForgotHistory, pp. 59-60]


<!-- #### A "nova" escola histórica de economia política -->

Com todas as contradições e limites, a velha escola histórica foi
superada por uma nova geração que, a despeito das disputas internas
(e, como falamos da *Methodenstreit*, também e talvez fundamentalmente
externas), se unia em torno de um princípio metodológico mais claro,
reivindicando a tradição historicista alemã de maneira mais coerente
que a geração anterior. Mesmo com o influxo do positivismo entre as
humanidades e a confusa concepção da metodologia histórica da "velha"
escola histórica de economia, "mais importante no pensamento histórico
alemão da época era a atitude que punha fatos acima das teorias e
assumia que somente o evento individualmente documentado era certo."
[@Iggers-GermanConceptioOfHistory, p. 130]

> Tanto Gustav Schmoller (1838--1917) quanto Otto von Gierke
> (1841--1921) combinavam convicção ética com a fé de que a história
> é a única guia para a compreensão do comportamento humano e social.
>
> Para Schmoller, como para os economistas da escola histórica antes
> dele, a economia é uma ciência normativa, uma ciência da sociedade
> (*\foreignlanguage{german}{Gesellschaftswissenschaft}*) "que deveria
> estudar as relações não somente entre o homem e os bens materiais
> mas também entre o homem e seus pares. A ordem econômica deveria ser
> considerada como somente um aspecto e como parte integral de toda a
> vida social e, enquanto tal, deveria ser avaliada de um ponto de
> vista ético." A economia e as ciências sociais visavam à explicação
> causal e sistemática de fenômenos sociais. Mas somente o estudo
> histórico poderia prover as fundações teóricas para tal ciência.
> [@Iggers-GermanConceptioOfHistory, pp. 130-131]

A chamada "nova escola histórica de economia política", composta por
Adolph Wagner (1835--1917), Karl Bücher (1847--1930), Georg Friedrich
Knapp (1842--1926) e Lujo Brentano (1844--1931) e chefiada por Gustav
von Schmoller (1838--1917), teria procurado demonstrar "a imbricação
de eventos econômicos no desenvolvimento social e histórico geral."
[@HistorischeSchule-HWPh] De certa forma, pode-se dizer que essa
segunda geração de economistas históricos tem mais clareza
metodológica que a geração anterior, e um horizonte normativo claro.

> A escola histórica acredita que a fonte de erros definitiva da
> economia clássica pode ser o método falso pelo qual ela era
> perseguida. Ele era quase inteiramente abstrato-dedutivo e, em sua
> opinião, a economia política deveria ser somente, ou pelo menos
> principalmente, indutiva. A fim de realizar a reforma necessária da
> ciência, devemos primeiro mudar o método de investigação; devemos
> abandonar a abstração e nos pôr à coleta de material empírico ---
> devotarmo-nos à história e à estatística.
> [@BohmBawerk-TheAustrianEconomists, p. 362]

Embora essa seja a descrição de um adversário (Eugen von Böhm-Bawerk,
1851--1914, foi um dos pioneiros, junto com Carl Menger, da escola
austríaca e o artigo citado trata-se de um relato da querela dos
economistas austríacos contra a escola histórica), ela é precisa em
apontar para o aspecto central da disposição metodológica da escola
histórica --- a concepção das ciências econômicas como ciências
empíricas, não abstratas, que leva em conta os fatores culturais
extraeconômicos na explicação dos fenômenos econômicos e que recusa a
construção de leis abstratas de comportamento econômico.


#### A contradição entre as escolas de direito e de economia {#contradicao-escolas-historicas}

Não se pode deixar de notar o fato de que, a despeito da disposição
metodológica em comum e da comum reivindicação da herança histórica da
tradição alemã, há uma profunda contradição entre as motivações das
duas escolas --- de direito e economia --- históricas.

Carl Menger já apontava em sua crítica da escola histórica de economia
política para uma profunda diferença metodológica entre as duas
correntes: em sua interpretação, enquanto a escola histórica de
direito não admitia a existência de uma ciência teórica do direito,
reconhecendo apenas a sua própria abordagem histórica como a ciência
jurídica, a escola histórica de economia parte de uma consideração da
orientação histórica como etapa anterior à formulação da teoria
econômica. Nesse sentido, diz Menger, diferenciam-se as duas escolas
"como *história* e uma *teoria refinada por estudos históricos*."
[@Menger-Untersuchungen, p. 16, nota 14] Nesse sentido, ele continua,

> Ambas as escolas, em desafio à sua divisa comum, encontram-se numa
> profunda oposição metodológica, e a transferência mecânica dos
> postulados e ponto de vista de pesquisa da jurisprudência histórica
> em nossa ciência é por isso um processo com o qual, com alguma
> consideração, nenhum pesquisador metodologicamente treinado pode
> concordar. [@Menger-Untersuchungen, p. 16, nota 14]

Seguindo a crítica de Weber a Roscher [@Weber-RoscherUndKnies;
@Weber-RoscherUndKnies-pt], @Freund-Introduction-GAzW descreve a posição
de Wilhelm Roscher como autocontraditória, já apontando para a
contradição entre as disposições históricas das escolas de direito e
economia:

> Roscher pretende seguir o método histórico porquanto se reivindica
> da escola jurídica de Savigny, ainda que por outro lado ele vê em
> Adam Smith e Malthus os precursores. Daí uma contradição
> dificilmente superável. De fato, Savigny combateu o racionalismo
> legalista do Iluminismo e colocou a ênfase sobre o caráter
> irracional e singular do direito afirmando que ele funciona como a
> língua e outros fenômenos culturais do "espírito do povo"
> (*\foreignlanguage{german}{Völksgeist}*), e que ele não se deixa
> deduzir em leis ou normas gerais. A escola clássica inglesa,
> permeada do espírito do Iluminismo, busca, ao contrário, descobrir
> as leis naturais da economia e de seu desenvolvimento. Roscher
> imagina poder conciliar os dois pontos de vista opostos tomando por
> sua própria conta, sem depender de submetê-lo à crítica, o conceito
> de povo e interpretando-o como uma totalidade individual no sentido
> de um organismo biológico. [@Freund-Introduction-GAzW, p. 32]

Outro aspecto dessa oposição é aquele apontado por Ferdinand Tönnies
(1855-1936), de natureza normativa. Os motivos para o rechaço do
racionalismo e da exaltação da abordagem histórica, em ambas as
escolas, seriam dirigidas a orientações normativas opostas com relação
ao papel do Estado.

A escola histórica de direito busca, inspirada pela filosofia da
natureza de Schelling, preservar o caráter "orgânico" do direito que
emana da vontade natural de um povo contra a introdução de princípios
jurídicos externos, em especial a contaminação racionalista do
direito natural. Busca preservar assim o aspecto contingente,
irracional, da construção do direito contra a justificação racionalista
do direito como oriundo de um direito natural, preservando o direito
tradicional de uma tentativa de revisão racionalista. A escola
histórica de economia política, por sua vez, tem por objetivo
demonstrar que a economia é resultado da cultura e dos costumes de um
povo, constituída historicamente, e busca preservá-la da política do
laissez-faire. [Cf. @Montes-Smith, p. 27: "\[...\] a ênfase da 'jovem
escola histórica' estava mais em como resolver problemas sociais
provocadas pela industrialização. Não é coincidência que esse grupo de
economistas políticos alemães, dominado por Gustav Schmoller
(1838--1917) estabeleceu a *\foreignlanguage{german}{Verein für
Sozialpolitik}* (Sociedade para a política social) em 1872--73 para
enfrentar problemas sociais, alegando uma abordagem 'realista' aos
problemas econômicos."; Cf. também @Ringer-Decline, p. 144: "Schmoller
e seus seguidores se opunham às abstrações e regras atemporais da
teoria clássica inglesa. Eles consideravam errado deduzir proposições
sobre a economia de qualquer nação em qualquer época de alguns
pressupostos axiomáticos a respeito do comportamento do homem
econômico ou as condições do mercado livre. Eles sentiam que a vida
econômica de uma nação só podia ser compreendida no contexto das
instituições, padrões sociais e atitudes culturais em que haviam se
desenvolvido."]

Em ambos os casos, mobiliza-se a tradição histórica para enfatizar a
arbitrariedade ou irracionalidade do direito e economia
respectivamente, assim como se mobiliza tal abordagem e disposição
metodológica com fins práticos bastante claros: no caso dos seguidores
de Savigny, em particular, num contexto de reforma constitucional que
visava frear reformas liberais; [Cf.
@Enderle-MarxManifestoEscolaHistoricaDireito, p. 114: "Com essas
linhas programáticas, a Escola Histórica do Direito esgrimia contra
toda tentativa de instauração, na Prússia do *Vormärz* (pré-1848), de
um quadro jurídico-político-institucional de perfil liberal e
democrático. Nesse combate, Savigny e seus seguidores (Puchta,
Niebuhr, Eichhorn) alinhavam-se com conservadores mais radicais, como
os teocratas Stahl, Haller e Heinrich Leo, situados mais à direita no
espectro político-ideológico. A uni-los, um propósito em comum:
realizar, contra as reformas liberais, um 'compromisso' entre a
aristocracia dos proprietários fundiários (a *Junkertum*) e a
burguesia emergente, com supremacia das estruturas feudais. Um
compromisso, em suma, entre passado e presente, em que caberia à
Escola Histórica do Direito garantir a hegemonia do passado,
sistematizando-o 'numa ciência do direito que progrida
organicamente'."] no caso dos de Schmoller e da
*\foreignlanguage{german}{Verein für Sozialpolitik}*, no contexto da
"questão social", buscando aliviar os impactos da industrialização e
evitar convulsões sociais. [Cf. @Ringer-Decline, pp. 148-149: "\[...
Schmoller\] acreditava que um governo paternalista poderia levar
adiante reformas sociais conservadoras limitando as consequências
<!--149--> mais destrutivas da guerra entre trabalho e
administração."]

Entretanto, cada uma dessas disposições se opõe ao racionalismo por
motivações políticas opostas, no que se refere à maneira como concebem
o papel do Estado: enquanto Savigny quer estabelecer um direito
orgânico como emanando do povo, fazendo do Estado, assim, sujeito à
letra de tal lei orgânica, os socialistas de cátedra acreditam que só
um Estado paternalista está em condições de apaziguar os conflitos
sociais.

> Do lado histórico, *Savigny* quer destruir o equívoco que afirma que
> "em condições normais, todo direito surge de leis, isto é, de
> regulações do mais elevado poder estatal". Quando esse sentido está
> dado, segue como um postulado necessário a substituição do direito
> especial e inadequado do Estado por um geral e racional. Os
> economistas políticos querem refutar, do lado histórico, o erro que
> afirma que em toda parte, indivíduos livres estiveram em posição ---
> se o Estado protege-lhes a vida e a propriedade --- de se
> autorregular em comércio e negócios; ao contrário, seria *relativo*
> a cada estágio da cultura o que acontece, particularmente a
> restrição ostensiva que em regra também seria útil. Desta
> compreensão histórica facilmente se chega ao postulado de que,
> também para a economia do presente, a utilidade da atividade do
> Estado se colocaria em cada caso dado como um problema, ao invés de
> como sua negação enquanto axioma; assim ela deve ser designada
> porque uma prova da adequação só pode ser atingida de maneira
> empírico-indutiva, isto é, histórica. A jurisprudência histórica,
> portanto, põe o "orgânico" no povo --- "as forças que se movem em
> silêncio" --- e reconhece no Estado e em sua arbitrariedade um poder
> que age mecanicamente. Ao contrário: a economia política histórica
> se inclina a ver somente relações mecânicas nos vínculos baseados em
> contratos entre indivíduos isolados. Ela insiste, pelo contrário, na
> "teoria orgânica do Estado" e simpatiza com a doutrina de que o
> indivíduo só pode "existir no interior do Estado" que regularmente
> confunde com o *zoon politikon* de Aristóteles.
> [@Tonnies-HistorismusRationalismus, pp. 230-231;
> @Tonnies-HistorismusRationalismus-en, pp. 269-270]



#### Excurso: "\foreignlanguage{german}{Historismus}" {#historismus .unnumbered}

Falar da tradição histórica alemã é falar do historicismo alemão. Numa
definição que tenta ser isenta,

> Historicismo é a crença de que uma compreensão adequada da natureza
> de qualquer coisa e uma avaliação adequada de seu valor são
> conquistados ao considerá-la em termos do lugar que ela ocupava e o
> papel que desempenhava dentro de um processo de desenvolvimento.
> \[...\] historicismo envolve um modelo genético de explicação e uma
> tentativa de embasar toda avaliação na natureza do próprio processo
> histórico. [@Mandelbaum-Historicism-TEoP, p. 25]

Embora a expressão *\foreignlanguage{german}{Historismus}* já tivesse
aparecido no século XVIII, com Novalis, ela só passou a ser empregada
de maneira corrente a partir dos anos 1920, assumindo significados
muito diversos: @Historismus-HWPh traz exemplos de seu emprego em
meados do século XIX, em oposição ao naturalismo, no contexto de uma
filosofia idealista da história (com Braniss, entre 1847 e 1848); como
uma "relação histórica hermenêutica que --- distanciando-se do
idealismo especulativo --- era caracterizada pela orientação à
categoria de causalidade, pela pesquisa individual histórica e pelo
abandono de um ponto de vista absoluto" (com Prantl, em 1852). Fichte
(1872--1814) o emprega de maneira crítica à escola histórica de
direito, criando uma tradição que desemboca na relação mais belicosa
entre Menger e Schmoller; ao mesmo tempo, o termo é empregado, no
final do anos 1870, para caracterizar o pensamento de Vico, de maneira
neutra. [@Historismus-HWPh]

Nas duas últimas décadas do século XIX e no início do XX, o sentido
polêmico da expressão é que veria o uso mais difundido. Segundo
@Mandelbaum-Historicism-TEoP, a difusão do termo
*\foreignlanguage{german}{Historismus}* (historicismo) se deu em meio
à controvérsia metodológica, em particular na crítica mengeriana da
escola histórica de economia. [A expressão aparece no título de
@Menger-Irrthumer, a tréplica de Menger à resenha que Schmoller
escrevera de sua primeira intervenção promovendo o debate; ver
@Schmoller-Methodologie; e @Menger-Untersuchungen] Nesse sentido,
portanto, longe de se referir a uma doutrina com princípios definidos,
*\foreignlanguage{german}{Historismus}* servia como categoria de
acusação destinada a ridicularizar o que se compreendia como um
exagero dos economistas da escola histórica.

> Embora o termo \[*\foreignlanguage{german}{Historismus}*,
> historicismo\] tenha sido mais tarde empregado como um meio de
> caracterizar o pensamento de Vico, seu primeiro uso difundido
> provavelmente data de debates metodológicos entre economistas
> políticas germanófonos. Nesses debates, Carl Menger criticava Gustav
> Schmoller e sua escola por tornar a teoria econômica indevidamente
> dependente da história econômica; isso ele caracterizava como
> *\foreignlanguage{german}{Historismus}*. Assim, o termo adotou um
> sentido depreciativo; ele sugeria um uso inadequado do conhecimento
> histórico e uma confusão com respeito ao tipo de pergunta que
> poderia ser respondida mediante tal conhecimento.
> [@Mandelbaum-Historicism-TEoP, p. 22]

O conceito de historicismo, despido dessa carga negativa e pejorativa,
floresceria somente no pós-Primeira Guerra, na Alemanha republicana.
Ali, com Ernst Troeltsch, ter-se-ia a primeira definição não-polêmica
da expressão. Em seu *O historicismo e seus problemas: o problema
lógico da filosofia da história*, @Troeltsch-Historismus-GS concebia o
historicismo como uma "tendência a ver todo conhecimento e toda forma
de experiência num contexto de mudança histórica"
[@Mandelbaum-Historicism-TEoP, p. 22], confrontando-o enquanto
abordagem ao naturalismo como uma abordagem generalizante e
quantitativista. No entanto, Troeltsch não defendia uma diferença de
metodologia entre as ciências naturais e as ciências humanas
(*\foreignlanguage{german}{Geisteswissenschaften}*), senão uma
diferença de *perspectiva*: a cada uma correspondia uma diferente
visão de mundo (*\foreignlanguage{german}{Weltanschauung}*), não uma
metodologia.

Dois anos depois da publicação do livro de Troeltsch foi a vez de Karl
Mannheim tratar do historicismo também num registro não-polêmico. Em
seu ensaio *\foreignlanguage{german}{Historismus}*, de 1924 [Do qual
consta uma tradução em inglês em @Mannheim-Historicism], o
historicismo seria considerado como uma visão de mundo básica, de
maneira semelhante àquela em que o concebia Troeltsch. Na
interpretação de Mannheim, a
*\foreignlanguage{german}{Weltanschauung}* teleológica da Idade Média
teria sido secularizada no Iluminismo, na medida em que ali se retinha
a imutabilidade das leis da Razão. Somente com o advento de uma visão
de mundo da mudança histórica, propriamente moderna, é que se superava
a concepção teleológica do mundo medieval e pré-moderno. Essa nova
visão de mundo da mudança é que Mannheim denomina historicismo. Em
comparação com Troeltsch, a noção de historicismo em Mannheim deixa de
ver um risco no relativismo moral, que passa a ser visto de maneira
positiva. [Mais tarde, Mannheim iria propor o que denomina de
relacionismo como um passo além do mero relativismo (creio que se
possa aqui acrescentar: particularmente o moral): o relacionismo
representa a perspectiva da sociologia do conhecimento que se funda
numa generalização da noção marxiana de ideologia. Ver
@Mannheim-IdeologyAndUtopia, cap. 2, § 5]

<!--
  Em 1936 é a vez de Friedrich Meinecke retomar o conceito de
  historicismo. Meinecke diverge bastante de Troeltsch e Mannheim na
  medida em que concebe o historicismo a partir de sua suposta
  acentuação nos aspectos concretos, únicos, individuais --- daí uma
  ênfase exagerada na individualidade dos fenômenos históricos,
  abandonando a visão troeltsch-mannheimiana que favorecia a
  *\foreignlanguage{german}{Weltanschauung}* da mudança. Nesse novo
  conceito, autores que não poderiam ser tidos como historicistas
  segundo os conceitos de Troeltsch e Mannheim passam a sê-lo: Ranke,
  Hegel e Marx são exemplos.
-->

Um novo momento de mudança semântica da noção polêmica de historicismo
encontra-se na sua reinterpretação nas críticas de Karl Popper
(1902--1994) e Friedrich Hayek (1899--1992), a partir dos anos 1930.
Segundo Mandelbaum, Popper e Hayek parecem se apoiar na definição de
historicismo de Menger, mobilizando-a para atacar as teorias do século
XIX que buscavam encontrar leis do desenvolvimento histórico. Aqui é
que ocorre a associação do historicismo à noção de holismo, que era
ausente das concepção anteriores.[^historismus-popper-hayek] Essa
reinterpretação de Popper e Hayek deu o sentido que dominou o termo ao
longo dos debates metodológicos, por eles provocados, entre
individualismo e holismo que ocuparam boa parte dos esforços
metodológicos nas ciências sociais dos anos 1950,
[@Bhargava-HolismIndividualism-REP] provavelmente até o
rejuvenescimento do historicismo no final dos anos 1960.

<!-- NOTAS {{{ -->

[^lamprecht-circulo-leipzig]: Sobre o chamado círculo de Leipzig, ver
nota \ref{circulo-leipzig} na p. \pageref{circulo-leipzig}.

[^ranke-metafisica]: O fato é que, a despeito de sua intenção
original, não parece que Ranke se queixaria desse movimento. "Quando
Karl Lamprecht anos depois acusou Ranke de contrabandear metafísica de
volta para sua visão da história, ele foi acusado de blasfêmia. Mas
ele estava certamente correto." [@Chickering-Lamprecht, pp. 30-31] "O
postulado ontológico da *\foreignlanguage{german}{Ideenlehre}*
\[Teoria das Ideias\] de Ranke era neoplatônico. A realidade
definitiva era ideal. Ela abrangia grandes ideias transcendentes que
atravessavam as épocas e informavam os pensamentos e comportamento de
atores históricos e de comunidades humanas. As implicações
epistemológicas dessa posição eram que a busca por conhecimento
histórico requeria qualificações especiais daqueles que a empreendiam.
\[...\] <!--30--> O estudo da história requeria talentos que eram mais
do que intelectuais, pois ele era um esforço estético."
[@Chickering-Lamprecht, pp. 29-30]

[^ambivalencia-roscher]: Cf. @Hodgson-HowEconomicsForgotHistory,
cap. 4. Naturalmente, as coisas eram mais complicadas do que um jogo
de tradução de métodos duma área noutra em reação a uma fundação vista
como equivocada. Roscher é suficientemente ambivalente para
reivindicar o método histórico de Savigny e ver Adam Smith como seu
precursor. É ao exame dessas inconsistências que Max Weber se dedica
nos artigos que compõe seu "Roscher e Knies e os problemas lógicos da
economia histórica" [@Weber-RoscherUndKnies; consultei a edição
inglesa em @Weber-RoscherUndKnies-en; e a tradução brasileira em
@Weber-RoscherUndKnies-pt] --- que não trataremos aqui.

[^historismus-popper-hayek]: "Pode-se inferir plausivelmente da
discussão de Hayek do historicismo que o sentido em que ele e Popper
concebiam a noção provavelmente era derivado do contraste original de
Menger entre construção de teoria científica e uma abordagem
essencialmente histórica a problemas nas ciências sociais. No entanto,
a forma específica de historicismo que tanto Hayek quanto Popper
atacaram era a doutrina do século XIX de que há leis de
desenvolvimento que caracterizam totalidades sociais e de que é
possível, com base num conhecimento de tais leis, fazer previsões
científicas sobre o futuro. _Assim, a noção de 'holismo', que não
havia anteriormente sido diretamente associada com a definição de
historicismo, foi injetada na discussão_, e os principais
protagonistas do historicismo passaram a ser identificado como Hegel,
Comte e Marx. Tomadas nesse sentido, três teses seriam comuns às
doutrinas historicistas: (1) uma rejeição do 'individualismo
metodológico' em favor da visão de que há totalidade sociais que não
são redutíveis às atividades dos indivíduos; (2) a doutrina de que há
leis de desenvolvimento dessas totalidades, consideradas como
totalidades; (3) a crença em que tais leis permitem previsões sobre o
curso que o futuro tomará. Enquanto essas três teses foram intimamente
conectadas com algumas das doutrinas anteriormente caracterizadas como
exemplos de historicismo, não parece haver necessidade de identificar
o historicismo com o pensamento holista e com a crença na
possibilidade de previsão, como Popper e Hayek tendem a fazer."
[@Mandelbaum-Historicism-TEoP, p. 24, grifo meu]

[^desconcertante-ranke]: "Esse é o desconcertante do exemplo
estabelecido por Ranke: em sua aparência mais óbvia, ele era um
investigador de 'fatos'; em temperamento, ele era um metafísico ---
pelo menos, um de um tipo peculiarmente vago e insatisfatório; quase
nunca ele 'pensa' sobre a história numa maneira comunicável
inequivocamente." [@Hughes, pp. 186-187]

[^savigny-codigo-civil]: @Krystufek-SavignyThibaut, p. 66. A questão
se debateu ao longo de todo o século XIX e o código civil alemão viria
somente no Segundo Império, quase cem anos depois da provocação de
Thibaut, no início do século XX. Para a redação do código civil, um
primeiro esboço, redigido por uma comissão de juristas de inspiração
no método de Savigny, foi rejeitado e uma nova comissão, com membros
de áreas mais variada, criou um novo esboço que foi aprovado e entrou
em vigor em 1900 [Thibaut acabou assim coroado como o vencedor da
quase centenária querela, segundo @Krystufek-SavignyThibaut, p. 75]

<!-- }}} -->
