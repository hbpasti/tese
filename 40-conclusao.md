# Considerações finais {.unnumbered} 

\epigraph{O todo da verdade é talvez tão pouco verdadeiro quanto
o todo da matéria é pesado.}{Simmel apud
\textcite[251]{Bloch-AussprucheSimmels}}


Tendo partido de dois momentos em que Simmel se referiu
especificamente à controvérsia metodológica de sua época --- em
especial àquela travada entre os economistas ---, dediquei este estudo
a esclarecer as questões que estavam em debate na
*\foreignlanguage{german}{Methodenstreit}*, com a esperança de que
tenha sido assim que o próprio Simmel as via, a fim de compreender
*como* Simmel teria nela intervindo.

Foi com o intuito de contextualizar esses dois pequenos trechos de sua
obra que busquei concebê-los como a resposta simmeliana à controvérsia
sobre a metodologia das ciências sociais. Para isso,
tomei por pressuposta a relevância da
*\foreignlanguage{german}{Methodenstreit}* para os cientistas sociais
alemães da virada do século e assumi que Simmel, como um intelectual
incomumente atento às tendências do pensamento de sua
época[^simmel-antenado], e a exemplo de alguns de seus colegas
(especialmente Max Weber), também teria se ocupado dela.

Tentei reconstruir a atmosfera intelectual em que ocorreu a
controvérsia metodológica. Para tanto, enfoquei talvez com excesso as
narrativas de intérpretes da história das ideias que buscam
compreender as grandes linhagens do pensamento, negligenciando a
maneira como as ideias são sempre disputadas e perdendo, assim, a
chance de olhar para os embates em torno dos vários aspectos desse
senso comum acadêmico [^kohnke-beiser]. Também perdi a oportunidade de
acompanhar aspectos específicos do debate com uma lupa, no que uma
história dos conceitos poderia ser mais útil: é como se tivesse me
concentrado em movimentos macroscópicos da história das ideias quando
poderia ter sido mais interessante, talvez, um olhar microscópico. De
qualquer maneira, a intenção era uma reconstrução da *Methodenstreit*
e dos debates em que ela se inseria; longe de um mero exercício
expositivo, foi no estudo das controvérsias que percebi a maneira como
o positivismo, real ou imaginário, orientava boa parte dos debates que
ali se cruzam. Assim, na maneira em que o reconstruí aqui, a
*Methodenstreit* se dá num contexto de reação ao positivismo: quando
uma classe média educada que já se encontra estabelecida em posições
algo dominantes, portanto recuada a posições reacionárias de defesa de
seus privilégios de detentora dos discursos legítimos sobre as ideias,
defronta-se com uma filosofia da ciência que almeja a construção de
uma ética fundada cientificamente no conhecimento do comportamento
humano, o positivismo. 

Porém, é importante refletir, também, se os breves comentários de
Simmel sobre a *\foreignlanguage{german}{Methodenstreit}* podem ser de
fato compreendidos como um posicionamento nela: de que maneira se pode
afirmar que essas menções destinam-se a intervir na controvérsia
metodológica de sua época? A despeito das citações explícitas à
controvérsia e em vista de sua brevidade --- a controvérsia só é
tematizada de maneira marginal em sua obra, num texto de momento, uma
resenha curta (em "Para a metodologia das ciências sociais"), e como
exemplo da fecundidade de seu relativismo para a epistemologia das
ciências sociais (na *Filosofia do dinheiro*)[^ref:simmel-mencoes]
---, é preciso refazer a pergunta não mais para *como* Simmel teria
intervindo na controvérsia metodológica, mas *se de fato chegou a
fazê-lo*.[^pesquisa-detida]

Quero tentar, aqui, um exercício interpretativo da questão respondendo
negativamente a esse posicionamento, que tratei, até aqui, como um
pressuposto. Em vista de sua formulação, não se pode assumir que
Simmel tenha dedicado sua concepção do relativismo a intervir na
*\foreignlanguage{german}{Methodenstreit}*, mas, pelo contrário,
diante dele a *\foreignlanguage{german}{Methodenstreit}* e toda
controvérsia metodológica perdem sentido na medida em que falham em
considerar as abordagens que colocam em conflito como momentos
complementares de uma metodologia unificada. [As reflexões que seguem
são inspiradas, além de no relativismo de Simmel que tive a
oportunidade de expor, nos estudos epistemológicos de
@Passeron-Raisonnement-pt; @BourdieuChamboredonPasseron-Metier-pt; e
@Bachelard-FormacaoEspiritoCientifico]

\hr 

O famosos detetives literários das epígrafes --- contemporâneos, senão
Padre Brown, pelo menos Sherlock Holmes, dos cientistas sociais cujas
querelas acompanhamos --- nos mostram que não é só nas ciências
sociais que a explicação do comportamento humano é controversa. Com
sua famosa arte da dedução, Sherlock Holmes constrói seus casos como
silogismos, suplantando, onde faltam os dados, com o cálculo sagaz de
um raciocínio quase infalível (este herdado do primeiro detetive
literário ocidental, Auguste Dupin). Enquanto isso, Padre Brown,
precisamente porque criado como reação ao racionalismo de
Holmes, recorre a um "exercício religioso" de introspecção empática,
colocando-se no lugar dos criminosos a fim de conceber e planejar o
crime como eles, recorrendo a seu extenso conhecimento da alma humana
adquirido em anos de prática confessional.

Essa analogia, entretanto, não é de todo feliz. Embora a tendência
historicista seja bem representada na metodologia de Padre Brown ---
ambos tomam por objeto individualidades em sua unicidade, e procuram,
por um tipo de intuição empática, se colocar no lugar do sujeito da
ação observada --- a metodologia de Holmes passa um pouco mais longe
do racionalismo abstrato a que se opõe o historicismo na controvérsia
metodológica. A diferença fundamental encontra-se no objetivo do
conhecimento em cada caso: enquanto Holmes une conhecimento objetivo
com evidências singulares para descobrir uma causa, encadeando uma
série de eventos que possam apontar um responsável, as ciências
sociais de orientação racionalista buscam construir o conhecimento
objetivo que pode figurar como premissa maior do tipo de inferência
que tornou o detetive famoso.

Ainda assim, a analogia serve para evocar um aspecto relevante que a
controvérsia metodológica, lida somente nos seus termos, tende a
esconder: que a disputa entre uma abordagem racionalista e uma
empática não é somente metodológica, mas também ideológica; que a
recusa do positivismo envolve fatores que não são somente os colocados
na disputa metodológica, mas que dizem respeito aos efeitos práticos
do predomínio de uma disposição científica --- a busca por soluções
cientificamente objetivas para as questões sociais, que revela
desprezo pela tradição hereditária dando sustentação à visão
aristocrática de mundo dos mandarins mais
ortodoxos.[^analogia-holmes-brown]. Se a controvérsia sobre o
positivismo diz respeito, inicialmente, ao papel da ciência na
sociedade, ela também promove uma discussão sobre a organização social
como um todo. O positivismo é a filosofia da ciência de uma sociedade
republicana; essa é a ameaça que representa ao império e à
estratificação aristocrática que os mandarins alemães fazem passar por
meritocracia.

É preciso lembrar, porém, que essas disputas são mediadas por um
sem-número de fatores ao serem introduzidas no universo do discurso
erudito. Particularmente em vista de se tratar de um encontro de
visões de mundo radicalmente opostas [Com @Ringer-Decline-pt, num
contexto de estratificação aristocrática em que o governante procura
minar o poder dos nobres favorecendo a formação de uma classe média
educada de professores e oficiais que se vê em posição vantajosa
diante de uma burguesia em disputa com a nobreza já decadente etc.],
os laços frouxos de causalidade fraca ou afinidade eletiva entre as
várias posições possíveis --- utilitarismo, individualismo
metodológico, individualismo político e liberalismo econômico, por
exemplo --- vão se transformando pelas mudanças semânticas trazidas
pelas disputas e, a cada geração de intelectuais, novas alianças são
forjadas, complexificando o universo dos possíveis posicionamentos
políticos --- e metodológicos.

Essa leitura, por assim dizer, macroscópica do movimento das ideias
num ambiente social, que leva em conta o posicionamento dos grupos (e
que foi a maneira que adotei para a reconstrução da
*\foreignlanguage{german}{Methodenstreit}* na primeira parte deste
estudo), porém, ignora a dinâmica própria dos microcosmos mais
ensimesmados e relativamente autônomos como o universo do discurso
erudito, o campo intelectual, especialmente o campo acadêmico ou
científico.[^def:campo] No interior de tais universos, em vista da
autonomia relativa de que dispõe para estabelecer quais são os objetos
dignos de disputa e em vista da preservação dessa própria autonomia,
as disputas sociais mais amplas, enquanto fatores que lhes são
externos, entram nas disputas internas de maneira
refratada.[^refracao-campo] A princípio, as controvérsias
metodológicas são as disputas em torno da legitimidade e,
consequentemente, da classificação na luta por posições no interior
dos campos. As disputas em torno dos métodos são, no fundo, disputas
em torno das formas legítimas de construção de objetos, implicando nas
formas legítimas de distribuição de estima no interior desses
universos de discurso erudito. Esse movimento é particularmente claro
na controvérsia entre os economistas porque ali o discurso heterodoxo
de Menger vem acompanhado, como vimos, de uma nova classificação das
várias áreas da ciência econômica que visa estabelecer os economistas
teóricos, de orientação exata, em posição de destaque, reposicionando
os mais hegemônicos historicistas numa área subalterna. 

No entanto, as controvérsias metodológicas não parecem cumprir somente
esse papel de regulação da distribuição interna de prestígio nos
campos. Parece-me, nelas também se encenam as disputas políticas que,
de outra maneira, não encontrariam lugar legítimo de disputa no
interior de campos relativamente autônomos. Se generalizarmos a
análise que @Bourdieu-Heidegger-pt faz da maneira como o
posicionamento político de Heidegger aparece eufemizado sob a forma do
mais puro discurso filosófico para conceber o funcionamento dos
universos de produção e circulação de discursos eruditos, as
controvérsias metodológicas aparecem como eminentemente políticas:
seria nelas que os discursos políticos encontrariam vazão em universos
que se concebem como autônomos com relação às disputas do mundo
"externo".

Como só são legítimas (isto é, aceitas como válidas e dignas de serem
disputadas nesse universo) no interior de um campo científico aquelas
disputas em torno de objetos propriamente científicos, os embates
éticos só podem comparecer ali de maneira legítima eufemizados,
despidos de suas especificidades e vestidos nas formas que lhes
emprestam legitimidade, na forma de discurso sobre o método. Dito de
outro modo, para que as disputas políticas do espaço social mais amplo
sejam inseridas nesses universos de discurso erudito sem recorrer a
uma reivindicação de capacidade de legislação e de construção de uma
ética cientificamente fundada (como no caso do positivismo), o objeto
dessas disputas só pode ser mobilizado sob a forma sublimada de
controvérsia metodológica[^weber-interpelacao]. Assim, os objetos e os
métodos legítimos, adequados, autorizados para o debate científico,
que são ali disputados podem representar não somente as formas de
classificação e distribuição de bens, posições e estima no interior
dos campos, mas também as da sociedade, sendo feitas sob a forma de
embates sobre métodos porque esse é o espaço legítimo de discussão
normativa no interior do campo científico. Assim como o campo refrata,
as disputas de classificações também não se referem meramente àquelas
classificações internas a esses espaços, mas representam formas
eufemizadas das disputas por classificações no espaço social mais
amplo.

O fato de que os discursos metodológicos sejam normativos,
prescritivos e proscritivos, parece-me ser uma boa pista disso: da
mesma maneira que as disputas políticas em torno das regras do viver
social são feitas sob a forma de um discurso ético, também as
controvérsias metodológicas, antes de uma disputa em torno de
hipóteses e problemas, como se supõe ser a natureza dos embates
científicos, são discursos normativos sobre o adequado fazer
científico. Isso joga luz no estranho fenômeno de as práticas
científicas frequentemente não encontrarem paralelo nos discursos
programáticos de seus autores: é porque, nos discursos metodológicos,
todo argumento é normativo, porque, na medida em que se dedica a tal
discurso programático, não se faz ciência, mas filosofia da ciência, é
que o encontro entre abordagens que, na prática científica, seriam
construtivos, são interditados no discurso programático das posições
metodológicas.

Poder-se-ia então falar de uma lógica das controvérsias metodológicas
não fosse o fato de que a sua lógica é a mesma lógica das disputas por
classificação e pela prescrição das normas de distribuição de bens,
posições e estima do mundo social e de seus microcosmos relativamente
autônomos.

---

Simmel nos ajuda a compreender essa diferença entre prática científica
e discurso metodológico com sua interpretação da distinção kantiana
entre princípios constitutivos e regulativos: na medida em que
concebemos as posições metodológicas --- e, para extrapolar o universo
dos discursos eruditos, também as políticas --- como princípios
constitutivos, no espaço do discurso normativo, excluímos as posições
contraditórias por princípio.[^metodo-constitutivo]

Nesse sentido, como Simmel poderia ter intervindo num tal debate,
então? A polarização das posições empiricista e racionalista (que não
era novidade)[^empirismo-historismo-longo-debate] não se apoia
precisamente no estabelecimento de uma ou outra como princípio
constitutivo da construção de objetos? Diante disso, não se pode
afirmar que Simmel tenha respondido à controvérsia metodológica senão
como uma terceira via que busca demonstrar a futilidade da própria
controvérsia --- com uma posição que busca a "alternativa
à alternativa" entre posições polarizadas que partem de princípios
constitutivos.

Conceber as posições metodológicas opostas como momentos de uma
metodologia unificada, como princípios meramente regulativos e não
mais constitutivos, nos previne de trilhar o labirinto dos debates em
torno de discursos normativos, permitindo-nos jogar luz ora nuns, ora
noutros aspectos de um fenômeno observado e construir objetos que, na
medida em que podem parecer radicalmente opostos, iluminam facetas
diferentes dos mesmos fenômenos, criando condições para a construção
possivelmente cumulativa de objetos cada vez mais detalhados,
nuançados, ricos.

Não são esses os princípios que guiam o espírito científico moderno,
para longe dos obstáculos epistemológicos da familiaridade, do
substancialismo e do teleologismo [Para falar como
@Bachelard-FormacaoEspiritoCientifico]? Não são esses os princípios
que instituem a ciência como espaço discursivo de trocas, como um
mundo --- para falar como o próprio Simmel[^simmel-mundos-ref] ---
relativamente autônomo em que o que está em jogo é a construção de
conhecimento? 

O que Simmel nos ensina é que seria mais frutífero conceber toda
posição metodológica como princípio regulativo, como um horizonte
normativo enquadrado num contexto específico da construção de um olhar
possível sobre um objeto, de uma construção particular do objeto que
será, sempre, heurística e jamais esgotará o universo de possíveis de
seu sentido --- instituindo a possibilidade de encontros que, jamais
deixando de colocar em questão as construções parciais de seu sentido,
não se impedem de se renovar no confronto com um contraditório que,
não mais constitutivo, torna-se complementar.



<!-- NOTAS {{{ -->

[^ref:simmel-mencoes]: Os pontos de sua obra em que encontrei menção
explícita à *\foreignlanguage{german}{Methodenstreit}* entre
economistas são: na resenha da obra de Stammler [@Mthdk-orig;
reimpresso em @Mthdk, e do qual segue uma tradução em
\autoref{methodik}] e, com muita brevidade, na *Filosofia do dinheiro*
[@PHG, pp. 111-113].

[^simmel-antenado]: Do fato de que Simmel estava sempre atento aos
movimentos intelectuais e ideológicos de sua época dão testemunho suas
relações com os movimentos estudantil e naturalista alemão, com os
movimentos artísticos secessionistas e o *Jugendstil* (de que fala
alto sua presença constante, sob pseudônimo, na revista "Jugend"), com
o círculo de Stefan George, para não mencionar o papel que teve na
introdução do pensamento de Bergson na Alemanha [de tudo isso,
a terceira parte de @Waizbort trata muito bem]. Há inúmeros relatos de
como Simmel mantinha-se interessado por todos os movimentos
ideológicos da época, particularmente nos breves memoriais de @BDGS.
Para uma referência em primeira mão que dá mostra disso de maneira
enfática, cf. @TiGLaT, originalmente publicado em inglês.

[^weber-interpelacao]: Parece-me um bom exemplo disso o fato de, à
posição explicitamente politicamente orientada da segunda geração da
escola histórica de economia política, Weber responder com uma
exortação à objetividade nas ciências sociais em seu clássico
manifesto no *Arquivo para a ciência e a política sociais*
[@Weber-Objektivitat-pt].

[^refracao-campo]: "Falar do campo", diz @Bourdieu-Flaubert-en, p.
524, "lembra-nos que fatores externos --- crises econômicas, mudanças
tecnológicas, revoluções políticas \[... etc\] --- exercem um efeito
só através de transformações na estrutura do campo onde tais fatores
prevalecem. O campo *refrata*".

[^pesquisa-detida]: Uma pesquisa mais detida em busca de elementos
objetivos para responder a essa pergunta teria de levar em conta
a relação de Simmel com Schmoller e Menger na altura da redação da
*Filosofia do dinheiro*, para começar com os personagens mais
conhecidos. @PHG-en-Preface3, pp. xiv ss., demonstra que Simmel estava
em contato com ambos: os dois ensaios que contêm os germes da
*Filosofia do dinheiro* ["Para a psicologia do dinheiro" e "O dinheiro
na cultura moderna", @orig-PsyG; reimpresso em @PsyG; com uma tradução
brasileira em @PsyG-pt_br; e @GMoK-orig; reimpressa em @GMoK; com duas
traduções brasileiras, em @GMoK-pt-br; e @GMoK-pt_br] foram
apresentados em seminários ligados aos dois autores: o primeiro, no
seminário de ciência política de Schmoller e o segundo, em palestra na
Sociedade de economistas austríacos (este último caso coincide com uma
tentativa de assumir uma cátedra em Viena). Além disso, cumpriria
reconstruir as controvérsias sobre a filosofia da história em torno de
seus *Problemas da filosofia da história*. 


[^def:campo]: Numa definição rápida, um *campo*, enquanto "um espaço
de relações *objetivas* entre posições" [@Bourdieu-Flaubert-en, p.
544] é "o lugar, o espaço de jogo de uma luta concorrencial". Mais
detalhadamente, tratam-se de "espaços estruturados de posições (ou de
postos) cujas propriedades dependem das posições nesses espaços,
podendo ser analisadas independentemente das características de seus
ocupantes (em parte determinadas por elas)" [@campocientifico, p.
122], sendo caracterizados pela "definição dos *objetos de disputas* e
dos *interesses específicos* que são irredutíveis aos objetos de
disputas e aos interesses próprios de outros campos." [@propriedades,
p. 89] Cumpre diferenciar o campo científico ou acadêmico do
intelectual, em que se encontram os intelectuais que não integram a
academia (artistas, escritores, jornalistas), o que, especialmente no
caso alemão da virada do século, os coloca em posição dominada diante
dos professores universitários. [Cf., nesse tocante, também
@Bourdieu-MarcheBiensSymboliques-pt]

[^bourdieu-heidegger]: Vale dizer, generalizando a análise que
@Bourdieu-Heidegger-pt faz da maneira como o posicionamento político
de Heidegger aparece eufemizado em seu discurso filosófico para
a o funcionamento dos universos de produção e circulação de discursos
eruditos.

[^metodo-constitutivo]: É preciso dizer que os debates metodológicos,
embora não produzam nenhum conhecimento, são definitivamente
constitutivos num sentido que nenhum golpe dialético pode ignorar: no
sentido sócio-lógico da formação de grupos. Embora possamos,
recorrendo ao relativismo de Simmel, dispensar as limitações impostas
pelos discursos metodológicos na produção do conhecimento, eles
cumprem uma função propriamente sociológica de formação de laços sob a
forma de escolas, herdeiros e "sobrinhos", que só pode ser alterada na
conjunção das transformações objetivas da estrutura de distribuição de
posições e bens simbólicos relevantes nos campos com a tomada de
posição que implica numa tomada de consciência --- que não deixa de
ser o que Simmel está tentando --- das limitações metodológicas e de
seu papel na formação de grupo [pensando com
@Bourdieu-RaisonsPratiques-pt, cap. 6]

[^empirismo-historismo-longo-debate]: Vale lembrar que esse é um
debate mais velho que o feudalismo; a título de exemplo, basta olhar
para os escritos de Galeno referentes às controvérsias metodológicas
na medicina dos três primeiros séculos de nossa era, por exemplo:
"Alguns dizem que a experiência sozinha é suficiente para a arte \[da
medicina, acréscimo meu\], enquanto outros pensam que a razão, também,
tem uma importante contribuição a fazer. Aqueles que se apoiam na
experiência [*empeiria*] sozinha são consequentemente chamados
empiricistas. Da mesma forma, aqueles que se apoiam na razão sozinha
são chamados racionalistas. E essas são as duas principais facções na
medicina. A primeira procede mediante a experiência à descoberta de
medicamentos, a segunda mediante indícios. E, assim, eles nomearam
suas facções empiricista e racionalista." "\[...\] Mas os dogmatistas
levantaram várias críticas contra o empiricismo. Alguns dizem que esse
tipo de experiência é irrealizável, outros, que elas são incompletas,
enquanto um terceiro grupo alega que elas não são técnicas. Os
empiricistas, por sua vez, criticaram o pensamento racionalista como
sendo plausível, mas não verdadeiro. Consequentemente, a descrição
que ambos oferecem é dupla e acaba por ser bastante longa, na medida
em que levantam críticas particulares e se defendem contra elas."
[@Galen-SectsForBeginners, pp. 5, 8]

[^simmel-mundos-ref]: Ver \autoref{simmel-mundos}, na p.
\pageref{simmel-mundos}.

[^kohnke-beiser]: Nisto, parece-me, sobressaem
@Kohnke-NeoKantianismus-en e
@Beiser-ContextProblematicPostKantian-CCP, que acompanham as
controvérsias filosóficas atravessando o século XIX, em especial em
torno da obra de Kant e da recepção e interpretação da Revolução
Francesa nos estados alemães.

[^analogia-holmes-brown]: A interpretação parece exagerada se tomamos
somente as passagens mencionadas numa leitura exclusivamente
intratextual; ela torna-se mais reveladora, porém, na medida em que se
leva em conta a maneira como Padre Brown é concebido por um autor
explicitamente conservador como reação ao racionalismo e cientificismo
de Sherlock Holmes.

<!-- }}} -->
