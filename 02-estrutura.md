
---

O \autoref{contexto}, *\nameref{contexto}*, busca situar os debates
que comporiam a controvérsia metodológica entre intelectuais alemães
do final do século XIX. O objetivo é estabelecer a proeminência do
positivismo no pensamento social da segunda metade do século XIX e
como pode-se pensar a controvérsia metodológica como uma reação ou
resultado de sua lenta e controversa hegemonia. Para tanto, procuro
seguir de maneira mais heurística que analítica o argumento de alguns
poucos intérpretes da história do pensamento do século XIX ---
com Célestin Bouglé, Henry Stuart Hughes, e Talcott
Parsons ---, discutindo o percurso da filosofia alemã que resultaria
no contexto de inserção do positivismo segundo a leitura de um
sociólogo contemporâneo da controvérsia e próximo de Simmel, Bouglé
(\autoref{quatro-correntes}); a maneira como o positivismo foi mal
recebido entre os intelectuais alemães do século XIX, a reação
antipositivista alemã e o seu triunfo algo subterrâneo posterior
(\autoref{antipositivismo}); e as tradições de pensamento idealista
(\autoref{tradicao-idealista}) e histórica
(\autoref{tradicao-historica}) alemãs.

No \autoref{methodenstreit}, *\nameref{methodenstreit}*, procuro fazer
uma reconstrução das questões disputadas na
*\foreignlanguage{german}{Methodenstreit}*, a controvérsia
metodológica, em algumas áreas relevantes para a metodologia das
ciências sociais: a disputa sobre a classificação de ciências humanas
e naturais e os critérios legítimos para tanto
(\autoref{ciencias-humanas-naturais}); a disputa entre historiadores,
centrada na obra de Karl Lamprecht (\autoref{lamprecht}); e a
controvérsia entre economistas alemães e austríacos, que como que dá o
tom da recepção do debate ao longo do século XX, definindo a
*\foreignlanguage{german}{Methodenstreit}* como uma disputa entre
métodos dedutivo e indutivo ou entre as orientações exata e realista
(\autoref{methodenstreit-economistas}).

Por fim, no \autoref{relativismo}, *\nameref{relativismo}*, procuro
reconstruir o relativismo de Simmel acompanhando o argumento de uma
resenha publicada em 1896 [@Mthdk-orig; reimpresso em @Mthdk],
interpretando-o como o seu posicionamento na controvérsia
metodológica, só para desconstruir esse pressuposto nas *Considerações
Finais*, quando recoloco a questão --- com uma resposta negativa ---
se de fato podemos assumir que Simmel tenha se posicionado na
*\foreignlanguage{german}{Methodenstreit}*.

Anexo uma tradução da referida resenha de Simmel, "Para a metodologia
das ciências sociais", de 1896, no Anexo \ref{methodik}.


