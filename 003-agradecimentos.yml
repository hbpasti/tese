agradecimentos: |

  Nunca serei capaz de expressar adequadamente a gratidão pelo apoio que tive a felicidade de receber de inúmeras pessoas durante a feitura deste trabalho. Embora as menções aqui sejam insuficientes para expressar quão profundamente todas essas pessoas ajudaram nesse período tão difícil, que sirvam pelo menos de reconhecimento da eterna dívida de carinho que contraí com todas elas. Obrigado! Boa parte dessas pessoas ofereceu sugestões inestimáveis para o trabalho; elas não tem nenhuma culpa se não lhes dei ouvidos. Aproveito para nomear aqui aquelas que pagaram talvez o preço mais alto e que tiveram direta relação com o resultado.

  ---

  Este trabalho não seria possível sem o pronto e dedicado apoio de um sem-número de trabalhadores que cuidam para que a Unicamp mantenha seu papel de destaque na produção do conhecimento. Muito obrigado a todos os funcionários do IFCH, em especial àqueles que passaram pela Secretaria do Programa de Pós-Graduação em Sociologia durante a realização do doutorado, Christina Faccioni, Daniel Monte Cardoso, Sônia Beatriz Miranda Cardoso e Priscila Gartier.

  Estou eternamente endividado pela rara autonomia com que pude trabalhar sob a orientação de Jesus Ranieri, cuja confiança, compreensão, amizade e estímulo foram cruciais e tiveram amplo impacto em minha formação.

  Agradeço aos professores Leopoldo Waizbort, Frédéric Vandenberghe, Michel Nicolau Netto e Fábio Mascaro Querido pela leitura generosa e pelas sugestões preciosas na defesa. Sou imensamente grato também à banca de qualificação, aos professores Josué Pereira da Silva e Fernando Lourenço, cujas leituras e sugestões serviram de mapa para guiar a escrita final do trabalho. Nos primeiros passos deste estudo, contei também com o estímulo e a disponibilidade generosos de Natàlia Cantó Milà, a quem sou particularmente grato.

  Pela confiança e todo o suporte necessário, agradeço ao Programa de Pós-Graduação em Sociologia do IFCH, nas figuras de seus coordenadores Marcelo Ridenti, Sílvio César Camargo, Michel Nicolau Netto e Mário Augusto Medeiros da Silva. Agradeço também ao Departamento de Sociologia do IFCH, com destaque ao então chefe de departamento, o mestre e amigo Fernando Lourenço, pela oportunidade de oferecer uma disciplina no Programa de Estágio Docente durante o doutorado. Esse experimento, que foi crucial para minha formação, permitindo-me aprofundar algumas das leituras da obra de Simmel de uma maneira que só a prática da docência exige e promove, só foi possível graças à tolerância dos alunos, a quem deixo um agradecimento caloroso.

  Dedico o trabalho aos caríssimos colegas da turma de 2012, especialmente aos meus mais queridos amigos Márcio Moneta, Nara Roberta da Silva, Vinícius Oliveira Santos e Sheyla Diniz. Este trabalho jamais se realizaria também sem os debates calorosos e os ombros amigos de Andrea Azevedo, Camila Teixeira Lima, Flávia Paniz, Hugo Ciavatta, Igor Figueiredo, Juliana Miraldi, Laura Luedy, Mariana Martinelli, Murillo Van der Laan, Pedro Henrique Queiroz, Raphael Silveiras e Raúl Vinícius Araújo Lima.

  À minha querida amiga Nara Roberta da Silva, com quem divido agonias acadêmicas desde a graduação, cabe um agradecimento destacado pela amizade, companheirismo e aconselhamento sempre pronto. Abusei de seu ombro e de suas sempre sábias palavras, que serviram de consolo nas horas mais difíceis. Sua atitude, coragem e generoso senso de justiça são e sempre foram uma inspiração para mim.

  Preciso destacar um agradecimento também ao meu grande amigo e mais duro leitor, Luã Ferreira Leal, cuja inteligência e erudição explorei em demasia na feitura deste trabalho. Se não segui algumas de suas sugestões foi por pura teimosia e a prejuízo do resultado.

  A amizade e o entusiasmo pela obra de Simmel de meu caro amigo Michel Nicolau Netto, já repetidamente mencionado, me serviram de estímulo para prosseguir nesse trabalho. Os debates que travamos, quer num almoço, num café ou nos encontros do GEBU, foram sempre os mais enriquecedores para mim, e sua leitura é certamente aquela em que mais confio. Tê-lo como membro da banca foi uma alegria muito especial.

  O carinho, a atenção, a disponibilidade e demais incontáveis qualidades de meu querido irmão, André Pasti, foram postos à prova de maneiras indizíveis nesse período. Sua disponibilidade persistente sempre serviu de importante apoio afetivo e material. Ele também me emprestou, junto com o Leonardo Dias Nunes, a casa onde boa parte dessas páginas viram a luz do dia pela primeira vez. Se eu tivesse transcrito nossas conversas sobre a tese, ela seria certamente melhor.

  Meus pais, Carlos e Eliana, foram um porto seguro durante toda a pós-graduação, me ajudando com coragem, clareza e afeto e contribuindo com toda a logística que torna possível esse tipo de trabalho. Meus queridos cunhados Taís, Neander e Ruxelli foram os melhores amigos que eu poderia ter tido nestes últimos anos, com a paciência para aguentar toda a distância e sempre à pronta disposição para o que fosse preciso.

  Por último mas mais importante, agradeço pela paciência, coragem, compreensão, atenção, carinho e companheirismo de minha amada parceira de vida, alegrias e dores, Viviani, que soube suportar minha ausência e cuja força e coragem serviram e servem de inspiração, me trazendo de volta dos cantos mais sombrios de minha alma e dando todo o suporte para a realização deste trabalho, que também é dela.

  A maior inspiração, porém, veio da alegria contagiante e da insaciável curiosidade de meus amados filhos, André, Clara e Diego, que souberam suportar com a necessária impaciência infantil a presença errática e distante do pai e que serviram de farol para me guiar para longe das maiores dificuldades de um longo trabalho como este.

  ---

  Esta tese também contou com apoio financeiro da Coordenação de Aperfeiçoamento de Pessoal de Nível Superior (**CAPES**) entre 2012 e 2016.
