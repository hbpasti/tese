# Apresentação {.unnumbered #apresentacao}

\epigraph{As pessoas fazem a sua própria história, mas não a fazem segundo a
sua livre vontade, em circunstâncias escolhidas por elas mesmas, mas
nas circunstâncias imediatamente encontradas e transmitidas pelo
passado.}{\textcite[207]{Marx-Brumaire-pt}}


Esse bem poderia ser o mote da sociologia, pelo menos quando da sua
fundação na virada do século XX.[^fundacao-sociologia] A grande
questão aí colocada continua a incomodar o pensamento sociológico e a
servir de marcador para a visão e divisão de mundo de pensadores na
disciplina, opondo aqueles que enfatizam a ação individual àqueles que
enfatizam a determinação estrutural da ação, aqueles que se alinham ao
lado do paradigma do princípio da não consciência àqueles que se
alinham na perspectiva da liberdade dos agentes, assim como todos
aqueles que procuram, a fim de se desvencilhar da alternativa forçada
entre esses opostos, a alternativa à alternativa, colocando-se num
sem-número de caminhos intermediários.

Num certo sentido, essa questão é fundadora para a sociologia como
disciplina das ciências sociais: a explicação da livre ação de agentes
conscientes, assim como, pelo menos parcialmente, a explicação
determinista que via na ação o resultado de impulsos naturais ou, nos
agentes, portadores de forças suprapessoais, já estava em voga desde
pelo menos o Iluminismo. A sociologia surge no contexto de
encontro dessas duas grandes linhas explicativas, particularmente no
contexto alemão, em que o naturalismo positivista --- que, pelo menos
a partir de sua versão associada ao darwinismo social, procurava
estabelecer as leis naturais do comportamento --- se defrontava com
uma tradição idealista acalentada no seio de um individualismo
romântico que recusava o positivismo como herdeiro do materialismo
iluminista.

Desse encontro é que surgem as principais disputas que compõe o
contexto intelectual de surgimento da sociologia.

---

No final do século XIX e início do século XX, no período de formação
da sociologia, as ciências sociais alemãs viram-se envoltas num
conjunto de debates e disputas metodológicas que punham em cheque o
estatuto científico das ciências históricas. A
*\foreignlanguage{german}{Methodenstreit}*, ou controvérsia
metodológica, tinha por objetos de disputa os limites e as condições
do conhecimento sócio-histórico, o estatuto científico da história, do
direito, da economia política, da psicologia e da nascente sociologia, e os
princípios metodológicos mais adequados para o conhecimento das
relações sociais.

Dentre a série de disputas que compõe a
*\foreignlanguage{german}{Methodenstreit}*, a controvérsia entre
economistas políticos tornou-se a mais conhecida, em especial no que
diz respeito à formação da sociologia alemã. Pode-se talvez atribuir a
Max Weber, pelo menos em parte, a longevidade do debate e de sua fama
contemporânea, em vista de que suas intervenções nele foram
posterizadas na coletânea póstuma de seus escritos metodológicos.
[^weber-witschaftslehre-refs] Entretanto, independente do motivo da
extraordinária sobrevivência do debate metodológico entre economistas
e de sua pertinência para a sociologia, o fato é que, a despeito de a
*\foreignlanguage{german}{Methodenstreit}* ter sido então travada numa
série de outros espaços (desde as definições dos limites das ciências
naturais e as ciências humanas entre os neokantianos de Baden e os de
Marburg ou entre estes e Dilthey, passando pela disputa metodológica
entre historiadores, até a controvérsia sobre os julgamentos de valor
promovida por Weber e que resultou em seu distanciamento da Sociedade
Alemã de Sociologia na primeira década do século XX), a sua instância
mais conhecida e que, por isso mesmo, acabou por definir os termos do
debate, foi a controvérsia travada entre economistas políticos,
protagonizada na disputa entre o economista austríaco Carl Menger e o
"socialista de cátedra" alemão, Gustav Schmoller.

Ainda que em larga medida as posições metodológicas dos protagonistas
do debate inicial não fossem de fato tão polarizadas quanto a sua
ferrenha querela deixava crer, o fato é
que os termos do debate foram ali instituídos: na resposta de
Schmoller, que capitaneava a "nova" escola histórica, à crítica que
lhe dirigia Menger, fundador do que viria a se tornar conhecida como a
escola austríaca, a controvérsia foi reduzida às posições fundamentais
e polarizadas de um método abstrato indutivo, de um racionalismo
individualista, e um método empírico dedutivo, de um empiricismo
historicista.

Posterizada para a história da sociologia pelos escritos de Max Weber
[É importante mencionar que o Max Weber que intervém nesse debate é
ainda um que se considera economista, não sociólogo: segundo
@Roth-ValueNeutrality, p. 37, Weber só passa a declarar-se sociólogo
após 1910], a controvérsia assim definida deixou ecos profundos
no debate acerca do estatuto epistemológico das ciências sociais,
apesar de jamais resolvida. Mesmo a posição de Weber --- se a sua
perspectiva seria ou não a do "individualismo metodológico" --- é
ainda disputada; que dizer dos representantes menos vinculados à
economia política, como Georg Simmel?

---

Com este trabalho, quero entender como o filósofo e sociólogo alemão
Georg Simmel (1858--1918) se posicionou na controvérsia metodológica
que colocou em rebuliço as ciências sociais na academia alemã do final
do século XIX.

Estudar o conjunto de disputas metodológicas e epistemológicas desse
período é importante para compreender o contexto de fundação da
sociologia alemã, que surge precisamente nessa época. Para uma
referência rápida: a publicação das *Investigações sobre o método das
ciências sociais*, em que Carl Menger atacava a escola histórica
alemã, é de 1883 [@Menger-Untersuchungen], mesmo ano da *Introdução às
ciências do espírito* de Dilthey [@Dilthey-Einleitung-DGS], assim como
da resposta de Schmoller, sob a forma de resenha das duas obras, mas
com especial atenção à de Menger, "Para a metodologia das ciências do
Estado e sociais" [@Schmoller-Methodologie]. A publicação do clássico
de Ferdinand Tönnies, *Comunidade e sociedade* data de 1887
[@Tonnies-GG-en], a primeira obra de Simmel, *Sobre a diferenciação
social*, é de 1890 [@SD-orig], sua *Introdução à ciência da moral*, de
1892 e 1893 [@EMw1-orig; @EMw2-orig], e seu ensaio fundador "O
problema da sociologia" data de 1894 [@orig-PSoz]. Mais adiante, a
chamada "grande" *Sociologia* de Simmel apareceu em 1908 [@Soz-orig] e
a fundação da "Sociedade alemã para a sociologia" [A *Deutsche
Gesellschaft für Soziologie*, ver @DGS-Geschichte] foi em janeiro de
1909. 

Para um contexto mais ampliado: do lado mais obscuro da
sociologia positivista, o *Curso de Filosofia Positiva* de Auguste
Comte é de 1853 e os *Princípios de Sociologia* de Herbert Spencer, de
1874--1875. Do lado ainda mais obscuro da sociologia organicista,
*Estrutura e vida do corpo social* de Albert Schäffle é de 1875--1878,
e os *Pensamentos sobre a ciência social do futuro*, de Paul von
Lilienfeld apareceu em 1872 em russo e no ano seguinte em alemão. Da
sociologia francesa, *Da divisão social do trabalho* de Émile Durkheim
é de 1893 e seu *As regras do método sociológico* de 1895 (foi entre
1885 e 1887 que Durkheim estudou na Alemanha). A *Revista
Internacional de Sociologia*, de René Worms, foi fundada em 1893,
assim como o Instituto Internacional de Sociologia.
*\foreignlanguage{french}{L'Année Sociologique}*, a revista de
Durkheim, data de 1898. O *\foreignlanguage{english}{American Journal
of Sociology}* foi fundado em 1895, dez anos antes da Associação
Americana de Sociologia, cujo fundador, Albion W. Small, conheceu
Simmel ainda como estudante na Universidade de Berlim em 1880 e se
dedicou a traduzir e publicar alguns de seus escritos nos EUA.

O pensamento sociológico por assim dizer "clássico" desses fundadores
não tem a sua relevância atual determinada somente pela tradição
disciplinar de revisitá-los. [Uma tendência que é combatida por
@Merton-SocialTheoryAndSocialStructure-1968; e bem explicada por
@Passeron-Raisonnement-pt] Pelo contrário, os embates metodológicos
colocados na fundação da sociologia continuam atuais porque fadados a
permanecer sem solução. À luz do relativismo de Simmel (que
conheceremos melhor no \autoref{relativismo}), especulo que isso se
deva à própria natureza normativa dos discursos e, consequentemente,
dos debates e embates metodológicos.

---

A proposta de uma *metodologia relativista* de Simmel, largamente
negligenciada por seus pares e em parte também por sua posteridade, é
considerada aqui como a sua solução para o impasse entre as posições
metodológicas e epistemológicas em disputa. Nela, veremos, as posições
metodológicas opostas são concebidas, antes de numa maneira
contraditória, como complementares, e defende-se um relativismo, que
se poderia caracterizar como perspectivista e relacionista, como
metodologia abrangente unificada capaz de dar fim à controvérsia.

O pensamento de Simmel notoriamente se ocupa de vastos campos e
aspectos da modernidade, só raramente de questões específicas de sua
época (e mesmo quando o faz, frequentemente é de uma perspectiva ou de
história das ideias, buscando compreender o papel de determinados
movimentos intelectuais no quadro geral da modernidade, ou da crítica
da cultura). Tratar a maneira como um autor que nunca lida com
autores, somente com escolas e que só raramente faz referências
bibliográficas, se posiciona sobre uma controvérsia que terminou com
ataques e insultos pessoais não é factível sem alguma violência. Ela
se dá na forma de um argumento que se limita a um conjunto bastante
restrito de seus escritos.

<!-- NOTAS {{{ -->

[^fundacao-sociologia]: Refiro-me à fundação que viria a ser
oficializada na cátedra de Durkheim na Sorbonne, em 1913; não é nenhum
exagero considerar essa sua geração de cientistas sociais uma geração
de fundadores, seletivamente esquecendo as infames tentativas
anteriores --- de Comte e Spencer, respectivamente --- a despeito de
sua profana influência sobre esta primeira geração de "arquitetos e
heróis fundadores" [para tomar de empréstimo a expressão de
@Ortiz-DurkheimArquitetoHeroiFundador]: Émile Durkheim e Marcel Mauss,
na França, Simmel, Max Weber, Ferdinand Tönnies na Alemanha (para não
falar dos americanos: a Associação Americana de Sociologia foi fundada
em 1905). Mas é claro que, assim, ficam apagados muitos outros
pioneiros (como Worms e seu Instituto Internacional de Sociologia ---
a mais antiga associação da profissão, no qual os alemães --- mas não
Durkheim --- participaram), ou clássicos negligenciados (como Gabriel
Tarde).

[^contexto-ampliado-sociologia]: Para um contexto mais ampliado: do
lado mais obscuro da sociologia positivista, o *Curso de Filosofia
Positiva* de Auguste Comte é de 1853 e os *Princípios de Sociologia*
de Herbert Spencer, de 1874--1875; do lado ainda mais obscuro da
sociologia organicista, *Estrutura e vida do corpo social* de Albert
Schäffle é de 1875--1878, e os *Pensamentos sobre a ciência social do
futuro*, de Paul von Lilienfeld apareceu em 1872 em russo e no ano
seguinte em alemão. Da sociologia francesa, *Da divisão social do
trabalho* de Émile Durkheim é de 1893 e seu *As regras do método
sociológico* de 1895 (foi entre 1885 e 1887 que Durkheim estudou na
Alemanha). A *Revista Internacional de Sociologia*, de René Worms, foi
fundada em 1893, assim como o Instituto Internacional de Sociologia;
*\foreignlanguage{french}{L'Année Sociologique}*, a revista de
Durkheim, data de 1898. O *\foreignlanguage{english}{American Journal
of Sociology}* foi fundado em 1895, dez anos antes da Associação
Americana de Sociologia; seu fundador, Albion W. Small, conheceu
Simmel ainda como estudante na Universidade de Berlim em 1880 e se
dedicou a traduzir e publicar alguns de seus escritos nos EUA.

[^weber-witschaftslehre-refs]: Os *Ensaios reunidos sobre a doutrina
da ciência*, obra editada por Marianne Weber nos anos 1920, que
posteriormente passou a ser referida como "Ensaios sobre a metodologia
das ciências sociais" no trânsito da obra para as línguas inglesa e
francesa. [@Weber-Wissenschaftslehre; há duas traduções em inglês,
@Weber-Wissenschaftslehre-en; e @Weber-GAzW-en; uma francesa,
incompleta, @Weber-GAzW-fr; e uma brasileira,
@Weber-Wissenschaftslehre-pt; sobre a mudança, ver @Seneda-MaxWeber]

<!-- }}} -->
